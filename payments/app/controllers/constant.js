exports = PAYMENT_GATEWAY = {
    stripe: 10,
    paystack: 11,
    payu: 12,
    paytabs: 13,
    paypal: 14,
    razorpay:15
};
exports = IS_ADD_CARD = {
    10: true,
    11: true,
    12: false,
    13: true,
    14: false,
    15: false
}
exports = PAYMENT_STATUS = {
    WAITING: 0,
    COMPLETED: 1,
    FAILED: 2
}

exports = ERROR_CODE = {
    INVALID_SERVER_TOKEN: 4002,
};