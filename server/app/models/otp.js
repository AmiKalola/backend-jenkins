var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var otpSchema = new Schema({
    phone: { type: String},
    country_phone_code: { type: String},
    email : {type : String}, 
    otp_sms: { type: String},
    otp_mail : {type : String},
    type : {type : Number},
    created_at: {
        type: Date,
        default: Date.now
    },
});

var Otps = mongoose.model('Otps', otpSchema);
module.exports = Otps;
