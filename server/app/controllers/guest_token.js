var Country = require('mongoose').model('Country');
var Setting = require('mongoose').model('Settings');
let Guest_Token = require('mongoose').model('guest_token');
let Trip = require('mongoose').model('Trip');
var Trip_history = require('mongoose').model('Trip_history');
var TripLocation = require('mongoose').model('trip_location');

exports.track_trip_new = async function (req, res) {
    let guest_user_token = req.query.token;
    let trip_id = req.query.trip_id;
    let trip_unique_id = req.query.trip_unique_id;
    let now = new Date();
    let guest_token = await Guest_Token.findOne({ token_value: guest_user_token, state: true, start_date: { $lte: now }, code_expiry: { $gte: now } });
    if (!guest_token) {
        return res.json({ success: false });
    }
    let condition = { unique_id: trip_unique_id, user_type_id: guest_token._id };
    if (trip_id) {
        condition = { _id: trip_id, user_type_id: guest_token._id };
    }
    let trip_data = await Trip.findOne(condition);
    let trip_path_data = null;
    if (!trip_data) {
        trip_data = await Trip_history.findOne(condition);
    }
    if (trip_data) {
        trip_path_data = await TripLocation.findOne({ tripID: trip_data._id });
    }

    return res.json({ success: true, guest_token, trip_data, trip_path_data });
}



