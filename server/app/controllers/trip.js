var utils = require('./utils');
require('./constant');
var myAnalytics = require('./provider_analytics');
var allemails = require('./emails');
var Trip = require('mongoose').model('Trip');
var Trip_history = require('mongoose').model('Trip_history');
var Type = require('mongoose').model('Type');
var Trip_Service = require('mongoose').model('trip_service');
var User = require('mongoose').model('User');
var Provider = require('mongoose').model('Provider');
var Dispatcher = require('mongoose').model('Dispatcher');
var Hotel = require('mongoose').model('Hotel');
var TripLocation = require('mongoose').model('trip_location');
var Citytype = require('mongoose').model('city_type');
var Reviews = require('mongoose').model('Reviews');
var Promo_Code = require('mongoose').model('Promo_Code');
var User_promo_use = require('mongoose').model('User_promo_use');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Card = require('mongoose').model('Card');
var EmergencyContactDetail = require('mongoose').model('emergency_contact_detail');
var pad = require('pad-left');
var Country = require('mongoose').model('Country');
var City = require('mongoose').model('City');
var moment = require('moment');
var geolib = require('geolib');
var Citytype = require('mongoose').model('city_type');
var CityZone = require('mongoose').model('CityZone');
var ZoneValue = require('mongoose').model('ZoneValue');
var Airport = require('mongoose').model('Airport');
var AirportCity = require('mongoose').model('Airport_to_City');
var CitytoCity = require('mongoose').model('City_to_City');
var Partner = require('mongoose').model('Partner');
// var console = require('./console');
var utils = require('./utils');
var Corporate = require('mongoose').model('Corporate');
const https = require('https')
let country_list = require('../../country_list.json')
var cards = require('./card');
const { type } = require('os');
const { table } = require('console');
var Cancel_reason = require('mongoose').model('cancellation_reason');
var Languages = require('mongoose').model('language')
var myAnalytics = require('./provider_analytics');
let user_controller = require('./users');
let Find_provider_logs = require('mongoose').model('find_provider_logs');

var OpenRide = require('mongoose').model('Open_Ride');


var Settings = require('mongoose').model('Settings')

var Mutex = require('async-mutex').Mutex;
const lock = new Mutex();

exports.create_trip = function (user_data, trip_type, city_type_id, req_data) {
    return new Promise(async (resolve, reject) => {
        const release = await lock.acquire();
        try {
            const setting_detail = await Settings.findOne({})
            if (req_data.car_rental_id == "") {
                delete req_data.car_rental_id;
            }
            let tripData = req_data;
            let user_id = tripData.user_id;

            if (user_data.is_approved == 0) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_USER_NOT_APPROVED });
                return;
            }
            
            const existingOrder = await Trip.findOne({
                user_id: user_id,
                is_schedule_trip: false,
                is_provider_accepted: 0,
                is_trip_completed: 0,
                is_trip_cancelled: 0
            });

            if (existingOrder ) {
                return resolve({ success: false, error_code: error_message.ERROR_CODE_TRIP_ALREADY_RUNNING });
            }

            if (tripData.trip_type !== undefined) {
                trip_type = tripData.trip_type;
            }
            if (trip_type == constant_json.TRIP_TYPE_CORPORATE) {
                tripData.user_type_id = user_data.user_type_id;
                user_id = user_data.user_type_id;
            }

            let corporate_data = await Corporate.findOne( {$or: [{
                _id: tripData.corporate_id
            }, {
                _id: tripData.user_type_id
            }]})


            if ( trip_type == constant_json.TRIP_TYPE_CORPORATE && tripData.corporate_id) {
                if (user_data.corporate_wallet_limit < 0 ) {
                    resolve({ success: false, trip_id: null, error_code: error_message.ERROR_CODE_YOUR_TRIP_PAYMENT_IS_PENDING});
                    return;
                }if(corporate_data?.wallet < 0)  {
                    resolve({ success: false, trip_id: null, error_code: error_message.ERROR_CODE_CORPORATE_TRIP_PAYMENT_IS_PENDING });
                    return;
                }
            }else if ( trip_type == constant_json.TRIP_TYPE_CORPORATE && !tripData.corporate_id) {
                if (user_data.corporate_wallet_limit < 0 ) {
                    resolve({ success: false, trip_id: null, error_code: error_message.ERROR_CODE_CUSTOMER_TRIP_PAYMENT_IS_PENDING});
                    return;
                }if(corporate_data?.wallet < 0)  {
                    resolve({ success: false, trip_id: null, error_code: error_message.ERROR_CODE_YOUR_TRIP_PAYMENT_IS_PENDING });
                    return;
                }
            }


            if ((user_data.wallet < 0) || (trip_type == constant_json.TRIP_TYPE_CORPORATE && tripData.user_type_id && user_data.corporate_wallet_limit < 0)) {
                resolve({ success: false, trip_id: null, error_code: error_message.ERROR_CODE_YOUR_TRIP_PAYMENT_IS_PENDING });
                return;
            }

            if (tripData.trip_type !== undefined) {
                trip_type = tripData.trip_type;
            }

            let citytype = await Citytype.findOne({ _id: city_type_id })
            if (!citytype) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_CITY_TYPE_NOT_FOUND })
                return;
            }

            if (citytype.is_business !== 1) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_OUR_BUSINESS_NOT_IN_YOUR_AREA })
                return;
            }

            let city_id = citytype.cityid;
            let country_id = citytype.countryid;

            let country_data = await Country.findOne({ _id: country_id })
            if (!country_data) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_COUNTRY_NOT_FOUND })
                return;
            }

            if (country_data.isBusiness !== 1) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_OUR_BUSINESS_NOT_IN_YOUR_COUNTRY })
                return;
            }

            let city_detail = await City.findOne({ _id: city_id })
            if (!city_detail) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_CITY_TYPE_NOT_FOUND });
                return;
            }

            if (city_detail.isBusiness !== 1) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_OUR_BUSINESS_NOT_IN_YOUR_CITY })
                return;
            }


            if (tripData.promo_id) {
                let promocode = await Promo_Code.findOne({ _id: tripData.promo_id })
                if (promocode) {
                    promocode.user_used_promo = promocode.user_used_promo + 1;
                    
                    if(promocode.user_used_promo > promocode.code_uses){

                        resolve({ success: false, error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE })
                        return;                        
                    }
                    
            }}
            
            let payment_gateway_type = setting_detail.payment_gateway_type;
            if (country_data.payment_gateways && country_data.payment_gateways.length > 0) {
                payment_gateway_type = country_data.payment_gateways[0];
            }


            let card = await Card.find({ user_id: user_id, payment_gateway_type: payment_gateway_type })
            if (tripData.payment_mode == Number(constant_json.PAYMENT_MODE_CARD) && ((Number(payment_gateway_type) !== PAYMENT_GATEWAY.payu) && (Number(payment_gateway_type) !== PAYMENT_GATEWAY.paypal) && (Number(payment_gateway_type) !== PAYMENT_GATEWAY.razorpay))) {
                if (card.length == 0 && trip_type != constant_json.TRIP_TYPE_CORPORATE) {
                    resolve({ success: false, error_code: error_message.ERROR_CODE_ADD_CREDIT_CARD_FIRST });
                    return;
                }
                if (card.length == 0 && trip_type == constant_json.TRIP_TYPE_CORPORATE && tripData.corporate_id) {
                    resolve({ success: false, error_code: error_message.ERROR_CODE_ADD_CREDIT_CARD_FIRST_FOR_CORPORATE_USER });
                    return;
                }
                if(card.length == 0 && trip_type == constant_json.TRIP_TYPE_CORPORATE && tripData.user_type_id){
                    resolve({ success: false, error_code: error_message.ERROR_CODE_ADD_CREDIT_CARD_FIRST });
                    return;
                }
            }

            let is_fixed_fare = false;
            let fixed_price = 0;
            let received_trip_from_gender = [];
            let provider_language = [];
            let accessibility = [];
            let destination_addresses = [];
            let type_detail = await Type.findById(citytype.typeid)

            if (tripData.is_fixed_fare != undefined) {
                is_fixed_fare = tripData.is_fixed_fare;
                if (is_fixed_fare) {
                    fixed_price = tripData.fixed_price;
                }
            }

            if (tripData.received_trip_from_gender != undefined) {
                received_trip_from_gender = tripData.received_trip_from_gender;
            }

            if (tripData.provider_language != undefined) {
                provider_language = tripData.provider_language;
            }

            if (tripData.accessibility != undefined) {
                accessibility = tripData.accessibility;
            }

            let dateNow = new Date();
            let schedule_start_time = null;
            let server_start_time_for_schedule = null;
            let is_schedule_trip = false;

            if (tripData.start_time) {
                is_schedule_trip = true;
                schedule_start_time = Number(tripData.start_time);
                let addMiliSec = dateNow.getTime() + +schedule_start_time;
                server_start_time_for_schedule = new Date(addMiliSec);
            }
            if (tripData.destination_addresses) {
                destination_addresses = tripData.destination_addresses;
                destination_addresses.forEach((element, index) => {
                    destination_addresses[index].location = [
                        Number(destination_addresses[index].location[0]),
                        Number(destination_addresses[index].location[1])
                    ]
                });
            }
            let json = {
                user_last_name: user_data.last_name,
                user_first_name: user_data.first_name,
                user_unique_id: user_data.unique_id,
                service_type_id: citytype._id,
                type_id: citytype.typeid,
                typename: type_detail.typename,
                user_id: user_data._id,
                is_trip_inside_zone_queue: tripData.is_trip_inside_zone_queue,
                token: tripData.token,
                current_provider: null,
                provider_id: null,
                confirmed_provider: null,
                trip_type: trip_type,
                car_rental_id: tripData.car_rental_id,
                is_surge_hours: tripData.is_surge_hours,
                surge_multiplier: tripData.surge_multiplier,
                hotel_name: tripData.hotel_name,
                room_number: tripData.room_number,
                floor: tripData.floor,
                source_address: tripData.source_address,
                destination_address: tripData.destination_address,
                sourceLocation: [tripData.latitude, tripData.longitude],
                payment_gateway_type: payment_gateway_type,
                destinationLocation: [],
                initialDestinationLocation: [],
                timezone: city_detail.timezone,
                payment_mode: tripData.payment_mode,
                user_create_time: tripData.user_create_time,
                payment_id: tripData.payment_id,
                unit: city_detail.unit,
                country_id: country_id,
                city_id: city_detail._id,
                fixed_price: fixed_price,
                is_fixed_fare: is_fixed_fare,
                is_provider_earning_set_in_wallet: false,
                received_trip_from_gender: received_trip_from_gender,
                provider_language: provider_language,
                accessibility: accessibility,
                is_schedule_trip: is_schedule_trip,
                schedule_start_time: schedule_start_time,
                server_start_time_for_schedule: server_start_time_for_schedule,
                user_app_version: user_data.app_version,
                user_device_type: user_data.device_type,
                zone_queue_id: tripData.zone_queue_id,
                destination_addresses,
                is_ride_share: citytype.is_ride_share != 1 ? 0 : 1,
                ride_share_limit: type_detail.ride_share_limit ? type_detail.ride_share_limit : null,
                created_by: tripData.created_by,
                estimate_time: tripData.estimate_time,
                estimate_distance: tripData.estimate_distance,
                booking_type : [],
                is_trip_bidding : tripData.is_trip_bidding
            }
                        

            // Set booking type
            json.booking_type = await utils.getTripBookingTypes(json)
            let trip = await new Trip(json);

            
            if (setting_detail.sms_notification) {
                    utils.sendOtherSMS(user_data.country_phone_code + user_data.phone, SMS_TEMPLATE.RIDE_BOOKING )
            }
            
            if (tripData.d_longitude && tripData.d_latitude) {
                trip.destinationLocation = [tripData.d_latitude, tripData.d_longitude];
                trip.initialDestinationLocation = trip.destinationLocation;
            }
            if (tripData.user_type_id) {
                trip.user_type = tripData.user_type;
                trip.user_type_id = tripData.user_type_id;
                let Table1
                if (trip.user_type == constant_json.USER_TYPE_CORPORATE  ||  trip.user_type == constant_json.USER_TYPE_DISPATCHER   ||  trip.user_type == constant_json.USER_TYPE_HOTEL) {
                    
                    switch ((trip.user_type).toString()) {
                        case constant_json.USER_TYPE_CORPORATE:
                            Table1 = Corporate;
                            break;
                        case constant_json.USER_TYPE_HOTEL:
                            Table1 = Hotel;
                            break;
                        case constant_json.USER_TYPE_DISPATCHER:
                            Table1 = Dispatcher;
                            break;
                        default:
                            Table1 = Corporate;
                            break;
                    }
    
                    if(trip.user_type != 0) {
                        const user_data = await Table1.findOne({ _id: mongoose.Types.ObjectId(trip.user_type_id)})
                        if(user_data) trip.support_phone_user =  user_data.country_phone_code + user_data.phone
                    }
                }
            } else {
                trip.user_type = constant_json.USER_TYPE_NORMAL;
                trip.user_type_id = null;
            }

            if (tripData.device == undefined && trip_type != constant_json.TRIP_TYPE_PROVIDER) {
                trip.is_tip = setting_detail.is_tip;
            }
            trip.is_toll = setting_detail.is_toll;

            if (trip_type != constant_json.TRIP_TYPE_PROVIDER && trip_type != constant_json.TRIP_TYPE_DISPATCHER) {
                trip.is_otp_verification = setting_detail.is_otp_verification_start_trip;
                if (trip.is_otp_verification) {
                    trip.confirmation_code = utils.generateOtp(6);
                }
            }

            if (tripData.is_trip_bidding) {
                trip.is_trip_bidding = tripData.is_trip_bidding;
                trip.is_user_can_set_bid_price = tripData.is_user_can_set_bid_price;
                trip.bid_price = tripData.bid_price;

                // if bid trip then set price as a fixed fare
                is_fixed_fare = true;
                fixed_price = trip.bid_price;
            }
            let currency = country_data.currencysign;
            let currencycode = country_data.currencycode;
            trip.currency = currency;
            trip.currencycode = currencycode;

            user_data.total_request = user_data.total_request + 1;
            await User.updateOne({ _id: user_data._id }, user_data.getChanges());

            let service_type_id = tripData.service_type_id;
            if (tripData.car_rental_id) {
                service_type_id = tripData.car_rental_id;
            }

            let trip_service = await Trip_Service.findOne({ service_type_id: service_type_id }).sort({ _id: -1 })
            if (!trip_service) {
                trip_service = new Trip_Service({
                    _id: new ObjectId(),
                    service_type_id: citytype._id,
                    city_id: citytype.cityid,
                    service_type_name: citytype.typename,
                    min_fare: citytype.min_fare,
                    typename: citytype.typename,
                });
                await trip_service.save()
            }
            trip.trip_service_city_type_id = trip_service._id;
            if (is_fixed_fare) {
                trip.provider_service_fees = Number((fixed_price * trip_service.provider_profit * 0.01).toFixed(3));
            }

            // Set trip status
            let trip_user_type = TYPE_VALUE.USER
            switch (trip.user_type) {
                case constant_json.USER_TYPE_NORMAL:
                    trip_user_type = TYPE_VALUE.USER
                    break;
            
                case constant_json.USER_TYPE_CORPORATE:
                    trip_user_type = TYPE_VALUE.CORPORATE
                    break;

                case constant_json.USER_TYPE_DISPATCHER:
                    trip_user_type = TYPE_VALUE.DISPATCHER
                    break;

                case constant_json.USER_TYPE_HOTEL:
                    trip_user_type = TYPE_VALUE.HOTEL
                    break;

                case constant_json.USER_TYPE_PROVIDER:
                    trip_user_type = TYPE_VALUE.PROVIDER
                    break;
                default:
                    break;
            }
            trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.CREATED, trip_user_type )

            await trip.save();
            let unique_id = pad(trip.unique_id, 7, '0');
            let invoice_number = constant_json.INVOICE_APP_NAME_CODE + " " + constant_json.INVOICE_PROVIDER_TRIP_EARNING_CODE + " " + (moment(new Date())).format(constant_json.DATE_FORMAT_MMDDYYYY) + " " + unique_id;
            trip.invoice_number = invoice_number;
            await trip.save();
            let triplocation = new TripLocation({
                tripID: trip._id,
                trip_unique_id: trip.unique_id,
                providerStartTime: dateNow,
                providerStartLocation: [0, 0],
                startTripTime: dateNow,
                startTripLocation: [0, 0],
                endTripTime: dateNow,
                endTripLocation: [0, 0],
                providerStartToStartTripLocations: [],
                startTripToEndTripLocations: [],
                googlePathStartLocationToPickUpLocation: "",
                googlePickUpLocationToDestinationLocation: tripData.googlePickUpLocationToDestinationLocation ? tripData.googlePickUpLocationToDestinationLocation : ""
            });

            await triplocation.save();

            resolve({
                success: true,
                trip: trip,
                message: success_messages.MESSAGE_CODE_YOUR_FUTURE_TRIP_CREATE_SUCCESSFULLY
            });
            return;

        } catch (err) {
            console.log("exports.create_trip")
            console.log(err)
            resolve({ success: false, error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG });
        } finally {
            release();
        }
    })
};

// FIND NEAREST PROVIDER
exports.nearest_provider = function (trip, provider_id, user_favourite_providers, city_type_data = null, req_data = null) {
    return new Promise(async (resolve, reject) => {
        let provider_query = {};
        let is_save_log = false;
        let city_id ;
        try {

            const setting_detail = await Settings.findOne({});

            city_id = trip?.city_id || city_type_data?.cityid;
            let city_detail = await City.findOne({ _id: city_id })
            if (!city_detail && !req_data) {
                resolve({ success: false, error_code: error_message.ERROR_CODE_CITY_TYPE_NOT_FOUND });
                return;
            }
           
            // added for common function
            let providers_id_that_rejected_trip = trip?.providers_id_that_rejected_trip || [];
            let type_id = trip?.type_id || city_type_data?.typeid;
            let country_id = trip?.country_id || city_type_data?.countryid;
            let is_ride_share = trip?.is_ride_share || req_data?.is_ride_share;
            let payment_mode = trip?.payment_mode || req_data?.payment_mode;
            let sourceLocation = trip?.sourceLocation || [req_data.latitude, req_data.longitude];
            let provider_language = trip?.provider_language || req_data?.provider_language || [];
            let received_trip_from_gender = trip?.received_trip_from_gender || req_data?.received_trip_from_gender || [];
            let accessibility = trip?.accessibility || req_data?.accessibility;
            let is_trip_inside_zone_queue = trip?.is_trip_inside_zone_queue;
            let ride_share_limit = trip?.ride_share_limit || 2;
            let destination_location = trip?.destinationLocation
            if (!destination_location && req_data?.destination_latitude && req_data?.destination_longitude) {
                destination_location = [req_data.destination_latitude, req_data.destination_longitude];
            }
            let city_timezone = city_detail?.timezone;
            let is_check_provider_wallet_amount_for_received_cash_request = city_detail?.is_check_provider_wallet_amount_for_received_cash_request;
            let provider_min_wallet_amount_set_for_received_cash_request = city_detail?.provider_min_wallet_amount_set_for_received_cash_request;
            let distance = setting_detail.default_Search_radious / constant_json.DEGREE_TO_KM;
            // end added for common function

            provider_query["_id"] = { $nin: providers_id_that_rejected_trip };
            if (type_id) {
                provider_query["admintypeid"] = type_id;
            }
            if (country_id) {
                provider_query["country_id"] = country_id;
            }

            let is_trip_condition = []
            let is_available_condition = []

            is_trip_condition.push({ "is_trip": [] })
            is_trip_condition.push({ "bids": {$ne: []} })
            is_available_condition.push({ "is_available": 1 })

            if (setting_detail.is_receive_new_request_near_destination) {
                is_trip_condition.push({ "is_near_trip": [] })
                is_available_condition.push({ "is_near_available": 1 })
            }

            if (setting_detail.is_allow_ride_share && is_ride_share === true) {
                is_trip_condition.push({ "is_ride_share": 1 })
                is_available_condition.push({ "is_ride_share": 1 })
            }
            let is_trip_query = { $or: is_trip_condition }
            let is_available_query = { $or: is_available_condition }

            provider_query["is_active"] = 1;

            let admin_type_query = [{ "provider_type": Number(constant_json.PROVIDER_TYPE_NORMAL) }, { "is_approved": 1 }]

            if (is_check_provider_wallet_amount_for_received_cash_request && payment_mode == Number(constant_json.PAYMENT_MODE_CASH)) {
                wallet_query = { 'wallet': { $gte: provider_min_wallet_amount_set_for_received_cash_request } };
                admin_type_query.push(wallet_query)
            }
            provider_query["is_vehicle_document_uploaded"] = true;

            provider_query["providerLocation"] = { $near: sourceLocation, $maxDistance: distance };

            let provider_normal_type_query = {
                $and: admin_type_query
            };
            let provider_admin_type_query = {
                $and: [
                    { "provider_type": Number(constant_json.PROVIDER_TYPE_ADMIN) },
                    { "is_approved": 1 }
                ]
            };
            let provider_partner_type_query = {
                $and: [
                    { "provider_type": Number(constant_json.PROVIDER_TYPE_PARTNER) },
                    { "is_approved": 1 },
                    { "is_partner_approved_by_admin": 1 }
                ]
            };
            let provider_type_query = { $or: [provider_normal_type_query, provider_admin_type_query, provider_partner_type_query] };
            let languages_exists_query = { $and: [{ "languages": { $in: provider_language } }] };

            let received_trip_from_gender_exists_query = {
                $and: [{
                    "gender": {
                        $exists: true,
                        $all: received_trip_from_gender
                    }
                }]
            }

            let provider_query_and = [];
            provider_query_and.push(is_trip_query);
            provider_query_and.push(is_available_query);
            provider_query_and.push(provider_type_query);
            if (provider_id != null) {
                provider_query_and.push({ $and: [{ "_id": { $eq: provider_id } }] });
            }

            if (accessibility != undefined && accessibility.length > 0) {
                let accessibility_query = {
                    vehicle_detail: { $elemMatch: { is_selected: true, accessibility: { $exists: true, $ne: [], $all: accessibility } } }
                }
                provider_query_and.push(accessibility_query);
            }

            if (provider_language.length > 0) {
                provider_query_and.push(languages_exists_query);
            }
            if (received_trip_from_gender.length > 0 && received_trip_from_gender.length != 2) {
                provider_query_and.push(received_trip_from_gender_exists_query);
            }

            let limit = 1;
            if (setting_detail.find_nearest_driver_type == Number(constant_json.NEAREST_PROVIDER_TYPE_MULTIPLE)) {
                limit = setting_detail.request_send_to_no_of_providers;
            }

            if ( city_type_data?.provider_type == 2 ) {
                limit = 1;
            }

            if(trip?.is_trip_bidding){
                let country_detail = await Country.findOne({ _id: country_id })
                limit = country_detail.no_of_providers_can_bid;
            }
            if(req_data?.type == TYPE_VALUE.HOTEL || req_data?.type == TYPE_VALUE.DISPATCHER){
                limit = 1000; // Manual assign from hotel and dispatcher all drivers will be fetched.
            }
            provider_query["$and"] = provider_query_and;

            let favourite_providers = [];
            if (user_favourite_providers) {
                favourite_providers = user_favourite_providers;
            }
            let favourite_providersSet = new Set(user_favourite_providers || []);

            let query;
            query = Provider.find(provider_query)
            let providers = await query.exec()
            if (trip) {
                delete trip.provider_to_user_estimated_distance;
                delete trip.provider_to_user_estimated_time;
            }

            if (providers.length == 0) {
                await handleNoProviderFound(trip, resolve)
            }

            async function handleNoProviderFound(trip, resolve){
                if (!trip) {
                    is_save_log = true;
                    resolve({ success: false, error_code: error_message.ERROR_CODE_NO_PROVIDER_FOUND_SELECTED_SERVICE_TYPE_AROUND_YOU });
                    return;
                }

                if (!trip.is_schedule_trip) {
                    trip.current_provider = null;
                    trip.provider_first_name = "";
                    trip.provider_last_name = "";
                    trip.provider_unique_id = null;
                    trip.provider_phone_code = ""
                    trip.provider_phone = ""
                    // trip.providers_id_that_rejected_trip = [];

                    if (String(trip.trip_type) !== String(constant_json.TRIP_TYPE_DISPATCHER)) {
                        trip.provider_trip_end_time = new Date();
                        var complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
                        var complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
                        trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
                        trip.complete_date_tag = complete_date_tag;
                        trip.is_trip_cancelled = 1;
                        trip.is_provider_accepted = 0;
                    } else {
                        trip.is_provider_accepted = 3;
                    }
                }

                if (trip.is_trip_cancelled == 0) {
                    await Trip.updateOne({ _id: trip._id }, trip.getChanges())
                    utils.update_request_status_socket(trip._id);

                    let user = await User.findOne({ _id: trip.user_id })
                    if (user) {
                        if (!trip.is_schedule_trip) {
                            user.current_trip_id = null;
                            await User.updateOne({ _id: user._id }, user.getChanges());
                        }
                        utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_NO_PROVIDER_FOUND, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                    }
                    is_save_log = true;
                    resolve({ success: false, error_code: error_message.ERROR_CODE_NO_PROVIDER_FOUND_AROUND_YOU });
                    return;
                } else {
                    // Set trip status
                    trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, null, "System" )
                }
                await Trip.updateOne({ _id: trip._id }, trip.getChanges())
                await utils.move_trip_to_completed(trip._id)
                utils.update_request_status_socket(trip._id);
                await utils.remove_trip_promo_code(trip)

                let user = await User.findOne({ _id: trip.user_id })
                if (user) {
                    if (!trip.is_schedule_trip) {
                        user.current_trip_id = null;
                        await User.updateOne({ _id: user._id }, user.getChanges());
                    }
                    utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_NO_PROVIDER_FOUND, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                }
                is_save_log = true;
                resolve({ success: false, error_code: error_message.ERROR_CODE_NO_PROVIDER_FOUND_AROUND_YOU });
                return;
            }

            function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            }
            favourite_providers = favourite_providers.filter(onlyUnique)

            let final_providers = [];
            let finalProvidersIds = new Set();

            let providerIndexMap = new Map();
            providers.forEach((provider, index) => {
                providerIndexMap.set(provider._id.toString(), index);
            });
            
            if (is_trip_inside_zone_queue && trip.zone_queue_id) {
                let zone = await CityZone.findById(trip.zone_queue_id);
                let zone_providers = zone.total_provider_in_zone_queue;

                for (const zone_provider of zone_providers) {
                    let zoneProviderId = zone_provider.toString();
                    if (providerIndexMap.has(zoneProviderId)) {
                        let zoneProviderIndex = providerIndexMap.get(zoneProviderId);
                        if (final_providers.length < limit) {
                            final_providers.push(providers[zoneProviderIndex]);
                            finalProvidersIds.add(zoneProviderId);
                        } else {
                            break;
                        }
                    }
                }
            }

            for (const fav_provider of favourite_providers) {
                var dup_index = final_providers.findIndex((x) => (x._id).toString() == fav_provider.toString());
                if (dup_index == -1) {
                    var fav_index = providers.findIndex((x) => (x._id).toString() == fav_provider.toString())
                    if (fav_index !== -1) {
                        if (Number(final_providers.length) < Number(limit)) {
                            final_providers.push(providers[fav_index]);
                        }else{
                            break;
                        }
                    }
                }
            }

            for (const provider of providers) {
                if (final_providers.length >= limit) {
                    break;
                }
                let providerId = provider._id.toString();
                if (favourite_providersSet.has(providerId) && !finalProvidersIds.has(providerId)) {
                    final_providers.push(provider);
                    finalProvidersIds.add(providerId);
                }
            }

            for (const provider of providers) {
                if (final_providers.length >= limit) {
                    break;
                }
                let providerId = provider._id.toString();
                if (!finalProvidersIds.has(providerId)) {
                    final_providers.push(provider);
                    finalProvidersIds.add(providerId);
                }
            }

            if ((setting_detail.is_allow_ride_share && is_ride_share) || trip?.is_trip_bidding) {
                final_providers = final_providers.filter(provider => {
                    if (provider.is_trip.length !== 0) {
                        if (destination_location && destination_location.length) {
                            if (provider.is_trip.length < ride_share_limit || trip?.is_trip_bidding) {
                                let pickup_diff_km = Math.abs(utils.getDistanceFromTwoLocation(sourceLocation, provider.providerLocation));
                                if (pickup_diff_km <= setting_detail.ride_share_pickup_radius) {
                                    let destination_condition = true;
                                    if (!provider.destinationLocation) {
                                        provider.destinationLocation = [];
                                    }
                                    for (const destinationLocation of provider.destinationLocation) {
                                        let destination_diff_km = Math.abs(utils.getDistanceFromTwoLocation(destination_location, destinationLocation));
                                        if (destination_diff_km > setting_detail.ride_share_destination_radius) {
                                            destination_condition = false;
                                            break;
                                        }
                                    }
                                    return destination_condition || trip?.is_trip_bidding;
                                }
                            }
                        }
                    }
                    return true;
                });
            }

            if (!destination_location) {
                final_providers = final_providers.filter(provider => provider.is_go_home !== 1);
            } else if (setting_detail.is_driver_go_home && destination_location) {
                final_providers = final_providers.filter(provider => {
                    if (!provider.address_location || provider.address_location.length === 0) {
                        provider.address_location = [0, 0];
                    }
            
                    if (provider.is_go_home && provider.address_location.toString() !== [0, 0].toString()) {
                        if (destination_location.length !== 0) {
                            let destination_diff_km = Math.abs(utils.getDistanceFromTwoLocation(destination_location, provider.address_location));
                            let destination_diff_meter = destination_diff_km * 1000;
                            return destination_diff_meter <= setting_detail.driver_go_home_radius;
                        }
                    }
                    return true;
                });
            }
          
            if (final_providers.length == 0) {
                await handleNoProviderFound(trip, resolve)
            }

            if (!trip) {
                // terminate for provider list
                resolve({
                    success: true,
                    message: success_messages.MESSAGE_CODE_YOU_GET_NEARBY_DRIVER_LIST,
                    providers: final_providers
                });
                return;
            }

            if (setting_detail.find_nearest_driver_type == Number(constant_json.NEAREST_PROVIDER_TYPE_SINGLE) && !trip.is_trip_bidding) {
                trip.current_provider = final_providers[0]._id;
                trip.provider_type = final_providers[0].provider_type;
                trip.provider_type_id = final_providers[0].provider_type_id;
            }

            trip.unit = city_detail.unit;
            trip.is_provider_accepted = 0;

            let current_providers = [];

            if (setting_detail.find_nearest_driver_type == Number(constant_json.NEAREST_PROVIDER_TYPE_SINGLE) && !trip.is_trip_bidding) {
                current_providers.push(final_providers[0]._id);
            } else {
                current_providers = final_providers.map(provider => provider._id);
            }

            trip.find_nearest_provider_time = new Date();
            trip.current_providers = current_providers;

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            let trips = [];
            trips.push(trip._id);


            for (let final_provider of final_providers) {
                let idx = current_providers.findIndex(i => String(i) == String(final_provider._id));
                if (idx != -1) {
                    let is_trip_condition = { _id: final_provider._id, is_trip: [] };
                    let is_trip_update = { is_available: 0, is_trip: trips, is_near_available: 0, $inc: { total_request: 1 } };
                    
                    if (trip.is_ride_share && setting_detail.is_allow_ride_share) {
                        is_trip_condition = { _id: final_provider._id };
                        is_trip_update = { is_available: 0, $push: { is_trip: trip._id }, is_ride_share: 1, is_near_available: 0, $inc: { total_request: 1 } };
                    }

                    if(trip.is_trip_bidding || final_provider.bids.length > 0 ){
                        is_trip_update = { is_available: 0, $push: { is_trip: trip._id }, is_near_available: 0, $inc: { total_request: 1 } };
                        delete is_trip_condition.is_trip
                    }
                    
                    if (setting_detail.is_receive_new_request_near_destination && final_provider.is_trip.length != 0 && 
                        final_provider.is_near_available == 1 && !trip.is_ride_share) {
                        is_trip_condition = { _id: final_provider._id, is_near_trip: [] };
                        is_trip_update = { is_near_available: 0, is_near_trip: trips, $inc: { total_request: 1 } };
                    }

                    let updateCount = await Provider.updateOne(is_trip_condition, is_trip_update)
                    if (updateCount.modifiedCount != 0) {
                        let is_trip_condition = { _id: final_provider._id, is_trip: trip._id };
                        if (setting_detail.is_receive_new_request_near_destination) {
                            if (final_provider.is_trip.length != 0 && final_provider.is_near_available == 1) {
                                is_trip_condition = { _id: final_provider._id, is_near_trip: trip._id };
                            }
                        }
                        let provider = await Provider.findOne(is_trip_condition);
                        if (provider) {
                            if (trip.is_ride_share && final_provider.is_trip.length != 0) {
                                utils.update_request_status_socket(final_provider.is_trip[0], trip._id);
                                myAnalytics.insert_daily_provider_analytics(city_timezone, provider._id, TRIP_STATUS.WAITING_FOR_PROVIDER);
                                utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_NEW_NEAREST_TRIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                            } else if (final_provider.is_trip.length != 0 && final_provider.is_near_available == 1) {
                                utils.update_request_status_socket(final_provider.is_trip[0], trip._id);
                                myAnalytics.insert_daily_provider_analytics(city_timezone, provider._id, TRIP_STATUS.WAITING_FOR_PROVIDER);
                                utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_NEW_NEAREST_TRIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                            } else {
                                utils.send_socket_request(trip._id, provider._id);
                                myAnalytics.insert_daily_provider_analytics(city_timezone, provider._id, TRIP_STATUS.WAITING_FOR_PROVIDER);
                                utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_NEW_TRIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                            }
                        }
                    }
                }
            }
                     
            
            resolve({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CREATED_SUCCESSFULLY,
                trip_id: trip._id,
                is_schedule_trip: trip.is_schedule_trip,
                trip_unique_id: trip.unique_id
            });
            return;

        } catch (err) {
            console.log("exports.nearest_provider")
            console.log(err)
            resolve({ success: false, error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG });
        } finally {
            if (is_save_log) {
                //save logs 
                let trip_id = trip?._id || null
                let log;
                let no_of_time_send_request = 0;
                if(trip_id){
                    log = await Find_provider_logs.findOne({trip_id : trip_id})
                    no_of_time_send_request = trip.no_of_time_send_request
                }else if(req_data){
                    log = await Find_provider_logs.findOne({"body.user_id" : req_data?.user_id})
                }
                if (!log) {
                    log = new Find_provider_logs()
                }
                log.body = req_data 
                log.quey = provider_query
                log.online_providers = await Provider.find({cityid : city_id , is_available : 1 , is_active : 1 ,is_approved : 1},{ providerLocation : 1 }) || []
                log.city_id = city_id
                log.trip_id = trip_id
                log.no_of_time_send_request = no_of_time_send_request
                log.save();
            }
        }
    })
};


////  START USER CREATE TRIP SERVICE //// ////////
exports.create = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'service_type_id', type: 'string' },
            { name: 'timezone', type: 'string' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user_data = await User.findOne({ _id: req.body.user_id })
        if (!user_data) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (user_data.current_trip_id) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_ALREADY_RUNNING });
            return;
        }
        if(user_data.is_documents_expired){
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DOCUMENT_EXPIRED });
            return;

        }

        let provider_id = null
        if (req.body.provider_id || req.body.select_trip_provider) {
            provider_id = req.body.provider_id ? req.body.provider_id : req.body.select_trip_provider
        }
        if (typeof req.body.created_by == 'undefined') {
            req.body.created_by = constant_json.CREATED_BY_USER_APP
        }
        req.body.created_by = Number(req.body.created_by);

        let citytype = await Citytype.findOne({ _id: req.body.service_type_id })
        let data = await exports.check_trip_inside_zone_queue_async(citytype.cityid, req.body.latitude, req.body.longitude)

        req.body.is_trip_inside_zone_queue = data.is_trip_inside_zone_queue;
        req.body.zone_queue_id = data.zone_queue_id;


        let trip_type = constant_json.TRIP_TYPE_NORMAL;

        
        let trip_response = await exports.create_trip(user_data, trip_type, req.body.service_type_id, req.body)
        if (!trip_response.success) {
            res.json(trip_response);
            return;
        }

        let trip = trip_response.trip;
        if (req.body.promo_id) {
            let promocode = await Promo_Code.findOne({ _id: req.body.promo_id })
            if (promocode) {
                promocode.user_used_promo = promocode.user_used_promo + 1;

                await Promo_Code.updateOne({ _id: req.body.promo_id }, promocode.getChanges())

                trip.promo_id = promocode._id;
                await Trip.updateOne({ _id: trip._id }, trip.getChanges())

                let userpromouse = new User_promo_use({
                    promo_id: promocode._id,
                    promocode: promocode.promocode,
                    user_id: req.body.user_id,
                    promo_type: promocode.code_type,
                    promo_value: promocode.code_value,
                    trip_id: trip._id,
                    user_used_amount: 0
                });
                await userpromouse.save();
            }
        }

        if (trip.trip_type == constant_json.TRIP_TYPE_HOTEL) {
            message = admin_messages.success_create_trip;
            req.flash('response_code', admin_messages.success_create_trip);
        }

        if (trip.is_schedule_trip) {
            res.json({
                success: true,
                trip: trip,
                message: success_messages.MESSAGE_CODE_YOUR_FUTURE_TRIP_CREATE_SUCCESSFULLY
            });
            return;
        }

        let nearest_provider_response = await exports.nearest_provider(trip, provider_id, user_data.favourite_providers,req.body)
        if (nearest_provider_response.success) {
            user_data.current_trip_id = trip._id;
            await User.updateOne({ _id: user_data._id }, user_data.getChanges());
        if ((trip.trip_type == constant_json.TRIP_TYPE_CORPORATE  || trip.trip_type == constant_json.TRIP_TYPE_DISPATCHER || trip.trip_type == constant_json.TRIP_TYPE_HOTEL)  && req.body?.user_type_id != null  &&  req.body?.corporate_id == null) {
                var phoneWithCode = user_data.country_phone_code + user_data.phone;
                let user_panel_url = setting_detail.user_panel_url
                let map = user_panel_url+'/track-trip?user_id='+user_data._id+'&trip_id='+ trip._id
                utils.sendSmsToEmergencyContact(phoneWithCode, 8, user_data.first_name + " " + user_data.last_name, map, trip.providerLocation.providerLocation);
            }
            res.json(nearest_provider_response);
            return;
        }

        console.log('no provider')
        res.json({
            success: false,
            error_code: error_message.ERROR_CODE_CREATE_TRIP_FAILED
        });
        return;

    } catch (err) {
        console.log("exports.create")
        utils.error_response(err, req, res)
    }
};

exports.provider_create = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [
            { name: 'provider_id', type: 'string' },
            { name: 'phone', type: 'string' },
            { name: 'service_type_id', type: 'string' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let provider_detail = await Provider.findOne({ _id: req.body.provider_id })
        if (!provider_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            return;
        }

        let trip_count = await Trip.count({
            provider_id: req.body.provider_id,
            is_trip_cancelled: 0,
            is_trip_completed: 0,
            is_trip_end: 0
        })
        if (trip_count) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_ALREADY_RUNNING });
            return;
        }

        req.body.created_by = Number(constant_json.CREATED_BY_PROVIDER_APP);
        let user_data = await User.findOne({ country_phone_code: req.body.country_phone_code, phone: req.body.phone });
        if (!user_data && req.body.email) {
            user_data = await User.findOne({ email: req.body.email });
        }
        if (user_data) {
            if (user_data.current_trip_id) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_ALREADY_RUNNING });
                return;
            }

            let trip_response = await exports.create_trip(user_data, constant_json.TRIP_TYPE_PROVIDER, req.body.service_type_id, req.body)
            if (!trip_response.success) {
                res.json(trip_response);
                return;
            }

            let trip = trip_response.trip;
            if (trip.is_schedule_trip) {
                res.json({
                    success: true,
                    trip: trip,
                    message: success_messages.MESSAGE_CODE_YOUR_FUTURE_TRIP_CREATE_SUCCESSFULLY
                });
                return;
            }

            let now_date = new Date()
            trip.is_toll = setting_detail.is_toll;
            trip.provider_unique_id = provider_detail.unique_id;
            trip.provider_first_name = provider_detail.first_name;
            trip.provider_last_name = provider_detail.last_name;
            trip.provider_phone_code = provider_detail.country_phone_code;
            trip.provider_phone = provider_detail.phone;
            trip.provider_type = provider_detail.provider_type;
            trip.provider_type_id = provider_detail.provider_type_id;
            trip.current_provider = provider_detail._id;
            trip.current_providers = [provider_detail._id];
            trip.confirmed_provider = provider_detail._id;
            trip.provider_id = provider_detail._id;
            trip.is_provider_accepted = 1;
            trip.is_provider_status = 4;
            trip.accepted_time = now_date;
            trip.provider_arrived_time = now_date;
            trip.provider_trip_start_time = now_date;
            // let unique_id = pad(trip.unique_id, 7, '0');
            // let invoice_number = constant_json.INVOICE_APP_NAME_CODE + " " + constant_json.INVOICE_PROVIDER_TRIP_EARNING_CODE + " " + (moment(new Date())).format(constant_json.DATE_FORMAT_MMDDYYYY) + " " + unique_id;
            // trip.invoice_number = invoice_number;

            let is_favourite_provider = false;
            if (user_data) {
                let index = user_data.favourite_providers.findIndex((x) => (x).toString() == (provider_detail._id).toString());
                if (index !== -1) {
                    is_favourite_provider = true;
                }
            }
            trip.is_favourite_provider = is_favourite_provider;

            // Set trip status
            trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.ACCEPTED, TYPE_VALUE.PROVIDER )

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            let trips = [];
            trips.push(trip._id);
            provider_detail.is_trip = trips;
            provider_detail.total_request = provider_detail.total_request + 1;
            provider_detail.accepted_request = provider_detail.accepted_request + 1;
            provider_detail.is_available = 0;

            await Provider.updateOne({ _id: provider_detail._id }, provider_detail.getChanges())
            myAnalytics.insert_daily_provider_analytics(trip.timezone, provider_detail._id, TRIP_STATUS.INITIATE_TRIP);

            user_data.current_trip_id = trip._id;
            await User.updateOne({ _id: user_data._id }, user_data.getChanges());
            utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_data.device_type, user_data.device_token, push_messages.PUSH_CODE_FOR_PROVIDER_INITATE_TRIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
            // socket for user get trip when provider create trip
            utils.user_get_trip(user_data._id)
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CREATED_SUCCESSFULLY,
                trip_id: trip._id,
                user: user_data,
                is_schedule_trip: trip.is_schedule_trip
            });
            return;
        }

        let duplicate_user_email = await User.findOne({ email: req.body.email })
        if(duplicate_user_email && req.body.email){
            return res.json({success:false,error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED})
        }

        let duplicate_user_phone = await User.findOne({ phone: req.body.phone,country_phone_code:req.body.country_phone_code })
        if(duplicate_user_phone){
            return res.json({success:false,error_code: error_message.ERROR_CODE_PHONE_NUMBER_ALREADY_USED})
        }

        let country_phone_code = req.body.country_phone_code;
        let country = await Country.findOne({ countryphonecode: country_phone_code })
        let wallet_currency_code = "";
        let countryname = "";
        if (country) {
            wallet_currency_code = country.currencycode;
            countryname = country.countryname;
        } else {
            let i = country_list.findIndex(i => i.code == country_phone_code);
            if (i != -1) {
                wallet_currency_code = country_list[i].currency_code;
            } else {
                wallet_currency_code = "";
            }
        }

        let first_name = req.body.first_name;
        let last_name = req.body.last_name;
        let encrypt_password = require('crypto').createHash('md5').update(req.body.phone).digest('hex');
        let referral_code = (utils.tokenGenerator(8)).toUpperCase();
        let user = new User({
            first_name: first_name,
            last_name: last_name,
            email: ((req.body.email).trim()).toLowerCase(),
            password: encrypt_password,
            user_type: Number(constant_json.USER_TYPE_PROVIDER),
            user_type_id: provider_detail._id,
            country_phone_code: req.body.country_phone_code,
            phone: req.body.phone,
            token: utils.tokenGenerator(32),
            country: countryname,
            referral_code: referral_code,
            wallet_currency_code: wallet_currency_code,
        });

        await user.save();
        
        // Trigger admin notification
        utils.addNotification({
            type: ADMIN_NOTIFICATION_TYPE.USER_REGISTERED,
            user_id: user._id,
            username: user.first_name + " " + user.last_name,
            picture: user.picture,
            country_id: country._id,
            user_unique_id: user.unique_id,
        })

        if (setting_detail.email_notification) {
            allemails.sendUserRegisterEmail(req, user);
        }
        let trip_response = await exports.create_trip(user, constant_json.TRIP_TYPE_PROVIDER, req.body.service_type_id, req.body)
        if (!trip_response.success) {
            res.json(response);
            return;
        }

        let trip = trip_response.trip;


        if (trip.is_schedule_trip) {
            res.json({
                success: true,
                trip: trip,
                message: success_messages.MESSAGE_CODE_YOUR_FUTURE_TRIP_CREATE_SUCCESSFULLY
            });
            return;
        }

        trip.is_toll = setting_detail.is_toll;
        trip.provider_unique_id = provider_detail.unique_id;
        trip.provider_phone_code = provider_detail.country_phone_code;
        trip.provider_phone = provider_detail.phone;
        trip.provider_first_name = provider_detail.first_name;
        trip.provider_last_name = provider_detail.last_name;
        trip.provider_type = provider_detail.provider_type;
        trip.provider_type_id = provider_detail.provider_type_id;
        trip.current_provider = provider_detail._id;
        trip.current_providers = [provider_detail._id];
        trip.confirmed_provider = provider_detail._id;
        trip.provider_id = provider_detail._id;
        trip.is_provider_accepted = 1;
        trip.is_provider_status = 4;
        let now_date = new Date();
        trip.accepted_time = now_date;
        trip.provider_arrived_time = now_date;
        trip.provider_trip_start_time = now_date;
        // let unique_id = pad(trip.unique_id, 7, '0');
        // let invoice_number = constant_json.INVOICE_APP_NAME_CODE + " " + constant_json.INVOICE_PROVIDER_TRIP_EARNING_CODE + " " + (moment(new Date())).format(constant_json.DATE_FORMAT_MMDDYYYY) + " " + unique_id;
        // trip.invoice_number = invoice_number;

        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        let trips = [];
        trips.push(trip._id);
        provider_detail.is_trip = trips;
        provider_detail.total_request = provider_detail.total_request + 1;
        provider_detail.accepted_request = provider_detail.accepted_request + 1;
        provider_detail.is_available = 0;

        await Provider.updateOne({ _id: provider_detail._id }, provider_detail.getChanges())
        myAnalytics.insert_daily_provider_analytics(trip.timezone, provider_detail._id, TRIP_STATUS.INITIATE_TRIP);

        user.current_trip_id = trip._id;
        await User.updateOne({ _id: user._id }, user.getChanges());
        // socket for user get trip when provider create trip
        utils.user_get_trip(user._id)
        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_YOUR_TRIP_CREATED_SUCCESSFULLY,
            trip_id: trip._id,
            user: user,
            is_schedule_trip: trip.is_schedule_trip
        });
        return;

    } catch (err) {
        console.log("exports.provider_create")
        utils.error_response(err, req, res)
    }
};

exports.send_request_from_dispatcher = async function (req, res) {
    try {
        let params_array = [{ name: 'trip_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let provider_id = null
        if (req.body.provider_id) {
            provider_id = req.body.provider_id;
        }
        let tripData = await Trip.findOne({ _id: req.body.trip_id })

        let nearest_provider_response = await exports.nearest_provider(tripData, provider_id, [],null,req.body)
        res.json(nearest_provider_response);
        return;

    } catch (err) {
        console.log("exports.send_request_from_dispatcher")
        utils.error_response(err, req, res)
    }
}

exports.get_near_by_provider = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }
        let type = req.body.type
        let Table;
        switch (type) {
            case TYPE_VALUE.USER:
                Table = User
                break;
            case TYPE_VALUE.PROVIDER:
                Table = Provider
                break;
            case TYPE_VALUE.PARTNER:
                Table = Partner
                break;
            case TYPE_VALUE.CORPORATE:
                Table = Corporate
                break;
            case TYPE_VALUE.HOTEL:
                Table = Hotel
                break;
            case TYPE_VALUE.DISPATCHER:
                Table = Dispatcher
                break;
            default:
                Table = User
                break;
        }
        let user = await Table.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_PROVIDER_FOUND_SELECTED_SERVICE_TYPE_AROUND_YOU });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        if (req.body.service_type_id) {
            let citytype = await Citytype.findOne({ _id: req.body.service_type_id })
            if (citytype) {
                let nearest_provider_response = await exports.nearest_provider(null, null, [], citytype, req.body)
                res.json(nearest_provider_response);
                return;
            }
        }
        let nearest_provider_response = await exports.nearest_provider(null, null, [], null, req.body)
        res.json(nearest_provider_response);
        return;

    } catch (err) {
        console.log("exports.get_near_by_provider")
        utils.error_response(err, req, res)
    }
};

exports.provider_get_trips = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }], function (response) {
        if (response.success) {
            Provider.findOne({ _id: req.body.provider_id }).then(async (provider) => {
                if (provider) {
                    if (req.body.token != null && provider.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        let openride_acceted_trip
                        if (!provider.is_near_trip) { provider.is_near_trip = [] }
                        if (provider.is_trip.length == 0 && provider.is_near_trip.length != 0) {
                            provider.is_trip = provider.is_near_trip;
                            provider.is_available = 0;
                            provider.is_near_trip = []

                            let acceted_trip = await Trip.count({$or: [{ $and: [{_id: {$in: provider.is_trip}}, {provider_id: provider.id}]}, {_id: {$in: provider.schedule_trip}}]})
                            openride_acceted_trip = await OpenRide.count({$or: [  {provider_id: provider.id}, {_id: {$in: provider.open_ride}}]})
                            if(acceted_trip == 0){
                                provider.is_available = 1;
                            }
                            if(openride_acceted_trip == 0){
                                provider.is_available = 1;
                            }
                            await provider.save();
                        }
                            let filtered_is_trip

                            filtered_is_trip = provider.is_trip
                            if (openride_acceted_trip != 0) {
                                filtered_is_trip = provider.open_ride
                            }
                            
                            if(provider.is_trip.length > 0){
                                filtered_is_trip = provider.is_trip.filter(trip => !provider.bids.some(bid => bid.trip_id.toString() === trip.toString()));
                            } else if(provider.schedule_trip.length > 0){
                                filtered_is_trip = [provider.schedule_trip[provider.schedule_trip.length - 1]]
                            }
                            else if(provider.open_ride.length > 0){
                                filtered_is_trip = [provider.open_ride[provider.open_ride.length - 1]]
                            }
                            // console.log('filtered_is_trip')
                            // console.log(filtered_is_trip)

                        return res.json({ success: true, message: success_messages.MESSAGE_CODE_YOU_GET_TRIP, trip_detail: filtered_is_trip, bids: provider.bids })
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });

                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.provider_get_trip_details = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }], async function (response) {
        if (!response.success) {
            return res.json({ success: false, error_code: response.error_code, error_description: response.error_description });
        }
        try {
            const setting_detail = await Settings.findOne({});


            let provider = await Provider.findOne({ _id: req.body.provider_id })
            if (!provider) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            }
            if (req.body.token != null && provider.token != req.body.token) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            }
            if (!provider.is_near_trip) { provider.is_near_trip = [] }
            if (provider.is_trip.length == 0 && provider.is_near_trip.length != 0) {
                provider.is_trip = provider.is_near_trip;
                provider.is_available = 0;
                provider.is_near_trip = []

                let acceted_trip = await Trip.count({_id: {$in: provider.is_trip}, provider_id: provider.id})
                if(acceted_trip == 0){
                    provider.is_available = 1;
                }
            }
            if (provider.is_trip.length == 0 && provider.schedule_trip?.length == 0 && provider.open_ride?.length == 0) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP_FOUND });
            }
            
            let filtered_is_trip = provider.is_trip;
            if(provider.is_trip.length > 0){
                filtered_is_trip = provider.is_trip.filter(trip => !provider.bids.some(bid => bid.trip_id.toString() === trip.toString()));
            }
            let open_ride = provider.open_ride;
            if(provider.open_ride.length > 0){
                filtered_is_trip = provider.open_ride.filter(trip => !provider.bids.some(bid => bid.trip_id.toString() === trip.toString()));
            }
            let trip = await Trip.find({ _id: { $in: filtered_is_trip } });

            let schedule_trip = provider.schedule_trip
            let schedule_trips = await Trip.find({ _id: { $in: schedule_trip}})  
            let openride_schedule_trips = await OpenRide.find({ _id: { $in: provider.is_trip}})

            let trip_histories = await Trip_history.find({ _id: { $in: filtered_is_trip } });
            // let openride_histories = await OpenRide.find({ _id: { $in: open_ride } });
            let trips = [...schedule_trips, ...openride_schedule_trips, ...trip, ...trip_histories];
            let trip_detail = [];

            for (let trip of trips) {
                if (trip.is_trip_cancelled == 0 && trip.is_provider_invoice_show == 0 && trip.is_trip_completed == 0) {
                    let userid
                    if (trip.openride) {
                        const filteredObjects = trip.user_details.filter(item => 
                            item.status == 0 && item.send_req_to_provider_first_time == 0 
                        );
                        if (filteredObjects.length != 0) {
                            userid = filteredObjects[0].user_id
                        }
                        
                    } else {
                        userid = trip.user_id
                    }

                    let user_detail = await User.findOne({ _id: userid })
                        let start_time = trip.updated_at;
                        let end_time = new Date();
                        let res_sec = utils.getTimeDifferenceInSecond(end_time, start_time);
                        let provider_timeout = setting_detail.provider_timeout;
                        
                        if(trip.is_trip_bidding){
                            let country_detail = await Country.findOne({_id: trip.country_id},{provider_bidding_timeout: 1});
                            provider_timeout = country_detail.provider_bidding_timeout;
                        }
                        let time_left_to_responds_trip = provider_timeout - res_sec;

                        trip_detail.push({
                            trip_id: trip._id,
                            unique_id: trip.unique_id,
                            user_id: userid,
                            is_provider_accepted: trip.is_provider_accepted,
                            is_provider_status: trip.is_provider_status,
                            trip_type: trip.trip_type,
                            source_address: trip.source_address,
                            destination_address: trip.destination_address,
                            sourceLocation: trip.sourceLocation,
                            destinationLocation: trip.destinationLocation,
                            is_trip_end: trip.is_trip_end,
                            time_left_to_responds_trip: time_left_to_responds_trip,
                            user: {
                                first_name: user_detail?.first_name ? user_detail?.first_name : '',
                                last_name: user_detail?.last_name ? user_detail?.last_name : '',
                                phone: user_detail?.phone ? user_detail?.phone : '',
                                country_phone_code: user_detail?.country_phone_code ? user_detail?.country_phone_code : '',
                                rate: user_detail?.rate ? user_detail?.rate : 0,
                                rate_count: user_detail?.rate_count ? user_detail?.rate_count : 0,
                                picture: user_detail?.picture ? user_detail?.picture : ''
                            }
                        })
                        if (trip.openride) {
                            let userdetails_index = trip.user_details.findIndex(item => String(item.user_id) == String(userid))
                            if (userdetails_index != -1) {
                                trip.user_details[userdetails_index].send_req_to_provider_first_time = 1 
                                await OpenRide.updateOne({ _id: trip._id }, trip.getChanges())
                            }
                        }
                    
                } else {
                    provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)
                    if (!provider.is_near_trip) { provider.is_near_trip = [] }
                    if ((String(provider.is_near_trip[0]) == String(trip._id))) {
                        provider.is_near_available = 1;
                        provider.is_near_trip = [];
                    }
                }
            }
            await Provider.updateOne({ _id: provider._id }, provider.getChanges())

            if (trip_detail.length == 0) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP_FOUND });
            }
            trip_detail.sort((a, b) => a.is_provider_accepted - b.is_provider_accepted);
            return res.json({ success: true, message: success_messages.MESSAGE_CODE_YOU_GET_TRIP, trip_detail });
        } catch (e) {
            console.log(e)
            return res.json({ success: false, error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG });
        }
    });
};


////////////USER  GET TRIPSTATUS//////////// ///
exports.user_get_trip_status = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }
        if (user.is_approved == 0) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_NOT_APPROVED })
            return;
        }

        if (req.body?.trackUrl != true &&   user.token != req.body?.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }
        var json;
        if (req.body.type == "web") {
            json = { user_id: req.body.user_id, _id: user.current_trip_id }
        } else {
            json = { user_id: req.body.user_id, _id: user.current_trip_id };
        }
        let openride_json = {'user_details.user_id': req.body.user_id, _id: user.current_trip_id}
        let trip = await Trip.findOne(json) || await Trip_history.findOne(json) || await OpenRide.findOne(openride_json)
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP })
            return
        }
        let country = await Country.findOne({ _id: trip.country_id })
        if (trip.is_trip_cancelled_by_provider == 1) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_CANCELLED_BY_PROVIDER });
            return;
        }
        let tripservice = await Trip_Service.findOne({ _id: trip.trip_service_city_type_id })
        if (!tripservice) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP });
            return;
        }
        let citytype_detail = await Citytype.findById(trip.service_type_id)
        let type_detail = await Type.findById(citytype_detail.typeid)
        var cancellation_fee = tripservice.cancellation_fee;
        if (trip.car_rental_id) {
            var rental_main_citytype = await Citytype.findOne({ car_rental_ids: trip.car_rental_id });
            if (rental_main_citytype) {
                cancellation_fee = rental_main_citytype.cancellation_fee
            }
        }
        var waiting_time_start_after_minute = tripservice.waiting_time_start_after_minute;
        var price_for_waiting_time = tripservice.price_for_waiting_time;
        var total_wait_time = 0;
        var provider_arrived_time = trip.provider_arrived_time;
        if (provider_arrived_time != null) {
            var end_time = new Date();
            total_wait_time = utils.getTimeDifferenceInSecond(end_time, provider_arrived_time);
            total_wait_time = total_wait_time - waiting_time_start_after_minute * 60;
        }
        // if(trip.destination_addresses.length > 0){
        //     var end_time = new Date();
        //     total_wait_time_for_multiple_stop = utils.getTimeDifferenceInSecond(end_time, provider_arrived_time);
        //     total_wait_time_for_multiple_stop = total_wait_time_for_multiple_stop - waiting_time_start_after_minute * 60;
        // }
        let cityDetail = await City.findOne({ _id: citytype_detail.cityid })
        if (!cityDetail) {
            res.json({
                success: false,
                error_code: error_message.ERROR_CODE_NO_CITY_LIST_FOUND
            });
            return;
        }
        let user_promo_use = await User_promo_use.findOne({ trip_id: trip._id })
        var isPromoUsed = 0;
        var PAYMENT_TYPES = utils.PAYMENT_TYPES();
        if (user_promo_use) {
            isPromoUsed = 1;
        }
        if (trip.is_provider_status == PROVIDER_STATUS.TRIP_STARTED) {
            var now = new Date();
            var minutes = utils.getTimeDifferenceInMinute(now, trip.provider_trip_start_time);
            trip.total_time = minutes;
            if(trip.openride){
                await OpenRide.updateOne({ _id: trip._id }, trip.getChanges())
            }else{
                await Trip.updateOne({ _id: trip._id }, trip.getChanges())
            }
        }
        var now = new Date();
        var provider_trip_end_time = trip.provider_trip_end_time;
        var diff = utils.getTimeDifferenceInSecond(now, provider_trip_end_time);
        var tip_timeout = 30;
        if (diff < 0) {
            diff = 0;
        }
        var time_left_for_tip = tip_timeout - diff;
        let provider = await Provider.findOne({ _id: req.body.provider_id })
        utils.socket_provider_location_update(trip.unique_id, trip.provider_id)
        res.json({
            success: true,
            map_pin_image_url: type_detail.map_pin_image_url,
            type_name:type_detail.typename,
            message: success_messages.MESSAGE_CODE_YOU_GET_TRIP_STATUS,
            city_detail: cityDetail,
            trip: trip,
            time_left_for_tip: time_left_for_tip,
            waiting_time_start_after_minute: waiting_time_start_after_minute,
            price_for_waiting_time: price_for_waiting_time,
            total_wait_time: total_wait_time,
            isPromoUsed: isPromoUsed,
            server_time: new Date(),
            cancellation_fee: cancellation_fee,
            payment_gateway: PAYMENT_TYPES,
            country_code: country?.alpha2
        });
    } catch (error) {
        utils.error_response(error, req, res)
    }
};

/////////////RESPOND TRIP///////////////////

exports.responds_trip = async function (req, res) {
    try {
        let params_array = [{ name: 'provider_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let provider = await Provider.findOne({ _id: req.body.provider_id })
        if (!provider) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && provider.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        if (!ObjectId.isValid(req.body.trip_id)) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_ACCEPTED });
            return;
        }

        let trip = await Trip.findOne({ _id: req.body.trip_id, current_providers: provider._id });
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_ACCEPTED });
            return;
        }

        let is_provider_accepted = req.body.is_provider_accepted;
        if (trip.is_trip_cancelled == 1) {
            if (is_provider_accepted == 1) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED });
                return;
            }

            res.json({ success: true, message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_REJECTED_TRIP });
            return;
        }

        if (trip.is_provider_accepted == 1) {
            if (is_provider_accepted == 1) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_ACCEPTED });
                return;
            }

            res.json({ success: true, message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_REJECTED_TRIP });
            return;
        }

        if (is_provider_accepted == 1 ) {

            if(trip.is_trip_bidding && req.body.bid_price && (trip.bid_price < +req.body.bid_price)){
                let bid_trip_response = await exports.driver_bids_trip(req.body.bid_price, provider, trip)
                res.json(bid_trip_response);
            }else{
                let accept_trip_response = await exports.accept_trip(provider, trip, req)
                res.json(accept_trip_response);
            }
            return;
        }

        let reject_trip_response = await exports.reject_trip(provider, trip, req.body.is_request_timeout)
        res.json(reject_trip_response);
        return;

    } catch (err) {
        console.log("exports.responds_trip")
        utils.error_response(err, req, res)
    }
};

exports.driver_bids_trip = function (bid_price, provider, trip) {
    return new Promise(async (resolve, reject) => {
        try {
            // const setting_detail = await Settings.findOne({});
            let country_detail = await Country.findOne({_id: trip.country_id},{provider_bidding_timeout: 1,user_bidding_timeout: 1});
                    
            let now = new Date();
            let end = new Date();
            end.setSeconds(end.getSeconds() + +country_detail.user_bidding_timeout);
            end = new Date(end)

            let user_detail = await User
                .findOne({ _id: trip.user_id })
                .select({
                    favourite_providers: 1,
                    country_phone_code: 1,
                    phone: 1,
                    device_type: 1,
                    device_token: 1,
                    picture: 1,
                    rate: 1,
                    rate_count: 1
                })
            let is_favourite_provider = false;

            if (user_detail) {
                let index = user_detail.favourite_providers.findIndex((x) => (x).toString() == (provider._id).toString());
                if (index !== -1) {
                    is_favourite_provider = true;
                }
            }


            let bids = trip.bids;
            bids.push({ 
                provider_id: provider._id, 
                first_name: provider.first_name,
                last_name: provider.last_name,
                picture: provider.picture,
                is_favourite_provider: is_favourite_provider,
                currency: trip.currency,
                ask_bid_price: bid_price, 
                bid_at: now,
                bid_end_at: end,
                rate: provider.rate,
                rate_count: provider.rate_count,
                status: 0
            });
            trip.bids = bids
            
            if(trip.current_providers.length == 0){
                await exports.nearest_provider(trip, null, []);
            }
            
            await Trip.updateOne({ _id: trip._id, is_provider_accepted: 0 }, trip.getChanges())


            let provider_bids = provider.bids;
            provider_bids.push({ 
                trip_id: trip._id, 
                unique_id: trip.unique_id, 
                first_name: trip.user_first_name,
                last_name: trip.user_last_name,
                picture: user_detail.picture,
                currency: trip.currency,
                bid_price: trip.bid_price, 
                ask_bid_price: bid_price, 
                bid_at: now,
                bid_end_at: end,
                rate: user_detail.rate,
                rate_count: user_detail.rate_count,
                status: 0
            });
            provider.bids = provider_bids
            provider.is_available = 1;
            await Provider.updateOne({ _id: provider._id }, provider.getChanges())

            utils.update_request_status_socket(trip._id);
            resolve({
                success: true,
                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_BID_TRIP_SUCCESSFULLY,
                is_trip_bidding: trip.is_trip_bidding
            });
        } catch (e) {
            console.log("exports.accept_trip")
            console.log(e)
            resolve({ success: false, error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG });
        }
    })
};

exports.user_reject_bid = async function (req, res) {
    try {
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'trip_id', type: 'string' },
            { name: 'provider_id', type: 'string' }
        ];
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            if(res){
                res.json(response);
            }
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            if(res){
                res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            }
            return;
        }

        if (user.token != req.body.token && !req.body.is_from_cron) {
            if(res){
                res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            }
            return;
        }

        let trip = await Trip.findOne({ _id: req.body.trip_id });
        if (!trip) {
            if(res){
                res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP });
            }
            return;
        }

        // remove bid entry of specific provider
        trip.bids = trip.bids.filter(function(bid) { return (bid.provider_id).toString() !== (req.body.provider_id).toString(); });
        trip.current_providers = trip.current_providers.filter(provider_id => provider_id.toString() !== (req.body.provider_id).toString());
        trip.markModified('bids');

        // remove bid entry of specific provider
        let provider = await Provider.findOne({ _id: req.body.provider_id });
        provider.bids = provider.bids.filter(function(bid) { return (bid.trip_id).toString() !== (req.body.trip_id).toString(); });
        provider.markModified('bids');
        
        trip.providers_id_that_rejected_trip.push(provider._id)
        trip.markModified('providers_id_that_rejected_trip');

        await Provider.updateOne({ _id: provider._id }, provider.getChanges())
        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        if(trip.current_providers.length == 0){
            await exports.nearest_provider(trip, null, []);
        }

        utils.update_request_status_socket(trip._id);

        if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
            utils.get_service_id_socket(trip.user_type_id)
        }

        if(res){
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_FOR_USER_YOU_REJECT_BID_SUCCESSFULLY
            });
        }
        return;
    } catch (err) {
        console.log(err);
        utils.error_response(err, req, res)
    }
};

exports.user_accept_bid = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'trip_id', type: 'string' },
            { name: 'provider_id', type: 'string' }
        ];
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let trip = await Trip.findOne({ _id: req.body.trip_id, is_provider_accepted: 0 });
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP });
            return;
        }

        let provider = await Provider.findOne({ _id: req.body.provider_id });
        let trips = Trip.find({_id: {$in: provider.is_trip, is_provider_accepted: 1 }})
        if(trips.length > 0 || (provider.is_available == 0 && provider.is_near_trip.length == 0)){
            trip.bids = trip.bids.filter(function(bid) { return (bid.provider_id).toString() !== (provider._id).toString(); });
            trip.markModified('bids');
            await Trip.updateOne({ _id: trip._id }, trip.getChanges())
            return res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_ALREADY_RUNNING });
        }

        let now = new Date();
 
        // web panel future histoy socket
        if (trip.trip_type == constant_json.TRIP_TYPE_SCHEDULE) {
            utils.req_type_id_socket(trip.user_id)
        }
    
        // set trip values same as accept_trip api
        
        // set driver bid price as a fixed trip price
        trip.is_fixed_fare = true;
        let index = trip.bids.findIndex((x) => (x.provider_id).toString() == (provider._id).toString());
        if(index != -1){
            trip.fixed_price = trip.bids[index].ask_bid_price;

            let service_type_id = trip.service_type_id;
            if (trip.car_rental_id) {
                service_type_id = trip.car_rental_id;
            }
            let trip_service = await Trip_Service.findOne({ service_type_id: service_type_id }).sort({ _id: -1 })
            
            trip.provider_service_fees = Number((trip.fixed_price * trip_service.provider_profit * 0.01).toFixed(3));
        }

        // assign provider details to trip
        trip.current_provider = provider._id;
        trip.provider_type = provider.provider_type;
        trip.provider_type_id = provider.provider_type_id;
        trip.confirmed_provider = provider._id;
        trip.provider_app_version = provider.app_version;
        trip.provider_device_type = provider.device_type;
        trip.is_provider_accepted = 1;
        trip.is_provider_status = 1;
        trip.accepted_time = now;
        trip.is_schedule_trip = false;
        trip.provider_unique_id = provider.unique_id;
        trip.provider_phone_code = provider.country_phone_code;
        trip.provider_phone = provider.phone;
        trip.provider_first_name = provider.first_name;
        trip.provider_last_name = provider.last_name;

        let unique_id = pad(trip.unique_id, 7, '0');
        let invoice_number = constant_json.INVOICE_APP_NAME_CODE + " " + constant_json.INVOICE_PROVIDER_TRIP_EARNING_CODE + " " + (moment(now)).format(constant_json.DATE_FORMAT_MMDDYYYY) + " " + unique_id;
        trip.invoice_number = invoice_number;
        trip.provider_id = provider._id;
        trip.providerLocation = provider.providerLocation;
        trip.bearing = provider.bearing;
        let current_providers = trip.current_providers.filter((el) => { return String(el) !== String(provider._id) });
        trip.current_providers = [];
        let is_favourite_provider = false;
        if (user) {
            let index = user.favourite_providers.findIndex((x) => (x).toString() == (provider._id).toString());
            if (index !== -1) {
                is_favourite_provider = true;
            }
        }
        trip.is_favourite_provider = is_favourite_provider

        let providers_list = await Provider.find({ _id: current_providers }).select({
            device_type: 1,
            device_token: 1,
            is_trip: 1,
            is_near_trip: 1,
            bids: 1,
        })  

        for (let provider of providers_list) {
            utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_TRIP_ACCEPTED_BY_ANOTHER_PROVIDER, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);

            let is_trip_condition = { _id: provider._id };
            let is_trip_update = { is_available: 1, is_trip: [] };
            if(trip.is_trip_bidding){
                is_trip_update = { $pull: { is_trip: trip._id } };
            }else if (provider.is_trip.length > 1) {
                is_trip_update = { is_available: 0, $pull: { is_trip: trip._id } };
            }
            if (!provider.is_near_trip) { provider.is_near_trip = [] }
            if (provider.is_near_trip.length != 0) {
                is_trip_update = { is_near_available: 1, is_near_trip: [] };
            }

            is_trip_update.bids = provider.bids;
            is_trip_update.bids = is_trip_update.bids.filter(function(bid) { return (bid.trip_id).toString() !== (trip._id).toString(); });

            await Provider.updateOne(is_trip_condition, is_trip_update);
        }

        if (user) {
            if (setting_detail.sms_notification) {
                utils.sendOtherSMS(user.country_phone_code + user.phone, 5, "");
            }
            utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_ACCEPT_TRIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
        }
        myAnalytics.insert_daily_provider_analytics(trip.timezone, provider._id, TRIP_STATUS.PROVIDER_ACCEPTED);

        provider.accepted_request = provider.accepted_request + 1;
        provider.is_available = 0;
        if (trip.is_ride_share) {
            provider.is_ride_share = 1
            if (!provider.destinationLocation) {
                provider.destinationLocation = [];
            }
            provider.destinationLocation.push(trip.destinationLocation);
        }


        if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
            utils.get_service_id_socket(trip.user_type_id)
        }

        // create bid array on accept
        trip.bids = []
        let provider_bids = provider.bids;

        if(provider.is_trip.length > 0){
            utils.update_request_status_socket(provider.is_trip[0]);
        }
        // remove bid entry from other trips
        for await (const bid of provider_bids) {
            if(bid.trip_id.toString() != trip._id.toString()){
                let other_bid_trip = await Trip.findOne({_id: bid.trip_id})
                if(other_bid_trip){
                    other_bid_trip.bids = other_bid_trip.bids.filter(function(bid) { return (bid.provider_id).toString() !== (provider._id).toString(); });
                    let current_providers = other_bid_trip.current_providers.filter((el) => { return String(el) !== String(provider._id) });
                    other_bid_trip.current_providers = current_providers
                    await Trip.updateOne({ _id: other_bid_trip._id }, other_bid_trip.getChanges())
                    utils.update_request_status_socket(other_bid_trip._id);
                    
                    let index = provider.is_trip.findIndex((x) => (x).toString() == (other_bid_trip._id).toString());
                    if(index != -1){
                        provider.is_trip.splice(index, 1);
                    }
                }
            }
        }

        provider.bids = []

        if(!provider.is_trip.includes(trip._id) && !provider.is_near_trip.includes(trip._id)){
            provider.is_trip.push(trip._id);
        }
        provider.markModified('is_trip');
        
        await Provider.updateOne({ _id: provider._id }, provider.getChanges())
        await Trip.updateOne({ _id: trip._id }, trip.getChanges())


        utils.update_request_status_socket(trip._id);
        utils.send_socket_request(trip._id, provider._id);
        
        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_ACCEPTED_TRIP_SUCCESSFULLY,
            is_provider_accepted: trip.is_provider_accepted
        });
        return;
    } catch (err) {
        console.log("exports.user_reject_bid")
        utils.error_response(err, req, res)
    }
};

exports.accept_trip = function (provider, trip, req = null) {
    return new Promise(async (resolve, reject) => {
        try {
            const setting_detail = await Settings.findOne({});
            if (provider.zone_queue_id) {
                provider = await utils.remove_from_zone_queue_new(provider);
            }
            let now = new Date();
            if(provider.zone_queue_id){
                provider = await utils.add_in_zone_queue_new(provider?.zone_queue_id, provider);
            }
            // web panel future histoy socket
            if (trip.trip_type == constant_json.TRIP_TYPE_SCHEDULE) {
                utils.req_type_id_socket(trip.user_id)
            }
            trip.current_provider = provider._id;
            trip.provider_type = provider.provider_type;
            trip.provider_type_id = provider.provider_type_id;
            trip.confirmed_provider = provider._id;
            trip.provider_app_version = provider.app_version;
            trip.provider_device_type = provider.device_type;
            trip.is_provider_accepted = 1;
            trip.is_provider_status = 1;
            trip.accepted_time = now;
            trip.is_schedule_trip = false;
            trip.provider_unique_id = provider.unique_id;
            trip.provider_phone_code = provider.country_phone_code;
            trip.provider_phone = provider.phone;
            trip.provider_first_name = provider.first_name;
            trip.provider_last_name = provider.last_name;

            // let unique_id = pad(trip.unique_id, 7, '0');
            // let invoice_number = constant_json.INVOICE_APP_NAME_CODE + " " + constant_json.INVOICE_PROVIDER_TRIP_EARNING_CODE + " " + (moment(now)).format(constant_json.DATE_FORMAT_MMDDYYYY) + " " + unique_id;
            // trip.invoice_number = invoice_number;
            trip.provider_id = provider._id;
            trip.providerLocation = provider.providerLocation;
            trip.bearing = provider.bearing;
            let current_providers = trip.current_providers.filter((el) => { return String(el) !== String(provider._id) });
            trip.current_providers = [];
            
            // Set trip status
            trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.ACCEPTED, TYPE_VALUE.PROVIDER )

            if(trip.is_trip_bidding){
                 // set driver bid price as a fixed trip price
                trip.is_fixed_fare = true;
                trip.fixed_price = req.body.bid_price;
                let service_type_id = trip.service_type_id;
                if (trip.car_rental_id) {
                    service_type_id = trip.car_rental_id;
                }
                let trip_service = await Trip_Service.findOne({ service_type_id: service_type_id }).sort({ _id: -1 })
                trip.provider_service_fees = Number((trip.fixed_price * trip_service.provider_profit * 0.01).toFixed(3));
                
                trip.bids = [];
            }
            let user_detail = await User
                .findOne({ _id: trip.user_id })
                .select({
                    favourite_providers: 1,
                    country_phone_code: 1,
                    phone: 1,
                    device_type: 1,
                    device_token: 1,
                    webpush_config:1
                })
            let is_favourite_provider = false;
            if (user_detail) {
                let index = user_detail.favourite_providers.findIndex((x) => (x).toString() == (provider._id).toString());
                if (index !== -1) {
                    is_favourite_provider = true;
                }
            }
            trip.is_favourite_provider = is_favourite_provider

            let update = await Trip.updateOne({ _id: trip._id, $or: [{is_provider_accepted: 0}, {is_provider_assigned_by_dispatcher: true}]  }, trip.getChanges())
            if (!update.modifiedCount) {
                resolve({
                    success: false,
                    error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_ACCEPTED
                });
                return;
            }

            let providers_list = await Provider
                .find({ _id: current_providers })
                .select({
                    device_type: 1,
                    device_token: 1,
                    is_trip: 1,
                    is_near_trip: 1,
                    bids: 1
                })
            for (let provider of providers_list) {
                utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_TRIP_ACCEPTED_BY_ANOTHER_PROVIDER, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);

                let is_trip_condition = { _id: provider._id };
                let is_trip_update = { is_available: 1, is_trip: [] };
                if (provider.is_trip.length > 1) {
                    is_trip_update = { is_available: 0, $pull: { is_trip: trip._id } };
                }
                if (!provider.is_near_trip) { provider.is_near_trip = [] }
                if (provider.is_near_trip.length != 0) {
                    is_trip_update = { is_near_available: 1, is_near_trip: [] };
                }

                is_trip_update.bids = provider.bids;
                is_trip_update.bids = is_trip_update.bids.filter(function(bid) { return (bid.trip_id).toString() !== (trip._id).toString(); });

                await Provider.updateOne(is_trip_condition, is_trip_update);
            }

            if (user_detail) {
                if (setting_detail.sms_notification) {
                    utils.sendOtherSMS(user_detail.country_phone_code + user_detail.phone, 5, "");
                }
                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_detail.device_type, user_detail.device_token, push_messages.PUSH_CODE_FOR_ACCEPT_TRIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user_detail.webpush_config);
            }
            myAnalytics.insert_daily_provider_analytics(trip.timezone, provider._id, TRIP_STATUS.PROVIDER_ACCEPTED);

            provider.accepted_request = provider.accepted_request + 1;
            provider.is_available = 0;
            
            let provider_bids = provider.bids;
            // remove bid entry from other trips
            console.log(provider.is_trip);
            //provider_bids.forEach(async (bid) => {
            for await (const bid of provider_bids) {

                if((bid.trip_id).toString() != (trip._id).toString()){
                    let other_bid_trip = await Trip.findOne({_id: bid.trip_id})
                    if(other_bid_trip){
                        other_bid_trip.bids = other_bid_trip.bids.filter(function(bid) { return (bid.provider_id).toString() !== (provider._id).toString(); });
                        await Trip.updateOne({ _id: other_bid_trip._id }, other_bid_trip.getChanges())
                        utils.update_request_status_socket(other_bid_trip._id);

                        let index = provider.is_trip.findIndex((x) => (x).toString() == (other_bid_trip._id).toString());
                        if(index != -1){
                            provider.is_trip.splice(index, 1);
                        }
                        
                        console.log(provider.is_trip);
                    }
                }
            }
            console.log(provider.is_trip);

            if(!provider.is_trip.includes(trip._id) && !provider.is_near_trip.includes(trip._id)){
                provider.is_trip.push(trip._id);
            }
            console.log(provider.is_trip);

            provider.bids = [];
            // provider.markModified('bids');

            
            if (trip.is_ride_share) {
                provider.is_ride_share = 1
                if (!provider.destinationLocation) {
                    provider.destinationLocation = [];
                }
                provider.destinationLocation.push(trip.destinationLocation);
            }
            await Provider.updateOne({ _id: provider._id }, provider.getChanges())

            utils.update_request_status_socket(trip._id,null,trip.is_provider_status);
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }
            resolve({
                success: true,
                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_ACCEPTED_TRIP_SUCCESSFULLY,
                is_provider_accepted: trip.is_provider_accepted
            });
        } catch (e) {
            console.log("exports.accept_trip")
            console.log(e)
            resolve({ success: false, error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG });
        }
    })
};

exports.reject_trip = function (provider, trip, is_request_timeout) {
    return new Promise(async (resolve, reject) => {
        try {
            const setting_detail = await Settings.findOne({});

            let now = new Date();
            // let unique_id = pad(trip.unique_id, 7, '0');
            // let invoice_number = constant_json.INVOICE_APP_NAME_CODE + " " + constant_json.INVOICE_PROVIDER_TRIP_EARNING_CODE + " " + (moment(now)).format(constant_json.DATE_FORMAT_MMDDYYYY) + " " + unique_id;
            // trip.invoice_number = invoice_number;

            let zone_queue_id = provider.zone_queue_id;
            if (provider.zone_queue_id) {
                provider = await utils.remove_from_zone_queue_new(provider);
                provider = await utils.add_in_zone_queue_new(zone_queue_id, provider);
                if (is_request_timeout) {
                    myAnalytics.insert_daily_provider_analytics(trip.timezone, provider._id, TRIP_STATUS.NOT_ANSWERED);
                } else {
                    myAnalytics.insert_daily_provider_analytics(trip.timezone, provider._id, TRIP_STATUS.PROVIDER_REJECTED);
                }
                provider.rejected_request = provider.rejected_request + 1;
                provider = utils.remove_is_trip_from_provider(provider, trip._id)
                if (!provider.is_near_trip) { provider.is_near_trip = [] }
                if ((String(provider.is_near_trip[0]) == String(trip._id))) {
                    provider.is_near_available = 1;
                    provider.is_near_trip = [];
                }
                await Provider.updateMany({ zone_queue_id: provider.zone_queue_id, _id: { $ne: provider._id } }, { '$inc': { zone_queue_no: -1 } }, { multi: true });
            } else {
                if (is_request_timeout) {
                    myAnalytics.insert_daily_provider_analytics(trip.timezone, provider._id, TRIP_STATUS.NOT_ANSWERED);
                } else {
                    myAnalytics.insert_daily_provider_analytics(trip.timezone, provider._id, TRIP_STATUS.PROVIDER_REJECTED);
                }
                provider.rejected_request = provider.rejected_request + 1;
                provider = utils.remove_is_trip_from_provider(provider, trip._id)

                if (!provider.is_near_trip) { provider.is_near_trip = [] }
                if ((String(provider.is_near_trip[0]) == String(trip._id))) {
                    provider.is_near_available = 1;
                    provider.is_near_trip = [];
                }
            }


            trip.bids = trip.bids.filter(function(bid) { return (bid.provider_id).toString() !== (provider._id).toString(); });
            trip.markModified('bids');
    
            provider.bids = provider.bids.filter(function(bid) { return (bid.trip_id).toString() !== (trip._id).toString(); });
            provider.markModified('bids');
    
            await Provider.updateOne({ _id: provider._id }, provider.getChanges())

            let trip_changes = trip.getChanges()
            trip_changes['$pull'] = { current_providers: provider._id }
            trip_changes['$push'] = { providers_id_that_rejected_trip: provider._id }
            if(trip.is_schedule_trip) {
                trip_changes['$addToSet'] = {providers_id_that_rejected_trip_for_schedule: provider._id}
            }
            // let update = await Trip.updateOne({ _id: trip._id, is_provider_accepted: 0 }, trip_changes)
            let updateTrip = await Trip.findOneAndUpdate({_id : trip._id , is_provider_accepted : 0} , trip_changes , {new : true})
            if (!updateTrip) {
                resolve({
                    success: false,
                    error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_ACCEPTED
                });
                return;
            }
            utils.update_request_status_socket(updateTrip._id);

            resolve({
                success: true,
                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_REJECTED_TRIP,
                is_provider_accepted: updateTrip.is_provider_accepted
            });

            if ((setting_detail.find_nearest_driver_type == Number(constant_json.NEAREST_PROVIDER_TYPE_SINGLE) && !trip.is_trip_bidding) || updateTrip.current_providers.length == 0) {
                await exports.nearest_provider(updateTrip, null, []);
            }

        } catch (e) {
            console.log(e)
            resolve({ success: false, error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG });
        }
    })
};

exports.trip_cancel_by_user = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'trip_id', type: 'string' }
        ];
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }
        let corporate = ''
        if(user.user_type_id){
            corporate = await Corporate.findOne({_id: user.user_type_id})
            if (!corporate) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                return;
            }

        }

        if (req.body.type != constant_json.TRIP_TYPE_DISPATCHER && req.body.type != constant_json.TRIP_TYPE_CORPORATE && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let trip = await Trip.findOne({ _id: req.body.trip_id });
        let cancel_reason = req.body.cancel_reason;
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP });
            return;
        }
        if(trip.is_provider_status >= PROVIDER_STATUS.TRIP_STARTED){

            return res.json({ success: false, error_code: error_message.ERROR_CODE_CANCEL_TRIP_FAILED });

        }
        if (!(trip.is_trip_completed == 0 && trip.is_trip_end == 0)) {
            utils.update_request_status_socket(trip._id);
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }
            res.json({
                success: true,
                payment_status: trip.payment_status,
                message: success_messages.ERROR_CODE_TRIP_ALREADY_COMPLETED
            });
            return;
        }

        if (!(trip.is_trip_cancelled == 0 && trip.is_trip_cancelled_by_user == 0 && trip.is_trip_cancelled_by_provider == 0)) {
            if (user.current_trip_id) {
                user.current_trip_id = null;
                user.cancelled_request = user.cancelled_request + 1;
                await User.updateOne({ _id: user._id }, user.getChanges())
            }
            utils.update_request_status_socket(trip._id);
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }
            res.json({
                success: true,
                payment_status: trip.payment_status,
                message: success_messages.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED
            });
            console.log('Line no 2424');
            return;
        }

        let trip_type = trip.trip_type;
        let providerID = trip.confirmed_provider;
        let status = trip.is_provider_status;
        let complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
        let complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
        trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
        trip.complete_date_tag = complete_date_tag;
        trip.payment_status = PAYMENT_STATUS.COMPLETED;
        trip.provider_trip_end_time = new Date();

        let provider_list = await Provider
            .find({ _id: trip.current_providers })
            .select({
                is_near_trip: 1,
                is_near_available: 1,
                is_trip: 1,
                is_available: 1,
                bids: 1,
                is_ride_share: 1,
                destinationLocation: 1
            })
        for (let provider of provider_list) {
            provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)
            if (!provider.is_near_trip) { provider.is_near_trip = [] }
            if (provider.is_near_trip.length != 0) {
                provider.is_near_available = 1;
                provider.is_near_trip = [];
            }

            if(provider.bids && provider.bids.length > 0) {
                provider.bids = provider.bids.filter(function(bid) { return (bid.trip_id).toString() !== (trip._id).toString(); });
                provider.markModified('bids');
            }
            await Provider.updateOne({ _id: provider._id }, provider.getChanges())
        }
        let trip_user_type = TYPE_VALUE.USER
        switch (req.body.type) {
            case constant_json.TRIP_TYPE_CORPORATE:
                trip_user_type = TYPE_VALUE.CORPORATE
                break;

            case constant_json.TRIP_TYPE_DISPATCHER:
                trip_user_type = TYPE_VALUE.DISPATCHER
                break;

            case constant_json.TRIP_TYPE_HOTEL:
                trip_user_type = TYPE_VALUE.HOTEL
                break;

            default:
                trip_user_type = TYPE_VALUE.USER
                break;
        }
        if (trip_user_type != TYPE_VALUE.USER) {

           await utils.remove_trip_promo_code(trip)

        }

        if (status == 0) {
            trip.cancel_reason = cancel_reason;
            trip.is_trip_cancelled = 1;
            trip.is_trip_cancelled_by_user = 1;

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            user.current_trip_id = null;
            user.cancelled_request = user.cancelled_request + 1;

            await User.updateOne({ _id: user._id }, user.getChanges())

            utils.update_request_status_socket(trip._id);
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }

            await utils.move_trip_to_completed(req.body.trip_id)

            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY
            });
            return;
        }

        trip.cancel_reason = cancel_reason;
        trip.is_trip_cancelled = 1;

     
        trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, trip_user_type )

        if (trip.is_provider_accepted == constant_json.YES) {
            trip.is_trip_cancelled_by_user = 1;
        }

        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        let provider = await Provider.findOne({ _id: providerID })
        if (provider) {
            utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_USER, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
            provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)

            await Provider.updateOne({ _id: provider._id }, provider.getChanges())
        }

        if (status != 4) {
            trip.provider_service_fees = 0;

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            user.cancelled_request = user.cancelled_request + 1;
            user.current_trip_id = null;
            await User.updateOne({ _id: user._id }, user.getChanges())

            utils.update_request_status_socket(trip._id);
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }

            await utils.move_trip_to_completed(req.body.trip_id)

            res.json({
                success: true,
                payment_status: trip.payment_status,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY
            });
            return;
        }

        let tripservice_data = await Trip_Service.findOne({ _id: trip.trip_service_city_type_id })
        let cancellationCharges = tripservice_data.cancellation_fee;
        if (trip.car_rental_id) {
            let rental_main_citytype = await Citytype.findOne({ car_rental_ids: trip.car_rental_id });
            if (rental_main_citytype) {
                cancellationCharges = rental_main_citytype.cancellation_fee
            }
        }
        let provider_profit = tripservice_data.provider_profit;
        trip.is_cancellation_fee = 1;
        let current_rate = 1;

        if (cancellationCharges <= 0) {
            trip.provider_service_fees = 0;

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            user.cancelled_request = user.cancelled_request + 1;
            user.current_trip_id = null;

            await User.updateOne({ _id: user._id }, user.getChanges())

            utils.update_request_status_socket(trip._id);
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }

            await utils.move_trip_to_completed(req.body.trip_id)

            res.json({
                success: true,
                payment_status: trip.payment_status,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY
            });
            return;
        }

        let admin_currencycode = setting_detail.adminCurrencyCode;
        let admin_currency = setting_detail.adminCurrency;
        let countryCurrencyCode = trip.currencycode;
        let city = await City.findOne({ _id: trip.city_id })
        let is_provider_earning_set_in_wallet_on_other_payment = false;
        let is_provider_earning_set_in_wallet_on_cash_payment = false;
        if (city) {
            is_provider_earning_set_in_wallet_on_other_payment = city.is_provider_earning_set_in_wallet_on_other_payment;
            is_provider_earning_set_in_wallet_on_cash_payment = city.is_provider_earning_set_in_wallet_on_cash_payment;
        }

        let currency_response = await utils.getCurrencyConvertRateAsync(1, countryCurrencyCode, admin_currencycode)
        if (currency_response.success) {
            current_rate = currency_response.current_rate;
        } else {
            current_rate = 1;
        }

        let provider_service_fees = 0;
        let total_in_admin_currency = 0;
        let service_total_in_admin_currency = 0;
        let provider_service_fees_in_admin_currency = 0;

        provider_service_fees = cancellationCharges * provider_profit * 0.01;
        provider_service_fees_in_admin_currency = provider_service_fees * current_rate;

        total_in_admin_currency = cancellationCharges * current_rate;
        service_total_in_admin_currency = cancellationCharges * current_rate;

        trip.total_service_fees = cancellationCharges;
        trip.total = cancellationCharges;
        trip.provider_service_fees = (provider_service_fees).toFixed(2);
        trip.pay_to_provider = trip.provider_service_fees
        trip.total_in_admin_currency = total_in_admin_currency;
        trip.service_total_in_admin_currency = service_total_in_admin_currency;
        trip.provider_service_fees_in_admin_currency = provider_service_fees_in_admin_currency;
        trip.current_rate = current_rate;
        trip.payment_status = PAYMENT_STATUS.WAITING;

        trip.admin_currency = admin_currency;
        trip.admin_currencycode = admin_currencycode;


        let user_id = req.body.user_id;
        if (trip_type == constant_json.TRIP_TYPE_CORPORATE) {
            user_id = trip.user_type_id;
        }

        if (trip.payment_mode == Number(constant_json.PAYMENT_MODE_CASH) && is_provider_earning_set_in_wallet_on_cash_payment) {
            let provider = await Provider.findOne({ _id: providerID })
            if (provider) {
                if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                    let total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                        provider.wallet_currency_code, trip.currencycode,
                        1, trip.pay_to_provider, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Profit Of This Trip : " + trip.unique_id);
                    provider.wallet = total_wallet_amount;
                    await Provider.updateOne({ _id: provider._id }, provider.getChanges())
                } else {
                    let partner = await Partner.findOne({ _id: provider.provider_type_id })
                    let total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                        partner.wallet_currency_code, trip.currencycode,
                        1, trip.pay_to_provider, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Profit Of This Trip : " + trip.unique_id);
                    partner.wallet = total_wallet_amount;
                    await Partner.updateOne({ _id: partner._id }, partner.getChanges())
                }

                trip.is_provider_earning_set_in_wallet = true;
                if(trip.pay_to_provider>=0){
                    trip.is_provider_earning_added_in_wallet = true;
                } else {
                    trip.is_provider_earning_added_in_wallet = false;
                }
                trip.provider_income_set_in_wallet = Math.abs(trip.pay_to_provider);
            }
        }

        trip.remaining_payment = cancellationCharges
        if (trip.payment_mode == constant_json.PAYMENT_MODE_APPLE_PAY) {
            trip.payment_status = PAYMENT_STATUS.FAILED;

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())
            utils.update_request_status_socket(trip._id);
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
                payment_status: trip.payment_status,
            });
            return;
        }

        if (trip.payment_mode == constant_json.PAYMENT_MODE_CASH) {
            let card_detail = await Card.findOne({ user_id: user._id, payment_gateway_type: trip.payment_gateway_type, is_default: true })
            if (card_detail) {
                let wallet_amount = user.wallet
                let total_after_wallet_payment = trip.total
                let wallet_payment = 0
                let remaining_payment = 0
                let is_use_wallet = user.is_use_wallet;

                if (wallet_amount > 0 && total_after_wallet_payment > 0 && is_use_wallet == constant_json.YES) {
                    if (wallet_amount > total_after_wallet_payment) {
                        wallet_payment = total_after_wallet_payment;
                    } else {
                        wallet_payment = wallet_amount;
                    }
                    if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                        let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, corporate.unique_id, corporate._id, null,
                            corporate.wallet_currency_code, trip.currencycode,
                            trip.wallet_current_rate, wallet_payment, corporate.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                        corporate.wallet = total_wallet_amount;
                        await Corporate.updateOne({ _id: corporate._id }, corporate.getChanges())
                        user.corporate_wallet_limit = user.corporate_wallet_limit - wallet_payment;
                    } else {
                        let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, null,
                            user.wallet_currency_code, trip.currencycode,
                            1, wallet_payment, user.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                        user.wallet = total_wallet_amount;
                    }
                    total_after_wallet_payment = total_after_wallet_payment - wallet_payment;
                } else {
                    wallet_payment = 0;
                }

                trip.payment_mode = constant_json.PAYMENT_MODE_CARD;
                total_after_wallet_payment = Number((total_after_wallet_payment).toFixed(2));
                wallet_payment = Number((wallet_payment).toFixed(2));
                remaining_payment = trip.total - wallet_payment;
                remaining_payment = Number((remaining_payment).toFixed(2));
                trip.wallet_payment = wallet_payment;
                trip.total_after_wallet_payment = total_after_wallet_payment;
                trip.remaining_payment = remaining_payment;

                // when payment not remaining
                if (total_after_wallet_payment <= 0) {
                    trip.payment_status = PAYMENT_STATUS.COMPLETED;
                    trip.is_paid = 1;
                    trip.is_pending_payments = 0;
                    trip.card_payment = 0;
                    await Trip.updateOne({ _id: trip._id }, trip.getChanges())

                    user.cancelled_request = user.cancelled_request + 1;
                    user.current_trip_id = null;
                    await User.updateOne({ _id: user._id }, user.getChanges());

                    utils.update_request_status_socket(trip._id)
                    if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                        utils.get_service_id_socket(trip.user_type_id)
                    }

                    await utils.move_trip_to_completed(req.body.trip_id)
                    res.json({
                        success: true,
                        message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
                        payment_status: trip.payment_status,
                    });
                    return;
                }
                // when payment remaining
                trip.payment_mode = constant_json.PAYMENT_MODE_CARD;
                trip.payment_status = PAYMENT_STATUS.FAILED;
                await Trip.updateOne({ _id: trip._id }, trip.getChanges())
                await User.updateOne({ _id: user._id }, user.getChanges());

                utils.update_request_status_socket(trip._id);
                if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                    utils.get_service_id_socket(trip.user_type_id)
                }
                res.json({
                    success: true,
                    message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
                    payment_status: trip.payment_status,
                });
                return;
            }

            console.log('no card found deduct money from wallet')
            let wallet_amount = user.wallet
            let total_after_wallet_payment = trip.total
            let wallet_payment = 0
            let remaining_payment = 0

            if (total_after_wallet_payment > 0) {
                wallet_payment = total_after_wallet_payment
                if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                    let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, corporate.unique_id, corporate._id, null,
                        corporate.wallet_currency_code, trip.currencycode,
                        trip.wallet_current_rate, wallet_payment, corporate.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                    corporate.wallet = total_wallet_amount;
                    await Corporate.updateOne({ _id: corporate._id }, corporate.getChanges())
                    user.corporate_wallet_limit = user.corporate_wallet_limit - wallet_payment;
                } else {
                    let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, null,
                        user.wallet_currency_code, trip.currencycode,
                        1, wallet_payment, user.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                    user.wallet = total_wallet_amount;
                }
                total_after_wallet_payment = total_after_wallet_payment - wallet_payment;
            } else {
                wallet_payment = 0;
            }

            trip.payment_mode = constant_json.PAYMENT_MODE_CARD;
            total_after_wallet_payment = Number((total_after_wallet_payment).toFixed(2));
            wallet_payment = Number((wallet_payment).toFixed(2));
            remaining_payment = trip.total - wallet_payment;
            remaining_payment = Number((remaining_payment).toFixed(2));
            trip.wallet_payment = wallet_payment;
            trip.total_after_wallet_payment = total_after_wallet_payment;
            trip.remaining_payment = remaining_payment;
            trip.payment_status = PAYMENT_STATUS.COMPLETED;
            trip.is_paid = 1;
            trip.is_pending_payments = 0;
            trip.card_payment = 0;

            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            user.cancelled_request = user.cancelled_request + 1;
            user.current_trip_id = null;
            await User.updateOne({ _id: user._id }, user.getChanges());

            utils.update_request_status_socket(trip._id)
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }

            await utils.move_trip_to_completed(req.body.trip_id)

            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
                payment_status: trip.payment_status,
            });
            return;
        }
        let wallet_amount = user.wallet
        let total_after_wallet_payment = trip.total
        let wallet_payment = 0
        let remaining_payment = 0
        let is_use_wallet = user.is_use_wallet;
        
        if(trip.trip_type == constant_json.TRIP_TYPE_CORPORATE){
            wallet_amount = corporate.wallet
            total_after_wallet_payment = trip.total
            is_use_wallet = constant_json.YES
        }
        if ((wallet_amount > 0 || trip.trip_type == constant_json.TRIP_TYPE_CORPORATE) && total_after_wallet_payment > 0 && is_use_wallet == constant_json.YES) {
            if (wallet_amount > total_after_wallet_payment) {
                wallet_payment = total_after_wallet_payment;
            } else {
                wallet_payment = wallet_amount;
            }
            if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, corporate.unique_id, corporate._id, null,
                    corporate.wallet_currency_code, trip.currencycode,
                    trip.current_rate, wallet_payment, corporate.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                corporate.wallet = total_wallet_amount;
                await Corporate.updateOne({ _id: corporate._id }, corporate.getChanges())

                user.corporate_wallet_limit = user.corporate_wallet_limit - wallet_payment;
            } else {
                let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, null,
                    user.wallet_currency_code, trip.currencycode,
                    1, wallet_payment, user.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                user.wallet = total_wallet_amount;
            }
            total_after_wallet_payment = total_after_wallet_payment - wallet_payment;
        } else {
            wallet_payment = 0;
        }

        trip.payment_mode = constant_json.PAYMENT_MODE_CARD;
        total_after_wallet_payment = Number((total_after_wallet_payment).toFixed(2));
        wallet_payment = Number((wallet_payment).toFixed(2));
        remaining_payment = trip.total - wallet_payment;
        remaining_payment = Number((remaining_payment).toFixed(2));
        trip.wallet_payment = wallet_payment;
        trip.total_after_wallet_payment = total_after_wallet_payment;
        trip.remaining_payment = remaining_payment;
        // when payment not remaining
        if (total_after_wallet_payment <= 0) {
            trip.payment_status = PAYMENT_STATUS.COMPLETED;
            trip.is_paid = 1;
            trip.is_pending_payments = 0;
            trip.card_payment = 0;
            await Trip.updateOne({ _id: trip._id }, trip.getChanges())

            user.cancelled_request = user.cancelled_request + 1;
            user.current_trip_id = null;
            await User.updateOne({ _id: user._id }, user.getChanges());

            utils.update_request_status_socket(trip._id)
            if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                utils.get_service_id_socket(trip.user_type_id)
            }

            await utils.move_trip_to_completed(req.body.trip_id)

            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
                payment_status: trip.payment_status,
            });
            return;
        }

        // when payment remaining
        trip.payment_mode = constant_json.PAYMENT_MODE_CARD;
        trip.payment_status = PAYMENT_STATUS.FAILED;
        await Trip.updateOne({ _id: trip._id }, trip.getChanges())
        await User.updateOne({ _id: user._id }, user.getChanges())

        utils.update_request_status_socket(trip._id);
        if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
            utils.get_service_id_socket(trip.user_type_id)
        }
        await utils.move_trip_to_completed(req.body.trip_id)
        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
            payment_status: trip.payment_status,
        });
        return;
    } catch (err) {
        console.log("exports.trip_cancel_by_user")
        utils.error_response(err, req, res)
    }
};

exports.trip_cancel_by_provider = async function (req, res) {
    try {
        let params_array = [
            { name: 'provider_id', type: 'string' },
            { name: 'trip_id', type: 'string' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let provider = await Provider.findOne({ _id: req.body.provider_id })
        if (!provider) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && provider.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let trip = await Trip.findOne({
            _id: req.body.trip_id,
            is_trip_cancelled: 0,
            is_trip_cancelled_by_provider: 0,
            is_trip_cancelled_by_user: 0
        })
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED });
            return;
        }

        let city_timezone = trip.timezone;
        let cancel_reason = req.body.cancel_reason;
        let user = await User.findOne({ _id: trip.user_id })

        trip.cancel_reason = cancel_reason;
        trip.is_trip_cancelled = 1;
        trip.is_trip_cancelled_by_provider = 1;
        trip.provider_trip_end_time = new Date();

        let complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
        let complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
        trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
        trip.complete_date_tag = complete_date_tag;
        trip.provider_service_fees = 0;

        // Set trip status
        trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, TYPE_VALUE.PROVIDER )

        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        provider.cancelled_request = provider.cancelled_request + 1;
        provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation);

        await Provider.updateOne({ _id: provider._id }, provider.getChanges())

        myAnalytics.insert_daily_provider_analytics(city_timezone, provider._id, TRIP_STATUS.PROVIDER_CANCELLED);
        utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_PROVIDER, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);

        await utils.remove_trip_promo_code(trip)

        user.current_trip_id = null;

        await User.updateOne({ _id: user._id }, user.getChanges())

        utils.update_request_status_socket(trip._id,null,11);
        if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
            utils.get_service_id_socket(trip.user_type_id)
        }
        await utils.move_trip_to_completed(req.body.trip_id)

        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_CANCELLED_SUCCESSFULLY,
            is_trip_cancelled_by_provider: trip.is_trip_cancelled_by_provider
        });
        return;

    } catch (err) {
        console.log("exports.trip_cancel_by_provider")
        utils.error_response(err, req, res)
    }
};

exports.trip_cancel_by_admin = async function (req, res) {
    try {
        let params_array = [{ name: 'trip_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let trip = await Trip.findOne({
            _id: req.body.trip_id,
            is_trip_cancelled: 0,
            is_trip_cancelled_by_provider: 0,
            is_trip_cancelled_by_user: 0
        })
        if (!trip) {
            res.json({ success: false });
            return;
        }

        let provider = await Provider.findOne({ _id: trip.confirmed_provider })
        let user = await User.findOne({ _id: trip.user_id })
        if (provider) {
            provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)
            if (!provider.is_near_trip) { provider.is_near_trip = [] }
            if ((String(provider.is_near_trip[0]) == String(trip._id))) {
                provider.is_near_available = 1;
                provider.is_near_trip = [];
            }
            await Provider.updateOne({ _id: provider._id }, provider.getChanges())
            utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
        }

        let provider_list = await Provider
            .find({ _id: trip.current_providers })
            .select({
                is_near_trip: 1,
                is_trip: 1,
                is_available: 1,
                is_ride_share: 1,
                destinationLocation: 1,
                bids:1
            })
        for (let provider of provider_list) {
            let is_trip_condition = { _id: provider._id };
            let is_trip_update = {};
            provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)
            if (!provider.is_near_trip) { provider.is_near_trip = [] }
            if (provider.is_near_trip.length != 0) {
                is_trip_update = { is_near_available: 1, is_near_trip: [] };
            }
            provider.bids = provider.bids.filter((details) => details.trip_id.toString() != trip._id);
            await Provider.updateOne(is_trip_condition, {is_trip_update,bids:provider.bids});
        }

        trip.cancel_reason = '';
        trip.is_trip_cancelled = 1;
        trip.payment_status = 1
        trip.provider_trip_end_time = new Date();
        let complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
        let complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
        trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
        trip.complete_date_tag = complete_date_tag;

        // Set trip status
        trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, TYPE_VALUE.ADMIN, req.headers.username, req.headers.admin_id )

        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);
        await utils.remove_trip_promo_code(trip)

        if (String(trip._id) == String(user.current_trip_id)) {
            user.current_trip_id = null;
            await User.updateOne({ _id: user._id }, user.getChanges())
        }

        message = admin_messages.success_message_trip_cancelled;
        utils.update_request_status_socket(trip._id);

        await utils.move_trip_to_completed(req.body.trip_id)

        res.json({ success: true, message: message });
        return;
    } catch (err) {
        console.log("exports.trip_cancel_by_admin")
        utils.error_response(err, req, res)
    }
};

exports.scheduled_trip_cancel_by_admin = async function (req, res) {
    try {
        let params_array = [{ name: 'trip_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let trip = await Trip.findOne({
            _id: req.body.trip_id,
            is_trip_cancelled: 0,
            is_trip_cancelled_by_provider: 0,
            is_trip_cancelled_by_user: 0
        })
        if (!trip) {
            res.json({ success: false });
            return;
        }

        let user = await User.findOne({ _id: trip.user_id })
        trip.cancel_reason = '';
        trip.is_trip_cancelled = 1;
        trip.is_trip_cancelled_by_admin = 1;
        trip.payment_status = 1
        trip.provider_trip_end_time = new Date();
        let complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
        let complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
        trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
        trip.complete_date_tag = complete_date_tag;

        // Set trip status
        trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, TYPE_VALUE.ADMIN, req.headers.username, req.headers.admin_id )

        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        await utils.remove_trip_promo_code(trip)


        user.cancelled_request = user.cancelled_request + 1;
        await User.updateOne({ _id: user._id }, user.getChanges())

        message = admin_messages.success_message_trip_cancelled;
        utils.update_request_status_socket(trip._id);

        await utils.move_trip_to_completed(req.body.trip_id)

        res.json({ success: true,message:message });
        return;
    } catch (err) {
        console.log("exports.scheduled_trip_cancel_by_admin")
        utils.error_response(err, req, res)
    }
};

exports.provider_set_trip_status = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [
            { name: 'provider_id', type: 'string' },
            { name: 'trip_id', type: 'string' },
            { name: 'is_provider_status', type: 'number' },
            { name: 'latitude', type: 'number' },
            { name: 'longitude', type: 'number' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let provider = await Provider.findOne({ _id: req.body.provider_id });
        if (!provider) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && provider.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let trip = await Trip.findOne({ _id: req.body.trip_id, confirmed_provider: req.body.provider_id });
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP_FOUND });
            return;
        }

        if (!(trip.is_trip_cancelled == 0 && trip.is_trip_cancelled_by_provider == 0)) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_MISMATCH_PROVIDER_ID_OR_TRIP_ID });
            return;
        }

        let is_provider_status = Number(req.body.is_provider_status);
        let timeline_status = is_provider_status;
        let now = new Date();
        if (is_provider_status == PROVIDER_STATUS.TRIP_STARTED) {
            if (trip.actual_destination_addresses.length == 0) {
                trip.provider_trip_start_time = now;
            }

            if (trip.is_otp_verification && trip.actual_destination_addresses.length == 0 && req.body.user_type != TYPE_VALUE.ADMIN) {
                let confirmation_code = req.body.trip_start_otp;
                if (trip.confirmation_code != confirmation_code) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TRIP_START_OTP });
                    return;
                }
            }

            if (trip.destination_addresses.length != 0) {
                let provider_arrived_time = trip.provider_arrived_time
                if (trip.actual_destination_addresses.length != 0) {
                    provider_arrived_time = trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].arrived_time;
                }
                let waiting_time = utils.getTimeDifferenceInMinute(now, provider_arrived_time);
                if (waiting_time < 0) {
                    waiting_time = 0;
                }
                trip.actual_destination_addresses.push({
                    address: '',
                    location: [],
                    arrived_time: null,
                    start_time: new Date(),
                    total_time: 0,
                    waiting_time: Number(waiting_time)
                });
                trip.markModified('actual_destination_addresses');
            }
        }

        if (is_provider_status == is_provider_status == PROVIDER_STATUS.ARRIVED) {
            trip.provider_arrived_time = now;
        }
        
        if(is_provider_status == PROVIDER_STATUS.TRIP_STARTED){
            timeline_status = TRIP_STATUS_TIMELIME.TRIP_STARTED;
        } else if(is_provider_status == PROVIDER_STATUS.ARRIVED){
            timeline_status = TRIP_STATUS_TIMELIME.ARRIVED;
        } else {
            timeline_status = is_provider_status;
        }

        trip.is_provider_status = is_provider_status;

        // Set trip status
        if(req.body.user_type && req.body.user_type == TYPE_VALUE.ADMIN){
            trip.trip_status = await utils.addTripStatusTimeline(trip, timeline_status, TYPE_VALUE.ADMIN, req.headers.username )
        }else{
            trip.trip_status = await utils.addTripStatusTimeline(trip, timeline_status, TYPE_VALUE.PROVIDER )
        }

        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        if (trip.actual_destination_addresses && trip.actual_destination_addresses.length > 1) {
            utils.update_request_status_socket(trip._id);
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_SET_TRIP_STATUS_SUCCESSFULLY,
                trip: trip
            });
            return;
        }

        let tripLocation = await TripLocation.findOne({ tripID: req.body.trip_id })
        let latlong = [0, 0];
        latlong = [Number(req.body.latitude), Number(req.body.longitude)];
        switch (is_provider_status) {
            case 2:
                tripLocation.providerStartTime = now;
                tripLocation.providerStartLocation = latlong;
                tripLocation.providerStartToStartTripLocations.push(latlong);
                break;
            case 6:
                tripLocation.startTripTime = now;
                tripLocation.startTripLocation = latlong;
                tripLocation.startTripToEndTripLocations.push(latlong);
                break;
        }
        await TripLocation.updateOne({ _id: tripLocation._id }, tripLocation.getChanges());

        let user = await User.findOne({ _id: trip.user_id })
        let device_token = user.device_token;
        let device_type = user.device_type;

        if (is_provider_status == PROVIDER_STATUS.TRIP_STARTED) {
            if (setting_detail.sms_notification) {
            let emergencyContactDetails = await EmergencyContactDetail.find({ user_id: trip.user_id, is_always_share_ride_detail: 1 })
            emergencyContactDetails.forEach((emergencyContactDetail) => {
                let phoneWithCode = emergencyContactDetail.phone;
                    utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, SMS_TEMPLATE.START_RIDE, [user.first_name + " " + user.last_name, provider.first_name + " " + provider.last_name, trip.source_address, trip.destination_address]);
                });
                utils.sendSmsForOTPVerificationAndForgotPassword(user.country_phone_code + user.phone, SMS_TEMPLATE.START_RIDE, [user.first_name + " " + user.last_name, provider.first_name + " " + provider.last_name, trip.source_address, trip.destination_address]);
            }
        }

        let message_code;
        let providerStatusCase = trip.is_provider_status;
        switch (providerStatusCase) {
            case 2:
                message_code = push_messages.PUSH_CODE_FOR_PROVIDER_COMMING_YOUR_LOCATION;
                break;
            case 4:
                message_code = push_messages.PUSH_CODE_FOR_PROVIDER_ARRIVED;
                break;
            case 6:
                message_code = push_messages.PUSH_CODE_FOR_YOUR_TRIP_STARTED;
                break;
        }
        if (message_code) {
            utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, device_type, device_token, message_code, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);
        }
        console.log('update_request_status_socket')
        utils.update_request_status_socket(trip._id,null,trip.is_provider_status);
        if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
            utils.get_service_id_socket(trip.user_type_id)
        }

        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_SET_TRIP_STATUS_SUCCESSFULLY,
            trip: trip
        });
        return;
    } catch (err) {
        console.log("exports.provider_set_trip_status")
        utils.error_response(err, req, res)
    }
};


exports.check_destination = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            var geo = false;
            var geo2 = false
            var zone1, zone2, k = 0;
            Provider.findOne({ _id: req.body.provider_id }).then((provider) => {
                if (provider) {
                    if (req.body.token != null && provider.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        let Table
                        if (req.body.is_open_ride) {
                            Table = OpenRide
                        } else {
                            Table = Trip
                        }
                        Table.findOne({ _id: req.body.trip_id }).then((trip) => {
                            if (trip) {
                                Citytype.findOne({ _id: trip.service_type_id }).then((citytype) => {
                                    if (citytype) {
                                        City.findOne({ _id: citytype.cityid, zone_business: true }).then((city) => {
                                            if (trip.destination_addresses && trip.destination_addresses.length > 0) {
                                                res.json({ success: true })
                                            } else if (city) {
                                                CityZone.find({ cityid: citytype.cityid }).then((cityzone) => {
                                                    if (citytype.is_zone == 1 && cityzone !== null && cityzone.length > 0) {

                                                        var zone_count = cityzone.length;
                                                        cityzone.forEach(function (cityzoneDetail) {

                                                            geo = geolib.isPointInside(
                                                                {
                                                                    latitude: trip.sourceLocation[0],
                                                                    longitude: trip.sourceLocation[1]
                                                                },
                                                                cityzoneDetail.kmlzone
                                                            );
                                                            geo2 = geolib.isPointInside(
                                                                {
                                                                    latitude: req.body.latitude,
                                                                    longitude: req.body.longitude
                                                                },
                                                                cityzoneDetail.kmlzone
                                                            );

                                                            if (geo) {
                                                                zone1 = cityzoneDetail.id;
                                                            }
                                                            if (geo2) {
                                                                zone2 = cityzoneDetail.id;
                                                            }
                                                            k++;

                                                            if (k == zone_count) {

                                                                ZoneValue.findOne({
                                                                    service_type_id: trip.service_type_id,
                                                                    $or: [{ from: zone1, to: zone2 }, {
                                                                        from: zone2,
                                                                        to: zone1
                                                                    }]
                                                                }).then((zonevalue) => {
                                                                    if (zonevalue) {

                                                                        trip.trip_type = constant_json.TRIP_TYPE_ZONE;
                                                                        trip.trip_type_amount = (zonevalue.amount).toFixed(2);
                                                                        trip.save(function () {
                                                                            res.json({ success: true, zone: '' });
                                                                        });
                                                                    } else {
                                                                        airport(citytype.cityid, citytype, trip, req.body, res);
                                                                    }
                                                                })
                                                            }
                                                        });
                                                    } else {
                                                        airport(citytype.cityid, citytype, trip, req.body, res);
                                                    }
                                                });
                                            } else {
                                                airport(citytype.cityid, citytype, trip, req.body, res);
                                            }
                                        }, (err) => {
                                            console.log(err);
                                            res.json({
                                                success: false,
                                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                                            });
                                        });
                                    } else {
                                        res.json({
                                            success: false,
                                            error_code: error_message.ERROR_CODE_NO_CITY_LIST_FOUND
                                        });
                                    }
                                }, (err) => {
                                    console.log(err);
                                    res.json({
                                        success: false,
                                        error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                                    });
                                });
                            } else {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
                            }
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });
                        });
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
}

function airport(cityid, citytype, trip, body, res) {
    var airport;
    Airport.find({ city_id: cityid }).then((airport_data) => {

        if (airport_data != null && airport_data.length > 0) {
            var k = 0;
            City.findOne({ '_id': cityid, airport_business: true }).then((city) => {
                if (city) {
                    airport_data.forEach(function (airportDetail) {

                        if (airport == undefined) {

                            var pickup_airport = geolib.isPointInside(
                                {
                                    latitude: trip.sourceLocation[0],
                                    longitude: trip.sourceLocation[1]
                                },
                                airportDetail.kmlzone
                            );

                            var dest_airport = geolib.isPointInside(
                                {
                                    latitude: body.latitude,
                                    longitude: body.longitude
                                },
                                airportDetail.kmlzone
                            );

                            if (pickup_airport) {
                                city_distance = utils.getDistanceFromTwoLocation([body.latitude, body.longitude], city.cityLatLong);
                                if (city_distance < city.cityRadius) {

                                    AirportCity.findOne({
                                        airport_id: airportDetail._id,
                                        service_type_id: citytype._id
                                    }).then((airportcity) => {

                                        if (airportcity !== null && airportcity.price > 0) {
                                            airport = airportDetail._id;
                                            trip.trip_type = constant_json.TRIP_TYPE_AIRPORT;
                                            trip.trip_type_amount = (airportcity.price).toFixed(2);
                                            trip.save().then(() => {
                                                res.json({ success: true, airport: '' });
                                            });
                                        } else if (airport_data.length - 1 == k) {
                                            cityCheck(cityid, citytype, trip, body, res)
                                        } else {
                                            k++;
                                        }
                                    })
                                } else if (airport_data.length - 1 == k) {
                                    cityCheck(cityid, citytype, trip, body, res)
                                } else {
                                    k++;
                                }
                            } else if (dest_airport) {
                                city_distance = utils.getDistanceFromTwoLocation(trip.sourceLocation, city.cityLatLong);
                                if (city_distance < city.cityRadius) {


                                    AirportCity.findOne({
                                        airport_id: airportDetail._id,
                                        service_type_id: citytype._id
                                    }).then((airportcity) => {

                                        if (airportcity !== null && airportcity.price > 0) {
                                            airport = airportDetail._id;
                                            trip.trip_type = constant_json.TRIP_TYPE_AIRPORT;
                                            trip.trip_type_amount = (airportcity.price).toFixed(2);
                                            trip.save().then(() => {
                                                res.json({ success: true, airport: '' });
                                            });
                                        } else if (airport_data.length - 1 == k) {
                                            cityCheck(cityid, citytype, trip, body, res)
                                        } else {
                                            k++;
                                        }
                                    })
                                } else if (airport_data.length - 1 == k) {
                                    cityCheck(cityid, citytype, trip, body, res)
                                } else {
                                    k++;
                                }
                            } else if (airport_data.length - 1 == k && airport == undefined) {
                                cityCheck(cityid, citytype, trip, body, res)
                            } else {
                                k++;
                            }
                        }
                    });
                } else {
                    cityCheck(cityid, citytype, trip, body, res)
                }
            }, () => {
                cityCheck(cityid, citytype, trip, body, res)
            })
        } else {
            cityCheck(cityid, citytype, trip, body, res)
        }
    }, () => {
        cityCheck(cityid, citytype, trip, body, res)
    });

}

function cityCheck(cityid, citytype, trip, body, res) {

    var flag = 0;
    var k = 0;
    City.findOne({ '_id': cityid, city_business: true }).then((city) => {
        if (city) {
            CitytoCity.find({ city_id: cityid, service_type_id: citytype._id, destination_city_id: { $in: city.destination_city } }).then((citytocity) => {


                if (citytocity !== null && citytocity.length > 0) {

                    citytocity.forEach(function (citytocity_detail) {

                        City.findById(citytocity_detail.destination_city_id).then((city_detail) => {
                            if (flag == 0) {
                                var city_radius = city_detail.cityRadius;
                                var destination_city_radius = utils.getDistanceFromTwoLocation([body.latitude, body.longitude], city_detail.cityLatLong);

                                var inside_city;
                                if (city_detail.city_locations && city_detail.city_locations.length > 2) {
                                    inside_city = geolib.isPointInside(
                                        {
                                            latitude: body.latitude,
                                            longitude: body.longitude
                                        },
                                        city_detail.city_locations
                                    );
                                }

                                if (citytocity_detail.price > 0 && ((!city_detail.is_use_city_boundary && city_radius > destination_city_radius) || (city_detail.is_use_city_boundary && inside_city))) {

                                    trip.trip_type = constant_json.TRIP_TYPE_CITY;
                                    trip.trip_type_amount = (citytocity_detail.price).toFixed(2);
                                    flag = 1;
                                    trip.save().then(() => {
                                        res.json({ success: true, city: '' })
                                    });
                                } else if (citytocity.length - 1 == k) {
                                    res.json({ success: true })
                                } else {
                                    k++;
                                }
                            }
                        });
                    });
                } else {
                    res.json({ success: true })
                }
            });
        } else {
            res.json({ success: true })
        }
    }, (err) => {
        console.log(err);
        res.json({
            success: false,
            error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
        });
    });
}

exports.provider_complete_trip = async function (req, res) {
    const setting_detail = await Settings.findOne({});
    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }, { name: 'provider_id', type: 'string' }], async function (response) {
        if (response.success) {

            let provider = await Provider.findOne({ _id: req.body.provider_id })
            if(!provider){
                return res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            }

            if (req.body.token != null && provider.token != req.body.token) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            } 

            let trip = await Trip.findOne({
                _id: req.body.trip_id,
                confirmed_provider: req.body.provider_id,
                is_trip_completed: 0,
                is_trip_end: 0
            })
            if(!trip){
                res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP_FOUND });
            }
            if (trip.is_trip_cancelled != 0 || trip.is_trip_cancelled_by_user != 0 || trip.is_trip_cancelled_by_provider != 0) {
                utils.update_request_status_socket(trip._id);
                return res.json({
                    success: true,
                    message: success_messages.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED
                });
            }
            var city_timezone = trip.timezone;
            let user = await User.findOne({ _id: trip.user_id })

            if(req.body.complete_by_admin){
                var distance = req.body.total_distance;
                var distanceKmMile = distance;
                if (trip.unit == 1) {
                    distanceKmMile = distance * 0.001;
                } else {
                    distanceKmMile = distance * 0.000621371;
                }
                trip.total_distance = distanceKmMile
            }

            var total_distance = Number((trip.total_distance).toFixed(2));
            var total_time = Number((trip.total_time).toFixed(2));
            var total_waiting_time = 0;
            var total_stop_waiting_time = 0;
            var distance_cost = 0;
            var time_cost = 0;
            var waiting_time_cost = 0;
            var stop_waiting_time_cost = 0;
            var total_service_fees = 0;
            var tax_fee = 0;
            var provider_tax_fee = 0;
            var total_after_tax_fees = 0;
            var surge_fee = 0;
            var total_after_surge_fees = 0;
            var promo_payment = 0;
            var total_after_promo_payment = 0;

            var promo_value = 0;

            var total = 0;
            var is_min_fare_used = 0;
            var user_tax_fee = 0;
            var is_surge_hours = trip.is_surge_hours;
            var total_time_end = 0;
            var now = new Date();
            var dateNow = new Date();
            total_time_end = utils.getTimeDifferenceInMinute(dateNow, trip.provider_trip_start_time);
            if (total_time_end > total_time) {
                total_time = total_time_end;
            }

            if (total_time < 0) {
                total_time = 0;
            }
            trip.total_time = total_time;

            total_waiting_time = utils.getTimeDifferenceInMinute(trip.provider_trip_start_time, trip.provider_arrived_time);
            if (total_waiting_time < 0) {
                total_waiting_time = 0;
            }

            let tripLocation = await TripLocation.findOne({ tripID: req.body.trip_id })
            tripLocation.endTripTime = now;

            var total_distance_diff = 0;
            let start_end_Location = tripLocation.startTripToEndTripLocations;
            if (req.body.location && req.body.location.length > 0) {
                var prev_location = [Number(start_end_Location[0][0]), Number(start_end_Location[0][1])]
                var time = provider.location_updated_time;
                for (var i = 0; i < req.body.location.length; i++) {
                    var location = [Number(req.body.location[i][0]), Number(req.body.location[i][1])];
                    start_end_Location.push(location);
                    var distance_diff = Math.abs(utils.getDistanceFromTwoLocation(prev_location, location))
                    var time_diff = Math.abs(utils.getTimeDifferenceInSecond(new Date(time), new Date(Number(req.body.location[i][2]))));
                    var max_distance = 0.05;
                    // if ((distance_diff < max_distance * time_diff && distance_diff > 0.005) || time_diff == 0) {
                    if ((distance_diff < max_distance * time_diff && distance_diff > 0.005) || (distance_diff < max_distance && time_diff == 0)) {
                        total_distance_diff = total_distance_diff + distance_diff;
                        time = Number(req.body.location[i][2]);
                        prev_location = location;
                    }
                }
                if (trip.unit == 0) { /// 0 = mile
                    total_distance_diff = total_distance_diff * 0.621371;
                }
                trip.total_distance = (+trip.total_distance + +total_distance_diff).toFixed(2);
                total_distance = trip.total_distance;
            }

            if (!req.body.latitude || !req.body.longitude) {
                req.body.latitude = tripLocation.startTripToEndTripLocations[tripLocation.startTripToEndTripLocations.length - 1][0]
                req.body.longitude = tripLocation.startTripToEndTripLocations[tripLocation.startTripToEndTripLocations.length - 1][1]
            }

            start_end_Location.push([req.body.latitude, req.body.longitude]);
            start_end_Location = await utils.removeDuplicateCoordinates(start_end_Location);

            if (!req.body.complete_by_admin) {
                let distanceKmMile;
                let distance = await utils.calculateDistanceFromCoordinates(start_end_Location) * 1000
                if (trip.unit == 1) {
                    distanceKmMile = distance * 0.001;
                } else {
                    distanceKmMile = distance * 0.000621371;
                }
                trip.total_distance = distanceKmMile
                total_distance = trip.total_distance;
            }

            tripLocation.startTripToEndTripLocations = start_end_Location;
            tripLocation.endTripLocation = [req.body.latitude, req.body.longitude];

            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + req.body.latitude + "," + req.body.longitude + "&key=" + setting_detail.web_app_google_key;
            var request_data = require('request');
            if (!req.body.destination_address) {
                request_data(url, function (error, response, body) {
                    if (body.status == 'OK') {
                        req.body.destination_address = body.results[0].formatted_address;
                    }
                });
            }
            tripLocation.googlePathStartLocationToPickUpLocation = "";
            tripLocation.googlePickUpLocationToDestinationLocation = "";
            tripLocation.actual_startTripToEndTripLocations = tripLocation.startTripToEndTripLocations;
            await tripLocation.save();

            if (trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1]) {
                trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].address = req.body.destination_address;
                trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].location = [Number(req.body.latitude), Number(req.body.longitude)];
                trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].arrived_time = now;

                var actual_destination_time_diff = Math.abs(utils.getTimeDifferenceInMinute(now, trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].start_time));
                if (actual_destination_time_diff < 0) {
                    actual_destination_time_diff = 0
                }
                trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].total_time = actual_destination_time_diff;
                trip.markModified('actual_destination_addresses');
            }

            var index = tripLocation.index_for_that_covered_path_in_google;
            var startTripToEndTripLocations = tripLocation.startTripToEndTripLocations;
            var size = startTripToEndTripLocations.length;
            var gap = 95;
            var start_index = index * gap;
            var end_index = size;
            start_index--;
            if (start_index < 0) {
                start_index = 0;
            }
            var locations = [];

            for (; start_index < end_index; start_index++) {
                locations.push(startTripToEndTripLocations[start_index]);
            }

            utils.getSmoothPath(locations, function (getSmoothPathresponse) {

                utils.bendAndSnap(getSmoothPathresponse, locations.length, async function (response) {

                    if (response) {
                        var index = tripLocation.index_for_that_covered_path_in_google;
                        var google_start_trip_to_end_trip_locations = tripLocation.google_start_trip_to_end_trip_locations;
                        google_start_trip_to_end_trip_locations = google_start_trip_to_end_trip_locations.concat(response.temp_array);
                        tripLocation.google_start_trip_to_end_trip_locations = google_start_trip_to_end_trip_locations;
                        var google_total_distance = +tripLocation.google_total_distance + +response.distance;
                        tripLocation.google_total_distance = google_total_distance;
                        index++;
                        tripLocation.index_for_that_covered_path_in_google = index;
                        tripLocation.startTripToEndTripLocations = tripLocation.google_start_trip_to_end_trip_locations;
                        tripLocation.save();

                        var distance_diff = total_distance - google_total_distance;
                        if (distance_diff > 0.5 || distance_diff < -0.5) {
                            total_distance = (google_total_distance).toFixed(2);

                            if (trip.unit == 0) { /// 0 = mile
                                total_distance = total_distance * 0.621371;
                            }

                            trip.total_distance = total_distance;
                        }
                    }

                    let provider = await Provider.findOne({ _id: req.body.provider_id })
                    provider.providerLocation = [Number(req.body.latitude), Number(req.body.longitude)];
                    provider.bearing = req.body.bearing;
                    provider.save();

                    let tripservice = await Trip_Service.findOne({ _id: trip.trip_service_city_type_id })

                    var surge_multiplier = 0;
                    var min_fare = 0;
                    var base_price = 0;
                    var base_price_distance = 0;
                    var tax = 0;
                    var user_miscellaneous_fee = 0;
                    var provider_miscellaneous_fee = 0;
                    var user_tax = 0;
                    var provider_tax = 0;
                    var min_fare = 0;
                    var provider_profit = 0;
                    var price_per_unit_distance = 0;
                    var price_for_total_time = 0;
                    var price_for_waiting_time = 0;
                    var waiting_time_start_after_minute = 0;
                    var price_for_waiting_time_multiple_stops = 0;
                    var waiting_time_start_after_minute_multiple_stops = 0;
                    var base_price_time = 0;
                    var total_after_user_tax_fees = 0;
                    ///////////////// Distance cost and Time cost calculation /////
                    trip.is_provider_status = 9;
                    // DISTANCE CALCULATIONS
                    trip.destination_address = req.body.destination_address;
                    trip.destinationLocation = [req.body.latitude, req.body.longitude];
                    trip.provider_trip_end_time = now;

                    var toll_amount = req.body.toll_amount;

                    if (toll_amount == undefined) {
                        toll_amount = 0;
                    }

                    var trip_type_amount = trip.trip_type_amount;
                    provider_miscellaneous_fee = tripservice.provider_miscellaneous_fee;
                    provider_tax = tripservice.provider_tax;

                    provider_profit = tripservice.provider_profit;

                    tax = tripservice.tax;
                    user_miscellaneous_fee = tripservice.user_miscellaneous_fee;
                    user_tax = tripservice.user_tax;

                    if (trip.is_fixed_fare && trip.fixed_price > 0) {

                        total_after_surge_fees = trip.fixed_price;
                        trip.total_service_fees = total_after_surge_fees;

                        total_after_surge_fees = utils.get_reverse_service_fee(total_after_surge_fees, tax);
                        trip.fixed_price = total_after_surge_fees;
                        tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.tax_fee = tax_fee;
                        total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        user_tax_fee = Number((user_tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.user_tax_fee = user_tax_fee;
                        trip.user_miscellaneous_fee = user_miscellaneous_fee;
                    } else if (trip.trip_type == constant_json.TRIP_TYPE_AIRPORT) {

                        total_after_surge_fees = trip_type_amount;
                        trip.total_service_fees = total_after_surge_fees;

                        total_after_surge_fees = utils.get_reverse_service_fee(total_after_surge_fees, tax)
                        trip.fixed_price = total_after_surge_fees;
                        tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.tax_fee = tax_fee;
                        total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        user_tax_fee = Number((user_tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.user_tax_fee = user_tax_fee;
                        trip.user_miscellaneous_fee = user_miscellaneous_fee;
                    } else if (trip.trip_type == constant_json.TRIP_TYPE_ZONE) {
                        console.log('zone trip')

                        total_after_surge_fees = trip_type_amount;
                        trip.total_service_fees = total_after_surge_fees;
                        total_after_surge_fees = utils.get_reverse_service_fee(total_after_surge_fees, tax)
                        trip.fixed_price = total_after_surge_fees;
                        tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.tax_fee = tax_fee;
                        total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        user_tax_fee = Number((user_tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.user_tax_fee = user_tax_fee;
                        trip.user_miscellaneous_fee = user_miscellaneous_fee;
                    } else if (trip.trip_type == constant_json.TRIP_TYPE_CITY) {

                        total_after_surge_fees = trip_type_amount;
                        trip.total_service_fees = total_after_surge_fees;

                        total_after_surge_fees = utils.get_reverse_service_fee(total_after_surge_fees, tax)
                        trip.fixed_price = total_after_surge_fees;
                        tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.tax_fee = tax_fee;
                        total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        user_tax_fee = Number((user_tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.user_tax_fee = user_tax_fee;
                        trip.user_miscellaneous_fee = user_miscellaneous_fee;
                    } else if (trip.car_rental_id) {
                        if (trip.surge_multiplier) {
                            surge_multiplier = trip.surge_multiplier;
                        }
                        min_fare = tripservice.min_fare;
                        base_price = tripservice.base_price;
                        base_price_distance = tripservice.base_price_distance;

                        price_per_unit_distance = tripservice.price_per_unit_distance;
                        price_for_total_time = tripservice.price_for_total_time;
                        price_for_waiting_time = tripservice.price_for_waiting_time;
                        waiting_time_start_after_minute = tripservice.waiting_time_start_after_minute;
                        if (total_distance <= base_price_distance) {
                            distance_cost = 0;
                        } else {
                            distance_cost = Number(((total_distance - base_price_distance) * price_per_unit_distance).toFixed(2));
                        }

                        trip.distance_cost = distance_cost;

                        // TIME CALCULATIONS
                        // if (total_time > base_price_time) {
                        //     time_cost = (total_time - base_price_time) * price_for_total_time;
                        // }
                        base_price_time = tripservice.base_price_time;
                        if (total_time < base_price_time) {
                            time_cost = 0;
                        } else {
                            time_cost = (total_time - base_price_time) * price_for_total_time;
                        }
                        time_cost = Number((time_cost).toFixed(2));
                        trip.time_cost = time_cost;

                        total_waiting_time = total_waiting_time - waiting_time_start_after_minute;
                        trip.waiting_time_cost = 0;

                        trip.total_waiting_time = total_waiting_time;


                        total_service_fees = +base_price + +distance_cost + +time_cost + +waiting_time_cost;
                        trip.total_service_fees = total_service_fees;
                        if (is_surge_hours == constant_json.YES) {
                            surge_fee = Number((total_service_fees * (surge_multiplier - 1)).toFixed(2));
                            trip.surge_fee = surge_fee;
                            total_after_surge_fees = total_service_fees + surge_fee;
                            total_after_surge_fees = Number((total_after_surge_fees).toFixed(2));
                        } else {
                            surge_fee = 0;
                            trip.surge_fee = surge_fee;
                            total_after_surge_fees = total_service_fees;
                            total_after_surge_fees = Number((total_after_surge_fees).toFixed(2));
                        }

                        tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.tax_fee = tax_fee;
                        total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        if (total_after_tax_fees < min_fare) {
                            total_after_tax_fees = min_fare;
                            is_min_fare_used = 1;

                            total_after_surge_fees = utils.get_reverse_service_fee(min_fare, tax)

                            tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                            trip.tax_fee = tax_fee;
                            total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        }
                        trip.is_min_fare_used = is_min_fare_used;

                        user_tax_fee = Number((user_tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.user_tax_fee = user_tax_fee;
                        trip.user_miscellaneous_fee = user_miscellaneous_fee;
                    } else {
                        // surge_multiplier = tripservice.surge_multiplier;
                        if (trip.surge_multiplier) {
                            surge_multiplier = trip.surge_multiplier;
                        }
                        min_fare = tripservice.min_fare;
                        base_price = tripservice.base_price;
                        base_price_distance = tripservice.base_price_distance;

                        price_per_unit_distance = tripservice.price_per_unit_distance;
                        price_for_total_time = tripservice.price_for_total_time;
                        price_for_waiting_time = tripservice.price_for_waiting_time;
                        waiting_time_start_after_minute = tripservice.waiting_time_start_after_minute;

                        price_for_waiting_time_multiple_stops = tripservice.price_for_waiting_time_multiple_stops ? tripservice.price_for_waiting_time_multiple_stops : 0;
                        waiting_time_start_after_minute_multiple_stops = tripservice.waiting_time_start_after_minute_multiple_stops ? tripservice.waiting_time_start_after_minute_multiple_stops : 0;

                        if (total_distance <= base_price_distance) {
                            distance_cost = 0;
                        } else {
                            distance_cost = Number(((total_distance - base_price_distance) * price_per_unit_distance).toFixed(2));
                        }
                        trip.distance_cost = distance_cost;
                        // TIME CALCULATIONS
                        if (time_cost < 0) {
                            time_cost = 0;
                        }
                        if (trip.actual_destination_addresses) {
                            trip.actual_destination_addresses.forEach(destination_address => {
                                total_time = total_time - destination_address.waiting_time
                            });
                        }
                        if (total_time < 0) {
                            total_time = 0
                        }
                        time_cost = total_time * price_for_total_time;
                        time_cost = Number((time_cost).toFixed(2));
                        trip.time_cost = time_cost;
                        //  WAITING TIME CALCULATIONS
                        total_waiting_time = total_waiting_time - waiting_time_start_after_minute;
                        if (total_waiting_time < 0) {
                            total_waiting_time = 0;
                        }

                        if (total_waiting_time > 0) {

                            waiting_time_cost = Number((total_waiting_time * price_for_waiting_time).toFixed(2));
                        }
                        if (trip.actual_destination_addresses) {
                            if (setting_detail.is_multiple_stop_waiting_free_on_each_stop) {
                                trip.actual_destination_addresses.forEach(destination_address => {
                                    if ((destination_address.waiting_time - waiting_time_start_after_minute_multiple_stops) > 0) {
                                        total_stop_waiting_time += destination_address.waiting_time;
                                    }
                                });
                            } else {
                                trip.actual_destination_addresses.forEach(destination_address => {
                                    total_stop_waiting_time += destination_address.waiting_time;
                                });
                                total_stop_waiting_time = total_stop_waiting_time - waiting_time_start_after_minute_multiple_stops;
                                if (total_stop_waiting_time < 0) {
                                    total_stop_waiting_time = 0;
                                }
                            }
                        }
                        if (total_stop_waiting_time > 0) {
                            stop_waiting_time_cost = Number((total_stop_waiting_time * price_for_waiting_time_multiple_stops).toFixed(2));
                        }

                        trip.waiting_time_cost = waiting_time_cost;
                        trip.total_waiting_time = total_waiting_time;
                        trip.total_stop_waiting_time = total_stop_waiting_time;
                        trip.stop_waiting_time_cost = stop_waiting_time_cost;


                        total_service_fees = +base_price + +distance_cost + +time_cost + +waiting_time_cost + +stop_waiting_time_cost;
                        trip.total_service_fees = total_service_fees;

                        if (is_surge_hours == constant_json.YES) {
                            surge_fee = Number((total_service_fees * (surge_multiplier - 1)).toFixed(2));
                            trip.surge_fee = surge_fee;
                            total_after_surge_fees = total_service_fees + surge_fee;
                            total_after_surge_fees = Number((total_after_surge_fees).toFixed(2));
                        } else {
                            surge_fee = 0;
                            trip.surge_fee = surge_fee;
                            total_after_surge_fees = total_service_fees;
                            total_after_surge_fees = Number((total_after_surge_fees).toFixed(2));
                        }

                        tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.tax_fee = tax_fee;
                        total_after_tax_fees = +total_after_surge_fees + +tax_fee;

                        if (total_after_tax_fees < min_fare) {
                            total_after_tax_fees = min_fare;
                            is_min_fare_used = 1;

                            total_after_surge_fees = utils.get_reverse_service_fee(min_fare, tax)

                            tax_fee = Number((tax * 0.01 * total_after_surge_fees).toFixed(2));
                            trip.tax_fee = tax_fee;
                            total_after_tax_fees = +total_after_surge_fees + +tax_fee;
                        }
                        trip.is_min_fare_used = is_min_fare_used;

                        user_tax_fee = Number((user_tax * 0.01 * total_after_surge_fees).toFixed(2));
                        trip.user_tax_fee = user_tax_fee;
                        trip.user_miscellaneous_fee = user_miscellaneous_fee;
                    }
                    trip.total_after_tax_fees = total_after_tax_fees;
                    trip.total_after_surge_fees = total_after_surge_fees;

                    // Set booking type
                    trip.booking_type = await utils.getTripBookingTypes(trip)

                    // Set trip status
                    trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_COMPLETED, TYPE_VALUE.PROVIDER )
                    
                    ///////////////////////// FOR INVOICE //////////////////////////
                    var current_rate = 1;
                    var countryCurrencyCode = trip.currencycode;
                    var adminCurrencyCode = trip.currencycode;
                    var adminCurrency = trip.currency;

                    adminCurrencyCode = setting_detail.adminCurrencyCode;
                    adminCurrency = setting_detail.adminCurrency;

                    trip.current_rate = current_rate;

                    let promocode = await Promo_Code.findOne({ _id: trip.promo_id })

                    total_after_user_tax_fees = +total_after_tax_fees + +user_miscellaneous_fee + +user_tax_fee;

                    if (trip.promo_id != null && promocode) {
                        var promo_type = promocode.code_type;
                        promo_value = promocode.code_value;
                        if (promo_type == 1) { ///abs
                            promo_payment = promo_value;
                        } else { // perc
                            promo_payment = Number((promo_value * 0.01 * total_after_user_tax_fees).toFixed(2));
                        }


                        total_after_promo_payment = total_after_user_tax_fees - promo_payment;


                        if (total_after_promo_payment < 0) {
                            total_after_promo_payment = 0;
                            promo_payment = total_after_user_tax_fees;
                        }


                        let userpromouse = await User_promo_use.findOne({ trip_id: trip._id })
                        userpromouse.user_used_amount = promo_payment;
                        userpromouse.user_used_amount_in_admin_currency = promo_payment * current_rate;
                        userpromouse.save();
                    } else {
                        promo_payment = 0;
                        total_after_promo_payment = total_after_user_tax_fees;
                    }


                    total_after_promo_payment = Number((total_after_promo_payment).toFixed(2));
                    trip.promo_payment = promo_payment;
                    trip.total_after_promo_payment = total_after_promo_payment;

                    trip.total_after_referral_payment = total_after_promo_payment;
                    ////////ENTRY IN PROVIDER EARNING TABLE ///////////
                    var service_total_in_admin_currency = Number((total_after_user_tax_fees * current_rate).toFixed(3));

                    var provider_profit_fees = Number((total_after_tax_fees * provider_profit * 0.01).toFixed(2));


                    provider_tax_fee = Number((provider_tax * 0.01 * provider_profit_fees).toFixed(2));
                    trip.provider_miscellaneous_fee = provider_miscellaneous_fee;
                    trip.provider_tax_fee = provider_tax_fee;
                    provider_service_fees = +provider_profit_fees + +toll_amount - provider_miscellaneous_fee - provider_tax_fee;

                    var provider_service_fees_in_admin_currency = Number((provider_service_fees * current_rate).toFixed(3));

                    var promo_referral_amount = promo_payment;
                    total = total_after_promo_payment;

                    total = +total + +toll_amount;
                    total = Number((total).toFixed(2));
                    var total_in_admin_currency = Number((total * current_rate).toFixed(3));
                    trip.total_after_user_tax_fees = total_after_user_tax_fees
                    trip.base_distance_cost = base_price;
                    trip.admin_currency = adminCurrency;
                    trip.admin_currencycode = adminCurrencyCode;
                    trip.provider_service_fees = provider_service_fees;
                    trip.provider_profit_fees = provider_profit_fees;
                    trip.total_in_admin_currency = total_in_admin_currency;
                    trip.service_total_in_admin_currency = service_total_in_admin_currency;
                    trip.provider_service_fees_in_admin_currency = provider_service_fees_in_admin_currency;
                    trip.promo_referral_amount = promo_referral_amount;
                    trip.toll_amount = toll_amount;
                    trip.total = total;

                    var wallet_currency_code = user.wallet_currency_code;
                    if (wallet_currency_code == "" || !wallet_currency_code) {
                        wallet_currency_code = setting_detail.adminCurrencyCode;
                    }
                    var wallet_current_rate = 1;

                    trip.wallet_current_rate = wallet_current_rate;
                    let split_payment_users = trip.split_payment_users.filter((split_payment_user_detail) => {
                        if (split_payment_user_detail.status == SPLIT_PAYMENT.ACCEPTED) {
                            return split_payment_user_detail;
                        }
                    });
                    trip.split_payment_users = split_payment_users;
                    trip.split_payment_users.forEach((split_payment_user_detail) => {
                        split_payment_user_detail.total = (total / (trip.split_payment_users.length + 1));
                        if (split_payment_user_detail.payment_mode == constant_json.PAYMENT_MODE_CASH) {
                            split_payment_user_detail.cash_payment = split_payment_user_detail.total;
                            split_payment_user_detail.payment_status = PAYMENT_STATUS.COMPLETED;
                        } else {
                            split_payment_user_detail.remaining_payment = split_payment_user_detail.total
                        }
                    })
                    trip.markModified('split_payment_users');
                    await trip.save()

                    if (trip.is_tip == true) {
                        // utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, device_type, device_token, push_messages.PUSH_CODE_FOR_WAITING_FOR_TIP, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                    }
                    let country = await Country.findById(trip.country_id)
                    myAnalytics.insert_daily_provider_analytics(city_timezone, provider._id, TRIP_STATUS.TRIP_COMPLETED,null, country._id);
                    utils.update_request_status_socket(trip._id,null,trip.is_provider_status);
                    if (trip.trip_type >= constant_json.TRIP_TYPE_DISPATCHER) {
                        utils.get_service_id_socket(trip.user_type_id)
                    }

                    let total_redeem_point = '';
                    if(country?.user_redeem_settings[0]?.is_user_redeem_point_reward_on && (country?.user_redeem_settings[0]?.trip_redeem_point > 0)){
                        total_redeem_point = utils.add_redeem_point_history(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id,country._id,constant_json.TRIP_REDEEM_POINT,user.wallet_currency_code,`Get redeem point via Trips : ${trip.unique_id}`,country.user_redeem_settings[0]?.trip_redeem_point,user.total_redeem_point,constant_json.ADD_REDEEM_POINT)
                        user.total_redeem_point = total_redeem_point
                        await user.save()
                    }
                    if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE || trip.trip_type == constant_json.TRIP_TYPE_HOTEL) {
                        let user = await User.findOne({ _id: trip.user_id })
                        user.current_trip_id = null;
                        user.save();
                    }
                    await utils.generate_admin_profit(trip, user)           

                    res.json({
                        success: true,
                        message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                        trip: trip,
                        tripservice: tripservice
                    });

                });
            });
            
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });

};

exports.pay_payment = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});
        const trip = await Trip.findOne({ _id: req.body.trip_id, is_trip_end: 0 });
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
            return;
        }
        var is_provider_earning_set_in_wallet_on_cash_payment = false;
        const city = await City.findOne({ _id: trip.city_id });
        if (city) {
            is_provider_earning_set_in_wallet_on_cash_payment = city.is_provider_earning_set_in_wallet_on_cash_payment;
        }

        var payment_mode = trip.payment_mode;
        var is_user_need_save = false; // to avoid can't save() the same doc multiple times in parallel.

        const provider = await Provider.findOne({ _id: trip.confirmed_provider });
        const user = await User.findOne({ _id: trip.user_id });

        var user_device_token = user.device_token;
        var user_device_type = user.device_type;

        const corporate = await Corporate.findOne({ _id: trip.user_type_id });

        var countryCurrencyCode = trip.currencycode;
        
        trip.is_trip_end = 1;
        if (trip.user_type == Number(constant_json.USER_TYPE_DISPATCHER) || trip.user_type == Number(constant_json.USER_TYPE_HOTEL) || trip.user_type == Number(constant_json.USER_TYPE_PROVIDER)) {
            trip.is_user_invoice_show = 1;
            user.current_trip_id = null;
            is_user_need_save = true;
        }

        var wallet_amount = Number((Math.max(user.wallet, 0)).toFixed(2) * trip.wallet_current_rate);
        var is_use_wallet = user.is_use_wallet;
        if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
            wallet_amount = Number((Math.max(corporate.wallet, 0)).toFixed(2) * trip.wallet_current_rate);
            is_use_wallet = 1;
        }
        var wallet_payment = 0;
        var remaining_payment = 0;
        var total = trip.total;
        if (trip.split_payment_users.length > 0) {
            total = (trip.total / (trip.split_payment_users.length + 1));
        }
        var total_after_wallet_payment = total;

        if (wallet_amount > 0 && total_after_wallet_payment > 0 && is_use_wallet == constant_json.YES && trip.trip_type != constant_json.TRIP_TYPE_DISPATCHER) {
            if (wallet_amount > total_after_wallet_payment) {
                wallet_payment = total_after_wallet_payment;
            } else {
                wallet_payment = wallet_amount;
            }
            if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, corporate.unique_id, corporate._id, null,
                    corporate.wallet_currency_code, trip.currencycode,
                    trip.wallet_current_rate, wallet_payment, corporate.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                corporate.wallet = total_wallet_amount;
                await corporate.save();
                user.corporate_wallet_limit = user.corporate_wallet_limit - total;
                is_user_need_save = true;
            } else {
                var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, null,
                    user.wallet_currency_code, trip.currencycode,
                    trip.wallet_current_rate, wallet_payment, user.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                user.wallet = total_wallet_amount;
                is_user_need_save = true;
            }

            total_after_wallet_payment = total_after_wallet_payment - wallet_payment;
        }

        if (is_user_need_save) {
            await user.save();
        }

        total_after_wallet_payment = Number((total_after_wallet_payment).toFixed(2));
        wallet_payment = Number((wallet_payment).toFixed(2));
        remaining_payment = total - wallet_payment;
        remaining_payment = Number((remaining_payment).toFixed(2));
        trip.current_rate  = trip.current_rate ? trip.current_rate : 1
        trip.wallet_payment = wallet_payment;
        trip.total_after_wallet_payment = total_after_wallet_payment;
        trip.remaining_payment = remaining_payment;
        trip.payment_status = PAYMENT_STATUS.COMPLETED;
        trip.provider_service_fees = +trip.provider_service_fees;
        trip.total_in_admin_currency = Number(trip.total) * trip.current_rate ;
        trip.provider_service_fees_in_admin_currency = trip.provider_service_fees * trip.current_rate;
        if (trip.payment_mode == Number(constant_json.PAYMENT_MODE_CASH)) {
            // added condition for cash in hand
            trip.provider_have_cash = remaining_payment;
        }

        trip.split_payment_users.forEach(async(split_user) => {
            let split_user_remaining_payment = split_user.remaining_payment;
            if (split_user.payment_mode == Number(constant_json.PAYMENT_MODE_CASH)) {
                trip.provider_have_cash = trip.provider_have_cash + split_user.cash_payment;
                trip.cash_payment = trip.cash_payment + split_user.cash_payment;
            }
            if (split_user.payment_mode == null) {
                trip.wallet_payment = trip.wallet_payment + split_user.remaining_payment;
                split_user.wallet_payment = split_user.remaining_payment;
                split_user.remaining_payment = 0;
                split_user.payment_status = PAYMENT_STATUS.COMPLETED;
            }
            const split_user_detail = await User.findOne({ _id: split_user.user_id });
            let push_data = {
                trip_id: trip._id,
                first_name: user.first_name,
                last_name: user.last_name,
                is_trip_end: trip.is_trip_end,
                currency: trip.currency,
                phone: user.phone,
                country_phone_code: user.country_phone_code,
                user_id: trip.user_id,
                status: split_user.status,
                payment_mode: split_user.payment_mode,
                payment_status: split_user.payment_status,
                payment_intent_id: split_user.payment_intent_id,
                total: split_user.total,
                currency_code: trip.currencycode
            }
            await utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, split_user_detail.device_type, split_user_detail.device_token, push_messages.PUSH_CODE_FOR_YOUR_TRIP_END_SPLIT_PAYMENT, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,push_data,split_user_detail.webpush_config);
            utils.req_type_id_socket(split_user_detail._id)
            if (split_user.payment_mode == null) {

                var total_aplit_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, split_user_detail.unique_id, split_user_detail._id, null,
                    split_user_detail.wallet_currency_code, trip.currencycode,
                    trip.wallet_current_rate, split_user_remaining_payment, split_user_detail.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);

                split_user_detail.wallet = total_aplit_wallet_amount;
                await split_user_detail.save();
            }
        });

        trip.markModified('split_payment_users');

        // trip.cash_payment = trip.provider_have_cash;
        trip.pay_to_provider = (trip.payment_mode == constant_json.PAYMENT_MODE_CASH) ? trip.provider_service_fees - trip.provider_have_cash : trip.provider_service_fees;
        var complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
        var complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
        trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
        trip.complete_date_tag = complete_date_tag;

        var total_wallet_amount = 0;
        if (payment_mode == Number(constant_json.PAYMENT_MODE_CASH) && is_provider_earning_set_in_wallet_on_cash_payment) {
            if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                if (trip.pay_to_provider < 0) {
                    total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                        provider.wallet_currency_code, trip.currencycode,
                        1, Math.abs(trip.pay_to_provider), provider.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
                } else {
                    total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                        provider.wallet_currency_code, trip.currencycode,
                        1, trip.pay_to_provider, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
                }
                provider.wallet = total_wallet_amount;
                await provider.save();
            } else {
                const partner = await Partner.findOne({ _id: trip.provider_type_id });
                if (trip.pay_to_provider < 0) {
                    total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                        partner.wallet_currency_code, trip.currencycode,
                        1, Math.abs(trip.pay_to_provider), partner.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
                } else {
                    total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                        partner.wallet_currency_code, trip.currencycode,
                        1, trip.pay_to_provider, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);

                }
                partner.wallet = total_wallet_amount;
                await partner.save();
            }

            trip.is_provider_earning_set_in_wallet = true;
            trip.is_provider_earning_added_in_wallet = trip.pay_to_provider >= 0;
            trip.provider_income_set_in_wallet = Math.abs(trip.pay_to_provider);
        }

        // End 6 March //

        if (payment_mode == constant_json.PAYMENT_MODE_CASH) {
            trip.is_paid = 1;
            trip.is_pending_payments = 0;
            trip.cash_payment = trip.cash_payment + remaining_payment;
            trip.remaining_payment = 0;
            await trip.save();
            utils.trip_response(req, trip, user, provider, res);
        } else if (payment_mode == constant_json.PAYMENT_MODE_APPLE_PAY) {
            
            if (remaining_payment > 0) {
                trip.is_paid = 0;
                trip.payment_status = PAYMENT_STATUS.FAILED;
                await trip.save();
                utils.trip_response(req, trip, user, provider, res);
            } else {
                trip.is_paid = 1;
                trip.is_pending_payments = 0;
                trip.payment_status = PAYMENT_STATUS.COMPLETED;
                trip.card_payment = 0;
                await trip.save()
                utils.trip_response(req, trip, user, provider, res);
            }

        } else {
            
            if (remaining_payment > 0) {
                var user_id = trip.user_id;
                var email = user.email;
                var customer_id = user.customer_id;
                if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                    user_id = trip.user_type_id;
                    customer_id = corporate.customer_id;
                    email = corporate.email;
                }
                trip.is_paid = 0;
                trip.remaining_payment = remaining_payment;
                trip.payment_status = PAYMENT_STATUS.WAITING;

                const card_detail = await Card.findOne({ user_id: user_id, payment_gateway_type: trip.payment_gateway_type, is_default: true });

                if (!card_detail) {
                    trip.payment_status = PAYMENT_STATUS.FAILED;
                    await trip.save();
                    utils.trip_response(req, trip, user, provider, res);
                }
                if (countryCurrencyCode == "" || !countryCurrencyCode) {
                    countryCurrencyCode = setting_detail.adminCurrencyCode;
                }
                if (trip.payment_gateway_type == PAYMENT_GATEWAY.stripe) {
                    let url = setting_detail.payments_base_url + "/create_payment_intent"
                    let data = {
                        amount: Math.round((remaining_payment * 100)),
                        currency: countryCurrencyCode,
                        customer: customer_id,
                        payment_method: card_detail.payment_method,
                        setup_future_usage: 'off_session',
                        confirm: true
                    }
        
                    const request = require('request');
                    request.post(
                    {
                        url: url,
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify(data),
                    }, async (error, response, body) => {
                        if (error) {
                            console.error(error);
                            return error
                        } else {
                            body = JSON.parse(body);
                            paymentIntent = body.paymentIntent;
        
                            if (paymentIntent && paymentIntent.status == 'succeeded' && paymentIntent.charges && paymentIntent.charges.data && paymentIntent.charges.data.length > 0) {
                                trip.remaining_payment = 0
                                trip.is_paid = 1
                                trip.card_payment = paymentIntent.charges.data[0].amount / 100;
                                trip.payment_intent_id = paymentIntent.id;
                                trip.payment_status = PAYMENT_STATUS.COMPLETED
                                await utils.trip_provider_profit_card_wallet_settlement(trip)
                                await trip.save()
                                utils.trip_response(req, trip, user, provider, res);
                            } else if (paymentIntent && paymentIntent.status == 'requires_action') {
                                trip.payment_intent_id = paymentIntent.id;
                                await trip.save();
                                utils.update_request_status_socket(trip._id);
                                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_device_type, user_device_token, push_messages.PUSH_CODE_FOR_YOUR_TRIP_END, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                return res.json({
                                    success: true,
                                    message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                                    payment_status: trip.payment_status,
                                    payment_method: card_detail.payment_method,
                                    client_secret: paymentIntent.client_secret
                                });
                            } else {
                                utils.trip_payment_failed(trip, city, provider);
                                trip.payment_status = PAYMENT_STATUS.FAILED;
                                await trip.save();
                                utils.update_request_status_socket(trip._id, null, 0, true);
                                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_device_type, user_device_token, push_messages.PUSH_CODE_FOR_YOUR_TRIP_END, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                return res.json({
                                    success: true,
                                    error: error ? error.raw.message : '',
                                    message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                                    payment_status: trip.payment_status
                                });
                            }
                        }
                    });
                } else if(trip.payment_gateway_type == PAYMENT_GATEWAY.paystack)  {
                    const params = JSON.stringify({
                        "email": email,
                        "amount": Math.round((remaining_payment * 100)),
                        // currency : wallet_currency_code,
                        authorization_code: card_detail.payment_method
                    });
                    const options = {
                        hostname: 'api.paystack.co',
                        port: 443,
                        path: '/charge',
                        method: 'POST',
                        headers: {
                            Authorization: 'Bearer ' + setting_detail.paystack_secret_key,
                            'Content-Type': 'application/json'
                        }
                    }
                    const request = https.request(options, res_data => {
                        let data = ''
                        res_data.on('data', (chunk) => {
                            data += chunk
                        });
                        res_data.on('end', async () => {
                            var payment_response = JSON.parse(data);
                            if (payment_response.status) {
                                trip.payment_intent_id = payment_response.data.reference;
                                if (payment_response.data.status == 'success') {

                                    trip.is_paid = 1;
                                    trip.is_pending_payments = 0;
                                    trip.card_payment = 0;
                                    trip.payment_status = PAYMENT_STATUS.COMPLETED;
                                    trip.remaining_payment = 0;
                                    trip.card_payment = payment_response.data.amount / 100;

                                    // start provider profit after card payment done
                                    await utils.trip_provider_profit_card_wallet_settlement(trip);
                                    // end of provider profit after card payment done

                                    await trip.save();
                                    utils.trip_response(req, trip, user, provider, res);

                                } else if (payment_response.data.status == 'open_url') {
                                    trip.payment_status = PAYMENT_STATUS.FAILED;
                                    await trip.save();
                                    utils.update_request_status_socket(trip._id, null, 0, true)
                                    utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_device_type, user_device_token, push_messages.PUSH_CODE_FOR_YOUR_TRIP_END, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                    return res.json({ success: false, url: payment_response.data.url });
                                } else {
                                    utils.trip_payment_failed(trip, city, provider);
                                    trip.payment_status = PAYMENT_STATUS.FAILED;
                                    await trip.save();
                                    utils.update_request_status_socket(trip._id, null, 0, true)
                                    utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_device_type, user_device_token, push_messages.PUSH_CODE_FOR_YOUR_TRIP_END, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                    return res.json({ success: false, reference: payment_response.data.reference, required_param: payment_response.data.status });
                                }

                            } else {
                                var error_message = '';
                                if (payment_response.data) {
                                    error_message = payment_response.data.message;
                                } else {
                                    error_message = payment_response.message;
                                }
                                trip.payment_status = PAYMENT_STATUS.FAILED;
                                await trip.save();
                                utils.update_request_status_socket(trip._id, null, 0, true)
                                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_device_type, user_device_token, push_messages.PUSH_CODE_FOR_YOUR_TRIP_END, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                return res.json({
                                    success: true,
                                    error: error_message,
                                    message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                                    payment_status: trip.payment_status
                                });
                            }
                        })
                    }).on('error', error => {
                        console.error(error)
                    })
                    request.write(params)
                    request.end()
                } else{
                    trip.payment_status = PAYMENT_STATUS.FAILED;
                    await trip.save();
                    utils.trip_response(req, trip, user, provider, res);
                }
            } else {
                trip.is_paid = 1;
                trip.is_pending_payments = 0;
                trip.payment_status = PAYMENT_STATUS.COMPLETED;
                trip.card_payment = 0;
                await utils.trip_provider_profit_card_wallet_settlement(trip);
                await trip.save();
                utils.trip_response(req, trip, user, provider, res);
            }

        }
        
    } catch (error) {
        console.log(error);
        return res.json({
            success: false,
            error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
        }); 
    }

};

exports.pay_tip_payment = async function (req, res) {
    const setting_detail = await Settings.findOne({});
    console.log(req.query);
    if(req.query?.amount){
        req.body.amount = req.query?.amount 
        req.body.trip_id = req.query?.trip_id
    }
    if (req.body?.udf5) {
        req.body.trip_id = req.body.udf5;
    }else if(req.body?.user_defined?.udf5){
        req.body.trip_id = req.body.user_defined.udf5
    }
    Trip.findOne({ _id: req.body.trip_id }).then(async  (trip) => {
        Trip_history.findOne({ _id: req.body.trip_id, is_trip_end: 1 }).then(async (trip_history) => {
            if (!trip) {
                trip = trip_history;
            }
            if (trip) {
                let total_redeem_point = '';
                let country = await Country.findById(trip.country_id)
                let user = await User.findById(trip.user_id)
                if(country?.user_redeem_settings[0]?.is_user_redeem_point_reward_on && (country.user_redeem_settings[0]?.tip_redeem_point > 0)){
                    total_redeem_point = utils.add_redeem_point_history(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id,country._id,constant_json.TIP_REDEEM_POINT,user.wallet_currency_code,"Get redeem point via Tips",country.user_redeem_settings[0]?.tip_redeem_point,user.total_redeem_point,constant_json.ADD_REDEEM_POINT,trip.unique_id)

                    user.total_redeem_point = total_redeem_point
                    await user.save()
                }
                if (!trip.payment_gateway_type || trip.payment_gateway_type == PAYMENT_GATEWAY.stripe || req.body?.is_apple_pay) {
                    let url = setting_detail.payments_base_url + "/retrieve_payment_intent"
                    let data = {
                        payment_intent_id: trip.tip_payment_intent_id
                    }

                    const request = require('request');
                    request.post(
                    {
                        url: url,
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify(data),
                    }, (error, response, body) => {
                        if (error) {
                            console.error(error);
                            return error
                        } else {
                            body = JSON.parse(body);
                            intent = body.intent;

                            console.log(" *** intent");
                            console.log(intent);

                            if (intent && intent.charges && intent.charges.data && intent.charges.data.length > 0) {
                                trip.tip_amount = intent.charges.data[0].amount / 100;
                                trip.total = trip.total + trip.tip_amount;
                                trip.provider_service_fees = +trip.provider_service_fees + +trip.tip_amount;
                                trip.pay_to_provider = trip.pay_to_provider + +trip.tip_amount;
                                trip.card_payment = trip.card_payment + trip.tip_amount;
    
                                Provider.findOne({ _id: trip.confirmed_provider }, function (error, provider) {
                                    City.findOne({ _id: trip.city_id }).then((city) => {
                                        if (city.is_provider_earning_set_in_wallet_on_other_payment) {
                                            if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                                                var total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                                                    provider.wallet_currency_code, trip.currencycode,
                                                    1, trip.tip_amount, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
    
                                                provider.wallet = total_wallet_amount;
                                                provider.save();
                                            } else {
                                                Partner.findOne({ _id: trip.provider_type_id }).then((partner) => {
                                                    var total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                                                        partner.wallet_currency_code, trip.currencycode,
                                                        1, trip.tip_amount, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
    
                                                    partner.wallet = total_wallet_amount;
                                                    partner.save();
                                                });
                                            }
    
                                            trip.is_provider_earning_set_in_wallet = true;
                                            trip.provider_income_set_in_wallet = trip.provider_income_set_in_wallet + Math.abs(trip.tip_amount);
                                        }
    
                                        trip.save().then(() => {
                                            res.json({ success: true, message: success_messages.MESSAGE_CODE_PAYMENT_PAID_SUCCESSFULLY });
                                        });
                                    });
                                });
    
                            } else {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_PAYMENT_IS_PENDING })
                            }
                        }
                    })
                } else if (trip.payment_gateway_type == PAYMENT_GATEWAY.payu) {
                    trip.tip_amount = req.body.amount;
                    trip.total = trip.total + trip.tip_amount;
                    trip.provider_service_fees = +trip.provider_service_fees + +trip.tip_amount;
                    trip.pay_to_provider = trip.pay_to_provider + +trip.tip_amount;
                    trip.card_payment = trip.card_payment + trip.tip_amount;
                    trip.payment_intent_id = req.body.mihpayid;

                    Provider.findOne({ _id: trip.confirmed_provider }, function (error, provider) {
                        City.findOne({ _id: trip.city_id }).then((city) => {
                            if (city.is_provider_earning_set_in_wallet_on_other_payment) {
                                if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                                    var total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                                        provider.wallet_currency_code, trip.currencycode,
                                        1, trip.tip_amount, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);

                                    provider.wallet = total_wallet_amount;
                                    provider.save();
                                } else {
                                    Partner.findOne({ _id: trip.provider_type_id }).then((partner) => {
                                        var total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                                            partner.wallet_currency_code, trip.currencycode,
                                            1, trip.tip_amount, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);

                                        partner.wallet = total_wallet_amount;
                                        partner.save();
                                    });
                                }

                                trip.is_provider_earning_set_in_wallet = true;
                                trip.provider_income_set_in_wallet = trip.provider_income_set_in_wallet + Math.abs(trip.tip_amount);
                            }

                            trip.save().then(() => {
                                if (req.body.udf4) {
                                    res.redirect(req.body.udf4);
                                } else {
                                    res.json({ success: true, message: success_messages.MESSAGE_CODE_PAYMENT_PAID_SUCCESSFULLY });
                                }
                            });
                        });
                    });

                }else if(trip.payment_gateway_type == PAYMENT_GATEWAY.razorpay){
                   const key_secret = setting_detail.razorpay_secret_key
                   const crypto = require("crypto");
                   const generated_signature = crypto.createHmac("SHA256",key_secret).update(req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id).digest("hex"); 
                   let is_signature_valid = generated_signature == req.body.razorpay_signature;
                    
                    if (is_signature_valid) {
                        trip.tip_amount = req.body.amount;
                        trip.total = trip.total + trip.tip_amount;
                        trip.provider_service_fees = +trip.provider_service_fees + +trip.tip_amount;
                        trip.pay_to_provider = trip.pay_to_provider + +trip.tip_amount;
                        trip.card_payment = trip.card_payment + trip.tip_amount;
                        trip.tip_payment_intent_id = req.body.razorpay_payment_id;
    
                        Provider.findOne({ _id: trip.confirmed_provider }, function (error, provider) {
                            City.findOne({ _id: trip.city_id }).then((city) => {
                                if (city.is_provider_earning_set_in_wallet_on_other_payment) {
                                    if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                                        var total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                                            provider.wallet_currency_code, trip.currencycode,
                                            1, trip.tip_amount, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
    
                                        provider.wallet = total_wallet_amount;
                                        provider.save();
                                    } else {
                                        Partner.findOne({ _id: trip.provider_type_id }).then((partner) => {
                                            var total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                                                partner.wallet_currency_code, trip.currencycode,
                                                1, trip.tip_amount, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);
    
                                            partner.wallet = total_wallet_amount;
                                            partner.save();
                                        });
                                    }
    
                                    trip.is_provider_earning_set_in_wallet = true;
                                    trip.provider_income_set_in_wallet = trip.provider_income_set_in_wallet + Math.abs(trip.tip_amount);
                                }
    
                                trip.save().then(() => {
                                    if (req.query?.is_new) {
                                        res.redirect(req.query.is_new);
                                    } else {
                                        res.redirect(setting_detail.payments_base_url + '/success_payment');
                                    }
                                });
                            });
                        });
                    }else{
                        if (req.query?.is_new) {
                            utils.payu_status_fail_socket(trip.user_id)
                            res.redirect(req.query?.is_new);
                        } else {
                            utils.payu_status_fail_socket(trip.user_id)
                            res.redirect(setting_detail.payments_base_url + '/payment_fail');
                        }
                    }
                }else if (trip.payment_gateway_type == PAYMENT_GATEWAY.paytabs){
                    trip.tip_amount = req.body.tran_total;
                    trip.total = trip.total + trip.tip_amount;
                    trip.provider_service_fees = +trip.provider_service_fees + +trip.tip_amount;
                    trip.pay_to_provider = trip.pay_to_provider + +trip.tip_amount;
                    trip.card_payment = trip.card_payment + trip.tip_amount;
                    trip.tip_payment_intent_id = req.body.tran_ref;

                    Provider.findOne({ _id: trip.confirmed_provider }, function (error, provider) {
                        City.findOne({ _id: trip.city_id }).then((city) => {
                            if (city.is_provider_earning_set_in_wallet_on_other_payment) {
                                if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                                    var total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                                        provider.wallet_currency_code, trip.currencycode,
                                        1, trip.tip_amount, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id,req.body.tran_ref);

                                    provider.wallet = total_wallet_amount;
                                    provider.save();
                                } else {
                                    Partner.findOne({ _id: trip.provider_type_id }).then((partner) => {
                                        var total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                                            partner.wallet_currency_code, trip.currencycode,
                                            1, trip.tip_amount, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id,req.body.tran_ref);

                                        partner.wallet = total_wallet_amount;
                                        partner.save();
                                    });
                                }

                                trip.is_provider_earning_set_in_wallet = true;
                                trip.provider_income_set_in_wallet = trip.provider_income_set_in_wallet + Math.abs(trip.tip_amount);
                            }

                            trip.save().then(() => {
                                if (req.body.udf4) {
                                    res.redirect(req.body.udf4);
                                } else {
                                    res.json({ success: true, message: success_messages.MESSAGE_CODE_PAYMENT_PAID_SUCCESSFULLY });
                                }
                            });
                        });
                    });
                }else if(trip.payment_gateway_type == PAYMENT_GATEWAY.paypal) {
                    trip.tip_amount = req.body.amount;
                    trip.total = trip.total + trip.tip_amount;
                    trip.provider_service_fees = +trip.provider_service_fees + +trip.tip_amount;
                    trip.pay_to_provider = trip.pay_to_provider + +trip.tip_amount;
                    trip.card_payment = trip.card_payment + trip.tip_amount;
                    trip.tip_payment_intent_id = req.body.is_web ? (req.body?.payment_intent_id?.purchase_units[0]?.payments?.captures[0]?.id) : req.body?.payment_intent_id;
                    Provider.findOne({ _id: trip.confirmed_provider }, function (error, provider) {
                        City.findOne({ _id: trip.city_id }).then((city) => {
                            if (city.is_provider_earning_set_in_wallet_on_other_payment) {
                                if (provider.provider_type != PROVIDER_TYPE.PARTNER) {
                                    var total_wallet_amount = utils.addWalletHistory(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id, provider.country_id,
                                        provider.wallet_currency_code, trip.currencycode,
                                        1, trip.tip_amount, provider.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);

                                    provider.wallet = total_wallet_amount;
                                    provider.save();
                                } else {
                                    Partner.findOne({ _id: trip.provider_type_id }).then((partner) => {
                                        var total_wallet_amount = utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id, partner.country_id,
                                            partner.wallet_currency_code, trip.currencycode,
                                            1, trip.tip_amount, partner.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.SET_TRIP_PROFIT, "Set Profit Of This Trip : " + trip.unique_id);

                                        partner.wallet = total_wallet_amount;
                                        partner.save();
                                    });
                                }

                                trip.is_provider_earning_set_in_wallet = true;
                                trip.provider_income_set_in_wallet = trip.provider_income_set_in_wallet + Math.abs(trip.tip_amount);
                            }

                            trip.save().then(() => {
                                // if (req.body.udf4) {
                                //     res.redirect(req.body.udf4);
                                // } else {
                                    res.json({ success: true, message: success_messages.MESSAGE_CODE_PAYMENT_PAID_SUCCESSFULLY });
                                // }
                            });
                        });
                    });
                }
            } else {
                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND })
            }
        });
    });
}

exports.pay_stripe_intent_payment = async function (req, res) {
    const setting_detail = await Settings.findOne({});
    if(req.query.url !== undefined){
        req.body.user_id = req.query.url 
    }
    if((req.query.user_id !== undefined && req.query.trip_id  !== undefined) || (req.query.trip_id  !== undefined)){
        req.body.user_id = req.query.user_id 
        req.body.trip_id = req.query.trip_id 
    }
    if (req.body.txnid) {
        req.body.trip_id = req.body.txnid;
    }else if(req.body?.user_defined?.udf5){
        req.body.trip_id = req.body.user_defined.udf5
    }
    Trip.findOne({ _id: req.body.trip_id }).then(async (trip) => {
        console.log(trip);
        Trip_history.findOne({ _id: req.body.trip_id }).then(async (trip_history) => {
            console.log(trip_history);
            if (!trip) {
                trip = trip_history;
            }
            console.log(trip);
            if (trip) {
                let is_main_user = true;
                let split_payment_index = null;
                trip.split_payment_users.forEach((split_payment_user_detail, index) => {
                    if (split_payment_user_detail.user_id.toString() == req.body.user_id.toString()) {
                        is_main_user = false;
                        split_payment_index = index;
                    }
                })
                if (!trip.payment_gateway_type || trip.payment_gateway_type == PAYMENT_GATEWAY.stripe || req.body?.is_apple_pay) {

                    var stripe_secret_key = setting_detail.stripe_secret_key;

                    let payment_intent_id = trip.payment_intent_id;
                    if (!is_main_user) {
                        payment_intent_id = trip.split_payment_users[split_payment_index].payment_intent_id;
                    }
                    // console.log('payment_intent_id: ' + payment_intent_id)
                        let url = setting_detail.payments_base_url + "/retrieve_payment_intent"
                        let data = {
                            payment_intent_id: payment_intent_id
                        }
                        const request = require('request');
                        request.post(
                        {
                            url: url,
                            headers: {
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify(data),
                        }, async (error, response, body) => {
                            if (error) {
                                console.error(error);
                                return error
                            } else {
                                body = JSON.parse(body);
                                intent = body.intent;

                                console.log(" *** intent");
                                // console.log(intent);

                                if (intent && intent.charges && intent.charges.data && intent.charges.data.length > 0) {
                                    if (is_main_user) {
                                        trip.payment_status = PAYMENT_STATUS.COMPLETED;
                                        trip.remaining_payment = 0;
                                        trip.card_payment = intent.charges.data[0].amount / 100;
                                    } else {
                                        trip.split_payment_users[split_payment_index].card_payment = intent.charges.data[0].amount / 100;;
                                        trip.split_payment_users[split_payment_index].remaining_payment = 0;
                                        trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                                        trip.card_payment = trip.card_payment + (intent.charges.data[0].amount / 100);
                                    }
        
                                    if (trip.is_trip_cancelled == 1) {
                                        User.findOne({ _id: trip.user_id }).then((user) => {
                                            user.current_trip_id = null;
                                            user.save();
                                        });
                                    }
                                    // start provider profit after card payment done
                                    await utils.trip_provider_profit_card_wallet_settlement(trip);
                                    // end of provider profit after card payment done
        
        
                                    trip.markModified('split_payment_users');
                                    trip.save().then(() => {
                                        utils.update_request_status_socket(trip._id);
                                        if (is_main_user) {
                                            User.findOne({ _id: trip.user_id }, function (error, user) {
                                                user.corporate_wallet_limit = user.corporate_wallet_limit - trip.card_payment;
                                                user.save();
                                            })
                                        }
                                        Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                                            if (deleted_trip) {
                                                var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                                trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                                trip_history_data.save(function () {
                                                    res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                                });
                                            } else {
                                                res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                            }
                                        });
                                    });
                                } else {
                                    res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_PAYMENT_IS_PENDING })
                                }
                                
                            }
                        })
                } else if (trip.payment_gateway_type == PAYMENT_GATEWAY.payu) {
                    if (is_main_user) {
                        trip.payment_status = PAYMENT_STATUS.COMPLETED;
                        trip.card_payment = trip.remaining_payment;
                        trip.remaining_payment = 0;
                        trip.payment_intent_id = req.body.mihpayid;
                    } else {
                        trip.split_payment_users[split_payment_index].card_payment = trip.split_payment_users[split_payment_index].remaining_payment;
                        trip.card_payment = trip.card_payment + trip.split_payment_users[split_payment_index].remaining_payment;
                        trip.split_payment_users[split_payment_index].remaining_payment = 0;
                        trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                        trip.split_payment_users[split_payment_index].payment_intent_id = req.body.mihpayid;
                    }

                    if (trip.is_trip_cancelled == 1) {
                        User.findOne({ _id: trip.user_id }).then((user) => {
                            user.current_trip_id = null;
                            user.save();
                        });
                    }

                    // start provider profit after card payment done
                    await utils.trip_provider_profit_card_wallet_settlement(trip);
                    // end of provider profit after card payment done

                    trip.markModified('split_payment_users');
                    trip.save().then(() => {
                        utils.update_request_status_socket(trip._id);
                        if (is_main_user) {
                            User.findOne({ _id: trip.user_id }, function (error, user) {
                                user.corporate_wallet_limit = user.corporate_wallet_limit - trip.card_payment;
                                user.save();
                            })
                        }
                        Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                            if (deleted_trip) {
                                var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                trip_history_data.save(function () {
                                    if (req.body.udf4) {
                                        res.redirect(req.body.udf4);
                                    } else {
                                        res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                    }
                                });
                            } else {
                                if (req.body.udf4) {
                                    res.redirect(req.body.udf4);
                                } else {
                                    res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                }
                            }
                        });
                    });
                }else if(trip.payment_gateway_type == PAYMENT_GATEWAY.razorpay){
                    const key_secret = setting_detail.razorpay_secret_key
                    const crypto = require("crypto");
                    const generated_signature = crypto.createHmac("SHA256",key_secret).update(req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id).digest("hex");
                    let is_signature_valid = generated_signature == req.body.razorpay_signature;
                    console.log(generated_signature);
                    console.log(req.body.razorpay_signature);
                    if(is_signature_valid){
                        if (is_main_user) {
                            trip.payment_status = PAYMENT_STATUS.COMPLETED;
                            trip.card_payment = trip.remaining_payment;
                            trip.remaining_payment = 0;
                            trip.payment_intent_id = req.body.razorpay_payment_id;
                        } else {
                            trip.split_payment_users[split_payment_index].card_payment = trip.split_payment_users[split_payment_index].remaining_payment;
                            trip.card_payment = trip.card_payment + trip.split_payment_users[split_payment_index].remaining_payment;
                            trip.split_payment_users[split_payment_index].remaining_payment = 0;
                            trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                            trip.split_payment_users[split_payment_index].payment_intent_id = req.body.razorpay_payment_id;
                        }
                        if (trip.is_trip_cancelled == 1) {
                            User.findOne({ _id: trip.user_id }).then((user) => {
                                user.current_trip_id = null;
                                user.save();
                            });
                        }
    
                        // start provider profit after card payment done
                        await utils.trip_provider_profit_card_wallet_settlement(trip);
                        // end of provider profit after card payment done
                        trip.markModified('split_payment_users');
                        trip.save().then(() => {
                            utils.update_request_status_socket(trip._id);
                            if (is_main_user) {
                                User.findOne({ _id: trip.user_id }, function (error, user) {
                                    user.corporate_wallet_limit = user.corporate_wallet_limit - trip.card_payment;
                                    user.save();
                                })
                            }
                            Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                                if (deleted_trip) {
                                    var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                    trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                    trip_history_data.save(function () {
                                        if (req.query?.is_new) {
                                            res.redirect(req.query.is_new);
                                        } else {
                                            res.redirect(setting_detail.payments_base_url + '/success_payment');
                                        }
                                    });
                                } else {
                                    if (req.query?.is_new) {
                                        res.redirect(req.query.is_new);
                                    } else {
                                        res.redirect(setting_detail.payments_base_url + '/success_payment');
                                    }
                                }
                            });
                        });
                    }else{
                        if (req.query?.is_new) {
                            utils.payu_status_fail_socket(trip.user_id)
                            res.redirect(req.query?.is_new);
                        } else {
                            utils.payu_status_fail_socket(trip.user_id)
                            res.redirect(setting_detail.payments_base_url + '/payment_fail');
                        }
                    }
                    
                } else if( trip.payment_gateway_type == PAYMENT_GATEWAY.paytabs){
                        if (is_main_user) {
                            trip.payment_status = PAYMENT_STATUS.COMPLETED;
                            trip.card_payment = trip.remaining_payment;
                            trip.remaining_payment = 0;
                            trip.payment_intent_id = req.query.payment_intent_id;
                        } else {
                            trip.split_payment_users[split_payment_index].card_payment = trip.split_payment_users[split_payment_index].remaining_payment;
                            trip.card_payment = trip.card_payment + trip.split_payment_users[split_payment_index].remaining_payment;
                            trip.split_payment_users[split_payment_index].remaining_payment = 0;
                            trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                            trip.split_payment_users[split_payment_index].payment_intent_id = req.query.payment_intent_id;
                        }
                        if (trip.is_trip_cancelled == 1) {
                            User.findOne({ _id: trip.user_id }).then((user) => {
                                user.current_trip_id = null;
                                user.save();
                            });
                        }

                        // start provider profit after card payment done
                        await utils.trip_provider_profit_card_wallet_settlement(trip);
                        // end of provider profit after card payment done

                        trip.markModified('split_payment_users');
                        trip.save().then(() => {
                            utils.update_request_status_socket(trip._id);
                            if (is_main_user) {
                                User.findOne({ _id: trip.user_id }, function (error, user) {
                                    user.corporate_wallet_limit = user.corporate_wallet_limit - trip.card_payment;
                                    user.save();
                                })
                            }
                            Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                                if (deleted_trip) {
                                    var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                    trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                    trip_history_data.save(function () {
                                        if (req.query.is_new != 'undefined') {
                                            res.redirect(req.query.is_new);
                                        } else {
                                            res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                        }
                                    });
                                } else {
                                    if (req.query.is_new != 'undefined') {
                                        res.redirect(req.query.is_new);
                                    } else {
                                        res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                    }
                                }
                            });
                        });
                }else if( trip.payment_gateway_type == PAYMENT_GATEWAY.paypal){
                    if (is_main_user) {
                        trip.payment_status = PAYMENT_STATUS.COMPLETED;
                        trip.card_payment = trip.remaining_payment;
                        trip.remaining_payment = 0;
                        trip.payment_intent_id = req.body.is_web ? (req.body?.payment_intent_id?.purchase_units[0]?.payments?.captures[0]?.id) : req.body?.payment_intent_id;
                    } else {
                        trip.split_payment_users[split_payment_index].card_payment = trip.split_payment_users[split_payment_index].remaining_payment;
                        trip.card_payment = trip.card_payment + trip.split_payment_users[split_payment_index].remaining_payment;
                        trip.split_payment_users[split_payment_index].remaining_payment = 0;
                        trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                        trip.split_payment_users[split_payment_index].payment_intent_id = req.body.is_web ? (req.body?.payment_intent_id?.purchase_units[0]?.payments?.captures[0]?.id) : req.body?.payment_intent_id;
                    }
                    if (trip.is_trip_cancelled == 1) {
                        User.findOne({ _id: trip.user_id }).then((user) => {
                            user.current_trip_id = null;
                            user.save();
                        });
                    }
                    await utils.trip_provider_profit_card_wallet_settlement(trip);
                        // end of provider profit after card payment done

                        trip.markModified('split_payment_users');
                        trip.save().then(() => {
                            utils.update_request_status_socket(trip._id);
                            if (is_main_user) {
                                User.findOne({ _id: trip.user_id }, function (error, user) {
                                    user.corporate_wallet_limit = user.corporate_wallet_limit - trip.card_payment;
                                    user.save();
                                })
                            }
                            Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                                if (deleted_trip) {
                                    var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                    trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                    trip_history_data.save(function () {
                                            res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                    });
                                } else {
                                        res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                }
                            });
                        });
                }

            } else {
                console.log(trip);
                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND })
            }
        });
    });
}

exports.fail_stripe_intent_payment = function (req, res) {
    let trip_id 
    if(req.query.url){
        trip_id = req.query.url
    }else{
        trip_id = req.body.trip_id
    }
    if (req.body.txnid) {
        req.body.trip_id = req.body.txnid;
    }
    Trip.findOne({ _id: trip_id, $or: [{ payment_status: PAYMENT_STATUS.WAITING }, { payment_status: PAYMENT_STATUS.FAILED }] }).then((trip) => {
        if (trip) {
            Corporate.findOne({ _id: trip.user_type_id }).then((corporate) => {
                if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                    var wallet_payment = trip.remaining_payment;
                    var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, corporate.unique_id, corporate._id, null,
                        corporate.wallet_currency_code, trip.currencycode,
                        trip.wallet_current_rate, wallet_payment, corporate.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                    corporate.wallet = total_wallet_amount;
                    corporate.save();

                    utils.update_request_status_socket(trip._id);
                    trip.payment_status = PAYMENT_STATUS.COMPLETED;
                    trip.remaining_payment = 0;
                    trip.wallet_payment = wallet_payment;

                    if (trip.is_trip_cancelled == 1) {
                        User.findOne({ _id: trip.user_id }).then((user) => {
                            user.current_trip_id = null;
                            user.save();
                        });
                    }
                    trip.save().then(() => {
                        User.findOne({ _id: trip.user_id }, function (error, user) {
                            user.corporate_wallet_limit = user.corporate_wallet_limit - trip.card_payment;
                            user.save();
                        })
                        Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                            if (deleted_trip) {
                                var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                trip_history_data.save(function () {
                                    res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                                });
                            } else {
                                res.json({ success: true, message: success_messages.PAYMENT_PAID_SUCCESSFULLY });
                            }
                        });
                    });

                } else {
                    if (trip.payment_gateway_type != PAYMENT_GATEWAY.payu) {
                        utils.update_request_status_socket(trip._id);
                    }

                    trip.payment_status = PAYMENT_STATUS.FAILED;
                    trip.save().then(() => {
                        res.json({ success: true, message: success_messages.PAYMENT_FAILD });
                    }, (error) => {
                        console.log(error)
                    });
                }
            });

        } else {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND })
        }
    });
}

///////////////////GETTRIP STATUS PROVIDER SIDE //////
exports.providergettripstatus = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }], function (response) {
        if (response.success) {
            console.log('providergettripstatus')
            var provider_id = req.body.provider_id;
            var token = req.body.token;
            var country_phone_code = "";
            var phone = "";
            if (provider_id != undefined && token != undefined) {
                Provider.findOne({ _id: req.body.provider_id }).then(async (provider) => {
                    if (req.body.token != null && provider.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        if ((provider.is_trip.length > 0 && provider.is_trip.includes(req.body.trip_id)) || (provider.schedule_trip.length > 0 && provider.schedule_trip.includes(req.body.trip_id)) || (provider.open_ride.length > 0 && provider.open_ride.includes(req.body.trip_id))) {
                            const country = await Country.findById(provider.country_id)
                            if(!country){
                                res.json({ success: false, error_code: error_message.ERROR_CODE_COUNTRY_NOT_FOUND });
                            }
                            
                            Trip.findOne({
                                _id: req.body.trip_id,
                                $or: [{ current_providers: provider._id },
                                { confirmed_provider: provider._id }],

                                is_trip_cancelled: 0,
                                is_trip_cancelled_by_provider: 0
                            }).then((trip) => {

                                Trip_history.findOne({
                                    _id: req.body.trip_id,
                                    $or: [{ current_providers: provider._id },
                                    { confirmed_provider: provider._id }],

                                    is_trip_cancelled: 0,
                                    is_trip_cancelled_by_provider: 0
                                }).then(async (trip_history) => {
                                    if (!trip) {
                                        trip = trip_history;
                                    }
                                    let openride
                                    if(!trip){
                                        openride = await OpenRide.findOne({
                                            _id: req.body.trip_id,
                                            $or: [{ current_providers: provider._id },
                                            { confirmed_provider: provider._id }],

                                            is_trip_cancelled: 0,
                                            is_trip_cancelled_by_provider: 0
                                        })

                                        trip = openride;
                                    }

                                    if (trip) {
                                        Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
                                            let userid
                                            if (trip.openride) {
                                                const filteredObjects = trip.user_details.filter(item => 
                                                    item.status == 0 && item.send_req_to_provider_first_time == 1 
                                                );
                                                if (filteredObjects.length != 0) {
                                                    userid = filteredObjects[0].user_id
                                                }
                                                
                                            } else {
                                                userid = trip.user_id
                                            }
                                                                    if (tripservice) {
                                                User.findOne({ _id: userid }).then((user) => {

                                                    if (user) {
                                                        country_phone_code = user.country_phone_code;
                                                        phone = user.phone;
                                                    }

                                                    Citytype.findById(trip.service_type_id).then((citytype_detail) => {
                                                        Type.findById(citytype_detail.typeid).then((type_detail) => {
                                                            var waiting_time_start_after_minute = 0;
                                                            var price_for_waiting_time = 0;
                                                            var total_wait_time = 0;
                                                            var provider_arrived_time = trip.provider_arrived_time;
                                                            if (provider_arrived_time != null) {
                                                                var end_time = new Date();
                                                                waiting_time_start_after_minute = tripservice.waiting_time_start_after_minute;
                                                                price_for_waiting_time = tripservice.price_for_waiting_time;
                                                                total_wait_time = utils.getTimeDifferenceInSecond(end_time, provider_arrived_time);
                                                                total_wait_time = total_wait_time - waiting_time_start_after_minute * 60;
                                                            }
                                                            if (trip.is_provider_status == PROVIDER_STATUS.TRIP_STARTED) {
                                                                var now = new Date();
                                                                var minutes = utils.getTimeDifferenceInMinute(now, trip.provider_trip_start_time);
                                                                trip.total_time = minutes;
                                                                trip.save();
                                                            }
                                                            if (!trip.openride) {
                                                                var index = user.favourite_providers.findIndex((x) => (x).toString() == (req.body.provider_id).toString())
    
                                                                if (index !== -1) {
                                                                    trip.is_favourite_provider = true;
                                                                }
                                                            }

                                                            
                                                            let trip_to_send = JSON.parse(JSON.stringify(trip))
                                                            trip_to_send['driver_max_bidding_limit'] = country?.driver_max_bidding_limit
                                                            
                                                            res.json({
                                                                success: true,
                                                                map_pin_image_url: type_detail.map_pin_image_url,
                                                                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_GET_TRIP_STATUS_SUCCESSFULLY,
                                                                country_phone_code: country_phone_code,
                                                                phone: phone,
                                                                trip: trip_to_send,
                                                                user: user,
                                                                tripservice: tripservice,
                                                                waiting_time_start_after_minute: waiting_time_start_after_minute,
                                                                price_for_waiting_time: price_for_waiting_time,
                                                                total_wait_time: total_wait_time,
                                                                driver_max_bidding_limit: country?.driver_max_bidding_limit,
                                                                openride: trip_to_send?.openride
                                                            });
                                                        });
                                                    });
                                                });
                                            } else {
                                                res.json({
                                                    success: false,
                                                    error_code: error_message.ERROR_CODE_NOT_GET_TRIP_STATUS
                                                });
                                            }
                                        });
                                    } else {
                                        Trip.findOne({
                                            _id: req.body.trip_id,
                                            is_trip_cancelled_by_user: 1,
                                            is_trip_cancelled: 1
                                        }).then(async (cancel_trip) => {
                                            if (!cancel_trip) {
                                                openride = await OpenRide.findOne({
                                                    _id: req.body.trip_id,
                                                    is_trip_cancelled_by_user: 1,
                                                    is_trip_cancelled: 1
                                                })
                                                cancel_trip = openride;
                                            }

                                            if (cancel_trip) {
                                                res.json({
                                                    success: false,
                                                    error_code: error_message.ERROR_CODE_TRIP_CANCELLED_BY_USER
                                                });
                                            } else {

                                                provider = utils.remove_is_trip_from_provider(provider, req.body.trip_id)
                                                if (!provider.is_near_trip) { provider.is_near_trip = [] }
                                                if ((String(provider.is_near_trip[0]) == String(req.body.trip_id))) {
                                                    provider.is_near_available = 1;
                                                    provider.is_near_trip = [];
                                                }
                                                await Provider.updateOne({ _id: provider._id }, provider.getChanges())

                                                res.json({
                                                    success: false,
                                                    error_code: error_message.ERROR_CODE_NOT_GET_TRIP_STATUS
                                                });
                                            }

                                        });
                                    }
                                });
                            }, (err) => {
                                console.log(err);
                                res.json({
                                    success: false,
                                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                                });
                            });

                        } else {
                            res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_GET_TRIP_STATUS });

                        }

                    }
                }, (err) => {
                    console.log(err);
                    res.json({
                        success: false,
                        error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                    });
                });


            } else {

                Trip.findOne({
                    _id: req.body.trip_id,
                    is_trip_cancelled: 0,
                    is_trip_cancelled_by_provider: 0
                }).then((trip) => {

                    if (trip) {

                        Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {

                            if (tripservice) {
                                User.findOne({ _id: trip.user_id }).then(async(user) => {

                                    if (user) {
                                        country_phone_code = user.country_phone_code;
                                        phone = user.phone;
                                    }
                                    const country = await Country.findOne({"countryname":user.country})
                                    if(!country){
                                        res.json({ success: false, error_code: error_message.ERROR_CODE_COUNTRY_NOT_FOUND });
                                    }
                                    Citytype.findById(trip.service_type_id).then((citytype_detail) => {
                                        Type.findById(citytype_detail.typeid).then((type_detail) => {
                                            var waiting_time_start_after_minute = 0;
                                            var price_for_waiting_time = 0;
                                            var total_wait_time = 0;
                                            var provider_arrived_time = trip.provider_arrived_time;
                                            if (provider_arrived_time != null) {
                                                var end_time = new Date();
                                                waiting_time_start_after_minute = tripservice.waiting_time_start_after_minute;
                                                price_for_waiting_time = tripservice.price_for_waiting_time;
                                                total_wait_time = utils.getTimeDifferenceInSecond(end_time, provider_arrived_time);
                                                total_wait_time = total_wait_time - waiting_time_start_after_minute * 60;
                                            }

                                            let trip_to_send = JSON.parse(JSON.stringify(trip))
                                            trip_to_send['driver_max_bidding_limit'] = country?.driver_max_bidding_limit

                                            res.json({
                                                success: true,
                                                country_phone_code: country_phone_code,
                                                phone: phone,
                                                map_pin_image_url: type_detail.map_pin_image_url,
                                                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_GET_TRIP_STATUS_SUCCESSFULLY,
                                                trip: trip_to_send,
                                                user: user,
                                                waiting_time_start_after_minute: waiting_time_start_after_minute,
                                                price_for_waiting_time: price_for_waiting_time,
                                                total_wait_time: total_wait_time,
                                                driver_max_bidding_limit: country?.driver_max_bidding_limit
                                            });
                                        });
                                    });

                                });

                            } else {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_GET_TRIP_STATUS });
                            }
                        });
                    } else {

                        Trip.findOne({ _id: req.body.trip_id, is_trip_cancelled_by_user: 1 }).then((cancel_trip) => {

                            if (cancel_trip) {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_CANCELLED_BY_USER });
                            } else {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_GET_TRIP_STATUS });
                            }

                        });
                    }
                }, (err) => {
                    console.log(err);
                    res.json({
                        success: false,
                        error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                    });
                });
            }
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

//////////////////// user_history //////////////////////
exports.user_history = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user.token != req.body.token) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                } else {
                    var lookup1 = {
                        $lookup:
                        {
                            from: "providers",
                            localField: "confirmed_provider",
                            foreignField: "_id",
                            as: "provider_detail"
                        }
                    };
                    var unwind1 = {
                        $unwind: {
                            path: "$provider_detail",
                            preserveNullAndEmptyArrays: true
                        }
                    };

                    var lookup2 = {
                        $lookup:
                        {
                            from: "trip_services",
                            localField: "trip_service_city_type_id",
                            foreignField: "_id",
                            as: "service_type"
                        }
                    };
                    var unwind2 = { $unwind: "$service_type" };

                    var mongoose = require('mongoose');
                    var Schema = mongoose.Types.ObjectId;
                    var condition = { $match: { 'user_id': { $eq: Schema(req.body.user_id) } } };
                    var condition1 = { $match: { $or: [{ is_trip_cancelled: { $eq: 1 } }, { is_trip_end: { $eq: 1 } }, { is_trip_cancelled_by_user: { $eq: 1 } }, { is_trip_cancelled_by_provider: { $eq: 1 } }] } };

                    var group = {
                        $project: {
                            trip_id: '$_id', unique_id: 1, invoice_number: 1,
                            current_provider: 1, provider_service_fees: 1,
                            is_trip_cancelled_by_user: 1,
                            is_trip_completed: 1,
                            is_trip_cancelled: 1,
                            is_user_rated: 1,
                            is_provider_rated: 1,
                            is_trip_cancelled_by_provider: 1,
                            first_name: '$provider_detail.first_name',
                            last_name: '$provider_detail.last_name',
                            picture: '$provider_detail.picture',
                            total: 1,
                            unit: 1,
                            currency: 1,
                            currencycode: 1,
                            total_time: 1,
                            user_create_time: 1,
                            total_distance: 1,
                            source_address: 1,
                            destination_address: 1,
                            destination_addresses: 1,
                            provider_trip_end_time: 1,
                            timezone: 1,
                            created_at: 1,
                            cash_payment: 1,
                            card_payment: 1,
                            wallet_payment: 1,
                            service_type: 1,
                            payment_mode: 1
                        }
                    };

                    var search_item;
                    var search_value;
                    var sort_order;
                    var sort_field;

                    if (req.body.search_item == undefined) {
                        var request = req.path.split('/')[1];
                        search_item = 'unique_id';
                        search_value = '';
                        sort_order = -1;
                        sort_field = 'unique_id';
                    } else {
                        var request = req.body.request;
                        var value = req.body.search_value;
                        value = value.trim();
                        value = value.replace(/ +(?= )/g, '');
                        value = new RegExp(value, 'i');
                        sort_order = req.body.sort_item[1];
                        sort_field = req.body.sort_item[0];
                        search_item = req.body.search_item
                        search_value = req.body.search_value;
                    }

                    value = search_value;
                    value = value.trim();
                    value = value.replace(/ +(?= )/g, '');
                    var query1 = {};
                    var query2 = {};
                    var query3 = {};
                    var query4 = {};
                    var query5 = {};
                    var query6 = {};
                    if (search_item == "unique_id") {

                        query1 = {};
                        if (value != "") {
                            value = Number(value)
                            query1[search_item] = { $eq: value };
                            var search = { "$match": query1 };
                        }
                        else {
                            var search = { $match: {} };
                        }
                    } else if (search_item == "first_name") {
                        query1 = {};
                        query2 = {};
                        query3 = {};
                        query4 = {};
                        query5 = {};
                        query6 = {};

                        var full_name = value.split(' ');
                        console.log(full_name)
                        if (typeof full_name[0] == 'undefined' || typeof full_name[1] == 'undefined') {
                            query1['first_name'] = { $regex: new RegExp(full_name[0], 'i') };
                            query2['last_name'] = { $regex: new RegExp(full_name[1], 'i') };
                            var search = { "$match": { $or: [query1, query2] } };
                            console.log(query1)
                        } else {
                            query1[search_item] = { $regex: new RegExp(full_name[0], 'i') };
                            query2['last_name'] = { $regex: new RegExp(full_name[1], 'i') };
                            query3[search_item] = { $regex: new RegExp(full_name[0], 'i') };
                            query4['last_name'] = { $regex: new RegExp(full_name[0], 'i') };
                            query5[search_item] = { $regex: new RegExp(full_name[1], 'i') };
                            query6['last_name'] = { $regex: new RegExp(full_name[1], 'i') };

                            var search = { "$match": { $or: [query1, query2, query3, query4, query5, query6] } };
                        }
                    } else {
                        var search = { "$match": { search_item: { $regex: new RegExp(value, 'i') } } };
                    }


                    var start_date = req.body.start_date;
                    var end_date = req.body.end_date;
                    if (end_date == '' || end_date == undefined) {
                        end_date = new Date();
                    } else {
                        end_date = new Date(end_date);
                        end_date = end_date.setHours(23, 59, 59, 999);
                        end_date = new Date(end_date);
                    }

                    if (start_date == '' || start_date == undefined) {
                        start_date = new Date(0);
                        start_date = start_date.setHours(0, 0, 0, 0);
                        start_date = new Date(start_date);
                    } else {
                        start_date = new Date(start_date);
                        start_date = start_date.setHours(0, 0, 0, 0);
                        start_date = new Date(start_date);
                    }
                    query1['created_at'] = { $gte: start_date, $lt: end_date };
                    var filter = { "$match": query1 };

                    var number_of_rec = 10;
                    var skip = {};
                    var page = req.body.page
                    skip["$skip"] = (page - 1) * number_of_rec;

                    var limit = {};
                    limit["$limit"] = number_of_rec;

                    var sort = { "$sort": {} };
                    sort["$sort"][sort_field] = parseInt(sort_order);


                    Trip_history.aggregate([condition, condition1, lookup1, unwind1, lookup2, unwind2, group, filter, search]).then((array) => {
                        let total_page = Math.ceil(array.length / 10)
                        if (req.body.page) {
                            Trip_history.aggregate([condition, condition1, lookup1, unwind1, lookup2, unwind2, group, filter, search, sort, skip, limit]).then((array_list) => {
                                // array_list = array_list.concat(array)
                                // function compare( a, b ) {
                                //     return new Date(b.created_at) - new Date(a.created_at);
                                // }
                                // array_list.sort(compare)
                                res.json({ success: true, trips: array_list, pages: total_page });
                            });
                        } else {
                            Trip_history.aggregate([condition, condition1, lookup1, unwind1, lookup2, unwind2, group, filter, search, sort]).then((array_list) => {
                                // array_list = array_list.concat(array)
                                // function compare( a, b ) {
                                //     return new Date(b.created_at) - new Date(a.created_at);
                                // }
                                // array_list.sort(compare)
                                res.json({ success: true, trips: array_list, pages: total_page });
                            });
                        }
                    }, (err) => {
                        console.log(err);
                        res.json({
                            success: false,
                            error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                        });
                    });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
}


    /////////////////////// provider_history ///////////////////////////////////
    exports.provider_history = function (req, res) {
        utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }], async function (response) {
            if (!response.success) {
                return res.json({ success: false, error_code: response.error_code, error_description: response.error_description });
            }

            let provider = await Provider.findOne({ _id: req.body.provider_id })
            if (!provider) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            }

            if (provider.token != req.body.token) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            }
            let lookup1 = {
                $lookup:
                {
                    from: "users",
                    localField: "user_id",
                    foreignField: "_id",
                    as: "user_detail"
                }
            };
            let unwind1 = { $unwind: "$user_detail" };

            let mongoose = require('mongoose');
            let Schema = mongoose.Types.ObjectId;

            let condition = { $match: { 'confirmed_provider': { $eq: Schema(req.body.provider_id) } } };

            let group = {
                $project: {
                    trip_id: '$_id', unique_id: 1, invoice_number: 1,
                    current_provider: 1,
                    is_trip_cancelled_by_user: 1,
                    is_trip_cancelled: 1,
                    is_user_rated: 1,
                    is_trip_completed: 1,
                    is_provider_rated: 1,
                    is_trip_cancelled_by_provider: 1,
                    first_name: '$user_detail.first_name',
                    last_name: '$user_detail.last_name',
                    picture: '$user_detail.picture',
                    total: 1,
                    unit: 1,
                    currency: 1,
                    currencycode: 1,
                    total_time: 1,
                    user_create_time: 1,
                    total_distance: 1,
                    source_address: 1,
                    destination_address: 1,
                    destination_addresses: 1,
                    provider_trip_end_time: 1,
                    timezone: 1,
                    created_at: 1,
                    payment_mode: 1,
                    payment_status: 1,

                    sourceLocation: 1,
                    destinationLocation: 1,
                    // for invoice price details
                    base_distance_cost: 1,
                    distance_cost: 1,
                    time_cost: 1,
                    total_waiting_time: 1,
                    surge_fee: 1,
                    tax_fee: 1,
                    total_service_fees: 1,
                    user_tax_fee: 1,
                    user_miscellaneous_fee: 1,
                    tip_amount: 1,
                    toll_amount: 1,
                    promo_payment: 1,
                    wallet_payment: 1,
                    card_payment: 1,
                    cash_payment: 1,
                    remaining_payment: 1,
                    provider_profit_fees: 1,
                    provider_miscellaneous_fee: 1,
                    provider_service_fees: 1,
                    provider_tax_fee: 1,
                    fixed_price: 1,
                    total_after_surge_fees: 1,
                    total_after_tax_fees: 1,
                    provider_arrived_time: 1,
                    provider_trip_start_time: 1,
                    waiting_time_cost: 1,
                    is_trip_end: 1,
                    trip_type : 1,
                    is_fixed_fare : 1,
                    is_min_fare_used :1,
                    split_payment_users : 1

                }
            };

            // pangination and filter
            var search_item;
            var search_value;
            var sort_order;
            var sort_field;

            if (req.body.search_item == undefined) {
                var request = req.path.split('/')[1];
                search_item = 'unique_id';
                search_value = '';
                sort_order = -1;
                sort_field = 'unique_id';
            } else {
                var request = req.body.request;
                var value = req.body.search_value;
                value = value.trim();
                value = value.replace(/ +(?= )/g, '');
                value = new RegExp(value, 'i');
                sort_order = req.body.sort_item[1];
                sort_field = req.body.sort_item[0];
                search_item = req.body.search_item
                search_value = req.body.search_value;
            }

            value = search_value;
            value = value.trim();
            value = value.replace(/ +(?= )/g, '');
            var query1 = {};
            var query2 = {};
            var query3 = {};
            var query4 = {};
            var query5 = {};
            var query6 = {};
            if (search_item == "unique_id") {

                query1 = {};
                if (value != "") {
                    value = Number(value)
                    query1[search_item] = { $eq: value };
                    var search = { "$match": query1 };
                }
                else {
                    var search = { $match: {} };
                }
            } else if (search_item == "first_name") {
                query1 = {};
                query2 = {};
                query3 = {};
                query4 = {};
                query5 = {};
                query6 = {};

                var full_name = value.split(' ');
                if (typeof full_name[0] == 'undefined' || typeof full_name[1] == 'undefined') {
                    query1[search_item] = { $regex: new RegExp(full_name[0], 'i') };
                    query2['last_name'] = { $regex: new RegExp(full_name[1], 'i') };
                    var search = { "$match": { $or: [query1, query2] } };
                } else {
                    query1[search_item] = { $regex: new RegExp(full_name[0], 'i') };
                    query2['last_name'] = { $regex: new RegExp(full_name[1], 'i') };
                    query3[search_item] = { $regex: new RegExp(full_name[0], 'i') };
                    query4['last_name'] = { $regex: new RegExp(full_name[0], 'i') };
                    query5[search_item] = { $regex: new RegExp(full_name[1], 'i') };
                    query6['last_name'] = { $regex: new RegExp(full_name[1], 'i') };

                    var search = { "$match": { $or: [query1, query2, query3, query4, query5, query6] } };
                }
            } else {
                var search = { "$match": { search_item: { $regex: new RegExp(value, 'i') } } };
            }

            let start_date = req.body.start_date;
            let end_date = req.body.end_date;
            if (end_date == '' || end_date == undefined) {
                end_date = new Date();
            } else {
                end_date = new Date(end_date);
                end_date = end_date.setHours(23, 59, 59, 999);
                end_date = new Date(end_date);
            }

            if (start_date == '' || start_date == undefined) {
                start_date = new Date(0);
                start_date = start_date.setHours(0, 0, 0, 0);
                start_date = new Date(start_date);
            } else {
                start_date = new Date(start_date);
                start_date = start_date.setHours(0, 0, 0, 0);
                start_date = new Date(start_date);
            }
            query1['created_at'] = { $gte: start_date, $lt: end_date };
            let filter = { "$match": query1 };

            let number_of_rec = 10;
            let page = req.body.page || 1;
            let skip = {};
            skip["$skip"] = (page - 1) * number_of_rec;

            let limit = {};
            limit["$limit"] = number_of_rec;
            var sort = { "$sort": {} };
            sort["$sort"][sort_field] = parseInt(sort_order);
            /* Count Total Trips */
            let trips_total = await Trip_history.aggregate([condition, lookup1, unwind1, group, filter, search, sort]);
            let total_pages = Math.ceil(trips_total.length / number_of_rec)

            let trips = await Trip_history.aggregate([condition, lookup1, unwind1, group, filter, search, sort, skip, limit]);
            return res.json({ success: true, trips: trips, page: total_pages });
        })
    };


exports.provider_submit_invoice = async function (req, res) {
    const setting_detail = await Settings.findOne({});

    utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            Provider.findOne({ _id: req.body.provider_id }).then((provider) => {
                if (provider) {
                    if (req.body.token != null && provider.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        console.log('provider_submit_invoice')
                        Trip.findOne({ _id: req.body.trip_id, is_trip_end: 1 }).then((trip) => {
                            Trip_history.findOne({ _id: req.body.trip_id, is_trip_end: 1 }).then(async (trip_history) => {
                                if (!trip) {
                                    trip = trip_history;
                                }
                                if (!trip && ! trip_history) {
                                    trip = await OpenRide.findOne({ _id: req.body.trip_id, is_trip_end: 1  })
                                }
                                if (trip) {
                                    User.findOne({ _id: trip.user_id }).then((user) => {
                                        trip.is_trip_completed = 1;
                                        trip.is_provider_invoice_show = 1;
                                        trip.save(function () {
                                            provider.completed_request = provider.completed_request + 1;
                                            provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)
                                            if(trip.openride && provider.is_trip.length >0){
                                                provider.is_trip = []
                                            }
                                            provider.save();

                                            Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
                                                var email_notification = setting_detail.email_notification;
                                                if (email_notification == true) {
                                                    allemails.sendProviderInvoiceEmail(req, provider, trip, tripservice, user);
                                                }
                                            })

                                            if (trip.trip_type == Number(constant_json.TRIP_TYPE_DISPATCHER) || trip.trip_type == Number(constant_json.TRIP_TYPE_HOTEL) || trip.trip_type == Number(constant_json.TRIP_TYPE_PROVIDER) || trip.trip_type == Number(constant_json.TRIP_TYPE_GUEST_TOKEN)) {
                                                user.current_trip_id = null;
                                                user.save();
                                            }
                                            if(!trip.openride){

                                                Trip.findOneAndRemove({ _id: req.body.trip_id }).then((deleted_trip) => {
                                                    if (deleted_trip) {
                                                        var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                                        trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                                        trip_history_data.save(function (error) {
                                                            console.log(error)
                                                            res.json({ success: true });
                                                        });
                                                    } else {
                                                        res.json({ success: true });
                                                    }
                                                }, (error) => {
                                                    console.log(error)
                                                });
                                            }else{
                                                res.json({ success: true });
                                            }
                                        });
                                    });
                                } else {
                                    res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_IS_NOT_END });
                                }
                            });
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });
                        });
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.user_submit_invoice = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {
                    if (req.body.token != null && user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        Trip.findOne({ _id: req.body.trip_id, is_trip_end: 1}).then((trip) => {
                            Trip_history.findOne({ _id: req.body.trip_id, is_trip_end: 1}).then(async (trip_history) => {
                                if (!trip) {
                                    trip = trip_history;
                                }
                                console.log('user_submit_invoice')
                                if (!trip && ! trip_history) {
                                    trip = await OpenRide.findOne({ _id: req.body.trip_id })
                                }
                                if (trip) {
                                    let userdetails_index = -1 
                                    let trip_payment
                                    if (trip.openride) {
                                        userdetails_index = trip.user_details.findIndex(item => (item.user_id).toString() == (user._id).toString())
                                        trip_payment = trip.user_details[userdetails_index].payment_status
                                    } else {
                                        trip_payment = trip.payment_status
                                    }

                                    if(trip_payment == PAYMENT_STATUS.COMPLETED){
                                        if (trip.openride) {
                                            if (userdetails_index != -1) {
                                                trip.user_details[userdetails_index].is_user_invoice_show = 1
                                            }
                                        } else {
                                            trip.is_user_invoice_show = 1;   
                                        }
                                        trip.save(function () {
                                            Provider.findOne({ _id: trip.provider_id }).then((provider) => {
                                                Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
                                                    var email_notification = setting_detail.email_notification;
                                                    if (email_notification == true) {
                                                        // console.log("mail sent user invoice");
                                                        allemails.sendUserInvoiceEmail(req, user, provider, trip, tripservice);
                                                    }
                                                })
                                            })
    
                                            user.current_trip_id = null;
                                            user.save();
                                            if(!trip.openride){
                                                Trip.findOneAndRemove({ _id: req.body.trip_id }).then((deleted_trip) => {
                                                    if (deleted_trip) {
                                                        var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                                                        trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                                                        trip_history_data.save().then(() => {
                                                            res.json({ success: true });
                                                        }, (error) => {
                                                            console.log(error)
                                                        });
                                                    } else {
                                                        res.json({ success: true });
                                                    }
                                                }, (error) => {
                                                    console.log(error)
                                                });
                                            }else{
                                                res.json({ success: true });
                                            }
                                        });
                                    }else{
                                        res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_PAYMENT_IS_PENDING });
                                    }
                                   
                                } else {
                                    res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_IS_NOT_END });
                                }
                            });
                        });
                    }

                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};
////////////// PROVIDER RATING SERVICE  //////////////////////////

exports.provider_rating = async function (req, res) {
    try {
        let params_array = [{ name: 'provider_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let provider = await Provider.findOne({ _id: req.body.provider_id })
        if (!provider) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            return
        }

        if (req.body.token != null && provider.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return
        }
        let trip = await Trip.findOneAndUpdate({ _id: req.body.trip_id, is_trip_end: 1 }, req.body, { new: true }) || await Trip_history.findOne({ _id: req.body.trip_id, is_trip_end: 1 })
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND })
            return
        }

        if (trip.is_trip_end == 0) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_IS_NOT_END });
            return
        }
        let user = await User.findOne({ _id: trip.user_id })


        let country = await Country.findOne({countryname:user.country})
        if(!country){
            res.json({ success: false, error_code: error_message.ERROR_CODE_COUNTRY_NOT_FOUND });
            return;
        }

        let rating = req.body.rating;
        let old_rate = user.rate;
        let old_rate_count = user.rate_count;
        let new_rate_counter = (old_rate_count + 1);
        let new_rate = ((old_rate * old_rate_count) + rating) / new_rate_counter;
        user.rate = new_rate;
        user.rate_count++;
        await User.updateOne({ _id: user._id }, user.getChanges())
        let review = await Reviews.findOne({ trip_id: trip._id })
        if (!review) {

            let reviews = new Reviews({
                trip_id: trip._id,
                trip_unique_id: trip.unique_id,
                userRating: 0,
                userReview: "",
                providerRating: rating,
                providerReview: req.body.review,
                provider_id: trip.confirmed_provider,
                user_id: trip.user_id,

                country_id: trip.country_id,
                city_id: trip.city_id,

            });
            await reviews.save();
        } else {
            review.providerRating = rating;
            review.providerReview = req.body.review;
            await Reviews.updateOne({ _id: review._id }, review.getChanges())
        }
        // trip.is_user_rated = 1;
    
        if(country?.user_redeem_settings[0]?.is_user_redeem_point_reward_on && (country?.user_redeem_settings[0]?.user_review_redeem_point > 0)){
            let total_redeem_point = utils.add_redeem_point_history(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id,country._id,constant_json.REVIEW_REDEEM_POINT,user.wallet_currency_code,`Get redeem point via reviews: ${trip.unique_id}`,country?.user_redeem_settings[0]?.user_review_redeem_point, user?.total_redeem_point,constant_json.ADD_REDEEM_POINT,trip.unique_id)
            user.total_redeem_point = total_redeem_point
            user.save()
        }
        await Trip_history.findByIdAndUpdate(trip._id,{is_user_rated:1},{new:true})
        // await Trip.updateOne({ _id: trip._id }, trip.getChanges())
        Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
            var email_notification = setting_detail.email_notification;
            if (email_notification == true) {
                // console.log("mail sent user invoice");
                allemails.sendUserInvoiceEmail(req, user, provider, trip, tripservice);
            }
        })
        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_FOR_PROVIDER_GIVE_RATING_SUCCESSFULLY
        });
    } catch (error) {
        utils.error_response(error, req, res)
    }
};
////////////////////// USER  RATING  SERVICE/////////////

exports.user_rating = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        let country = await Country.findOne({countryname:user.country})
        if(!country){
            res.json({ success: false, error_code: error_message.ERROR_CODE_COUNTRY_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }
        let trip = await Trip.findOneAndUpdate({ _id: req.body.trip_id, is_trip_end: 1 }, req.body, { new: true }) || await Trip_history.findOne({ _id: req.body.trip_id, is_trip_end: 1 }) ||  await OpenRide.findOneAndUpdate({ _id: req.body.trip_id, is_trip_end: 1 }, req.body, { new: true })
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND })
            return
        }
        if (trip.is_trip_end == 0) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_IS_NOT_END });
            return
        }
        let provider = await Provider.findOne({ _id: trip.confirmed_provider })
        let city = await City.findOne({_id: provider.cityid})
        var rating = req.body.rating;
        var old_rate = provider.rate;
        var old_rate_count = provider.rate_count;
        var new_rate_counter = (old_rate_count + 1);
        // var new_rate = 3;
        var new_rate = ((Number(old_rate) * Number(old_rate_count)) + Number(rating)) / Number(new_rate_counter);
        var is_user_invoice_show = trip.is_user_invoice_show;
        provider.rate = new_rate;
        provider.rate_count++;
        await Provider.updateOne({ _id: provider._id }, provider.getChanges())
        let partner;
        if (provider.provider_type_id != null && provider.provider_type == PROVIDER_TYPE.PARTNER) {
            partner = await Partner.findById(provider.provider_type_id)
            if (!partner) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_DETAIL_NOT_FOUND });
                return;
            }
        }
        let review = await Reviews.findOne({ trip_id: trip._id })

        if (!review) {
            var reviews = new Reviews({
                trip_id: trip._id,
                trip_unique_id: trip.unique_id,
                userRating: rating,
                userReview: req.body.review,
                providerRating: 0,
                providerReview: "",
                provider_id: trip.confirmed_provider,
                user_id: trip.user_id,
                country_id: trip.country_id,
                city_id: trip.city_id,
            });
            await reviews.save();
        } else {
            review.userRating = rating;
            review.userReview = req.body.review;
            await Reviews.updateOne({ _id: review._id }, review.getChanges())
        }
        myAnalytics.insert_daily_provider_analytics(city.timezone, trip.confirmed_provider, TRIP_STATUS.FOR_REDEEM_POINTS, null,country._id,rating);

        trip.is_provider_rated = 1;
        trip.is_user_invoice_show = 1;
        user.completed_request = user.completed_request + 1;
        user.current_trip_id = null;

        if(country?.driver_redeem_settings[0]?.is_driver_redeem_point_reward_on && (country?.driver_redeem_settings[0]?.driver_review_redeem_point > 0)){
            if (provider.provider_type_id != null && provider.provider_type == PROVIDER_TYPE.PARTNER) {
                let total_redeem_point = utils.add_redeem_point_history(constant_json.PARTNER_UNIQUE_NUMBER, partner.unique_id, partner._id,country._id,constant_json.REVIEW_REDEEM_POINT,partner.wallet_currency_code,`Get redeem point via reviews: ${trip.unique_id}`,country?.driver_redeem_settings[0]?.driver_review_redeem_point, partner?.total_redeem_point,constant_json.ADD_REDEEM_POINT,trip.unique_id)
                partner.total_redeem_point = total_redeem_point
                partner.save()
            }else{
                let total_redeem_point = utils.add_redeem_point_history(constant_json.PROVIDER_UNIQUE_NUMBER, provider.unique_id, provider._id,country._id,constant_json.REVIEW_REDEEM_POINT,provider.wallet_currency_code,`Get redeem point via reviews: ${trip.unique_id}`,country?.driver_redeem_settings[0]?.driver_review_redeem_point, provider?.total_redeem_point,constant_json.ADD_REDEEM_POINT,trip.unique_id)
                provider.total_redeem_point = total_redeem_point
                provider.save()
            }
        }
        
        let need_to_move_ride = false
        await User.updateOne({ _id: user._id }, user.getChanges())
        if (trip.openride) {
            await OpenRide.updateOne({ _id: trip._id }, trip.getChanges())
        } else {
            await Trip_history.updateOne({ _id: trip._id }, trip.getChanges())
            need_to_move_ride = true
        }

        Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
            var email_notification = setting_detail.email_notification;
            if (email_notification == true) {
                // console.log("mail sent user invoice");
                allemails.sendUserInvoiceEmail(req, user, provider, trip, tripservice);
            }
        })

        if (need_to_move_ride && is_user_invoice_show == 1) {
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_USER_GIVE_RATING_SUCCESSFULLY
            });
            return;
        }

        if (need_to_move_ride) {
            await utils.move_trip_to_completed(trip._id)
        }



        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_USER_GIVE_RATING_SUCCESSFULLY
        });
    } catch (error) {
        utils.error_response(error, req, res)
    }

}
/////////// USER TRIP DETAIL ///////////////////
exports.user_tripdetail = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((users) => {
                if (users.token != req.body.token) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                } else {
                    let Table 
                    let Table1
                    if (req.body.is_open_ride) {
                        Table = OpenRide
                        Table1 = OpenRide
                    } else {
                        Table = Trip
                        Table1 = Trip_history
                    }
                    Table.aggregate([
                        {$match:  {_id: mongoose.Types.ObjectId(req.body.trip_id)} },
                        {
                            $lookup: {
                                from: 'promo_codes',
                                localField: 'promo_id',
                                foreignField: '_id',
                                as: 'promo_detail'
                            }
                        },
                        {
                            $unwind: {
                            path: "$promo_detail",
                            preserveNullAndEmptyArrays: true
                            }
                        }
                    ]).then((trip) => {
                        Table1.aggregate([
                            {$match:  {_id: mongoose.Types.ObjectId(req.body.trip_id)} },
                            {
                                $lookup: {
                                    from: 'promo_codes',
                                    localField: 'promo_id',
                                    foreignField: '_id',
                                    as: 'promo_detail'
                                }
                            },
                            {
                                $unwind: {
                                path: "$promo_detail",
                                preserveNullAndEmptyArrays: true
                                }
                            }
                        ]).then((trip_history) => {
                            if (trip.length == 0) {
                                trip = trip_history
                            }
                            if (trip.length > 0) {
                                if (trip[0].is_trip_cancelled == 0 || trip[0].is_trip_cancelled_by_user == 0 || trip[0].is_trip_cancelled_by_provider == 0 || trip[0].is_trip_end == 1) {

                                    Trip_Service.findOne({ _id: trip[0].trip_service_city_type_id }).then((tripservice) => {
                                        if (tripservice) {
                                            TripLocation.findOne({ tripID: trip[0]._id }).then((tripLocation) => {

                                                Provider.findOne({ _id: trip[0].confirmed_provider }).then((provider) => {

                                                    res.json({
                                                        success: true,
                                                        message: success_messages.MESSAGE_CODE_HISTORY_DETAIL_GET_SUCCESSFULLY,
                                                        trip: trip[0],
                                                        tripservice: tripservice,
                                                        provider: provider,
                                                        startTripToEndTripLocations: tripLocation.startTripToEndTripLocations
                                                    });
                                                });
                                            });

                                        } else {
                                            res.json({
                                                success: false,
                                                error_code: error_message.ERROR_CODE_TRIP_SERVICE_NOT_FOUND
                                            });

                                        }
                                    });

                                } else {
                                    res.json({
                                        success: false,
                                        error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED
                                    });

                                }
                            } else {

                                res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });


                            }
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });

                        });
                    });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

///////// PROVIDER TRIP DETAIL  //////////////
exports.provider_tripdetail = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            Provider.findOne({ _id: req.body.provider_id }).then((provider) => {
                if (provider.token != req.body.token) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                } else {
                    Trip.findOne({ _id: req.body.trip_id }).then((trip) => {
                        Trip_history.findOne({ _id: req.body.trip_id }).then(async (trip_history) => {
                            if (!trip) {
                                trip = trip_history
                            }
                            if(!trip){
                                trip = await OpenRide.findOne({ _id: req.body.trip_id })
                            }
                            if (trip) {
                                if (trip.is_trip_cancelled == 0 && trip.is_trip_cancelled_by_user == 0 && trip.is_trip_cancelled_by_provider == 0 && trip.is_trip_end == 1) {

                                    Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
                                        if (tripservice) {

                                            TripLocation.findOne({ tripID: trip._id }).then((tripLocation) => {


                                                User.findOne({ _id: trip.user_id }).then((user) => {

                                                    res.json({
                                                        success: true,
                                                        message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_DETAIL_GET_SUCCESSFULLY,
                                                        trip: trip,
                                                        tripservice: tripservice,
                                                        user: user,
                                                        startTripToEndTripLocations: tripLocation.startTripToEndTripLocations
                                                    });
                                                });
                                            });

                                        } else {
                                            res.json({
                                                success: false,
                                                error_code: error_message.ERROR_CODE_TRIP_SERVICE_NOT_FOUND
                                            });

                                        }
                                    });

                                } else {
                                    res.json({
                                        success: false,
                                        error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED
                                    });

                                }
                            } else {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });

                            }
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });
                        });
                    });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

// user_invoice //
exports.user_invoice = async function (req, res) {
    const setting_detail = await Settings.findOne({});

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {
                    if (req.body.token != null && user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        Trip.findOne({ _id: req.body.trip_id }).then((trip) => {
                            Trip_history.findOne({ _id: req.body.trip_id }).then(async (trip_history) => {
                                if (!trip) {
                                    trip = trip_history;
                                }
                                if (!trip && ! trip_history) {
                                    console.log('11111111')
                                    trip = await OpenRide.findOne({ _id: req.body.trip_id })
                                    
                                }
                                if (trip) {
                                    if (trip.is_trip_cancelled == 0 && trip.is_trip_cancelled_by_provider == 0) {
                                        let provider_details
                                        Provider.findOne({ _id: trip.provider_id }).then((provider) => {
                                            if(trip.openride){
                                                provider_details = {
                                                    first_name : trip.provider_details[0].first_name,
                                                    last_name : trip.provider_details[0].last_name,
                                                    picture : trip.provider_details[0].picture
                                                }
                                            }else{
                                                provider_details = {
                                                    first_name: provider.first_name,
                                                    last_name: provider.last_name,
                                                    picture: provider.picture
                                                }
                                            }
                                            

                                            Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {

                                                if (!tripservice) {
                                                    res.json({
                                                        success: false,
                                                        error_code: error_message.ERROR_CODE_TRIP_SERVICE_NOT_FOUND
                                                    });
                                                } else {
                                                    var email_notification = setting_detail.email_notification;
                                                    if (email_notification == true) {
                                                    }

                                                    res.json({
                                                        success: true,
                                                        message: success_messages.MESSAGE_CODE_GET_YOUR_INVOICE_SUCCESSFULLY,
                                                        trip: trip,
                                                        tripservice: tripservice,
                                                        provider_detail: provider_details
                                                    });
                                                }
                                            });
                                        });

                                    } else {
                                        res.json({
                                            success: false,
                                            error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED
                                        });
                                    }
                                } else {

                                    res.json({
                                        success: false,
                                        error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND
                                    });

                                }
                            });
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });
                        });
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

// provider_invoice // 
exports.provider_invoice = async function (req, res) {
    const setting_detail = await Settings.findOne({});

    utils.check_request_params(req.body, [{ name: 'provider_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            Provider.findOne({ _id: req.body.provider_id }).then((provider) => {
                if (provider) {
                    if (req.body.token != null && provider.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                       
                        Trip.findOne({ _id: req.body.trip_id }).then((trip) => {
                            Trip_history.findOne({ _id: req.body.trip_id }).then(async (trip_history) => {
                                if (!trip) {
                                    trip = trip_history;
                                }
                                if (!trip && ! trip_history) {
                                    trip = await OpenRide.findOne({ _id: req.body.trip_id })
                                }
                                if (trip) {
                                    if (trip.is_trip_cancelled == 0 && trip.is_trip_cancelled_by_provider == 0) {

                                        Trip_Service.findOne({ _id: trip.trip_service_city_type_id }).then((tripservice) => {
                                            if (!tripservice) {
                                                res.json({
                                                    success: false,
                                                    error_code: error_message.ERROR_CODE_TRIP_SERVICE_NOT_FOUND
                                                });
                                            } else {
                                                var email_notification = setting_detail.email_notification;
                                                if (email_notification == true) {
                                                }

                                                res.json({
                                                    success: true,
                                                    message: success_messages.MESSAGE_CODE_GET_INVOICE_SUCCESSFULLY,
                                                    trip: trip,
                                                    tripservice: tripservice
                                                });

                                            }
                                        });
                                    } else {
                                        res.json({
                                            success: false,
                                            error_code: error_message.ERROR_CODE_TRIP_IS_ALREADY_CANCELLED
                                        });
                                    }

                                } else {
                                    res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
                                }
                            });
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });
                        });
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};
////////////////////////////////////

////////////////////////// USER SET DESTINATION ///////////////////////////////////////
exports.user_setdestination = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, {
        name: 'trip_id',
        type: 'string'
    }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {
                    if (user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        Trip.findOne({
                            _id: req.body.trip_id,
                            is_trip_cancelled: 0,
                            is_trip_cancelled_by_provider: 0,
                            user_id: req.body.user_id,
                            is_trip_end: 0
                        }).then((trip) => {
                            if (trip) {

                                Provider.findOne({ _id: trip.confirmed_provider }).then((providers) => {
                                    var device_token = providers.device_token;
                                    var device_type = providers.device_type;
                                    //////////////////////  PUSH NOTIFICATION ///////////
                                    utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, device_type, device_token, push_messages.PUSH_CODE_FOR_SET_DESTINATION, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                });
                                trip.destination_address = req.body.destination_address;
                                trip.destinationLocation = [req.body.d_latitude, req.body.d_longitude];
                                trip.save().then(() => {
                                    utils.update_request_status_socket(trip._id);
                                    res.json({
                                        success: true,
                                        message: success_messages.MESSAGE_CODE_SET_DESTINATION_SUCCESSFULLY,
                                        destinationLocation: trip.destinationLocation
                                    });
                                });
                            } else {
                                res.json({ success: false, error_code: error_message.ERROR_CODE_DESTINATION_NOT_SET });
                            }
                        }, (err) => {
                            console.log(err);
                            res.json({
                                success: false,
                                error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                            });
                        });
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                }
            }, (err) => {
                console.log(err);
                res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG
                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};
// getgooglemappath

exports.getgooglemappath = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }], function (response) {
        if (response.success) {
            TripLocation.findOne({ tripID: req.body.trip_id }).then((tripLocation) => {

                if (tripLocation) {
                    res.json({ success: true, triplocation: tripLocation });
                } else {
                    res.json({ success: false });
                }

            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};
//setgooglemappath  
exports.setgooglemappath = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }], function (response) {
        if (response.success) {
            TripLocation.findOne({ tripID: req.body.trip_id }).then((tripLocation) => {

                tripLocation.googlePickUpLocationToDestinationLocation = req.body.googlePickUpLocationToDestinationLocation;
                tripLocation.googlePathStartLocationToPickUpLocation = req.body.googlePathStartLocationToPickUpLocation;
                tripLocation.save().then(() => {
                    res.json({ success: true });

                });
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};


exports.check_trip_inside_zone_queue = function (city_id, latitude, longitude, res_data) {
    CityZone.find({ cityid: city_id }, function (error, zone_queue_list) {
        if (zone_queue_list.length > 0) {
            var is_trip_inside_zone_queue = false;
            zone_queue_list.forEach(function (zone_queue_data, index) {
                var geo = geolib.isPointInside(
                    { latitude: latitude, longitude: longitude },
                    zone_queue_data.kmlzone
                );
                if (geo) {
                    is_trip_inside_zone_queue = true;
                }
                if (index == zone_queue_list.length - 1) {
                    res_data({ is_trip_inside_zone_queue: is_trip_inside_zone_queue, zone_queue_id: zone_queue_data._id });
                }
            });
        } else {
            res_data({ is_trip_inside_zone_queue: false, zone_queue_id: null });
        }
    });
};



exports.twilio_voice_call = async function (req, res) {
    const setting_detail = await Settings.findOne({});
    let { body } = req;
    let { trip_id } = body;

    let trip = await Trip.findOne({ _id: trip_id }) || await OpenRide.findOne({ _id: trip_id });
    if (!trip) {
        return res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
    }

    let user_id = trip.user_id;
    let provider_id = trip.confirmed_provider || body.provider_id;
    let twilio_account_sid = setting_detail.twilio_account_sid;
    let twilio_auth_token = setting_detail.twilio_auth_token;
    let twilio_number = setting_detail.twilio_number;
    let client = require('twilio')(twilio_account_sid, twilio_auth_token);
    let twiml_url = setting_detail.twiml_url;

    if (req.body.type == 1) {
        Provider.findOne({ _id: provider_id }, function (err, provider) {
            User.findOne({ _id: user_id }, function (err, user) {

                if (provider) {
                    var provider_number = provider.country_phone_code + provider.phone;
                    var user_number = user.country_phone_code + user.phone;

                    twiml_url = twiml_url + "?to=" + provider_number;
                    client.calls.create({
                        url: twiml_url,
                        to: user_number,
                        from: twilio_number
                    }, function (err) {
                        if (err) {
                            console.log(err)
                            res.json({ "success": false, error_code: error_message.ERROR_CODE_TWILIO_SERVICE_NOT_AVAILABLE });

                        } else {
                            res.json({ "success": true });
                        }
                    });
                }
            });
        });
    } else if (req.body.type == 3) {
        let Table
        let id
        if (req.body.is_user) {
            Table = User
            id = trip.user_id
        }
        else {
            Table = Provider
            id = trip.confirmed_provider || body.provider_id;
        }
        Table.findOne({ _id: id }, function (err, user) {
            var support_number = req.body.support_phone_user
            var user_number = user.country_phone_code + user.phone;
            twiml_url = twiml_url + "?to=" + user_number;

            client.calls.create({
                url: twiml_url,
                to: support_number,
                from: twilio_number
            }, function (err) {
                if (err) {
                    console.log(err)
                    res.json({ "success": false, error_code: error_message.ERROR_CODE_TWILIO_SERVICE_NOT_AVAILABLE });
                } else {
                    res.json({ "success": true });
                }
            });
        });
    }
    else {
        User.findOne({ _id: user_id }, function (err, user) {
            Provider.findOne({ _id: provider_id }, function (err, provider) {
                if (user) {
                    var provider_number = provider.country_phone_code + provider.phone;
                    var user_number = user.country_phone_code + user.phone;
                    twiml_url = twiml_url + "?to=" + user_number;

                    client.calls.create({
                        url: twiml_url,
                        to: provider_number,
                        from: twilio_number
                    }, function (err) {
                        if (err) {
                            console.log(err)
                            res.json({ "success": false, error_code: error_message.ERROR_CODE_TWILIO_SERVICE_NOT_AVAILABLE });
                        } else {
                            res.json({ "success": true });
                        }
                    });
                }
            });
        });
    }
}



exports.refund_amount_in_wallet = function (req, res) {
    if (typeof req.session.userid == 'undefined') {
        return res.json({ success: false });
    }
    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }], async function (response) {
        if (!response.success) {
            return res.json({ success: false, error_code: response.error_code, error_description: response.error_description });
        }
        let trip = await Trip.findOne({ _id: req.body.trip_id });
        let amount = Number(req.body.amount);
        if (!trip) {
            trip = await Trip_history.findOne({ _id: req.body.trip_id });
        }
        if (!trip) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
        }
        if (trip.total < amount) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_CAN_NOT_REFUND_MORE_THAN_TRIP_AMOUNT });
        }
        let user_data = await User.findById(trip.user_id);
        let status = constant_json.ADD_WALLET_AMOUNT;
        let total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user_data.unique_id, user_data._id, user_data.country_id, user_data.wallet_currency_code, user_data.wallet_currency_code,
            1, Math.abs(amount), user_data.wallet, status, constant_json.ADDED_BY_ADMIN, "Refund Of This Trip : " + trip.unique_id);
        user_data.wallet = total_wallet_amount;
        await user_data.save();

        trip.refund_amount += amount;
        trip.is_amount_refund = true;
        await trip.save();

        message = admin_messages.success_message_refund;
        res.json({ success: true });
    });
}

exports.refund_amount_in_card = function (req, res) {
    if (typeof req.session.userid == 'undefined') {
        return res.json({ success: false });
    }
    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }], async function (response) {
        if (!response.success) {
            return res.json({ success: false, error_code: response.error_code, error_description: response.error_description });
        }
        let trip = await Trip.findOne({ _id: req.body.trip_id });
        if (!trip) {
            trip = await Trip_history.findOne({ _id: req.body.trip_id });
        }
        if (!trip) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
        }

        // ###payment
        if (!trip.payment_gateway_type || trip.payment_gateway_type == PAYMENT_GATEWAY.stripe) {
            cards.refund_payment(trip.payment_intent_id, PAYMENT_GATEWAY.stripe);
        } else if (trip.payment_gateway_type == PAYMENT_GATEWAY.paystack) {
            cards.refund_payment(trip.payment_intent_id, PAYMENT_GATEWAY.paystack);
        } else if (trip.payment_gateway_type == PAYMENT_GATEWAY.payu) {
            cards.refund_payment(trip._id, PAYMENT_GATEWAY.payu);
        }
        trip.refund_amount += trip.card_payment;
        trip.is_amount_refund = true;
        await trip.save();

        message = admin_messages.success_message_refund;
        res.json({ success: true });
    });
}

exports.pay_by_other_payment_mode = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'trip_id', type: 'string' }], async function (response) {
        if (!response.success) {
            return res.json({ success: false, error_code: response.error_code, error_description: response.error_description });
        }
        let trip = await Trip.findOne({ _id: req.body.trip_id });

        if (!trip) {
            trip = await Trip_history.findOne({ _id: req.body.trip_id });
        }

        if (!trip) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_NOT_FOUND });
        }

        let city = await City.findOne({ _id: trip.city_id });
        let provider = await Provider.findOne({ _id: trip.confirmed_provider });
        let user = await User.findOne({ _id: trip.user_id });
        let corporate = await Corporate.findOne({ _id: trip.user_type_id });

        let is_main_user = true;
        let split_payment_index = null;
        trip.split_payment_users.forEach((split_payment_user_detail, index) => {
            if (split_payment_user_detail.user_id.toString() == req.body.user_id.toString()) {
                is_main_user = false;
                split_payment_index = index;
            }
        })

        if (city.is_payment_mode_cash == 1) {
            var wallet_payment = 0;
            var remaining_payment = 0;

            if (is_main_user) {
                var total = trip.remaining_payment;
                var total_after_wallet_payment = trip.remaining_payment;
                total_after_wallet_payment = Number((total_after_wallet_payment).toFixed(2));
                wallet_payment = Number((wallet_payment).toFixed(2));
                remaining_payment = total - wallet_payment;
                remaining_payment = Number((remaining_payment).toFixed(2));
                trip.wallet_payment += wallet_payment;
                trip.total_after_wallet_payment = total_after_wallet_payment;
                trip.remaining_payment = remaining_payment;
                trip.payment_status = PAYMENT_STATUS.COMPLETED;

                trip.payment_mode = constant_json.PAYMENT_MODE_CASH;
                trip.provider_have_cash = remaining_payment;
                trip.pay_to_provider = trip.provider_service_fees - trip.provider_have_cash;

                trip.is_paid = 1;
                trip.is_pending_payments = 0;
                trip.cash_payment = remaining_payment;
                trip.remaining_payment = 0;
                await trip.save()
            } else {
                remaining_payment = trip.split_payment_users[split_payment_index].remaining_payment;
                trip.split_payment_users[split_payment_index].remaining_payment = 0;
                trip.split_payment_users[split_payment_index].cash_payment = remaining_payment;
                trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                trip.split_payment_users[split_payment_index].payment_mode = constant_json.PAYMENT_MODE_CASH;
                trip.cash_payment = trip.cash_payment + remaining_payment;
                trip.provider_have_cash = trip.provider_have_cash + remaining_payment;
                trip.markModified('split_payment_users');
                trip.save();
            }
        } else {
            var wallet_payment = 0;
            var remaining_payment = 0;
            if (is_main_user) {
                var total = trip.remaining_payment;
                var total_after_wallet_payment = trip.remaining_payment;

                wallet_payment = total_after_wallet_payment;
                if (trip.trip_type == constant_json.TRIP_TYPE_CORPORATE && corporate) {
                    var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, corporate.unique_id, corporate._id, null,
                        corporate.wallet_currency_code, trip.currencycode,
                        trip.wallet_current_rate, wallet_payment, corporate.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                    corporate.wallet = total_wallet_amount;
                    await corporate.save();
                    user.corporate_wallet_limit = user.corporate_wallet_limit - wallet_payment;
                } else {
                    var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, null,
                        user.wallet_currency_code, trip.currencycode,
                        trip.wallet_current_rate, wallet_payment, user.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                    user.wallet = total_wallet_amount;
                }
                await user.save();

                total_after_wallet_payment = total_after_wallet_payment - wallet_payment;

                total_after_wallet_payment = Number((total_after_wallet_payment).toFixed(2));
                wallet_payment = Number((wallet_payment).toFixed(2));
                remaining_payment = total - wallet_payment;
                remaining_payment = Number((remaining_payment).toFixed(2));
                trip.wallet_payment += wallet_payment;
                trip.total_after_wallet_payment = total_after_wallet_payment;
                trip.remaining_payment = remaining_payment;
                trip.payment_status = PAYMENT_STATUS.COMPLETED;

                trip.is_paid = 1;
                trip.is_pending_payments = 0;
                trip.card_payment = 0;
                await trip.save()
            } else {
                remaining_payment = trip.split_payment_users[split_payment_index].remaining_payment;
                User.findOne({ _id: split_user.user_id }).then((split_user_detail) => {
                    var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, split_user_detail.unique_id, split_user_detail._id, null,
                        split_user_detail.wallet_currency_code, trip.currencycode,
                        trip.wallet_current_rate, remaining_payment, split_user_detail.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.PAID_TRIP_AMOUNT, "Charge Of This Trip : " + trip.unique_id);
                    split_user_detail.wallet = total_wallet_amount;
                    split_user_detail.save();

                    trip.split_payment_users[split_payment_index].remaining_payment = 0;
                    trip.split_payment_users[split_payment_index].wallet_payment = remaining_payment;
                    trip.split_payment_users[split_payment_index].payment_status = PAYMENT_STATUS.COMPLETED;
                    trip.wallet_payment = trip.wallet_payment + remaining_payment;
                    trip.markModified('split_payment_users');
                    trip.save();
                });
            }
        }

        await utils.trip_provider_profit_card_wallet_settlement(trip, city, provider);
        utils.update_request_status_socket(trip._id);
        if (trip.payment_status == PAYMENT_STATUS.COMPLETED) {
            Trip.findOneAndRemove({ _id: trip._id }).then((deleted_trip) => {
                if (deleted_trip) {
                    var trip_history_data = new Trip_history(JSON.parse(JSON.stringify(deleted_trip)));
                    trip_history_data.split_payment_users = deleted_trip.split_payment_users;
                    trip_history_data.save(function () {
                        return res.json({
                            success: true,
                            message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                            payment_status: trip.payment_status
                        });
                    });
                } else {
                    return res.json({
                        success: true,
                        message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                        payment_status: trip.payment_status
                    });
                }
            });
        } else {
            return res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOUR_TRIP_COMPLETED_SUCCESSFULLY,
                payment_status: trip.payment_status
            });
        }
    });
}

exports.provider_set_trip_stop_status = async function (req, res) {
    try {
        let params_array = [
            { name: 'provider_id', type: 'string' },
            { name: 'token', type: 'string' },
            { name: 'trip_id', type: 'string' },
            { name: 'address', type: 'string' },
            { name: 'latitude', type: 'number' },
            { name: 'longitude', type: 'number' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let provider = await Provider.findOne({ _id: req.body.provider_id })
        if (!provider) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_PROVIDER_DETAIL_NOT_FOUND });
            return;
        }
        if (req.body.token != null && provider.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }
        let trip = await Trip.findOne({ _id: req.body.trip_id, confirmed_provider: req.body.provider_id })
        if (!trip) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_TRIP_FOUND });
            return;
        }
        if (trip.is_trip_cancelled != 0 && trip.is_trip_cancelled_by_provider != 0) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_MISMATCH_PROVIDER_ID_OR_TRIP_ID });
            return;
        }
        let now = new Date();
        trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].address = req.body.address;
        trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].location = [Number(req.body.latitude), Number(req.body.longitude)];
        trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].arrived_time = now;

        var time_diff = Math.abs(utils.getTimeDifferenceInMinute(now, trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].start_time));
        if (time_diff < 0) {
            time_diff = 0
        }
        trip.actual_destination_addresses[trip.actual_destination_addresses.length - 1].total_time = time_diff;
        trip.markModified('actual_destination_addresses');
        await Trip.updateOne({ _id: trip._id }, trip.getChanges())

        utils.update_request_status_socket(trip._id);
        res.json({
            success: true,
            message: success_messages.MESSAGE_CODE_FOR_PROVIDER_YOU_SET_TRIP_STATUS_SUCCESSFULLY,
            trip: trip
        });
        return;
    } catch (err) {
        console.log("exports.provider_set_trip_stop_status")
        utils.error_response(err, req, res)
    }
};

exports.check_trip_inside_zone_queue_async = function (city_id, latitude, longitude) {
    return new Promise(async (resolve, reject) => {
        let zone_queue_list = await CityZone.find({ cityid: city_id })
        if (zone_queue_list.length == 0) {
            resolve({ is_trip_inside_zone_queue: false, zone_queue_id: null });
            return;
        }

        let is_trip_inside_zone_queue = false;
        zone_queue_list.forEach(function (zone_queue_data, index) {
            let geo = geolib.isPointInside(
                { latitude: latitude, longitude: longitude },
                zone_queue_data.kmlzone
            );
            if (geo) {
                is_trip_inside_zone_queue = true;
            }
            if (index == zone_queue_list.length - 1) {
                resolve({ is_trip_inside_zone_queue: is_trip_inside_zone_queue, zone_queue_id: zone_queue_data._id });
                return;
            }
        });
    });
};
/////////////// USER CHANGE PAYMENT TYPE  
exports.change_paymenttype = function (req, res) {

    utils.check_request_params(req.body, [{name: 'trip_id', type: 'string'}], function (response) {
        if (response.success) {
            User.findOne({_id: req.body.user_id}).then((user) => { 
                if (user)
                {
                    if (req.body.token != null && user.token != req.body.token) {
                        res.json({success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN});
                    } else
                    {
                        var payment_type = req.body.payment_type;
                        if (payment_type == Number(constant_json.PAYMENT_MODE_CARD)) {
                            Trip.findOne({_id: req.body.trip_id}).then(async (trip) => { 
                                if(!trip){
                                    trip = await OpenRide.findOne({_id: req.body.trip_id})
                                }
                                var user_id = trip.user_id;
                                if(trip.trip_type == constant_json.TRIP_TYPE_CORPORATE){
                                    user_id = trip.user_type_id;
                                }
                               
                                Card.find({user_id: user_id, payment_gateway_type: trip.payment_gateway_type}).then((card) => {
                                    if (card.length == 0 && (trip.payment_gateway_type !== PAYMENT_GATEWAY.payu
                                    &&  trip.payment_gateway_type !== PAYMENT_GATEWAY.paypal) && (trip.payment_gateway_type !== PAYMENT_GATEWAY.razorpay)) {
                                        res.json({success: false, error_code: error_message.ERROR_CODE_ADD_CREDIT_CARD_FIRST});
                                    } else {
                                            trip.payment_mode = req.body.payment_type;
                                            trip.save();
                                            Provider.findOne({_id: trip.confirmed_provider}).then((provider) => { 
                                               if(provider) {
                                                   var device_token = provider.device_token;
                                                   var device_type = provider.device_type;
                                                   utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, device_type, device_token, push_messages.PUSH_CODE_FOR_PAYMENT_MODE_CARD, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                               }
                                                utils.update_request_status_socket(trip._id);
                                                res.json({success: true, message: success_messages.MESSAGE_CODE_YOUR_PAYMEMT_MODE_CHANGE_SUCCESSFULLY});
                                            });
                                    }
                                });

                            });
                        } else {
                            Trip.findOne({_id: req.body.trip_id}).then(async (trip) => { 
                                if(!trip){
                                    trip = await OpenRide.findOne({_id: req.body.trip_id})
                                }
                                City.findOne({_id: trip.city_id}).then((city_detail) => {
                                    Provider.findOne({_id: trip.confirmed_provider}).then((provider) => { 
                                        if(provider && city_detail && city_detail.is_check_provider_wallet_amount_for_received_cash_request && city_detail.provider_min_wallet_amount_set_for_received_cash_request > provider.wallet){
                                            return res.json({
                                                success: false,
                                                error_code: error_message.ERROR_CODE_CAN_NOT_CHANGE_PAYMENT_MODE
                                            })
                                        }
                                        trip.payment_mode = req.body.payment_type;
                                        trip.save();
                                        if(provider) {
                                            var device_token = provider.device_token;
                                            var device_type = provider.device_type;
                                            utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, device_type, device_token, push_messages.PUSH_CODE_FOR_PAYMENT_MODE_CASH, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                                        }
                                        utils.update_request_status_socket(trip._id);
                                        res.json({success: true, message: success_messages.MESSAGE_CODE_YOUR_PAYMEMT_MODE_CHANGE_SUCCESSFULLY});
                                    });
                                });
                            });
                        }
                    }
                } else
                {
                    res.json({success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND});
                }

            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });

};

exports.get_cancellation_reason = async function (req, res) {
    try {
        let params_array = [{ name: "user_type", type: 'number' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let lang = req.body.lang;
        if (lang) {
            let langIndex = (await Languages.find({})).findIndex(value => value.code == req.body.lang)
            lang = langIndex == -1 ? 0 : langIndex
        }
        let list = await Cancel_reason.aggregate([{ $match: { user_type: req.body.user_type } }, { $group: { _id: null, reason_list: { $push: { $ifNull: [{ $arrayElemAt: ["$reason", Number(lang)] }, { $ifNull: [{ $arrayElemAt: ["$reason", 0] }, ""] }] } } } }])
        res.json({ success: true, reasons: list[0] ?  list[0].reason_list : [] })
    } catch (error) {
        utils.error_response(error, req, res)
    }
}

// exports.get_open_ride_users_detail = async function(req,res){
//     try{
//         // params = type_id , token ,trip id,

//        let params_array = [{name: "type_id", type: "string"},{ name: "token", type: "string" },{name:"type",type:"number"},{name:"trip_id",type:"string"}]
//     //    let params_response = await utils.check_request_params_async(req.body,params_array)
//     //    if (!params_response.success) {
//     //     return res.json(params_response)
//     //    }

//        let type = req.body.type
//        let Table;

//        switch (type) {
//            case TYPE_VALUE.USER:
//                Table = User
//                break;
//            case TYPE_VALUE.PROVIDER:
//                Table = Provider
//                break;
//         //    case TYPE_VALUE.PARTNER:
//         //        Table = Partner
//         //        break;

//            default:
//                Table = Provider
//                break;
//        }

//        let type_detail = await Table.findById(req.body.type_id)

//        if(!type_detail){
//         return res.json({success:false,error_code:error_message.ERROR_CODE_NOT_GET_YOUR_DETAIL});
//        }
//        if (type_detail.token !== req.body.token) {
//         return  res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN});    
//         }

//         // find trip by trip_id
//         let stage_1 = {
//         $match : mongoose.Schema.ObjectId(req.body.trip_id)
//         } 
         
//         // lookup user_ids array
//         let stage_2 = {
//             $lookup:
//             {
//                 from: "users",
//                 localField: "user_ids",
//                 pipeline: [{ $project: { 
//                     _id: 1, 
//                     email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1,
//                     unique_id: 1, 
//                     // wallet_currency_code: 1 ,
//                     first_name:1,
//                     last_name:1,
//                     email:1,
//                     country_phone_code,
//                     phone:1,
//                 } }],
//                 foreignField: "_id",
//                 as: "user_ids"
//             }
//         }
        
        
//         // destructure array of user_ids 
//         let stage_3 = {
            
//                 $unwind: "$user_ids"
              
//         }
        
//         // replace the root array with user_ids
//          let stage_4 = {
//                $replaceRoot: {
//                  newRoot: "$user_ids"
//                }
//              }
                          
//         let user_details = await Open_Ride.aggregate([stage_1,stage_2,stage_1,stage_4])

//         if(user_details.length == 0 || !user_details){
//           return  res.json({success:false,error_code:error_message.ERROR_CODE_DETAIL_NOT_FOUND})

//         }

//         res.json({ success: true, user_details })

//     }catch(error){
//         utils.error_response(error,res)

//     }

// }

exports.applepay_web_key = async (req, res) => {
    try {
        let fs = require('fs')
        let setting_detail = await Settings.findOne({},{is_production:1})
        let file;
        if (setting_detail?.is_production) {
            file = fs.readFileSync('data/apple_pay/live/apple-developer-merchantid-domain-association')
        } else {
            file = fs.readFileSync('data/apple_pay/developer/apple-developer-merchantid-domain-association')
        }
        res.send(file)
    } catch (error) {
        utils.error_response(error, req, res)
    }
}

exports.trip_remaning_payment = (req, res) => {
    let pramas_array = [{ name: "trip_id", type: 'string' }, { name: 'token', type: 'string' }, { name: 'user_id', type: 'string' }]
    utils.check_request_params(req.body, pramas_array, async (response) => {
        try {
            if (!response.success) {
                res.json(response)
                return
            }
            let user = await User.findOne({_id:req.body.user_id})
            if (user && user.token !== req.body.token) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN});
                return
            }
            const stripe = require('stripe')(setting_detail.stripe_secret_key)
            let trip = await Trip.findOne({ _id: req.body.trip_id })
            if (!trip) {
                trip = await Trip_history.findOne({ _id: req.body.trip_id })
            }
            let metadata_object = {
                is_apple_pay:true,
                is_tip_payment: req.body?.is_tip,
                is_split_payment:req.body?.is_split_payment,
                trip_id:req.body.trip_id,
                user_id:req.body.user_id,
                is_cancel_trip:req.body?.is_cancel_trip
            }
            // FOR NORMAL TRIP
            let amount = trip.remaining_payment
            // FOR TIP
            if(req.body.is_tip){
                amount = req.body.amount
            }
            // for cancel charges
            if(req.body.is_cancel_trip){
                amount = trip.total
            }
            // FOR SPLIT USER
            if(trip.split_payment_users && trip.split_payment_users.length > 0 && req.body.is_split_payment){
                var find_split_user_index = trip.split_payment_users.findIndex(value=> (value.user_id).toString() == req.body.user_id)
                if( find_split_user_index == -1){
                    res.json({ success: false, error_code: error_message.ERROR_CODE_PAYMENT_FAILED })
                    return
                }                
                amount = trip.split_payment_users[find_split_user_index].remaining_payment
            } 
            if (amount > 0) {
                let currencycode = trip.currencycode ? trip.currencycode : setting_detail.adminCurrencyCode
                let paymentIntent = await stripe.paymentIntents.create({
                    amount: Math.round((amount * 100)),
                    currency: currencycode,
                    metadata: metadata_object,
                })
                if (paymentIntent) {
                    let country = await Country.findOne({ _id: trip.country_id }, { alpha2: 1, currencycode: 1 })
                    if(req.body.is_split_payment){
                        trip.split_payment_users[find_split_user_index].payment_intent_id = paymentIntent.id
                        trip.markModified('split_payment_users');
                    }else if(req.body.is_tip){
                        trip.tip_payment_intent_id = paymentIntent.id
                    }else{
                        console.log("below code");
                        trip.payment_intent_id = paymentIntent.id;
                    }
                    await trip.save()
                    res.json({
                        success: true,
                        client_secret: paymentIntent.client_secret,
                        total_amount: amount,
                        country_code: country.alpha2,
                        currency_code: country.currencycode
                    });
                }else{
                    res.json({ success: false, error_code: error_message.ERROR_CODE_PAYMENT_FAILED })
                    return
                }
            } else {
                res.json({ success: false, error_code: error_message.ERROR_CODE_TRIP_PAYMENT_ALREADY_COMPLETED })
            }
        } catch (error) {
            if(error.raw.code){
                res.json({ success: false, error_message:error.raw.message})
                return
            }
            utils.error_response(error, req, res)
        }
    })
}

exports.apple_pay_webhooks = async (req, res) => {
    let body = req.body.data?.object
    console.log(body);
    let metadata = body?.metadata
    req.body.trip_id = metadata?.trip_id
    req.body.user_id = metadata?.user_id
    req.body.is_apple_pay = metadata?.is_apple_pay
    req.body.type = metadata?.type
    req.body.payment_intent_id = body?.payment_intent
    if (body.status != "succeeded") {
        res.json({ success: false, error_code: error_message.ERROR_CODE_PAYMENT_FAILED })
        return
    }
    if (metadata?.is_apple_pay) {
        if (metadata?.is_tip_payment) {
            await exports.pay_tip_payment(req, res)
            return
        } else if (metadata?.is_wallet_amount) {
            await user_controller.add_wallet_amount(req, res)
            return
        } else {
            // trip, spilt and cacellation charges payment
            await exports.pay_stripe_intent_payment(req, res)
            return
        }
    }else{
        res.json({success:true})
    }
}

exports.fixed_old_trip = async (req, res) => {
    let Trip = require('mongoose').model('Trip');
    let user_lookup = {
        $lookup: {
            from: 'users',
            localField: 'user_id',
            foreignField: '_id',
            pipeline: [{ $project: { unique_id: 1 } }],
            as: 'user_detail'
        }
    }
    let provider_lookup = {
        $lookup: {
            from: 'providers',
            localField: 'current_provider',
            pipeline: [{ $project: { unique_id: 1, phone: 1, country_phone_code: 1 } }],
            foreignField: '_id',
            as: 'provider_details'
        }
    }
    let vehicle_type_lookup = {
        $lookup: {
            from: 'types',
            localField: 'type_id',
            foreignField: '_id',
            pipeline: [{ $project: { typename: 1 } }],
            as: 'vehicle_type_details'
        }
    }
    let trip_history_list = await Trip.aggregate([{ $project: { _id: 1, user_id: 1, current_provider: 1, type_id: 1 } }, user_lookup, provider_lookup, vehicle_type_lookup])
    let i = 0;
    for (let trip of trip_history_list) {
        await Trip.findByIdAndUpdate(
            trip._id,
            {
                user_unique_id: trip.user_detail[0]?.unique_id || 0,
                typename: trip.vehicle_type_details[0]?.typename || "anonymous",
                provider_unique_id: trip.provider_details[0]?.unique_id || 0,
                provider_phone: trip.provider_details[0]?.phone || "0000000000",
                provider_phone_code: trip.provider_details[0]?.country_phone_code || "00"
            }
        )
        console.log(`${++i}/${trip_history_list.length}`)
    }
    res.json({ success: true })
}

exports.fixed_old_trip_history = async (req, res) => {
    let Trip_history = require('mongoose').model('Trip_history');
    let user_lookup = {
        $lookup: {
            from: 'users',
            localField: 'user_id',
            foreignField: '_id',
            pipeline: [{ $project: { unique_id: 1 } }],
            as: 'user_detail'
        }
    }
    let provider_lookup = {
        $lookup: {
            from: 'providers',
            localField: 'current_provider',
            pipeline: [{ $project: { unique_id: 1, phone: 1, country_phone_code: 1 } }],
            foreignField: '_id',
            as: 'provider_details'
        }
    }
    let vehicle_type_lookup = {
        $lookup: {
            from: 'types',
            localField: 'type_id',
            foreignField: '_id',
            pipeline: [{ $project: { typename: 1 } }],
            as: 'vehicle_type_details'
        }
    }
    let trip_history_list = await Trip_history.aggregate([{ $project: { _id: 1, user_id: 1, current_provider: 1, type_id: 1 } }, user_lookup, provider_lookup, vehicle_type_lookup])
    let i = 0;
    for (let trip of trip_history_list) {
        await Trip_history.findByIdAndUpdate(
            trip._id,
            {
                user_unique_id: trip.user_detail[0]?.unique_id || 0,
                typename: trip.vehicle_type_details[0]?.typename || "anonymous",
                provider_unique_id: trip.provider_details[0]?.unique_id || 0,
                provider_phone: trip.provider_details[0]?.phone || "0000000000",
                provider_phone_code: trip.provider_details[0]?.country_phone_code || "00"
            }
        )
        console.log(`${++i}/${trip_history_list.length}`)
    }
    res.json({ success: true })
}