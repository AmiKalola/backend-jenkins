var utils = require('../controllers/utils');
var crypto = require('crypto');
var Corporate = require('mongoose').model('Corporate');
var Dispatcher = require('mongoose').model('Dispatcher');
var Hotel = require('mongoose').model('Hotel');
var Partner = require('mongoose').model('Partner');
var Country = require('mongoose').model('Country');
var Citytype = require('mongoose').model('city_type');
var Type = require('mongoose').model('Type');
var Trip_Service = require('mongoose').model('trip_service');
var City = require('mongoose').model('City');

exports.add_detail = function (req, res) {

    var token = utils.tokenGenerator(32);
    var password = req.body.password;
    var hash = crypto.createHash('md5').update(password).digest('hex');
    var json = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: ((req.body.email).trim()).toLowerCase(),
        country_phone_code: req.body.country_phone_code,
        phone: req.body.phone,
        password: hash,
        country: req.body.country,
        country_name: req.body.country,
        country_id: req.body.country_id,
        city_id: req.body.city_id,
        city: req.body.city,
        token: token,
        is_approved: 1
    }
    Corporate.findOne({$or: [{email: ((req.body.email).trim()).toLowerCase()}, {phone: req.body.phone}]}).then((response) => { 

        if(!response){
            json.name = json.first_name + json.last_name
            var corporate_detail = new Corporate(json);
            corporate_detail.save();
        }

    });

    Dispatcher.findOne({$or: [{email: ((req.body.email).trim()).toLowerCase()}, {phone: req.body.phone}]}).then((response) => { 

        if(!response){
            Dispatcher.count({}).then((dispatcher_count)=>{
                json.unique_id = dispatcher_count+1;
                var dispatcher_detail = new Dispatcher(json);
                dispatcher_detail.save();
            })
        }

    });

    Partner.findOne({$or: [{email: ((req.body.email).trim()).toLowerCase()}, {phone: req.body.phone}]}).then((response) => { 

        if(!response){
            Partner.count({}).then((partner_count)=>{
                json.unique_id = partner_count+1;
                var partner_detail = new Partner(json);
                partner_detail.save();
            })
        }

    });

    Hotel.findOne({$or: [{email: ((req.body.email).trim()).toLowerCase()}, {phone: req.body.phone}]}).then((response) => { 

        if(!response){
            Hotel.count({}).then((hotel_count)=>{
                json.unique_id = hotel_count+1;
                json.hotel_name = req.body.first_name;
                var hotel_detail = new Hotel(json);
                hotel_detail.save();
            })
        }

    });

    setTimeout(()=>{
        res.json({success: true})
    }, 2000)

}

exports.check_service_type = function (request_data, response_data) {
    var request_data_body = request_data.body;

    Citytype.findOne({cityid: request_data_body.cityid, typeid: request_data_body.typeid}).then((citytype_data) => {
        if(!citytype_data){

            Country.findOne({_id: request_data_body.countryid}).then((country_data) => {
                var citytype_data = new Citytype({
                    countryid: request_data_body.countryid,
                    countryname: country_data.countryname,
                    cityname: request_data_body.cityname,
                    cityid: request_data_body.cityid,
                    typeid: request_data_body.typeid,
                    min_fare: 30,
                    surge_hours:[
                        {
                            "is_surge": false,
                            "day": "0",
                            "day_time": []
                        },
                        {
                            "is_surge": false,
                            "day": "1",
                            "day_time": []
                        },
                        {
                            "is_surge": false,
                            "day": "2",
                            "day_time": []
                        },
                        {
                            "is_surge": false,
                            "day": "3",
                            "day_time": []
                        },
                        {
                            "is_surge": false,
                            "day": "4",
                            "day_time": []
                        },
                        {
                            "is_surge": false,
                            "day": "5",
                            "day_time": []
                        },
                        {
                            "is_surge": false,
                            "day": "6",
                            "day_time": []
                        }
                    ]
                });

                citytype_data.save(() => {
                    Type.findOne({ _id: citytype_data.typeid }).then((type_detail) => {
                        var trip_service = new Trip_Service({
                            service_type_id: citytype_data._id,
                            city_id: citytype_data.cityid,
                            service_type_name: (type_detail.typename).trim(),
                            min_fare: citytype_data.min_fare,
                            typename: (type_detail.typename).trim(),
                            surge_hours:[
                                {
                                    "is_surge": false,
                                    "day": "0",
                                    "day_time": []
                                },
                                {
                                    "is_surge": false,
                                    "day": "1",
                                    "day_time": []
                                },
                                {
                                    "is_surge": false,
                                    "day": "2",
                                    "day_time": []
                                },
                                {
                                    "is_surge": false,
                                    "day": "3",
                                    "day_time": []
                                },
                                {
                                    "is_surge": false,
                                    "day": "4",
                                    "day_time": []
                                },
                                {
                                    "is_surge": false,
                                    "day": "5",
                                    "day_time": []
                                },
                                {
                                    "is_surge": false,
                                    "day": "6",
                                    "day_time": []
                                }
                            ]
                        });
                        trip_service.save().then(() => {
                            response_data.json({ success: true, service_type: citytype_data._id });
                        });
                    })
                })
            });
        } else {
            response_data.json({
                success: true, service_type: citytype_data._id
            });
        }
    });
}

exports.get_country_list = function (req, res) {
    let country_list = require('../../country_list.json');
    res.json(country_list);
};

exports.type_list = function (req, res) {
    Type.find({is_business:1}).then((type_list) => { 
        res.json({'type_list': type_list});
    });
    
};

exports.fetch_country_detail = function (req, res) {
    let country_list = require('../../country_list.json');
    let countryname = req.body.countryname;
    let i = country_list.findIndex(i => i.name == countryname);
    let country = {}
    if (i != -1) {
        country = country_list[i]
    }
    res.json({ country });
};

exports.check_country_exists = function (request_data, response_data) {
    Country.find({countryphonecode: request_data.body.country_phone_code ,countrycode:request_data.body.countrycode }).then(country => {
        console.log(country)
        if(country.length > 0){
            response_data.json({ success: true, country_id: country[0]._id, country_code: country[0].countrycode, message: 'country already exists' })
        } else {
            console.log(request_data_body)
            var request_data_body = request_data.body;
            request_data.countryname = request_data.body.countryname.replace(/'/g, '');
            request_data_body.countryphonecode = request_data.body.country_phone_code;
            var add_country = new Country(request_data_body);
            var file_new_name = (add_country.countryname).split(' ').join('_').toLowerCase() + '.gif';
            var file_upload_path = '/flags/' + file_new_name;
            add_country.flag_url = file_upload_path;
            add_country.save().then((country) => {
                response_data.json({
                    success: true,
                    country_id: country._id,
                    country_code: country.countrycode
                });
            }, (error) => {
                console.log(error);
                response_data.json({
                    success: false
                });
            });
        }
    })
}

exports.check_city = function (request_data, response_data) {
    var request_data_body = request_data.body;
    var city = (request_data_body.cityname).trim()
    City.findOne({ countryid: request_data_body.countryid, cityname: city}).then((city) => {
        if (!city) {
            var request_data_body = request_data.body;
            var cityname = (request_data_body.cityname).trim();
            cityname = cityname.charAt(0).toUpperCase() + cityname.slice(1);
            request_data_body.cityname = cityname;
            request_data_body.cityRadius = 20
            request_data_body.cityLatLong = [request_data_body.city_lat, request_data_body.city_lng];

            var city = new City(request_data_body);
            city.save().then(() => {
                response_data.json({ success: true, city_id: city._id});
            }, (error) => {
                console.log(error)
                response_data.json({
                    success: false
                });
            });
        } else {
            response_data.json({ success: false, city_id: city._id});
        }
    }, () => {
        response_data.json({
            success: false
        });
    });
}