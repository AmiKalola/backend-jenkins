var Trip_history = require('mongoose').model('Trip_history');
var utils = require('./utils');
var allemails = require('./emails');
var User = require('mongoose').model('User');
var Provider = require('mongoose').model('Provider');
var Promo_Code = require('mongoose').model('Promo_Code');
var Citytype = require('mongoose').model('city_type');
var User_promo_use = require('mongoose').model('User_promo_use');
var Trip = require('mongoose').model('Trip');
var Country = require('mongoose').model('Country');
var City = require('mongoose').model('City');
var CityZone = require('mongoose').model('CityZone');
var ZoneValue = require('mongoose').model('ZoneValue');
var Airport = require('mongoose').model('Airport');
var AirportCity = require('mongoose').model('Airport_to_City');
var CitytoCity = require('mongoose').model('City_to_City');
var Partner = require('mongoose').model('Partner');
// var console = require('./console');
var utils = require('./utils');
var geolib = require('geolib');
var Corporate = require('mongoose').model('Corporate');
var Wallet_history = require('mongoose').model('Wallet_history');
var mongoose = require('mongoose');
var Schema = mongoose.Types.ObjectId;
let crypto = require('crypto');
let country_list = require('../../country_list.json');
const { json } = require('body-parser');
var Card = require('mongoose').model('Card');
var User_Document = require('mongoose').model('User_Document');
var Wallet_history = require('mongoose').model('Wallet_history');
var Redeem_point_history = require('mongoose').model('redeem_point_history')
// let setting = require('mongoose').model('Settings');
let Otps = require('mongoose').model('Otps');
require('../utils/error_code')
var Settings = require('mongoose').model('Settings')
let Admin_notification = require('mongoose').model("admin_notification")
let country_json = require('../../country_list.json')
var OpenRide = require('mongoose').model('Open_Ride');
let Reviews = require('mongoose').model('Reviews')
let MassNotification = require('mongoose').model('mass_notification')

exports.update_password = async function (req, res) {
    try {
        let params_array = [{ name: 'phone', type: 'string' }, { name: 'password', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        var phone = req.body.phone;
        var country_phone_code = req.body.country_phone_code;
        var password = req.body.password;
        var query = { phone: phone }
        if (country_phone_code) {
            query = { phone: phone, country_phone_code: country_phone_code };
        }
        let user = await User.findOne(query)
        if (user) {
            user.password = utils.encryptPassword(password);
            user.save();    
            const setting_detail = await Settings.findOne({}).select({ sms_notification: 1 });
            console.log("🚀 ~ file: users.js:58 ~ setting_detail:"+ setting_detail)
            
            
            if (setting_detail.sms_notification) {
                console.log("🚀 ~ file: users.js:62 ~ sms_notification:"+ setting_detail.sms_notification)
                    utils.sendSmsForOTPVerificationAndForgotPassword( user.country_phone_code + user.phone, SMS_TEMPLATE.FORGOT_PASSWORD, password )
            }
            res.json({ success: true, message: success_messages.MESSAGE_CODE_PASSWORD_RESET_SUCCESSFULLY });
        } else {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_A_REGISTERED_USER });
        }

    } catch (err) {
        utils.error_response(err, req, res)
    }
};

exports.get_otp = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [{ name: 'phone', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let check_captcha  = await utils.verify_captcha(req.body.captcha_token, req.body.device_type)
        if (!check_captcha.success) {
           return res.json({ success: false, error_code:error_message.INVALID_CAPTCHA  });
        }
        var phone = req.body.phone;
        var country_phone_code = req.body.country_phone_code;
        var phoneWithCode = phone;

        var otpForSMS = utils.generateOtp(6);
        let user = await User.findOne({ phone: phone, country_phone_code: req.body.country_phone_code })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_A_REGISTERED_USER });
        }
        if (setting_detail.userSms == true || setting_detail.sms_notification) {
          if (country_phone_code) {
                phoneWithCode = country_phone_code + phoneWithCode;
            } else {
                phoneWithCode = user.country_phone_code + phoneWithCode;
            }
            console.log(req.body.type);
            switch (req.body.type) {
                case OTP_TYPE.OTP_LOGIN:
                    utils.sendSmsForOTPVerificationAndLogin(phoneWithCode, SMS_TEMPLATE.OTP_VERIFICATION, otpForSMS);
                    break;
                case OTP_TYPE.FORGOT_PASSWORD:
                    utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, SMS_TEMPLATE.FORGOT_PASSWORD_OTP, otpForSMS);
                    break;
                default:
                    utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, SMS_TEMPLATE.USER_OTP_VERIFICATION, otpForSMS);
                    break;
            }
        }
        user.otp_sms = otpForSMS
        await User.findByIdAndUpdate({_id : user._id}, {otp_sms : otpForSMS}, { new: true});
        res.json({ success: true});
    } catch (err) {
        utils.error_response(err, req, res)
    }
};

exports.check_sms_otp = async function (req, res) {
    try {
        let params_array = []
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        var phone = req.body.phone;
        var country_phone_code = req.body.country_phone_code;
        var email = req.body.email;
        if (req.body.type == 2) {
            if (req.body.otp_sms && req.body.otp_mail) {
                let otp = await Otps.findOne({ email: email}).select({otp_sms : 1,otp_mail : 1}).sort( { "created_at": -1 } ).lean()
                if (otp?.otp_mail == req.body.otp_mail && req.body.otp_sms == otp?.otp_sms) {
                    await Otps.deleteOne({_id : otp._id})
                    return res.json({ success: true, message: success_messages.MESSAGE_CODE_OTP_VERIFIED_SUCCESSFULLY});
                }else if (otp?.otp_mail == req.body.otp_mail ) {
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_SMS_OTP ,  smsError: true  });
                }else if (req.body?.otp_sms == otp?.otp_sms) {
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_MAIL_OTP  ,emailError : true });
                }else{
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_OTP , emailError : true ,smsError : true });
                }
            }else if(req.body.otp_sms){
                let otp = await Otps.findOne({ phone: phone , country_phone_code : country_phone_code}).select({otp_sms : 1}).sort( { "created_at": -1 } ).lean()
                if (req.body.otp_sms == otp?.otp_sms) {
                    await Otps.deleteOne({_id : otp._id})
                    return res.json({ success: true, message: success_messages.MESSAGE_CODE_OTP_VERIFIED_SUCCESSFULLY });
                }
            }else if (req.body.otp_mail) {
                let otp = await Otps.findOne({ email: email ,phone: phone , country_phone_code : country_phone_code}).select({otp_mail : 1}).sort( { "created_at": -1 } ).lean()
                if (otp?.otp_mail == req.body.otp_mail) {
                    await Otps.deleteOne({_id : otp._id})
                    return res.json({ success: true, message: success_messages.MESSAGE_CODE_OTP_VERIFIED_SUCCESSFULLY});
                }else{
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_MAIL_OTP , emailError : true });
                }
            }
        } else {
            if(req.body.is_register){
                let otp = await Otps.findOne({ phone: phone , country_phone_code : country_phone_code}).select({otp_sms : 1}).sort( { "created_at": -1 } ).lean()
                console.log(otp);
                if ( req.body.otp_sms == otp?.otp_sms) {
                    await Otps.deleteOne({_id : otp._id})
                    return res.json({ success: true, message: success_messages.MESSAGE_CODE_OTP_VERIFIED_SUCCESSFULLY});
                }else{
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_OTP });
                }
            }else{
                let user = await User.findOne({ phone: phone , country_phone_code : country_phone_code}).select({otp_sms : 1}).lean()
                if ( req.body.otp_sms == user?.otp_sms) {
                    return res.json({ success: true, message: success_messages.MESSAGE_CODE_OTP_VERIFIED_SUCCESSFULLY});
                }else{
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_A_REGISTERED_USER });
                }
            }
        }
        return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_OTP , emailError : true ,smsError : true });
    } catch (err) {
        utils.error_response(err, req, res)
    }
};

exports.check_user_registered = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [{ name: 'country_phone_code', type: 'string' }, { name: 'phone', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        var phone = req.body.phone;
        var country_phone_code = req.body.country_phone_code;
        var phoneWithCode = country_phone_code + phone;
        // generate otp //
        var otpForSMS = utils.generateOtp(6);
        let user = await User.findOne({ phone: phone, country_phone_code: country_phone_code })
        console.log(otpForSMS);
        if (user) {
            res.json({ success: true, message: success_messages.MESSAGE_CODE_USER_EXIST });
        } else {
            var userSms = setting_detail.userSms;
            if (userSms == true) {
                let otpOld = Otps.findOne({ phone: phone, country_phone_code: country_phone_code })
                if (otpOld?._id) {
                    await Otps.findByIdAndUpdate({_id : otpOld._id}, {otp_sms : otpForSMS})
                }else{
                    let otp = new Otps({
                        phone: phone, 
                        country_phone_code: country_phone_code,
                        otp_sms : otpForSMS
                    })
                    otp.save()
                }
                res.json({ success: true, otpForSMS: otpForSMS, userSms: userSms });
                utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, 1, otpForSMS);
            } else {
                res.json({ success: true, userSms: userSms });
            }
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }

};

// forgotpassword
exports.forgotpassword = async function (req, res) {
    try {
        let params_array = [{ name: 'email', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let check_captcha  = await utils.verify_captcha(req.body.captcha_token, req.body.device_type)
        if (!check_captcha.success) {
           return res.json({ success: false, error_code:error_message.INVALID_CAPTCHA  });
        }
        var type = req.body.type; //1 = user  0 = Provider 
        if (type == 1) {
            let user = await User.findOne({ email: req.body.email })
            if (!user) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_REGISTERED_OR_INVALID_EMAIL_ID });
            }
            var new_password = utils.generatePassword(6);
            user.password = utils.encryptPassword(new_password);
            user.save().then(() => {
            });
            var phoneWithCode = user.country_phone_code + user.phone;
            utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, 3, new_password);
            allemails.userForgotPassword(req, user, new_password);
            res.json({ success: true, message: success_messages.MESSAGE_CODE_RESET_PASSWORD_SUCCESSFULLY });
        } else {
            let provider = await Provider.findOne({ email: req.body.email })
            if (!provider) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_REGISTERED_OR_INVALID_EMAIL_ID });
            }
            var new_password = utils.generatePassword(6);
            provider.password = utils.encryptPassword(new_password);
            provider.save().then(() => {
            });
            var phoneWithCode = provider.country_phone_code + provider.phone;
            utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, 3, new_password);
            allemails.providerForgotPassword(req, provider, new_password);
            res.json({ success: true, message: success_messages.MESSAGE_CODE_RESET_PASSWORD_SUCCESSFULLY });
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }

};

// OTP verification
exports.verification = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [{ name: 'phone', type: 'string' }, { name: 'country_phone_code', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let check_captcha  = await utils.verify_captcha(req.body.captcha_token, req.body.device_type)
        if (!check_captcha.success) {
           return res.json({ success: false, error_code:error_message.INVALID_CAPTCHA  });
        }
        var type = req.body.type;
        var email = req.body.email;
        var phone = req.body.phone;
        var phoneWithCode = req.body.country_phone_code + phone;
        // generate otp //
        var otpForSMS = utils.generateOtp(6);
        var otpForEmail = utils.generateOtp(6);
        if (type == 1) {
            let user = await User.findOne({ email: req.body.email })

            if (user && email != "") {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED });
            }

            user = await User.findOne({ phone: req.body.phone })
            if (user) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_PHONE_NUMBER_ALREADY_USED });
            }

            var userEmailVerification = setting_detail.userEmailVerification;
            var userSms = setting_detail.userSms;
            if (userSms == true) {
                utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, 1, otpForSMS);
            }

            if (userEmailVerification == true) {
                allemails.emailForOTPVerification(req, email, otpForEmail, 2);
            }
            let otpOld = Otps.findOne({ phone: phone, country_phone_code: req.body.country_phone_code })
            if (otpOld?._id) {
                await Otps.findByIdAndUpdate({_id : otpOld._id}, {otp_sms : otpForSMS})
            }else{
                let otpOld = Otps.findOne({ phone: phone, country_phone_code: req.body.country_phone_code })
                if (otpOld?._id) {
                    await Otps.findByIdAndUpdate({_id : otpOld._id}, {otp_sms : otpForSMS})
                }else{
                    let otp = new Otps({
                        phone: phone, 
                        country_phone_code: req.body.country_phone_code,
                        otp_sms : otpForSMS,
                        type : TYPE_VALUE.USER
                    })
                    otp.save()
                }
            }

            res.json({ success: true, otpForSMS: otpForSMS, otpForEmail: otpForEmail });

        } else {
            let provider = await Provider.findOne({ email: req.body.email })
            if (provider) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED });
            }

            provider = await Provider.findOne({ phone: req.body.phone })
            if (provider) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_PHONE_NUMBER_ALREADY_USED });
            }

            var providerEmailVerification = setting_detail.providerEmailVerification;
            var providerSms = setting_detail.providerSms;
            ///////////// GENERATE OTP ///////////
            if (providerSms == true) {
                utils.sendSmsForOTPVerificationAndForgotPassword(phoneWithCode, 2, otpForSMS);
            }
            if (providerEmailVerification == true) {
                allemails.emailForOTPVerification(req, email, otpForEmail, 2);
            }

            let otpOld = Otps.findOne({ phone: req.body.phone, country_phone_code: req.body.country_phone_code , email : req.body.email})
                if (otpOld?._id) {
                    await Otps.findByIdAndUpdate({_id : otpOld._id}, {otp_sms : otpForSMS ,otp_mail :otpForEmail})
                }else{
                    let otp = new Otps({
                        phone: req.body.phone, 
                        country_phone_code: req.body.country_phone_code,
                        email : req.body.email ,
                        otp_mail :otpForEmail,
                        otp_sms : otpForSMS, 
                        type : TYPE_VALUE.PROVIDER
                    })
                    otp.save()
                }

            res.json({ success: true, otpForSMS: otpForSMS, otpForEmail: otpForEmail });

        }
    } catch (err) {
        utils.error_response(err, req, res)
    }

};

exports.verify_email_phone = async function (req, res) {
    try {
        let params_array = [{ name: 'phone', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            return res.json(response)

        }
        var type = req.body.type;
        let Table;
        switch (type) {
            case '1':
                Table = User
                break;
            case '2':
                Table = Provider
                break;
        }
        let user = await Table.findOne({ email: req.body.email })
        if (user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED });
        }
        user = await Table.findOne({ phone: req.body.phone, country_phone_code: req.body.country_phone_code })
        if (!user) {
            return res.json({ success: true });
        }
        res.json({ success: false, error_code: error_message.ERROR_CODE_PHONE_NUMBER_ALREADY_USED });

    } catch (err) {
        utils.error_response(err, req, res)
    }
};

// user_register_new //
exports.user_register = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({});

        let params_array = [{ name: 'first_name', type: 'string' }, { name: 'last_name', type: 'string' }, { name: 'email', type: 'string' },
        { name: 'phone', type: 'string' }, { name: 'country_phone_code', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let check_captcha  = await utils.verify_captcha(req.body.captcha_token, req.body.device_type)
        if (!check_captcha.success) {
           return res.json({ success: false, error_code:error_message.INVALID_CAPTCHA  });
        }

        var social_id = req.body.social_unique_id;
        var social_id_array = [];
        if (social_id == undefined || social_id == null || social_id == "") {
            social_id = null;
        } else {
            social_id_array.push(social_id);
        }

        var gender = req.body.gender;
        if (gender != undefined) {
            gender = ((gender).trim()).toLowerCase();
        }

        var first_name = req.body.first_name;
        var last_name = req.body.last_name;
        var email = req.body.email;

        if (email == undefined || email == null || email == "") {
            email = null;
        } else {
            email = ((req.body.email).trim()).toLowerCase();
        }
        var referral_code = (utils.tokenGenerator(8)).toUpperCase();
        var token = utils.tokenGenerator(32);
        let user_email = await User.findOne({ email: email })
        let user_phone = await User.findOne({ phone: req.body.phone, country_phone_code: req.body.country_phone_code })
        if (!user_email && !user_phone) {
            if (email == null) {
                email = "";
            }

            if (first_name.length > 0) {
                first_name = first_name.charAt(0).toUpperCase() + first_name.slice(1);
            } else {
                first_name = "";
            }

            if (last_name.length > 0) {
                last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1);
            } else {
                last_name = "";
            }

            var user = new User({
                first_name: first_name,
                last_name: last_name,
                email: email,
                country_phone_code: req.body.country_phone_code,
                phone: req.body.phone,
                gender: gender,
                device_token: req.body.device_token,
                device_type: req.body.device_type,
                address: req.body.address,
                social_ids: social_id_array,
                social_unique_id: req.body.social_unique_id,
                login_by: req.body.login_by,
                device_timezone: req.body.device_timezone,
                city: req.body.city,
                token: token,
                country: req.body.country,
                referral_code: referral_code,
                user_type: Number(constant_json.USER_TYPE_NORMAL),
                app_version: req.body.app_version
            });
            let alpha2 = country_json.filter((country) => country.name == user.country) || null

            // FOR PASSWORD
            if (social_id == null) {
                user.password = utils.encryptPassword(req.body.password);
            }

            // for web push
            if(req.body.device_type == "web" && req.body.webpush_config && Object.keys(req.body.webpush_config).length > 0){
                user.webpush_config = JSON.parse(req.body.webpush_config)
            }

            // FOR PROFILE IMAGE 
            if (req.files != undefined && req.files.length > 0) {
                var image_name = user._id + utils.tokenGenerator(4);
                var url = utils.getImageFolderPath(req, 1) + image_name + '.jpg';
                user.picture = url;
                utils.saveImageFromBrowser(req.files[0].path, image_name + '.jpg', 1);

            }


            var country_phone_code = user.country_phone_code;
            var match = {countryphonecode: country_phone_code}
         if(req.body.country_id){
            match = {_id: req.body.country_id}
         }
            let country = await Country.findOne(match)
            if (country) {
                user.wallet_currency_code = country.currencycode;
                user.country = country.countryname;
                user.save().then(() => {
                    var email_notification = setting_detail.email_notification;
                    if (email_notification == true) {
                        allemails.sendUserRegisterEmail(req, user, user.first_name + " " + user.last_name);
                    }
                    // FOR ADD DOCUEMNTS
                    utils.insert_documets_for_new_users(user, Number(constant_json.USER_TYPE_NORMAL), country._id, function (document_response) {
                        console.log(document_response)
                        var response = {};
                        response.first_name = user.first_name;
                        response.last_name = user.last_name;
                        response.email = user.email;
                        response.country_phone_code = user.country_phone_code;
                        response.is_document_uploaded = user.is_document_uploaded;
                        response.address = user.address;
                        response.is_approved = user.is_approved;
                        response.user_id = user._id;
                        response.social_ids = user.social_ids;
                        response.social_unique_id = user.social_unique_id;
                        response.login_by = user.login_by;
                        response.city = user.city;
                        response.country = user.country;
                        response.referral_code = user.referral_code;
                        response.rate = user.rate;
                        response.rate_count = user.rate_count;
                        response.is_referral = user.is_referral;
                        response.token = user.token;
                        response.phone = user.phone;
                        response.wallet_currency_code = user.wallet_currency_code;
                        response.alpha2 = alpha2[0].alpha2


                        response.country_detail = { "is_referral": country.is_referral }

                        // Trigger admin notification
                        utils.addNotification({
                            type: ADMIN_NOTIFICATION_TYPE.USER_REGISTERED,
                            user_id: user._id,
                            username: user.first_name + " " + user.last_name,
                            picture: user.picture,
                            country_id: country._id,
                            user_unique_id: user.unique_id,
                        })

                        return res.json({
                            success: true,
                            message: success_messages.MESSAGE_CODE_USER_REGISTERED_SUCCESSFULLY,
                            user_detail: response
                        });
                        
                    });
                });

            } else {

                let i = country_list.findIndex(i => i.code == country_phone_code);
                if (i != -1) {
                    user.wallet_currency_code = country_list[0].currency_code;
                } else {
                    user.wallet_currency_code = "";
                }
                user.is_document_uploaded = 1;
                await user.save();
                var email_notification = setting_detail.email_notification;
                if (email_notification == true) {
                    allemails.sendUserRegisterEmail(req, user, user.first_name + " " + user.last_name);
                }

                response.first_name = user.first_name;
                response.last_name = user.last_name;
                response.email = user.email;
                response.country_phone_code = user.country_phone_code;
                response.is_document_uploaded = user.is_document_uploaded;
                response.address = user.address;
                response.is_approved = user.is_approved;
                response.user_id = user._id;
                response.social_ids = user.social_ids;
                response.social_unique_id = user.social_unique_id;
                response.login_by = user.login_by;
                response.city = user.city;
                response.country = user.country;
                response.referral_code = user.referral_code;
                response.rate = user.rate;
                response.rate_count = user.rate_count;
                response.is_referral = user.is_referral;
                response.token = user.token;
                response.country_detail = { "is_referral": false }
                response.phone = user.phone;
                response.picture = user.picture;
                response.wallet_currency_code = user.wallet_currency_code;
                response.alpha2 = alpha2[0].alpha2
                // Trigger admin notification
                utils.addNotification({
                    type: ADMIN_NOTIFICATION_TYPE.USER_REGISTERED,
                    user_id: user._id,
                    username: user.first_name + " " + user.last_name,
                    picture: user.picture,
                    country_id: country?._id,
                    user_unique_id: user.unique_id,
                })
                return res.json({
                    success: true,
                    message: success_messages.MESSAGE_CODE_USER_REGISTERED_SUCCESSFULLY,
                    user_detail: response
                });
                
            }
        } else {

            if (social_id == null) {
                if (user_phone) {
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_PHONE_NUMBER_ALREADY_USED });
                } else {
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED });
                }
            } else {

                if (user_email && user_email.phone == req.body.phone) {
                    user_email.social_ids.push(social_id);
                    await user_email.save()
                    response.first_name = user_email.first_name;
                    response.last_name = user_email.last_name;
                    response.email = user_email.email;
                    response.country_phone_code = user_email.country_phone_code;
                    response.is_document_uploaded = user_email.is_document_uploaded;
                    response.address = user_email.address;
                    response.is_approved = user_email.is_approved;
                    response.user_id = user_email._id;
                    response.social_ids = user_email.social_ids;
                    response.social_unique_id = user_email.social_unique_id;
                    response.login_by = user_email.login_by;
                    response.city = user_email.city;
                    response.country = user_email.country;
                    response.referral_code = user_email.referral_code;
                    response.rate = user_email.rate;
                    response.rate_count = user_email.rate_count;
                    response.is_referral = user_email.is_referral;
                    response.token = user_email.token;
                    response.country_detail = { "is_referral": false }
                    response.phone = user_email.phone;
                    response.picture = user_email.picture;
                    response.wallet_currency_code = user_email.wallet_currency_code;
                    response.alpha2 = alpha2[0].alpha2
                    return res.json({
                        success: true,
                        message: success_messages.MESSAGE_CODE_USER_REGISTERED_SUCCESSFULLY,
                        user_detail: response
                    });
                } else if (user_phone && (user_phone.email == email || user_phone.email == "")) {
                    user_phone.social_ids.push(social_id);
                    user_phone.email = email;
                    await user_phone.save();
                    response.first_name = user_phone.first_name;
                    response.last_name = user_phone.last_name;
                    response.email = user_phone.email;
                    response.country_phone_code = user_phone.country_phone_code;
                    response.is_document_uploaded = user_phone.is_document_uploaded;
                    response.address = user_phone.address;
                    response.is_approved = user_phone.is_approved;
                    response.user_id = user_phone._id;
                    response.social_ids = user_phone.social_ids;
                    response.social_unique_id = user_phone.social_unique_id;
                    response.login_by = user_phone.login_by;
                    response.city = user_phone.city;
                    response.country = user_phone.country;
                    response.referral_code = user_phone.referral_code;
                    response.rate = user_phone.rate;
                    response.rate_count = user_phone.rate_count;
                    response.is_referral = user_phone.is_referral;
                    response.token = user_phone.token;
                    response.country_detail = { "is_referral": false }
                    response.phone = user_phone.phone;
                    response.wallet_currency_code = user_phone.wallet_currency_code;
                    response.alpha2 = alpha2[0].alpha2
                    return res.json({
                        success: true,
                        message: success_messages.MESSAGE_CODE_USER_REGISTERED_SUCCESSFULLY,
                        user_detail: response
                    });
                } else {
                    return res.json({
                        success: false,
                        error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED_WITH_SOCIAL
                    });
                }
            }
        }
    } catch (err) {
        // console.log(err);
        utils.error_response(err, req, res)
    } finally {
        // console.log(res);
    }
};


exports.user_login = async function (req, res) {
    try {
        let params_array = [{ name: 'email', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let check_captcha  = await utils.verify_captcha(req.body.captcha_token, req.body.device_type)
        if (!check_captcha.success) {
           return res.json({ success: false, error_code:error_message.INVALID_CAPTCHA  });
        }
        var email = req.body.email;
        if (email != undefined) {
            email = ((req.body.email).trim()).toLowerCase();
        }

        var social_id = req.body.social_unique_id;

        var encrypted_password = req.body.password;
        if (social_id == undefined || social_id == null || social_id == "") {
            social_id = "";
        }
        if (encrypted_password == undefined || encrypted_password == null || encrypted_password == "") {
            encrypted_password = "";
        } else {
            encrypted_password = utils.encryptPassword(encrypted_password);
        }

        var query = { $or: [{ 'phone': email, 'country_phone_code': req.body.country_phone_code }, { social_ids: { $all: [social_id] } }] };

        let user_detail = await User.findOne(query)
        if (social_id == undefined || social_id == null || social_id == "") {
            social_id = null;
        }
        if ((social_id == null && email == "")) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_A_REGISTERED_USER });
        } else if (user_detail) {
            if (social_id == null && (encrypted_password != "" || req.body.otp_sms != user_detail.otp_sms) && encrypted_password != user_detail.password ) {
                if((req.body.otp_sms != user_detail.otp_sms) && !req.body.password){
                    return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_OTP });
                }
                return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_PASSWORD });
            } else if (social_id != null && user_detail.social_ids.indexOf(social_id) < 0) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_YOU_ARE_NOT_REGISTERED_WITH_THIS_SOCIAL });
            } else {
                if (user_detail.device_token != "" && user_detail.device_token != req.body.device_token) {
                    utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user_detail.device_type, user_detail.device_token, push_messages.PUSH_CODE_FOR_USER_LOGIN_IN_OTHER_DEVICE, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
                }
                user_detail.device_token = req.body.device_token;
                user_detail.device_type = req.body.device_type;
                user_detail.login_by = req.body.login_by;
                user_detail.app_version = req.body.app_version;
                user_detail.user_type = Number(constant_json.USER_TYPE_NORMAL);
                user_detail.token = utils.tokenGenerator(32);

                if(req.body.device_type == "web" && req.body.webpush_config && Object.keys(req.body.webpush_config).length > 0){
                    user_detail.webpush_config = JSON.parse(req.body.webpush_config)
                }
                var document = await User_Document.find({user_id: user_detail._id, option: 1, is_visible: true, is_uploaded: 0})

                if(document.length > 0) {
                    user_detail.is_document_uploaded = 0
                } else { 
                    user_detail.is_document_uploaded = 1
                }

                await user_detail.save();
                let alpha2 = country_json.filter((country) => country.name == user_detail.country) || null
                response.alpha2 = alpha2[0]?.alpha2

                response.first_name = user_detail.first_name;
                response.last_name = user_detail.last_name;
                response.email = user_detail.email;
                response.country_phone_code = user_detail.country_phone_code;
                response.is_document_uploaded = user_detail.is_document_uploaded;
                response.address = user_detail.address;
                response.is_approved = user_detail.is_approved;
                response.user_id = user_detail._id;
                response.social_ids = user_detail.social_ids;
                response.social_unique_id = user_detail.social_unique_id;
                response.login_by = user_detail.login_by;
                response.city = user_detail.city;
                response.country = user_detail.country;
                response.referral_code = user_detail.referral_code;
                response.rate = user_detail.rate;
                response.rate_count = user_detail.rate_count;
                response.is_referral = user_detail.is_referral;
                response.token = user_detail.token;
                response.phone = user_detail.phone;
                response.picture = user_detail.picture;
                response.wallet_currency_code = user_detail.wallet_currency_code;

                var corporate_id = null;
                if (user_detail.corporate_ids && user_detail.corporate_ids.length > 0) {
                    corporate_id = user_detail.corporate_ids[0].corporate_id;
                }

                let corporate_detail = await Corporate.findOne({ _id: corporate_id })

                if (corporate_detail) {
                    response.corporate_detail = {
                        name: corporate_detail.name,
                        phone: corporate_detail.phone,
                        country_phone_code: corporate_detail.country_phone_code,
                        status: user_detail.corporate_ids[0].status,
                        _id: corporate_detail._id
                    }
                }

                let country = await Country.findOne({ countryphonecode: user_detail.country_phone_code })
                if (country) {
                    response.country_detail = { "is_referral": country.is_referral }
                } else {
                    response.country_detail = { "is_referral": false }
                }

                let pipeline = [
                    { $match: { 'split_payment_users.user_id': user_detail._id } },
                    { $match: { 'is_trip_cancelled': 0 } },
                    {
                        $project: {
                            trip_id: '$_id',
                            is_trip_end: 1,
                            currency: 1,
                            user_id: 1,
                            split_payment_users: {
                                $filter: {
                                    input: "$split_payment_users",
                                    as: "item",
                                    cond: { $eq: ["$$item.user_id", user_detail._id] }
                                }
                            }
                        }
                    },
                    { $unwind: "$split_payment_users" },
                    {
                        $match: {
                            $or: [
                                { 'split_payment_users.status': SPLIT_PAYMENT.WAITING },
                                {
                                    $and: [
                                        { 'split_payment_users.status': SPLIT_PAYMENT.ACCEPTED },
                                        { 'split_payment_users.payment_status': { $ne: PAYMENT_STATUS.COMPLETED } },
                                        { 'is_trip_end': 1 }
                                    ]
                                },
                                {
                                    $and: [
                                        { 'split_payment_users.status': SPLIT_PAYMENT.ACCEPTED },
                                        { 'split_payment_users.payment_status': { $ne: PAYMENT_STATUS.COMPLETED } },
                                        { 'split_payment_users.payment_mode': null }
                                    ]
                                }
                            ]
                        }
                    },
                    {
                        $lookup:
                        {
                            from: "users",
                            localField: "user_id",
                            foreignField: "_id",
                            as: "user_detail"
                        }
                    },
                    { $unwind: "$user_detail" },
                    {
                        $project: {
                            trip_id: 1,
                            first_name: '$user_detail.first_name',
                            last_name: '$user_detail.last_name',
                            phone: '$user_detail.phone',
                            country_phone_code: '$user_detail.country_phone_code',
                            user_id: '$user_detail._id',
                            is_trip_end: 1,
                            currency: 1,
                            status: '$split_payment_users.status',
                            payment_mode: '$split_payment_users.payment_mode',
                            payment_status: '$split_payment_users.payment_status',
                            payment_intent_id: '$split_payment_users.payment_intent_id',
                            total: '$split_payment_users.total',
                        }
                    },
                ]
                let split_payment_request = await Trip.aggregate(pipeline);
                if (split_payment_request.length == 0) {
                    split_payment_request = await Trip_history.aggregate(pipeline);
                }

                if (user_detail.current_trip_id) {
                    let trip_detail = await Trip.findOne({ _id: user_detail.current_trip_id }) || await Trip_history.findOne({ _id: user_detail.current_trip_id });
                    response.trip_id = user_detail.current_trip_id;
                    response.provider_id = trip_detail?.current_provider;
                    response.is_provider_accepted = trip_detail?.is_provider_accepted;
                    response.is_provider_status = trip_detail?.is_provider_status;
                    response.is_trip_end = trip_detail?.is_trip_end;
                    response.is_trip_completed = trip_detail?.is_trip_completed;
                    response.is_user_invoice_show = trip_detail?.is_user_invoice_show;
                    res.json({ success: true, message: 3, split_payment_request: split_payment_request[0], user_detail: response });
                } else {
                    res.json({ success: true, message: 3, split_payment_request: split_payment_request[0], user_detail: response });
                }
            }
        } else {
            res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_A_REGISTERED_USER });
        }

    } catch (err) {
        utils.error_response(err, req, res)
    }
};


////////// GET  USER DETAIL ///////
exports.get_user_detail = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        console.log(req.body.user_id)
        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_NOT_GET_YOUR_DETAIL });
        }
        let country = await Country.findOne({ countryphonecode: user.country_phone_code })
        var country_detail = { "is_referral": false };
        if (country) {
            country_detail = { "is_referral": country.is_referral };
        }

        res.json({
            success: true, message: success_messages.MESSAGE_CODE_GET_YOUR_DETAIL,

            user_id: user._id,
            first_name: user.first_name,
            last_name: user.last_name,
            country_phone_code: user.country_phone_code,
            phone: user.phone,
            email: user.email,
            wallet: user.wallet,
            wallet_currency_code: user.wallet_currency_code,
            picture: user.picture,
            bio: user.bio,
            address: user.address,
            city: user.city,
            country: user.country,
            zipcode: user.zipcode,
            login_by: user.login_by,
            gender: user.gender,
            social_unique_id: user.social_unique_id,
            social_ids: user.social_ids,
            device_token: user.device_token,
            device_type: user.device_type,
            device_timezone: user.device_timezone,
            referral_code: user.referral_code,
            token: user.token,
            is_approved: user.is_approved,
            app_version: user.app_version,
            is_referral: user.is_referral,
            is_document_uploaded: user.is_document_uploaded,
            country_detail: country_detail,
            rate: user.rate,
            rate_count: user.rate_count
        });

    } catch (err) {
        utils.error_response(err, req, res)
    }
};


exports.user_update = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }, { name: 'phone', type: 'string' },
        { name: 'first_name', type: 'string' }, { name: 'last_name', type: 'string' }, { name: 'country_phone_code', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }

        var user_id = req.body.user_id;
        var old_password = req.body.old_password;
        var social_id = req.body.social_unique_id;
        if (social_id == undefined || social_id == null || social_id == "") {
            social_id = null;
        }
        if (old_password == undefined || old_password == null || old_password == "") {
            old_password = "";
        } else {
            old_password = utils.encryptPassword(old_password);
        }
        let user = await User.findOne({ _id: user_id })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
        }
        if (req.body.token !== null && user.token !== req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
        } else if (social_id == null && old_password != "" && old_password != user.password) {
            res.json({
                success: false,
                error_code: error_message.ERROR_CODE_YOUR_PASSWORD_IS_NOT_MATCH_WITH_OLD_PASSWORD
            });

        } else if (social_id != null && user.social_ids.indexOf(social_id) < 0) {
            res.json({ success: false, error_code: 111 });
        } else {
            let country = await Country.findOne({ _id: user.country_id })
            var new_email = req.body.email;
            var new_phone = req.body.phone;

            if (req.body.new_password != "") {
                var new_password = utils.encryptPassword(req.body.new_password);
                req.body.password = new_password;
            }
            if (!new_email) {
                new_email = null;
            }

            req.body.social_ids = user.social_ids;

            let user_details = await User.findOne({ _id: { '$ne': user_id }, email: new_email })

            if (user_details) {

                return res.json({ success: false, error_code: error_message.ERROR_CODE_EMAIL_ID_ALREADY_REGISTERED });

            }
            let user_phone_details = await User.findOne({ _id: { '$ne': user_id }, country_phone_code: req.body.country_phone_code, phone: new_phone })
            if (user_phone_details) {
                return res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_PHONE_NUMBER_ALREADY_USED
                });
            }
            var social_id_array = [];
            if (social_id != null) {
                social_id_array.push(social_id);
            }
            var user_update_query = { $or: [{ 'password': old_password }, { social_ids: { $all: social_id_array } }] };
            user_update_query = { $and: [{ '_id': user_id }, user_update_query] };


            user = await User.findOneAndUpdate(user_update_query, req.body, { new: true })
            if (!user) {
                return res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND
                });
            }
            if (req.files != undefined && req.files.length > 0) {
                utils.deleteImageFromFolder(user.picture, 1);
                var image_name = user._id + utils.tokenGenerator(4);
                var url = utils.getImageFolderPath(req, 1) + image_name + '.jpg';
                user.picture = url;
                utils.saveImageFromBrowser(req.files[0].path, image_name + '.jpg', 1);
            }

            var first_name = (req.body.first_name).trim();
            if (first_name != "" && first_name != undefined && first_name != null) {
                first_name = first_name.charAt(0).toUpperCase() + first_name.slice(1);
            } else {
                first_name = "";
            }
            var last_name = (req.body.last_name).trim();
            if (last_name != "" && last_name != undefined && last_name != null) {
                last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1);
            } else {
                last_name = "";
            }
            user.first_name = first_name;
            user.last_name = last_name;
            user.email = req.body.email;
            user.country_phone_code = req.body.country_phone_code;
            user.phone = req.body.phone;
            user.bio = req.body.bio;
            user.gender = req.body.gender;
            user.address = req.body.address;
            user.zipcode = req.body.zipcode;
            user.city = req.body.city;
            await user.save();

            response.first_name = user.first_name;
            response.last_name = user.last_name;
            response.email = user.email;
            response.country_phone_code = user.country_phone_code;
            response.is_document_uploaded = user.is_document_uploaded;
            response.address = user.address;
            response.is_approved = user.is_approved;
            response.user_id = user._id;
            response.social_ids = user.social_ids;
            response.social_unique_id = user.social_unique_id;
            response.login_by = user.login_by;
            response.city = user.city;
            response.country = user.country;
            response.referral_code = user.referral_code;
            response.rate = user.rate;
            response.rate_count = user.rate_count;
            response.is_referral = user.is_referral;
            response.token = user.token;
            response.country_detail = { "is_referral": false }
            response.phone = user.phone;
            response.picture = user.picture;
            response.wallet_currency_code = user.wallet_currency_code;
            response.alpha2 = country?.alpha2

            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_PROFILE_UPDATED_SUCCESSFULLY,
                user_detail: response
            });
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }
};

//// LOGOUT USER  SERVICE /////
exports.logout = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let user = await User.findOne({ _id: req.body.user_id })
        if(req.body.is_admin_decline){
            message = ERROR_CODE.DECLINE_BY_ADMIN
            user.webpush_config = {}
            user.device_token = "";
            await user.save()
            res.json({ success: true, error_code: message })
            return
        }
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
        }
        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
        } else {
            user.webpush_config = {}
            user.device_token = "";
            await user.save()
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_LOGOUT_SUCCESSFULLY
            });
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }
};

///////////////////////////////// UPDATE DEVICE TOKEN //////////////////////
exports.update_device_token = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
        }
        if (user.token != req.body.token) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
        }

        user.device_token = req.body.device_token;
        user.save().then(() => {
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOUR_DEVICE_TOKEN_UPDATE_SUCCESSFULLY
            });
        });
    } catch (err) {
        utils.error_response(err, req, res)
    }
};


//////////////APPLY REFERAL CODE-//
exports.apply_referral_code = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }, { name: 'referral_code', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
        }
        if (req.body.token != null && user.token != req.body.token) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
        }
        var is_skip = req.body.is_skip;
        
        if (is_skip == 0) {
            if(user.referral_code == req.body.referral_code) {
                return res.json({ success: false, error_code: error_message.USER_OWN_REFERRAL})
            }
            var referral_code = req.body.referral_code;
            let userData = await User.findOne({ referral_code: referral_code })
            if (!userData) {
                return res.json({ success: false, error_code: error_message.ERROR_CODE_REFERRAL_CODE_INVALID });
            }
            if (userData.country != user.country) {
                return res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_YOUR_FRIEND_COUNTRY_NOT_MATCH_WITH_YOU
                });
            }
            if (user.is_referral == 1) {
                return res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_YOU_HAVE_ALREADY_APPLY_REFERRAL_CODE
                });
            }
            let country = await Country.findOne({ countryname: user.country })

            var userRefferalCount = userData.total_referrals;

            if (userRefferalCount >= country.userreferral) {
                return res.json({
                    success: false,
                    error_code: error_message.ERROR_CODE_REFERRAL_CODE_EXPIRED
                });
            }

            var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, userData.unique_id, userData._id, null,
                userData.wallet_currency_code, userData.wallet_currency_code,
                1, country.bonus_to_userreferral, userData.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_REFERRAL, "User used your referral code, User id : " + user.unique_id);

            userData.total_referrals = +userData.total_referrals + 1;
            userData.wallet = total_wallet_amount;
            await userData.save()
            user.is_referral = 1;
            user.referred_by = userData._id;

            total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, null,
                user.wallet_currency_code, user.wallet_currency_code,
                1, country.referral_bonus_to_user, user.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_REFERRAL, "Using refferal code : " + referral_code + " of User id : " + userData.unique_id);

            let total_redeem_point = '';
            if (country?.user_redeem_settings[0]?.is_user_redeem_point_reward_on && (country?.user_redeem_settings[0]?.referring_redeem_point_to_users_friend > 0)) {
                total_redeem_point = utils.add_redeem_point_history(
                constant_json.USER_UNIQUE_NUMBER,
                user.unique_id,
                user._id,
                country._id,
                constant_json.REFERRAL_REDEEM_POINT,
                user.wallet_currency_code,
                'Get redeem point via referral',
                country.user_redeem_settings[0]
                    ?.referring_redeem_point_to_users_friend,
                user.total_redeem_point,
                constant_json.ADD_REDEEM_POINT
                )
                user.total_redeem_point = total_redeem_point;
            }


            if (country?.user_redeem_settings[0]?.is_user_redeem_point_reward_on && (country?.user_redeem_settings[0]?.referring_redeem_point_to_user > 0)) {

                total_redeem_point = utils.add_redeem_point_history(
                constant_json.USER_UNIQUE_NUMBER,
                userData.unique_id,
                userData._id,
                country._id,
                constant_json.REFERRAL_REDEEM_POINT,
                userData.wallet_currency_code,
                'Get redeem point using referral',
                country.user_redeem_settings[0]?.referring_redeem_point_to_user,
                userData.total_redeem_point,
                constant_json.ADD_REDEEM_POINT
                )

                userData.total_redeem_point = total_redeem_point;
           
            }
            user.wallet = total_wallet_amount;
            await user.save()
            await userData.save()
            
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_REFERRAL_PROCESS_SUCCESSFULLY_COMPLETED,
                user_id: user._id,
                is_referral: user.is_referral,
                first_name: user.first_name,
                last_name: user.last_name,
                country_phone_code: user.country_phone_code,
                phone: user.phone,
                email: user.email,
                picture: user.picture,
                bio: user.bio,
                address: user.address,
                city: user.city,
                country: user.country,
                zipcode: user.zipcode,
                login_by: user.login_by,
                social_unique_id: user.social_unique_id,
                device_token: user.device_token,
                device_type: user.device_type,
                referral_code: user.referral_code,
                device_timezone: user.device_timezone
            });
        } else {
            user.is_referral = 1;
            await user.save()
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOU_HAVE_SKIPPED_FOR_REFERRAL_PROCESS,
                user_id: user._id,
                is_referral: user.is_referral,
                first_name: user.first_name,
                last_name: user.last_name,
                country_phone_code: user.country_phone_code,
                phone: user.phone,
                email: user.email,
                picture: user.picture,
                bio: user.bio,
                address: user.address,
                city: user.city,
                country: user.country,
                zipcode: user.zipcode,
                login_by: user.login_by,
                social_unique_id: user.social_unique_id,
                device_token: user.device_token,
                device_type: user.device_type,
                referral_code: user.referral_code,
                device_timezone: user.device_timezone
            });
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }

};
///////////////FARE CALCULATOR FOR ESTIMATE FARE///////

exports.getfareestimate = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'service_type_id', type: 'string' }], function (response) {
        if (response.success) {
            Citytype.findOne({ _id: req.body.service_type_id }).then((citytype) => {
                var geo = false;
                var geo2 = false
                var zone1, zone2, k = 0;
                if (!citytype) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_NO_SERVICE_TYPE_FOUND });
                } else {
                    var city_id = citytype.cityid;
                    City.findOne({ _id: city_id }).then(async (city) => {

                        if (!city) {
                            res.json({ success: false, error_code: error_message.ERROR_CODE_NO_SERVICE_TYPE_FOUND });
                        } else {        
                            var distance = req.body.distance;
                            var distanceKmMile = distance;
                            var unit_set = city.unit;
                            if (unit_set == 1) {
                                distanceKmMile = distance * 0.001;
                            } else {
                                distanceKmMile = distance * 0.000621371;
                            }

                            var time = req.body.time;
                            var timeMinutes;
                            timeMinutes = time * 0.0166667;
                            timeMinutes = Math.round(timeMinutes)
                            if (req.body.is_open_ride) {
                                let citytypeforopenride = await Citytype.findOne({ typeid: citytype.typeid,"is_ride_share" : 2 })
                                if (!citytypeforopenride) {
                                    res.json({ success: false, error_code: error_message.ERROR_CODE_NO_SERVICE_TYPE_FOUND });
                                    return
                                }else{
                                    other(citytypeforopenride.cityid, citytypeforopenride, req.body, timeMinutes, distanceKmMile, res)
                                    return
                                }
                            }
                            if (req.body.is_multiple_stop == 1) {
                                other(city_id, citytype, req.body, timeMinutes, distanceKmMile, res);
                            } else if (city.zone_business == 1) {
                                CityZone.find({ cityid: city_id }).then((cityzone) => {
                                    if (citytype.is_zone == 1 && cityzone !== null && cityzone.length > 0) {
                                        var zone_count = cityzone.length;
                                        cityzone.forEach(function (cityzoneDetail) {

                                            geo = geolib.isPointInside(
                                                { latitude: req.body.pickup_latitude, longitude: req.body.pickup_longitude },
                                                cityzoneDetail.kmlzone
                                            );
                                            geo2 = geolib.isPointInside(
                                                {
                                                    latitude: req.body.destination_latitude,
                                                    longitude: req.body.destination_longitude
                                                },
                                                cityzoneDetail.kmlzone
                                            );
                                            if (geo) {
                                                zone1 = cityzoneDetail.id;

                                            }
                                            if (geo2) {
                                                zone2 = cityzoneDetail.id;

                                            }
                                            k++;
                                            if (k == zone_count) {
                                                ZoneValue.findOne({
                                                    service_type_id: req.body.service_type_id,
                                                    $or: [{ from: zone1, to: zone2 }, {
                                                        from: zone2,
                                                        to: zone1
                                                    }]
                                                }).then((zonevalue) => {

                                                    if (zonevalue) {
                                                        var estimated_fare = (zonevalue.amount).toFixed(2);
                                                        var trip_type = constant_json.TRIP_TYPE_ZONE;

                                                        res.json({
                                                            success: true,
                                                            message: success_messages.MESSAGE_CODE_YOU_GET_FARE_ESTIMATE,
                                                            trip_type: trip_type,
                                                            time: timeMinutes,
                                                            distance: (distanceKmMile).toFixed(2),
                                                            estimated_fare: Number(estimated_fare),
                                                            unit_set: city.unit
                                                        });

                                                    } else {
                                                        airport(city_id, citytype, req.body, timeMinutes, distanceKmMile, res);
                                                    }
                                                })

                                            }

                                        });

                                    } else {
                                        airport(city_id, citytype, req.body, timeMinutes, distanceKmMile, res);
                                    }

                                });
                            } else {
                                airport(city_id, citytype, req.body, timeMinutes, distanceKmMile, res);
                            }
                        }
                    });
                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};


function airport(cityid, citytype, body, timeMinutes, distanceKmMile, res) {


    Airport.find({ city_id: cityid }).then((airport_data) => {
        if (airport_data != null && airport_data.length > 0) {
            City.findOne({ '_id': cityid, airport_business: 1 }).then((city) => {
                if (city) {

                    var pickup_airport;
                    var dest_airport;
                    var airport_id;
                    airport_data.forEach(function (airportDetail) {

                        pickup_airport = geolib.isPointInside(
                            {
                                latitude: body.pickup_latitude,
                                longitude: body.pickup_longitude
                            },
                            airportDetail.kmlzone
                        );

                        dest_airport = geolib.isPointInside(
                            {
                                latitude: body.destination_latitude,
                                longitude: body.destination_longitude
                            },
                            airportDetail.kmlzone
                        );

                        if (pickup_airport) {
                            city_distance = utils.getDistanceFromTwoLocation([body.destination_latitude, body.destination_longitude], city.cityLatLong);

                            if (city.is_use_city_boundary) {
                                var inside_city = geolib.isPointInside(
                                    {
                                        latitude: body.pickup_latitude,
                                        longitude: body.pickup_longitude
                                    },
                                    city.city_locations
                                );
                                if (inside_city) {
                                    airport_id = airportDetail._id;
                                }
                            } else {
                                if (city_distance < city.cityRadius) {
                                    airport_id = airportDetail._id;
                                }
                            }
                        }
                        if (dest_airport) {
                            city_distance = utils.getDistanceFromTwoLocation([body.pickup_latitude, body.pickup_longitude], city.cityLatLong);
                            if (city.is_use_city_boundary) {
                                var inside_city = geolib.isPointInside(
                                    {
                                        latitude: body.destination_latitude,
                                        longitude: body.destination_longitude
                                    },
                                    city.city_locations
                                );
                                if (inside_city) {
                                    airport_id = airportDetail._id;
                                }
                            } else {
                                if (city_distance < city.cityRadius) {
                                    airport_id = airportDetail._id;
                                }
                            }
                        }
                    });

                    if (airport_id) {
                        AirportCity.findOne({
                            airport_id: airport_id,
                            service_type_id: citytype._id
                        }).then((airportcity) => {
                            if (airportcity && airportcity.price > 0) {
                                var estimated_fare = (airportcity.price).toFixed(2);
                                var trip_type = constant_json.TRIP_TYPE_AIRPORT;
                                res.json({
                                    success: true,
                                    trip_type: trip_type,
                                    message: success_messages.MESSAGE_CODE_YOU_GET_FARE_ESTIMATE,
                                    time: timeMinutes,
                                    distance: (distanceKmMile).toFixed(2),
                                    estimated_fare: Number(estimated_fare),
                                    unit_set: city.unit
                                });

                            } else {
                                cityCheck(cityid, citytype, body, timeMinutes, distanceKmMile, res)
                            }

                        })


                    } else {
                        cityCheck(cityid, citytype, body, timeMinutes, distanceKmMile, res);
                    }


                } else {
                    cityCheck(cityid, citytype, body, timeMinutes, distanceKmMile, res)
                }
            })
        } else {
            cityCheck(cityid, citytype, body, timeMinutes, distanceKmMile, res)
        }

    });
}

function cityCheck(cityid, citytype, body, timeMinutes, distanceKmMile, res) {
    var flag = 0;
    var k = 0;
    City.findOne({ '_id': cityid, city_business: 1 }).then((city) => {
        
        if (city) {
            CitytoCity.find({ city_id: cityid, service_type_id: citytype._id, destination_city_id: { $in: city.destination_city } }).then((citytocity) => {

                if (citytocity !== null && citytocity.length > 0) {

                    citytocity.forEach(function (citytocity_detail) {

                        City.findById(citytocity_detail.destination_city_id).then((city_detail) => {
                            if (flag == 0) {

                                var city_radius = city_detail.cityRadius;
                                var destination_city_radius = utils.getDistanceFromTwoLocation([body.destination_latitude, body.destination_longitude], city_detail.cityLatLong);

                                var inside_city;
                                if (city_detail.city_locations && city_detail.city_locations.length > 2) {
                                    inside_city = geolib.isPointInside(
                                        {
                                            latitude: body.destination_latitude,
                                            longitude: body.destination_longitude
                                        },
                                        city_detail.city_locations
                                    );
                                }

                                if (citytocity_detail.price > 0 && ((!city_detail.is_use_city_boundary && city_radius > destination_city_radius) || (city_detail.is_use_city_boundary && inside_city))) {
                                    var estimated_fare = (citytocity_detail.price).toFixed(2);
                                    var trip_type = constant_json.TRIP_TYPE_CITY;
                                    flag = 1;
                                    res.json({
                                        success: true,
                                        trip_type: trip_type,
                                        message: success_messages.MESSAGE_CODE_YOU_GET_FARE_ESTIMATE,
                                        time: timeMinutes,
                                        distance: (distanceKmMile).toFixed(2),
                                        estimated_fare: Number(estimated_fare),
                                        unit_set: city.unit
                                    })

                                } else if (citytocity.length - 1 == k) {
                                    other(cityid, citytype, body, timeMinutes, distanceKmMile, res)
                                } else {
                                    k++;
                                }
                            }
                        });
                    });
                } else {
                    other(cityid, citytype, body, timeMinutes, distanceKmMile, res)
                }
            });
        } else {
            other(cityid, citytype, body, timeMinutes, distanceKmMile, res)
        }
    });
}

function other(cityid, citytype, body, timeMinutes, distanceKmMile, res) {
    City.findOne({ _id: cityid }).then((city) => {

        var base_distance = citytype.base_price_distance;
        var base_price = citytype.base_price;
        var price_per_unit_distance1 = citytype.price_per_unit_distance;
        var price_for_total_time1 = citytype.price_for_total_time;
        var tax = citytype.tax;
        var min_fare = citytype.min_fare;
        var surge_multiplier = citytype.surge_multiplier;

        if (body.surge_multiplier) {
            surge_multiplier = Number(body.surge_multiplier);
        }

        if (distanceKmMile <= base_distance) {
            price_per_unit_distance = 0;
        } else {
            price_per_unit_distance = (price_per_unit_distance1 * (distanceKmMile - base_distance)).toFixed(2);
        }

        price_for_total_time = Number((price_for_total_time1 * timeMinutes).toFixed(2));

        var total = 0;
        total = +base_price + +price_per_unit_distance + +price_for_total_time;
        try {
            if (Number(body.is_surge_hours) == 1) {
                total = total * surge_multiplier;
            }
        } catch (error) {

        }
        // tax cal
        total = total + total * 0.01 * tax;
        var is_min_fare_used = 0;
        if (total < min_fare) {
            total = min_fare;
            is_min_fare_used = 1;
        }
        var estimated_fare = Number(total.toFixed(2));
        var trip_type = constant_json.TRIP_TYPE_NORMAL;
        res.json({
            success: true,
            trip_type: trip_type,
            user_tax_fee: 0,
            user_miscellaneous_fee: 0,
            message: success_messages.MESSAGE_CODE_YOU_GET_FARE_ESTIMATE,
            time: timeMinutes,
            distance: (distanceKmMile).toFixed(2),
            is_min_fare_used: is_min_fare_used,
            base_price: base_price,
            price_per_unit_distance: price_per_unit_distance,
            price_per_unit_time: price_for_total_time,
            estimated_fare: estimated_fare,
            unit_set: city.unit
        });


    });
}


////APPLY PROMO CODE///

exports.remove_promo_code = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, { name: 'trip_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {

                if (user) {
                    if (req.body.token != null && user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        User_promo_use.findOneAndRemove({ user_id: req.body.user_id, trip_id: req.body.trip_id }, function () {
                            Trip.findOne({ _id: req.body.trip_id }, function (error, trip) {

                                Promo_Code.findOne({ _id: trip.promo_id }, function (error, promocode_data) {
                                    trip.promo_id = null;
                                    trip.save();
                                    if (promocode_data) {
                                        promocode_data.user_used_promo--;
                                        promocode_data.save();
                                    }
                                    res.json({ success: true, message: success_messages.MESSAGE_CODE_PROMOCODE_REMOVE_SUCCESSFULLY });
                                })
                            })
                        })
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });

                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.apply_promo_code = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, { name: 'promocode', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {
                    if (req.body.token != null && user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        var now = new Date();
                        if (req.body.trip_id) {
                            Trip.findOne({ _id: req.body.trip_id }).then((trip) => {
                                if (trip) {
                                    var country_id = trip.country_id;
                                    Promo_Code.findOne({
                                        promocode: req.body.promocode,
                                        state: 1,
                                        countryid: country_id
                                        // start_date: { $lte: now },
                                        // code_expiry: { $gte: now }
                                    }).then((promocode) => {
                                        if (promocode) {
                                           //check promo code is expiry or not
                                            if(promocode.start_date <= now && promocode.code_expiry >= now ){
                                                if (promocode.user_used_promo < promocode.code_uses) {
                                                    User_promo_use.findOne({
                                                        user_id: req.body.user_id,
                                                        promo_id: promocode._id
                                                    }).then((used_promo_data) => {
                                                        if (used_promo_data) {
                                                            res.json({
                                                                success: false,
                                                                error_code: error_message.ERROR_CODE_PROMOTIONAL_CODE_ALREADY_USED
                                                            });
                                                        } else {
                                                            Citytype.findOne({ _id: trip.service_type_id }).then((citytypedetail) => {
                                                                if (citytypedetail) {
                                                                    var cityid = citytypedetail.cityid;
                                                                    var countryid = citytypedetail.countryid;
                                                                    City.findOne({ _id: cityid }).then(async (citydetail) => {

                                                                        var promo_apply_for_cash = citydetail.isPromoApplyForCash;
                                                                        var promo_apply_for_card = citydetail.isPromoApplyForCard;
                                                                        var is_promo_apply = 0;
                                                                        if (trip.payment_mode == constant_json.PAYMENT_MODE_CASH && promo_apply_for_cash == constant_json.YES) {
                                                                            is_promo_apply = 1;
                                                                        } else if (trip.payment_mode == constant_json.PAYMENT_MODE_CARD && promo_apply_for_card == constant_json.YES) {
                                                                            is_promo_apply = 1;
                                                                        }
                                                                        const trips = await Trip_history.find({ user_id: req.body.user_id, is_trip_completed: 1, is_trip_cancelled: 0})
                                                                        trip_count = trips.length
                                                                        // trip_count = user.completed_request;
                                                                        is_promo_code_valid = false;
                                                                        if (!promocode.completed_trips_type) {
                                                                            promocode.completed_trips_type = 2;
                                                                        }
                                                                        if (!promocode.completed_trips_value) {
                                                                            promocode.completed_trips_value = 0;
                                                                        }
                                                                        if (promocode.completed_trips_type == 1) {
                                                                            is_promo_code_valid = (promocode.completed_trips_value == trip_count);
                                                                        } else if (promocode.completed_trips_type == 2) {
                                                                            is_promo_code_valid = (trip_count >= promocode.completed_trips_value);
                                                                        }
                                                                        if (is_promo_code_valid) {
                                                                            if (is_promo_apply) {

                                                                                if (promocode.cityid.indexOf(cityid) !== -1 && promocode.countryid.equals(countryid)) {
                                                                                    trip.promo_id = promocode._id;
                                                                                    trip.promo_code = promocode.promocode;
                                                                                    trip.save();
                                                                                    promocode.user_used_promo = promocode.user_used_promo + 1;
                                                                                    promocode.save();
                                                                                    var userpromouse = new User_promo_use({
                                                                                        promo_id: promocode._id,
                                                                                        promocode: promocode.promocode,
                                                                                        user_id: req.body.user_id,
                                                                                        promo_type: promocode.code_type,
                                                                                        promo_value: promocode.code_value,
                                                                                        trip_id: trip._id,
                                                                                        user_used_amount: 0

                                                                                    });
                                                                                    userpromouse.save().then(() => {
                                                                                        res.json({
                                                                                            success: true, promo_id: promocode._id,
                                                                                            message: success_messages.MESSAGE_CODE_PROMOTIONAL_CODE_APPLIED_SUCCESSFULLY
                                                                                        });
                                                                                    });
                                                                                } else {

                                                                                    res.json({
                                                                                        success: false,
                                                                                        error_code: error_message.ERROR_CODE_PROMO_CODE_NOT_FOR_YOUR_AREA
                                                                                    });
                                                                                }
                                                                            } else {
                                                                                res.json({
                                                                                    success: false,
                                                                                    error_code: error_message.ERROR_CODE_PROMO_CODE_NOT_APPLY_ON_YOUR_PAYMENT_MODE
                                                                                });
                                                                            }
                                                                        } else {
                                                                            res.json({
                                                                                success: false,
                                                                                error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE
                                                                            });
                                                                        }
                                                                    });
                                                                } else {
                                                                    res.json({
                                                                        success: false,
                                                                        error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE
                                                                    });
                                                                }
                                                            });

                                                        }
                                                    });
                                                } else {
                                                    res.json({
                                                        success: false,
                                                        error_code: error_message.ERROR_CODE_PROMO_CODE_EXPIRED_OR_INVALID
                                                    });
                                                }
                                            }else{
                                                res.json({
                                                    success: false,
                                                    error_code: error_message.ERROR_CODE_PROMO_CODE_EXPIRED_OR_INVALID
                                                });  
                                            }
                                        } else {
                                            res.json({
                                                success: false,
                                                error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE
                                            });
                                        }

                                    });
                                } else {
                                    res.json({
                                        success: false,
                                        error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE
                                    });
                                }
                            });
                        } else {
                            var country_id = req.body.country_id;
                            Promo_Code.findOne({
                                promocode: req.body.promocode,
                                state: 1,
                                countryid: country_id
                                // start_date: { $lte: now },
                                // code_expiry: { $gte: now }
                            }).then((promocode) => {

                                if (promocode) {
                                    //check promo code is expiry or not
                                    if(promocode.start_date <= now && promocode.code_expiry >= now ){
                                        if (promocode.user_used_promo < promocode.code_uses) {
                                            User_promo_use.findOne({
                                                user_id: req.body.user_id,
                                                promo_id: promocode._id
                                            }).then((used_promo_data) => {
                                                if (used_promo_data) {
                                                    res.json({
                                                        success: false,
                                                        error_code: error_message.ERROR_CODE_PROMOTIONAL_CODE_ALREADY_USED
                                                    });
                                                } else {

                                                    City.findOne({ _id: req.body.city_id }).then(async (citydetail) => {

                                                        var promo_apply_for_cash = citydetail.isPromoApplyForCash;
                                                        var promo_apply_for_card = citydetail.isPromoApplyForCard;
                                                        var is_promo_apply = 0;
                                                        if (req.body.payment_mode == constant_json.PAYMENT_MODE_CASH && promo_apply_for_cash == constant_json.YES) {
                                                            is_promo_apply = 1;
                                                        } else if (req.body.payment_mode == constant_json.PAYMENT_MODE_CARD && promo_apply_for_card == constant_json.YES) {
                                                            is_promo_apply = 1;
                                                        }

                                                        const trips = await Trip_history.find({ user_id: req.body.user_id, is_trip_completed: 1, is_trip_cancelled: 0})
                                                        trip_count = trips.length
                                                        // trip_count = user.completed_request;
                                                        is_promo_code_valid = false;
                                                        if (!promocode.completed_trips_type) {
                                                            promocode.completed_trips_type = 2;
                                                        }
                                                        if (!promocode.completed_trips_value) {
                                                            promocode.completed_trips_value = 0;
                                                        }
                                                        if (promocode.completed_trips_type == 1) {
                                                            is_promo_code_valid = (promocode.completed_trips_value == trip_count);
                                                        } else if (promocode.completed_trips_type == 2) {
                                                            is_promo_code_valid = (trip_count >= promocode.completed_trips_value);
                                                        }
                                                        if (is_promo_code_valid) {
                                                            if (is_promo_apply) {

                                                                if (promocode.cityid.indexOf(req.body.city_id) !== -1 && promocode.countryid.equals(country_id)) {
                                                                    res.json({
                                                                        success: true,
                                                                        promo_id: promocode._id,
                                                                        promocode_name: promocode.name,
                                                                        promo_apply_for_cash: promo_apply_for_cash,
                                                                        promo_apply_for_card: promo_apply_for_card,
                                                                        message: success_messages.MESSAGE_CODE_PROMOTIONAL_CODE_APPLIED_SUCCESSFULLY
                                                                    });
                                                                } else {

                                                                    res.json({
                                                                        success: false,
                                                                        error_code: error_message.ERROR_CODE_PROMO_CODE_NOT_FOR_YOUR_AREA
                                                                    });
                                                                }
                                                            } else {
                                                                res.json({
                                                                    success: false,
                                                                    error_code: error_message.ERROR_CODE_PROMO_CODE_NOT_APPLY_ON_YOUR_PAYMENT_MODE
                                                                });
                                                            }
                                                        } else {
                                                            res.json({
                                                                success: false,
                                                                error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE
                                                            });
                                                        }

                                                    });

                                                }
                                            });
                                        } else {
                                            res.json({
                                                success: false,
                                                error_code: error_message.ERROR_CODE_PROMO_CODE_EXPIRED_OR_INVALID
                                            });
                                        }
                                    }else{
                                        res.json({
                                            success: false,
                                            error_code: error_message.ERROR_CODE_PROMO_CODE_EXPIRED_OR_INVALID
                                        });                              
                                    }
                                } else {
                                    res.json({
                                        success: false,
                                        error_code: error_message.ERROR_CODE_INVALID_PROMO_CODE
                                    });
                                }

                            });
                        }
                    }
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });

                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.get_promo_code_list = function(req, res) {
    utils.check_request_params(req.body, [{ name: 'country_id', type: 'string' }, { name: 'city_id', type: 'string' }], function (response) {
        if (response.success) {
            const request_data_body = req.body
            let date = new Date()
            country_filter = {
                $match: {
                    $and: [
                        { countryid: { $eq: mongoose.Types.ObjectId(request_data_body.country_id) } },
                        { cityid:  mongoose.Types.ObjectId(request_data_body.city_id) },
                        { code_expiry: { $gte: date } },
                        { start_date: { $lte: date } },
                        { state: 1 },
                        { $expr:
                            {
                                $ne: ['$code_uses', '$user_used_promo']
                            }
                        }
                    ]
                }
            }
            Promo_Code.aggregate([country_filter]).then((promos) => {
                if(promos.length > 0) {
                    res.json({
                        success: true,
                        promo_codes: promos
                    });
                } else {
                    res.json({
                        success: false,
                        error_code: error_message.PROMO_CODE_NOT_FOUND
                    });
                }
            })
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
}

//////////////// USER REFERAL CREDIT////////

exports.get_user_referal_credit = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {
                    if (req.body.token != null && user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {

                        var condition = { $match: { user_id: { $eq: Schema(req.body.user_id) } } }
                        var referral_condition = { $match: { wallet_comment_id: { $eq: Number(constant_json.ADDED_BY_REFERRAL) } } }
                        var group = {
                            $group: {
                                _id: null,
                                total_referral_credit: { $sum: '$added_wallet' }
                            }
                        }

                        Wallet_history.aggregate([condition, referral_condition, group]).then((wallet_history_count) => {
                            if (wallet_history_count.length > 0) {
                                res.json({ success: true, total_referral_credit: wallet_history_count[0].total_referral_credit })
                            } else {
                                res.json({ success: true, total_referral_credit: 0 });
                            }
                        })
                    }

                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });

                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

//////// ADD WALLET AMOUNT ///

exports.add_wallet_amount = async function (req, res) {
    utils.check_request_params(req.body, [], function (response) {
        try {
            console.log(req.query);
            if (response.success) {
                if (req.body.udf2) {
                    req.body.type = req.body.udf2;
                }else if(req.body?.user_defined?.udf2){
                    req.body.type = req.body.user_defined.udf2
                    req.body.udf2 = req.body.user_defined.udf2
                }
                if (req.body?.udf3) {
                    req.body.user_id = req.body.udf3;
                }else if(req.body?.user_defined?.udf3){
                    req.body.user_id = req.body.user_defined.udf3
                }
                let type = Number(req.body.type);
                if(req.query?.payment_gateway_type){
                    req.body.payment_gateway_type = req.query?.payment_gateway_type
                    type = Number(req.query?.type)
                    req.body.user_id = req.query?.user_id
                    req.body.amount = req.query?.amount
                }
                if (req.body?.udf1) {
                    req.body.payment_gateway_type = req.body.udf1;
                }else if(req.body?.user_defined?.udf1){
                    req.body.payment_gateway_type = req.body.user_defined.udf1
                }
                switch (type) {
                    case Number(constant_json.USER_UNIQUE_NUMBER): // 10
                        type = Number(constant_json.USER_UNIQUE_NUMBER);
                        Table = User;
                        break;
                    case Number(constant_json.PROVIDER_UNIQUE_NUMBER): // 11
                        type = Number(constant_json.PROVIDER_UNIQUE_NUMBER);
                        Table = Provider;
                        break;
                    case Number(constant_json.CORPORATE_UNIQUE_NUMBER):
                        type = Number(constant_json.CORPORATE_UNIQUE_NUMBER);
                        Table = Corporate;
                        break;
                    case Number(constant_json.PARTNER_UNIQUE_NUMBER):
                        type = Number(constant_json.PARTNER_UNIQUE_NUMBER);
                        Table = Partner;
                        break;
                    default:
                        type = Number(constant_json.USER_UNIQUE_NUMBER); // 10
                        Table = User;
                        break;
                }
                Table.findOne({ _id: req.body.user_id }).then(async(detail) => {
                    if (req.body.token && detail.token != req.body.token && !req.body.udf2) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        var payment_id = Number(constant_json.PAYMENT_BY_STRIPE);
                        try {
                            payment_id = req.body.payment_id;
                        } catch (error) {
                            console.error(err);
                        }
    
                        switch (payment_id) {
                            case Number(constant_json.PAYMENT_BY_STRIPE):
                                break;
                            case Number(constant_json.PAYMENT_BY_PAYPAL):
                                break;
                        }
                        if (!req.body.payment_gateway_type || req.body.payment_gateway_type == PAYMENT_GATEWAY.stripe || req.body?.is_apple_pay) {
                            
                            let url = setting_detail.payments_base_url + "/retrieve_payment_intent"
                            let data = {
                                payment_intent_id: req.body.payment_intent_id
                            }
    
                            const request = require('request');
                            request.post(
                            {
                                url: url,
                                headers: {
                                    "Content-Type": "application/json",
                                },
                                body: JSON.stringify(data),
                            }, (error, response, body) => {
                                if (error) {
                                    console.error(error);
                                    return error
                                } else {
                                    body = JSON.parse(body);
                                    intent = body.intent;
    
                                    if (intent && intent.charges && intent.charges.data && intent.charges.data.length > 0) {
    
                                        var total_wallet_amount = utils.addWalletHistory(type, detail.unique_id, detail._id, detail.country_id, detail.wallet_currency_code, detail.wallet_currency_code,
                                            1, (intent.charges.data[0].amount / 100), detail.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_CARD, "Card : " + intent.charges.data[0].payment_method_details.card.last4)
                                        detail.wallet = total_wallet_amount;
                                        if(req.body?.is_apple_pay){
                                            utils.apple_pay_socket(req.body?.user_id)
                                        }
                                        detail.save().then(() => {
                                            res.json({
                                                success: true,
                                                message: success_messages.MESSAGE_CODE_WALLET_AMOUNT_ADDED_SUCCESSFULLY,
                                                wallet: detail.wallet,
                                                wallet_currency_code: detail.wallet_currency_code
            
                                            });
                                        });
            
                                    }
                                }
                            });
                        } else if (req.body.payment_gateway_type == PAYMENT_GATEWAY.paystack) {
                            var total_wallet_amount = utils.addWalletHistory(type, detail.unique_id, detail._id, detail.country_id, detail.wallet_currency_code, detail.wallet_currency_code,
                                1, (req.body.paystack_data.amount / 100), detail.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_CARD, "Card : " + req.body.paystack_data.authorization.last4)
    
                            detail.wallet = total_wallet_amount;
                            detail.save().then(() => {
                                message = "Wallet Amount Added Sucessfully.";
                                res.json({
                                    success: true,
                                    message: success_messages.MESSAGE_CODE_WALLET_AMOUNT_ADDED_SUCCESSFULLY,
                                    wallet: detail.wallet,
                                    wallet_currency_code: detail.wallet_currency_code
    
                                });
                            });
                        } else if (req.body.payment_gateway_type == PAYMENT_GATEWAY.razorpay) {
                            const key_secret = setting_detail.razorpay_secret_key
                            const crypto = require("crypto");
                            const generated_signature = crypto.createHmac("SHA256",key_secret).update(req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id).digest("hex");  
                            let is_signature_valid = generated_signature == req.body.razorpay_signature;
                            console.log('(is_signature_valid)'+is_signature_valid)
                            if (is_signature_valid) {
                                var total_wallet_amount = utils.addWalletHistory(type, detail.unique_id, detail._id, detail.country_id, detail.wallet_currency_code, detail.wallet_currency_code,
                                    1, (req.body.amount), detail.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_CARD, "Card : razorpay" )
        
                                detail.wallet = total_wallet_amount;
                                console.log('doneeeeeeeeeeeeeee')
                                utils.paytabs_status_socket(detail._id, true,1)
                                detail.save().then(() => {
                                    if (req.query?.is_new) {
                                        res.redirect(req.query?.is_new);
                                    } else {
                                        res.redirect(setting_detail.payments_base_url + '/success_payment');
                                    }
                                });
                            }else{
                                console.log('na thayu bhaiii')
                                if (req.query?.is_new) {
                                    utils.payu_status_fail_socket(detail._id)
                                    res.redirect(req.query?.is_new);
                                } else {
                                    utils.payu_status_fail_socket(detail._id)
                                    res.redirect(setting_detail.payments_base_url +  '/payment_fail');
                                }
                            }
                        }else if (req.body.payment_gateway_type == PAYMENT_GATEWAY.payu) {

                            var total_wallet_amount = utils.addWalletHistory(type, detail.unique_id, detail._id, detail.country_id, detail.wallet_currency_code, detail.wallet_currency_code,
                                1, (req.body.amount), detail.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_CARD, "Card : payu" )
    
                            detail.wallet = total_wallet_amount;
                            detail.save().then(() => {
                                if (req.body.udf4) {
                                    message = "Wallet Amount Added Sucessfully";
                                    res.redirect(req.body.udf4);
                                } else {
                                    res.json({
                                        success: true,
                                        message: success_messages.MESSAGE_CODE_WALLET_AMOUNT_ADDED_SUCCESSFULLY,
                                        wallet: detail.wallet,
                                        wallet_currency_code: detail.wallet_currency_code
    
                                    });
                                }
                            });
                        } else if (req.body.payment_gateway_type == PAYMENT_GATEWAY.paytabs){
                                var total_wallet_amount = utils.addWalletHistory(type, detail.unique_id, detail._id, detail.country_id, detail.wallet_currency_code, detail.wallet_currency_code,
                                    1, Number(req.body.wallet), detail.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_CARD, "Card : paytabs",req.body.tran_ref)
        
                                detail.wallet = total_wallet_amount;
                                utils.paytabs_status_socket(detail._id, true,1)
                                detail.save().then(() => {
                                    
                                        res.json({
                                            success: true,
                                            message: success_messages.MESSAGE_CODE_WALLET_AMOUNT_ADDED_SUCCESSFULLY,
                                            wallet: detail.wallet,
                                            wallet_currency_code: detail.wallet_currency_code
                                        });
                                });
                        }else if (req.body.payment_gateway_type == PAYMENT_GATEWAY.paypal) {
                            let total_wallet_amount = utils.addWalletHistory(type, detail.unique_id, detail._id, detail.country_id, detail.wallet_currency_code, detail.wallet_currency_code,
                                1, (req.body.wallet), detail.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_CARD, "Card : " + req.body.last_four ,req.body.tran_ref)
                            detail.wallet = total_wallet_amount;
                            detail.save().then(() => {
                                res.json({
                                    success: true,
                                    message: success_messages.MESSAGE_CODE_WALLET_AMOUNT_ADDED_SUCCESSFULLY,
                                    wallet: detail.wallet,
                                    wallet_currency_code: detail.wallet_currency_code
    
                                });
                            });
                        } 
    
                    }
                });
            } else {
                res.json({
                    success: false,
                    error_code: response.error_code,
                    error_description: response.error_description
                });
            }
        } catch (error) {
            console.log(error)
        }
    });
};

exports.change_user_wallet_status = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }, function (err, user) {

                if (user.token != req.body.token) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                } else {
                    var status = req.body.is_use_wallet;
                    user.is_use_wallet = status;
                    user.save().then((user) => {
                        res.json({
                            success: true,
                            message: success_messages.MESSAGE_CODE_CHANGE_WALLET_STATUS_SUCCESSFULLY,
                            is_use_wallet: user.is_use_wallet
                        });
                    });
                }

            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.set_home_address = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            if (req.body.home_address !== undefined) {
                req.body.home_location = [req.body.home_latitude, req.body.home_longitude]
            }

            if (req.body.work_address !== undefined) {
                req.body.work_location = [req.body.work_latitude, req.body.work_longitude]
            }

            User.findOne({ _id: req.body.user_id }).then((user) => {

                if (!user) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                } else {
                    if (user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        User.findByIdAndUpdate(req.body.user_id, req.body).then(() => {
                            res.json({ success: true, message: success_messages.MESSAGE_CODE_SET_ADDRESS_SUCCESSFULLY });

                        })
                    }
                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.get_home_address = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }, {
                token: 1,
                home_address: 1,
                work_address: 1,
                home_location: 1,
                work_location: 1
            }).then((user) => {
                if (!user) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                } else {
                    if (user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {

                        res.json({ success: true, user_address: user });
                    }
                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
};

exports.get_user_privacy_policy = async function (req, res) {
    const setting_detail = await Settings.findOne({});

    res.send(setting_detail.user_privacy_policy)
};

exports.get_user_terms_and_condition = async function (req, res) {
    const setting_detail = await Settings.findOne({});

    res.send(setting_detail.user_terms_and_condition)
};

exports.terms_and_condition = async function (req, res) {
    const setting_detail = await Settings.findOne({});

    res.json({ "success": true, "user_terms_and_condition": setting_detail.user_terms_and_condition, "user_privacy_policy": setting_detail.user_privacy_policy, "provider_terms_and_condition": setting_detail.provider_terms_and_condition, "provider_privacy_policy": setting_detail.provider_privacy_policy ,"user_delete_policy":setting_detail.user_delete_policy,"provider_delete_policy":setting_detail.provider_delete_policy})
};


    
    
exports.get_user_setting_detail = async function (req, res) {
    const setting_detail = await Settings.findOne({});
    var terms_and_condition_url = `${setting_detail.user_panel_url}/legal/user-terms-conditions`
    var privacy_policy_url = `${setting_detail.user_panel_url}/legal/user-privacy-policy`

    var setting_response = {};
    setting_response.terms_and_condition_url = terms_and_condition_url
    setting_response.privacy_policy_url = privacy_policy_url
    setting_response.is_user_social_login = setting_detail.is_user_social_login
    setting_response.is_user_login_using_otp = setting_detail.is_user_login_using_otp
    setting_response.is_allow_multiple_stop = setting_detail.is_allow_multiple_stop;
    setting_response.multiple_stop_count = setting_detail.multiple_stop_count;
    setting_response.web_app_google_key = setting_detail.web_app_google_key
    setting_response.firebase_apiKey = setting_detail.firebase_apiKey
    setting_response.firebase_databaseURL = setting_detail.firebase_databaseURL
    setting_response.firebase_projectId = setting_detail.firebase_projectId
    setting_response.firebase_storageBucket = setting_detail.firebase_storageBucket
    setting_response.firebase_messagingSenderId = setting_detail.firebase_messagingSenderId
    setting_response.is_allow_ride_share = setting_detail.is_allow_ride_share
    setting_response.paypal_client_id = setting_detail.paypal_client_id
    setting_response.paypal_secret_key = setting_detail.paypal_secret_key
    setting_response.paypal_client_id = setting_detail.paypal_client_id
    setting_response.paypal_environment = setting_detail.paypal_environment
    setting_response.webpush_public_key = setting_detail.webpush_public_key
    setting_response.decimal_point_value = setting_detail.decimal_point_value
    setting_response.android_user_app_gcm_key = setting_detail.android_user_app_gcm_key

    setting_response.android_client_app_url = setting_detail.android_client_app_url
    setting_response.ios_client_app_url = setting_detail.ios_client_app_url
    setting_response.is_use_captcha = setting_detail.is_use_captcha;
    setting_response.recaptcha_site_key_for_web = setting_detail.recaptcha_site_key_for_web;
    setting_response.recaptcha_secret_key_for_web = setting_detail.recaptcha_secret_key_for_web;
    setting_response.recaptcha_site_key_for_android = setting_detail.recaptcha_site_key_for_android;
    setting_response.recaptcha_secret_key_for_android = setting_detail.recaptcha_secret_key_for_android;
    setting_response.recaptcha_site_key_for_ios = setting_detail.recaptcha_site_key_for_ios;
    setting_response.recaptcha_secret_key_for_ios = setting_detail.recaptcha_secret_key_for_ios;
    setting_response.location = setting_detail.location

    setting_response.user_panel_google_key = setting_detail.user_panel_google_key;
    setting_response.dispatcher_panel_google_key = setting_detail.dispatcher_panel_google_key;
    setting_response.corporate_panel_google_key = setting_detail.corporate_panel_google_key;
    setting_response.hotel_panel_google_key = setting_detail.hotel_panel_google_key

    setting_response.flutter_user_app_google_places_autocomplete_key = setting_detail.flutter_user_app_google_places_autocomplete_key
    setting_response.flutter_driver_app_google_places_autocomplete_key = setting_detail.flutter_driver_app_google_places_autocomplete_key
    setting_response.driver_panel_url = setting_detail.driver_panel_url
    
    if (req.body.device_type == 'android') {
        setting_response.admin_phone = setting_detail.admin_phone;
        setting_response.contactUsEmail = setting_detail.contactUsEmail;
        setting_response.android_user_app_google_key = setting_detail.android_user_app_google_key;
        setting_response.android_user_app_version_code = setting_detail.android_user_app_version_code;
        setting_response.android_user_app_force_update = setting_detail.android_user_app_force_update;
        setting_response.is_tip = setting_detail.is_tip;
        setting_response.scheduled_request_pre_start_minute = setting_detail.scheduled_request_pre_start_minute;
        setting_response.stripe_publishable_key = setting_detail.stripe_publishable_key;
        setting_response.userPath = setting_detail.userPath;
        setting_response.userSms = setting_detail.userSms;
        setting_response.is_otp_verification_start_trip = setting_detail.is_otp_verification_start_trip;
        setting_response.userEmailVerification = setting_detail.userEmailVerification;
        setting_response.twilio_call_masking = setting_detail.twilio_call_masking;
        setting_response.is_show_estimation_in_provider_app = setting_detail.is_show_estimation_in_provider_app;
        setting_response.is_show_estimation_in_user_app = setting_detail.is_show_estimation_in_user_app;
        setting_response.android_places_autocomplete_key = setting_detail.android_places_autocomplete_key;
        setting_response.recaptcha_site_key_for_android = setting_detail.recaptcha_site_key_for_android;
        
        setting_response.android_user_app_google_map_key = setting_detail.android_user_app_google_map_key;
        setting_response.android_user_app_google_places_autocomplete_key = setting_detail.android_user_app_google_places_autocomplete_key;
        setting_response.android_user_app_google_geocoding_key = setting_detail.android_user_app_google_geocoding_key;
        setting_response.android_user_app_google_distance_matrix_key = setting_detail.android_user_app_google_distance_matrix_key;
        setting_response.android_user_app_google_direction_matrix_key = setting_detail.android_user_app_google_direction_matrix_key;
 
        
        
    } else {
        setting_response.admin_phone = setting_detail.admin_phone;
        setting_response.contactUsEmail = setting_detail.contactUsEmail;
        setting_response.ios_user_app_google_key = setting_detail.ios_user_app_google_key;
        setting_response.ios_user_app_version_code = setting_detail.ios_user_app_version_code;
        setting_response.ios_user_app_force_update = setting_detail.ios_user_app_force_update;
        setting_response.is_tip = setting_detail.is_tip;
        setting_response.scheduled_request_pre_start_minute = setting_detail.scheduled_request_pre_start_minute;
        setting_response.stripe_publishable_key = setting_detail.stripe_publishable_key;
        setting_response.userPath = setting_detail.userPath;
        setting_response.userSms = setting_detail.userSms;
        setting_response.is_otp_verification_start_trip = setting_detail.is_otp_verification_start_trip;
        setting_response.twilio_call_masking = setting_detail.twilio_call_masking;
        setting_response.is_show_estimation_in_provider_app = setting_detail.is_show_estimation_in_provider_app;
        setting_response.is_show_estimation_in_user_app = setting_detail.is_show_estimation_in_user_app;
        setting_response.ios_places_autocomplete_key = setting_detail.ios_places_autocomplete_key;
        setting_response.userEmailVerification = setting_detail.userEmailVerification;
        setting_response.recaptcha_site_key_for_ios = setting_detail.recaptcha_site_key_for_ios

        setting_response.ios_user_app_google_map_key = setting_detail.ios_user_app_google_map_key;
        setting_response.ios_user_app_google_places_autocomplete_key = setting_detail.ios_user_app_google_places_autocomplete_key;
        setting_response.ios_user_app_google_geocoding_key = setting_detail.ios_user_app_google_geocoding_key;
        setting_response.ios_user_app_google_distance_matrix_key = setting_detail.ios_user_app_google_distance_matrix_key;
        setting_response.ios_user_app_google_direction_matrix_key = setting_detail.ios_user_app_google_direction_matrix_key;
 
    }
       
    setting_response.image_base_url = setting_detail.image_base_url;
    setting_response.minimum_phone_number_length = setting_detail.minimum_phone_number_length;
    setting_response.maximum_phone_number_length = setting_detail.maximum_phone_number_length;
    setting_response.is_split_payment = setting_detail.is_split_payment;
    setting_response.max_split_user = setting_detail.max_split_user;
    setting_response.is_guest_token = setting_detail.is_guest_token;
    setting_response.active_guest_token = setting_detail.active_guest_token;

    var user_id = req.body.user_id;
    if (user_id == '') {
        user_id = null;
    }
    User.findOne({ _id: user_id }).then(async (user_detail) => {
        if (user_detail && user_detail.token !== req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN, setting_detail: setting_response });
        } else {

       
       
        var corporate_id = null;
        
    
            let response = {};
            if (user_detail) {

                let alpha2 = country_json.filter((country) => country.name == user_detail.country)
                if (user_detail.corporate_ids && user_detail.corporate_ids.length > 0) {
                    corporate_id = user_detail.corporate_ids[0].corporate_id;
                   }

                response.first_name = user_detail.first_name;
                response.last_name = user_detail.last_name;
                response.email = user_detail.email;
                response.country_phone_code = user_detail.country_phone_code;
                response.is_document_uploaded = user_detail.is_document_uploaded;
                response.address = user_detail.address;
                response.is_approved = user_detail.is_approved;
                response.user_id = user_detail._id;
                response.social_ids = user_detail.social_ids;
                response.social_unique_id = user_detail.social_unique_id;
                response.phone = user_detail.phone;
                response.login_by = user_detail.login_by;
                response.city = user_detail.city;
                response.country = user_detail.country;
                response.referral_code = user_detail.referral_code;
                response.refferal_credit = user_detail.refferal_credit
                response.rate = user_detail.rate;
                response.rate_count = user_detail.rate_count;
                response.is_referral = user_detail.is_referral;
                response.token = user_detail.token;
                response.picture = user_detail.picture;
                response.wallet_currency_code = user_detail.wallet_currency_code;
                response.created_at = user_detail.created_at;
                 response.alpha2 = alpha2[0]?.alpha2
                 response.is_documents_expired =  user_detail.is_documents_expired 

                var corporate_id = null;
                if (user_detail.corporate_ids && user_detail.corporate_ids.length > 0) {
                    corporate_id = user_detail.corporate_ids[0].corporate_id;
                }

                Corporate.findOne({ _id: corporate_id }).then(async (corporate_detail) => {

                    if (corporate_detail) {
                        response.corporate_detail = {
                            name: corporate_detail.name,
                            phone: corporate_detail.phone,
                            country_phone_code: corporate_detail.country_phone_code,
                            status: user_detail.corporate_ids[0].status,
                            _id: corporate_detail._id
                        }
                    }

                    Country.findOne({ countryphonecode: user_detail.country_phone_code }).then(async (country) => {
                        if (country) {
                            response.country_detail = { "is_referral": country.is_referral }
                            response.total_redeem_point = user_detail.total_redeem_point
                            response.user_redeem_point_value = country?.user_redeem_settings[0]?.user_redeem_point_value
                            response.user_minimum_point_require_for_withdrawal = country?.user_redeem_settings[0]?.user_minimum_point_require_for_withdrawal
                            setting_response.payment_gateway_type = country.payment_gateways[0]
                            response.is_send_money_for_user = country.is_send_money_for_user ? country.is_send_money_for_user : false
                        } else {
                            response.country_detail = { "is_referral": false }
                        }

                        let pipeline = [
                            { $match: { 'split_payment_users.user_id': user_detail._id } },
                            { $match: { 'is_trip_cancelled': 0 } },
                            {
                                $project: {
                                    trip_id: '$_id',
                                    is_trip_end: 1,
                                    currency: 1,
                                    user_id: 1,
                                    currencycode:1,
                                    split_payment_users: {
                                        $filter: {
                                            input: "$split_payment_users",
                                            as: "item",
                                            cond: { $eq: ["$$item.user_id", user_detail._id] }
                                        }
                                    }
                                }
                            },
                            { $unwind: "$split_payment_users" },
                            {
                                $match: {
                                    $or: [
                                        { 'split_payment_users.status': SPLIT_PAYMENT.WAITING },
                                        {
                                            $and: [
                                                { 'split_payment_users.status': SPLIT_PAYMENT.ACCEPTED },
                                                { 'split_payment_users.payment_status': { $ne: PAYMENT_STATUS.COMPLETED } },
                                                { 'is_trip_end': 1 }
                                            ]
                                        },
                                        {
                                            $and: [
                                                { 'split_payment_users.status': SPLIT_PAYMENT.ACCEPTED },
                                                { 'split_payment_users.payment_status': { $ne: PAYMENT_STATUS.COMPLETED } },
                                                { 'split_payment_users.payment_mode': null }
                                            ]
                                        }
                                    ]
                                }
                            },
                            {
                                $lookup:
                                {
                                    from: "users",
                                    localField: "user_id",
                                    foreignField: "_id",
                                    as: "user_detail"
                                }
                            },
                            { $unwind: "$user_detail" },
                            {
                                $project: {
                                    trip_id: 1,
                                    first_name: '$user_detail.first_name',
                                    last_name: '$user_detail.last_name',
                                    phone: '$user_detail.phone',
                                    country_phone_code: '$user_detail.country_phone_code',
                                    user_id: '$user_detail._id',
                                    is_trip_end: 1,
                                    currency: 1,
                                    status: '$split_payment_users.status',
                                    payment_mode: '$split_payment_users.payment_mode',
                                    payment_status: '$split_payment_users.payment_status',
                                    payment_intent_id: '$split_payment_users.payment_intent_id',
                                    total: '$split_payment_users.total',
                                    currency_code:"$currencycode"
                                }
                            },
                        ]
                        let split_payment_request = await Trip.aggregate(pipeline);
                        if (split_payment_request.length == 0) {
                            split_payment_request = await Trip_history.aggregate(pipeline);
                        }
                        if (user_detail.current_trip_id) {
                            Trip.findOne({ _id: user_detail.current_trip_id }).then((trip_detail) => {
                                Trip_history.findOne({ _id: user_detail.current_trip_id }).then((trip_history_detail) => {
                                    if (!trip_detail) {
                                        trip_detail = trip_history_detail;
                                    }
                                    if(!trip_detail){
                                        trip_detail = OpenRide.findOne({ _id: user_detail.current_trip_id })
                                    }
                                    response.trip_id = user_detail.current_trip_id;
                                    if(trip_detail){
                                        if (trip_detail.openride) {
                                            response.provider_id = trip_detail.provider_id;
                                            response.is_user_invoice_show = trip_detail.is_user_invoice_show;
                                        } else {
                                            response.provider_id = trip_detail.current_provider;
                                            response.is_user_invoice_show = trip_detail.is_user_invoice_show;
                                        }
                                        
                                        response.is_provider_accepted = trip_detail.is_provider_accepted;
                                        response.is_provider_status = trip_detail.is_provider_status;
                                        response.is_trip_end = trip_detail.is_trip_end;
                                        response.is_trip_completed = trip_detail.is_trip_completed;
                                    }
                                    console.log(response)
                                    res.json({ success: true, setting_detail: setting_response, user_detail: response, split_payment_request: split_payment_request[0] });
                                });
                            });
                        } else {
                            res.json({ success: true, setting_detail: setting_response, user_detail: response, split_payment_request: split_payment_request[0] });
                        }
                    });
                });
            } else {
                res.json({ success: true, setting_detail: setting_response })
            }
        }
    })

}


exports.user_accept_reject_corporate_request = function (req, res) {
    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (!user) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                } else {
                    if (user.token != req.body.token) {
                        res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                    } else {
                        if (req.body.is_accepted) {
                            if (user.corporate_wallet_limit < 0) {
                                var wallet = utils.precisionRoundTwo(Number(user.corporate_wallet_limit));
                                var status = constant_json.DEDUCT_WALLET_AMOUNT
                                var total_wallet_amount = utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id, user.country_id, user.wallet_currency_code, user.wallet_currency_code,
                                    1, Math.abs(wallet), user.wallet, status, constant_json.ADDED_BY_ADMIN, "Corporate Wallet Settlement")
                                user.wallet = total_wallet_amount;
                                user.corporate_wallet_limit = 0;
                            }

                            var index = user.corporate_ids.findIndex((x) => x.corporate_id == req.body.corporate_id);
                            user.user_type_id = req.body.corporate_id;
                            user.user_type = Number(constant_json.USER_TYPE_CORPORATE)
                            if (index != -1) {
                                user.corporate_ids[index].status = Number(constant_json.CORPORATE_REQUEST_ACCEPTED);
                            }
                            utils.req_type_id_socket(req.body.corporate_id)
                            user.markModified('corporate_ids');
                            user.save().then(() => {
                                res.json({ success: true, message: success_messages.MESSAGE_CODE_CORPORATE_REQUEST_ACCEPT_SUCCESSFULLY });
                            })
                        } else {
                            var index = user.corporate_ids.findIndex((x) => x.corporate_id == req.body.corporate_id);
                            utils.req_type_id_socket(req.body.corporate_id)
                            if (index != -1) {
                                user.corporate_ids.splice(index, 1);
                            }
                            user.markModified('corporate_ids');
                            user.save().then(() => {
                                res.json({ success: true, message: success_messages.MESSAGE_CODE_CORPORATE_REQUEST_REJECT_SUCCESSFULLY });
                            })
                        }
                    }
                }
            });
        } else {
            res.json({
                success: false,
                error_code: response.error_code,
                error_description: response.error_description
            });
        }
    });
}

exports.add_favourite_driver = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {

                    user.favourite_providers.push(req.body.provider_id);
                    user.save(() => {
                        res.json({ success: true, message: success_messages.MESSAGE_CODE_ADD_FAVOURITE_DRIVER_SUCCESSFULLY });
                    }, () => {

                    });
                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                }
            });
        }
    });
}

exports.get_favourite_driver = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then((user) => {
                if (user) {

                    var condition = { $match: { _id: { $in: user.favourite_providers } } }
                    var project = {
                        $project: {
                            first_name: 1,
                            last_name: 1,
                            picture: 1
                        }
                    }
                    Provider.aggregate([condition, project], function (error, provider_list) {
                        if (error) {
                            res.json({ success: true, provider_list: [] });
                        } else {
                            res.json({ success: true, provider_list: provider_list });
                        }
                    })

                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                }
            });
        }
    });
}

exports.remove_favourite_driver = function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }], function (response) {
        if (response.success) {
            User.findOne({ _id: req.body.user_id }).then(async (user) => {
                if (user) {

                    var index = user.favourite_providers.findIndex((x) => (x).toString() == req.body.provider_id);
                    if (index !== -1) {
                        user.favourite_providers.splice(index, 1);
                    }
                    if(user.current_trip_id != null) {
                        console.log('--------------');
                        const trip = await Trip.findOne({ _id: user.current_trip_id})
                        console.log(trip.provider_id);

                        // console.log((req.body.provider_id).equals(trip.provider_id))

                        console.log((trip.provider_id).toString() + '------------');
                        if((trip.provider_id).toString() == req.body.provider_id) {
                            console.log('+++++++++++')
                            await Trip.findOneAndUpdate({ _id: trip._id}, {is_favourite_provider: false}, { new: true})
                        }
                    }
                    user.save(() => {
                        res.json({ success: true, message: success_messages.MESSAGE_CODE_REMOVE_FAVOURITE_DRIVER_SUCCESSFULLY });
                    }, () => {

                    });

                } else {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
                }
            });
        }
    });
}

exports.get_all_driver_list = async function (req, res) {
    try {
        let params_array = [{ name: 'user_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let provider_list = await Provider
            .find({
                is_approved: 1,
                country_phone_code: user.country_phone_code,
                _id: { $nin: user.favourite_providers },
                $or: [
                    { email: req.body.search_value },
                    { phone: req.body.search_value }
                ]
            })
            .select({
                first_name: 1,
                last_name: 1,
                picture: 1
            })
            .lean();

        res.json({ success: true, provider_list: provider_list });
        return;

    } catch (err) {
        console.log("exports.get_all_driver_list")
        utils.error_response(err, req, res)
    }
}

exports.search_user_for_split_payment = async function (req, res) {
    try {
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'search_user', type: 'string' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        if (!req.body.search_user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        let trip_detail = await Trip.findOne({ _id: user.current_trip_id }, { split_payment_users: 1 });
        if (!trip_detail) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });
        }

        let phones = [];
        let emails = [];
        trip_detail.split_payment_users.forEach((split_payment_user_detail) => {
            phones.push(split_payment_user_detail.phone);
            emails.push(split_payment_user_detail.email);
        })

        let search_user_detail = await User
            .findOne({
                _id: { $ne: req.body.user_id },
                phone: { $nin: phones },
                email: { $nin: emails },
                country_phone_code: user.country_phone_code,
                $or: [
                    { phone: req.body.search_user },
                    { email: req.body.search_user }
                ]
            })
            .select({
                first_name: 1,
                last_name: 1,
                email: 1,
                phone: 1,
                country_phone_code: 1,
                picture: 1
            });

        if (!search_user_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_NOT_FOUND });
            return;
        }

        res.json({ success: true, search_user_detail: search_user_detail });
        return;

    } catch (err) {
        console.log("exports.search_user_for_split_payment")
        utils.error_response(err, req, res)
    }
}

exports.send_split_payment_request = async function (req, res) {
    try {
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'split_request_user_id', type: 'string' },
            { name: 'trip_id', type: 'string' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let search_user_detail = await User
            .findOne({
                country_phone_code: user.country_phone_code,
                _id: req.body.split_request_user_id
            })
            .select({
                unique_id: 1,
                first_name: 1,
                split_payment_requests: 1,
                last_name: 1,
                email: 1,
                phone: 1,
                device_token: 1,
                device_type: 1,
                country_phone_code: 1,
                picture: 1
            });

        if (!search_user_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_DETAIL_NOT_FOUND });
            return;
        }

        let detail = {
            user_id: req.body.split_request_user_id,
            first_name: search_user_detail.first_name,
            last_name: search_user_detail.last_name,
            phone: search_user_detail.phone,
            country_phone_code: search_user_detail.country_phone_code,
            email: search_user_detail.email,
            picture: search_user_detail.picture,
            payment_intent_id: "",
            status: 0,
            payment_mode: null,
            payment_status: 0,
            total: 0,
            remaining_payment: 0,
            cash_payment: 0,
            card_payment: 0,
            wallet_payment: 0,
            unique_id : search_user_detail.unique_id,
        }
        let trip_detail = await Trip.findOneAndUpdate({ _id: user.current_trip_id, 'split_payment_users.user_id': search_user_detail._id },
            { 'split_payment_users.$.status': 0 });
        if (!trip_detail) {
            trip_detail = await Trip.findOneAndUpdate({ _id: user.current_trip_id }, { '$push': { 'split_payment_users': detail } });
        }

        if (!trip_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });
            return;
        }

        let split_payment_request = {
            "_id": trip_detail._id,
            "is_trip_end": 0,
            "trip_id": trip_detail._id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "phone": user.phone,
            "country_phone_code": user.country_phone_code,
            "user_id": user._id,
            "status": 0,
            "payment_mode": null,
            "payment_status": 0,
            "payment_intent_id": "",
            "total": 0
        }
        utils.update_request_status_socket(trip_detail._id);
        utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, search_user_detail.device_type, search_user_detail.device_token, push_messages.PUSH_CODE_FOR_SPLIT_PAYMENT_REQUEST, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS, split_payment_request);
        utils.req_type_id_socket(req.body.split_request_user_id)
        res.json({ success: true, message: success_messages.MESSAGE_CODE_ADD_SPLIT_REQUEST_SEND_SUCCESSFULLY });
        return;

    } catch (err) {
        console.log("exports.send_split_payment_request")
        utils.error_response(err, req, res)
    }
}

exports.accept_or_reject_split_payment_request = async function (req, res) {
    try {
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'trip_id', type: 'string' },
            { name: 'status', type: 'number' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        if (!(req.body.status == SPLIT_PAYMENT.ACCEPTED || req.body.status == SPLIT_PAYMENT.REJECTED)) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        let trip_detail = await Trip.findOneAndUpdate({ _id: req.body.trip_id, 'split_payment_users.user_id': user._id },
            { 'split_payment_users.$.status': req.body.status });

        if (!trip_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });
            return;
        }

        let trip_user = await User.findOne({ _id: trip_detail.user_id })
        if (trip_user) {
            if (req.body.status == SPLIT_PAYMENT.ACCEPTED) {
                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, trip_user.device_type, trip_user.device_token, push_messages.PUSH_CODE_FOR_ACCEPT_SPLIT_PAYMENT_REQUEST, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",trip_user.webpush_config);
            } else {
                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, trip_user.device_type, trip_user.device_token, push_messages.PUSH_CODE_FOR_REJECT_SPLIT_PAYMENT_REQUEST, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",trip_user.webpush_config);
            }
        }
        utils.update_request_status_socket(trip_detail._id);

        if (req.body.status == SPLIT_PAYMENT.ACCEPTED) {
            res.json({ success: true, message: success_messages.MESSAGE_CODE_ADD_SPLIT_REQUEST_ACCEPTED_SUCCESSFULLY });
            return;
        }

        res.json({ success: true, message: success_messages.MESSAGE_CODE_ADD_SPLIT_REQUEST_REJECTED_SUCCESSFULLY });
        return;

    } catch (err) {
        console.log("exports.accept_or_reject_split_payment_request")
        utils.error_response(err, req, res)
    }
}

exports.remove_split_payment_request = async function (req, res) {
    try {
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'split_request_user_id', type: 'string' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        let trip_detail = await Trip.findOneAndUpdate({ _id: user.current_trip_id, 'split_payment_users.user_id': req.body.split_request_user_id },
            { $pull: { split_payment_users: { user_id: req.body.split_request_user_id } } });

        if (!trip_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });
            return;
        }

        let split_request_user = await User.findOne({ _id: req.body.split_request_user_id })
        if (split_request_user) {
            utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, split_request_user.device_type, split_request_user.device_token, push_messages.PUSH_CODE_FOR_REMOVE_SPLIT_PAYMENT_REQUEST, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",split_request_user.webpush_config);
        }
        utils.update_request_status_socket(trip_detail._id);
        utils.reject_split_request_socket( req.body.split_request_user_id)
        res.json({ success: true, message: success_messages.MESSAGE_CODE_ADD_SPLIT_REQUEST_CANCELLED_SUCCESSFULLY });
        return;

    } catch (err) {
        console.log("exports.remove_split_payment_request")
        utils.error_response(err, req, res)
    }
}

exports.update_split_payment_payment_mode = async function (req, res) {
    try {
        let params_array = [
            { name: 'user_id', type: 'string' },
            { name: 'payment_mode', type: 'number' }
        ]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let user = await User.findOne({ _id: req.body.user_id })
        if (!user) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            return;
        }

        if (req.body.token != null && user.token != req.body.token) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
            return;
        }

        if (req.body.payment_mode == Number(constant_json.PAYMENT_MODE_CARD)) {
            let trip = await Trip.findOne({ _id: req.body.trip_id });
            if (!trip) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });
                return;
            }
            if((trip.payment_gateway_type !== PAYMENT_GATEWAY.payu) && (trip.payment_gateway_type !== PAYMENT_GATEWAY.paypal) && (trip.payment_gateway_type !== PAYMENT_GATEWAY.razorpay)){
                let card_detail = await Card.findOne({ user_id: req.body.user_id, payment_gateway_type: trip.payment_gateway_type });
            if (!card_detail) {
                res.json({ success: false, error_code: error_message.ERROR_CODE_ADD_CREDIT_CARD_FIRST });
                return;
            }
            }
            
        }
        let trip_detail = await Trip.findOneAndUpdate({ _id: req.body.trip_id, 'split_payment_users.user_id': user._id },
            { 'split_payment_users.$.payment_mode': req.body.payment_mode });

        if (!trip_detail) {
            res.json({ success: false, error_code: error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND });
            return;
        }
        console.log('UPDATE SPLIT PAYMENT');
        res.json({ success: true, message: success_messages.MESSAGE_CODE_ADD_SPLIT_REQUEST_PAYMENT_MODE_SET_SUCCESSFULLY });
        return;

    } catch (err) {
        console.log("exports.update_split_payment_payment_mode")
        utils.error_response(err, req, res)
    }
}

exports.delete_user = async function (req, res) {

    utils.check_request_params(req.body, [{ name: 'user_id', type: 'string' }, { name: 'token', type: 'string' }], async function (response) {
        if (response.success) {
            let user = await User.findOne({ _id: req.body.user_id })
            if (user) {
                if (req.body.token != null && user.token != req.body.token) {
                    res.json({ success: false, error_code: error_message.ERROR_CODE_INVALID_TOKEN });
                } else {
                    let password = utils.encryptPassword(req.body.password ? req.body.password : '');
                    let social_index = user.social_ids.indexOf(req.body.social_id);

                    if (social_index !== -1 || user.password == password) {
                        if (user.current_trip_id != null) {
                            message = error_message.ERROR_CODE_TRIP_RUNNING
                            return res.json({ success: false, error_code: message })
                        }
                        let ride_details = await OpenRide.countDocuments({
                            $and: [
                                {
                                    user_details: {
                                        $elemMatch: { 
                                            user_id: user._id,
                                            booking_cancelled : 0,
                                            booking_cancelled_by_user : 0,
                                            booking_cancelled_by_provider : 0,
                                         }
                                    }
                                },
                                {is_trip_end: 0},
                                {is_trip_completed: 0},
                                {is_trip_cancelled: 0}
                            ]
                           
                        })
                        console.log(ride_details)
                        if (ride_details > 0) {
                            console.log('ride_details')
                            message = error_message.ERROR_CODE_PLEASE_DELETE_YOUR_FUTURE_RIDE_FIRST
                            return res.json({ success: false, error_code: message })
                        }
                        let user_detail = await User.findOne({ phone: '0000000000' });
                        if (!user_detail) {
                            user_detail = new User({
                                _id: Schema('0000000000000  00000000000'),
                                first_name: 'anonymous',
                                last_name: 'user',
                                email: 'anonymoususer@gmail.com',
                                phone: '0000000000',
                                country_phone_code: '',
                            })
                            await user_detail.save();
                        }
                        await Admin_notification.deleteOne({user_id : user._id });
                        await Trip_history.updateMany({ user_id: user._id }, { user_id: user_detail._id });
                        await Trip.deleteMany({ is_schedule_trip: true, user_id: user._id })
                        await OpenRide.updateMany({ 'user_details.user_id': user._id },{ $set: { 'user_details.$.user_id': user_detail._id } });
                        await Wallet_history.updateMany({ user_id: user._id }, { user_id: user_detail._id });
                        await Card.deleteMany({ user_id: user._id });
                        await User_Document.deleteMany({ user_id: user._id });
                        await User.deleteOne({ _id: user._id });
                        await User.updateMany({referred_by:req.body.user_id},{referred_by:user_detail._id})
                        await Redeem_point_history.updateMany(
                            { user_id: user._id },
                            { user_id: user_detail._id }
                        )
                        utils.delete_firebase_user(user.uid);

                        res.json({
                            success: true,
                            message: success_messages.MESSAGE_CODE_DELETE_SUCCESSFULLY
                        });

                    } else {
                        res.json({
                            success: false,
                            error_code: error_message.ERROR_CODE_YOUR_PASSWORD_IS_NOT_MATCH_WITH_OLD_PASSWORD
                        });
                    }

                }
            } else {
                res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND });
            }
        }
    });
}
exports.withdraw_redeem_point_to_wallet = async (req, res) => {
    try {
        utils.check_request_params(req.body,[{ name: 'user_id', type: 'string' },{ name: 'token', type: 'string' },{ name: 'redeem_point', type: 'number' },],async function (response) {
                if (response.success) {
                    var type = Number(req.body.type)
                    var Table = '';
                    switch (type) {
                        case Number(constant_json.USER_UNIQUE_NUMBER):
                            type = Number(constant_json.USER_UNIQUE_NUMBER)
                            Table = User;
                            break;
                        case Number(constant_json.PROVIDER_UNIQUE_NUMBER):
                            type = Number(constant_json.PROVIDER_UNIQUE_NUMBER)
                            Table = Provider;
                            break;
                        case Number(constant_json.PARTNER_UNIQUE_NUMBER):
                            type = Number(constant_json.PARTNER_UNIQUE_NUMBER)
                            Table = Partner;
                            break;
                    }
                    let user = await Table.findById(req.body.user_id)
                    if (!user) {
                        res.json({success: false,error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND})
                        return
                    }
                    let country = ''
                        if(type == Number(constant_json.USER_UNIQUE_NUMBER)){
                            country = await Country.findOne({countryname : user?.country})
                        }else{
                            country = await Country.findById(user?.country_id)
                        }
                        if(!country){
                            res.json({success: false, error_code: error_message.ERROR_CODE_NO_COUNTRY_FOUND});
                        }

                    if (req.body.token !== null && user.token !== req.body.token) {
                        res.json({success: false,error_code: error_message.ERROR_CODE_INVALID_TOKEN})
                        return
                    } else {
                        if ((req.body.type == Number(constant_json.PROVIDER_UNIQUE_NUMBER) && Number(req.body.redeem_point) < country?.driver_redeem_settings[0]?.driver_minimum_point_require_for_withdrawal)||(req.body.type == Number(constant_json.USER_UNIQUE_NUMBER) && Number(req.body.redeem_point) < country?.user_redeem_settings[0]?.user_minimum_point_require_for_withdrawal)|| (req.body.type == Number(constant_json.PARTNER_UNIQUE_NUMBER) && Number(req.body.redeem_point) < country?.driver_redeem_settings[0]?.driver_minimum_point_require_for_withdrawal) ||(user.total_redeem_point < Number(req.body.redeem_point))) {

                        res.json({success: false,error_code: error_message.INSUFFICIENT_REDEEM_POINT})
                        return
                        }
                        else {
                            // user.total_redeem_point -= Number(req.body.redeem_point)
                            if (
                                req.body.type == Number(constant_json.PROVIDER_UNIQUE_NUMBER)
                            ) {
                                utils.addWalletHistory(
                                    constant_json.PROVIDER_UNIQUE_NUMBER,
                                    user.unique_id,
                                    user._id,
                                    country._id,
                                    user.wallet_currency_code,
                                    user.wallet_currency_code,
                                    1,
                                    Number(req.body.redeem_point) * country?.driver_redeem_settings[0]?.driver_redeem_point_value,
                                    user.wallet,
                                    constant_json.ADD_WALLET_AMOUNT,
                                    constant_json.ADDED_BY_REDEEM_WITHDRAWAL,
                                    'Withdrawal Redeem Points'
                                    )
                                user.wallet += Number(req.body.redeem_point) * country?.driver_redeem_settings[0]?.driver_redeem_point_value;
                                    
                                let total_point = utils.add_redeem_point_history(constant_json.PROVIDER_UNIQUE_NUMBER, user.unique_id, user._id,country._id,constant_json.REVIEW_REDEEM_POINT,user.wallet_currency_code,'Redeem point Withdrawals',Number(req.body.redeem_point), user?.total_redeem_point,constant_json.DEDUCT_REDEEM_POINT)
                                user.total_redeem_point = total_point
                            } else if(req.body.type == Number(constant_json.PARTNER_UNIQUE_NUMBER)) {
                                utils.addWalletHistory(constant_json.PARTNER_UNIQUE_NUMBER,user.unique_id,user._id,country._id,user.wallet_currency_code,user.wallet_currency_code,1,Number(req.body.redeem_point) * country?.driver_redeem_settings[0]?.driver_redeem_point_value,user.wallet,constant_json.ADD_WALLET_AMOUNT,constant_json.ADDED_BY_REDEEM_WITHDRAWAL,'Withdrawal Redeem Points')
                                user.wallet += Number(req.body.redeem_point) * country?.driver_redeem_settings[0]?.driver_redeem_point_value;
                                
                                let total_point = utils.add_redeem_point_history(constant_json.PARTNER_UNIQUE_NUMBER, user.unique_id, user._id,country._id,constant_json.REVIEW_REDEEM_POINT,user.wallet_currency_code,'Redeem point Withdrawals',Number(req.body.redeem_point), user?.total_redeem_point,constant_json.DEDUCT_REDEEM_POINT)
                                user.total_redeem_point = total_point
                            } else {
                                utils.addWalletHistory(constant_json.USER_UNIQUE_NUMBER,user.unique_id,user._id,country._id,user.wallet_currency_code,user.wallet_currency_code,1,Number(req.body.redeem_point) * country?.user_redeem_settings[0]?.user_redeem_point_value,user.wallet,constant_json.ADD_WALLET_AMOUNT,constant_json.ADDED_BY_REDEEM_WITHDRAWAL,'Withdrawal Redeem Points')
                                user.wallet += Number(req.body.redeem_point) * country?.user_redeem_settings[0]?.user_redeem_point_value;

                                let total_point = utils.add_redeem_point_history(constant_json.USER_UNIQUE_NUMBER, user.unique_id, user._id,country._id,constant_json.REVIEW_REDEEM_POINT,user.wallet_currency_code,'Redeem point Withdrawals',Number(req.body.redeem_point), user?.total_redeem_point,constant_json.DEDUCT_REDEEM_POINT)
                                user.total_redeem_point = total_point
                            }

                            await user.save()
                            res.json({success: true,message:success_messages.MESSAGE_CODE_REDEEM_POINT_WITHDRAWAL_SUCCESSFULLY,total_redeem_point:user.total_redeem_point})
                        }
                    }
                } else {
                    res.json({success: false,error_code: error_message.ERROR_CODE_SOMETHING_WENT_WRONG})
                }
            }
        )
    } catch (error) {
        utils.error_response(error, req, res)
    }
};
exports.update_webpush_config = async (req, res) => {
    try {
        let params_array = [{ name: "user_id", type: "string" }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if(!response.success){
            return res.json(response)
        }
        let table;
        switch (req.body.type) {
            case 1:
            table = Provider
                break;
            default:
            table = User
                break;
        }
        let user = await table.findOne({ _id: req.body.user_id })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_USER_DETAIL_NOT_FOUND })
        }
        await table.findOneAndUpdate({_id:req.body.user_id},{webpush_config:req.body.webpush_config})
        res.json({success:true})
    } catch (error) {
        utils.error_response(error, req, res)
    }
}

exports.search_user_to_send_money = async (req, res) => {
    try {
        let params_array = [{ name: "user_id", type: "string" }, { name: "type", type: "number" }, { name: "phone", type: "string" }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if(!response.success){
            return res.json(response)
        }
        let Table;
        switch (req.body.type) {
            case 1:
                Table = User
                break;
            case 2:
                Table = Provider
                break;
            default:
                Table = User
                break;
        }
        let user = await Table.findOne({ _id: req.body.user_id })
        if (!user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_DETAIL_NOT_FOUND })
        }
        const search_user = await Table.findOne({ _id: {$ne: req.body.user_id}, phone: req.body.phone, wallet_currency_code: user.wallet_currency_code })
        if (!search_user) {
            return res.json({ success: false, error_code: error_message.ERROR_CODE_DETAIL_NOT_FOUND })
        }
        if(req.body.type==2 && search_user.provider_type_id){
            return res.json({ success: false, error_code: error_message.ERROR_CODE_IT_IS_PARTNER_PROVIDER_PLEASE_CONTACT_PARTNER_FIRST })
        }
        res.json({success:true, user_detail: { first_name: search_user.first_name, last_name: search_user.last_name, phone: search_user.phone, country_phone_code: search_user.country_phone_code, picture: search_user.picture, email: search_user.email, _id: search_user._id }})
    } catch (error) {
        utils.error_response(error, req, res)
    }
}

exports.send_money_to_friend = async (req, res) => {
    try {
        let params_array = [{ name: "user_id", type: "string" }, { name: "type", type: "number" }, { name: "friend_id", type: "string" }, { name: "amount", type: "number" }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if(!response.success){
            return res.json(response)
        }
        let Table;
        let uniuqe_value
        switch (req.body.type) {
            case 1:
                Table = User
                uniuqe_value = constant_json.USER_UNIQUE_NUMBER
                break;
            case 2:
                Table = Provider
                uniuqe_value = constant_json.PROVIDER_UNIQUE_NUMBER
                break;
            default:
                Table = User
                uniuqe_value = constant_json.USER_UNIQUE_NUMBER
                break;
        }
        let user = await Table.findOne({ _id: req.body.user_id })
        if (!user) return res.json({ success: false, error_code: error_message.ERROR_CODE_DETAIL_NOT_FOUND })
        
        let friend_user = await Table.findOne({ _id: req.body.friend_id })

        console.log(user)
        console.log(friend_user)

        if(req.body.user_id == req.body.friend_id  ){
            console.log('both ')
        }


        if(req.body.amount > user.wallet) return res.json({ success: false, error_code: error_message.INSUFFICIENT_WALLET_AMOUNT })

        let total_wallet_amount_of_user = utils.addWalletHistory(uniuqe_value, user.unique_id, user._id, user.country_id, user.wallet_currency_code, user.wallet_currency_code, 1, Math.abs(req.body.amount), user.wallet, constant_json.DEDUCT_WALLET_AMOUNT, constant_json.SEND_TO_FRIEND, `Send to ${friend_user.first_name} ${friend_user.last_name}`)
        user.wallet = total_wallet_amount_of_user

        let total_wallet_amount_of_friend = utils.addWalletHistory(uniuqe_value, friend_user.unique_id, friend_user._id, friend_user.country_id, friend_user.wallet_currency_code, friend_user.wallet_currency_code, 1, Math.abs(req.body.amount), friend_user.wallet, constant_json.ADD_WALLET_AMOUNT, constant_json.ADDED_BY_FRIEND, `Added by ${user.first_name} ${user.last_name}`)
        friend_user.wallet = total_wallet_amount_of_friend

        await friend_user.save()
        await user.save()
        res.json({success:true, message: success_messages.MONEY_SEND_SUCCESSFULLY})
    } catch (error) {
        utils.error_response(error, req, res)
    }
}

exports.get_server_time = function (req, res) {
    var server_date = new Date();
    res.json({server_date: server_date})
};

exports.get_fare_estimate_all_type = function (req, res, next){
    var currentCityLatLong = [req.body.pickup_latitude, req.body.pickup_longitude];
    var server_time = new Date();
	City.find({isBusiness: constant_json.YES}).then((cityList) => { 
        var size = cityList.length;
        var count = 0;
        if (size == 0) {
            return res.json({success: false, error_code: error_message.ERROR_CODE_OUR_BUSINESS_NOT_IN_YOUR_CITY});
        } else {
            var finalCityId = null;
            var finalCityDetail = null;
            var finalDistance = 1000000;
            var city_unit = null;
            cityList.forEach(function (city_detail) {
                count++;
                var cityLatLong = city_detail.cityLatLong;
                var distanceFromSubAdminCity = utils.getDistanceFromTwoLocation(currentCityLatLong, cityLatLong);
                var cityRadius = city_detail.cityRadius;

                if (!city_detail.is_use_city_boundary) {
                    if (distanceFromSubAdminCity < cityRadius) {
                        if (distanceFromSubAdminCity < finalDistance) {
                            finalCityDetail = city_detail;
                            finalDistance = distanceFromSubAdminCity;
                            finalCityId = city_detail._id;
                            city_unit = city_detail.unit;
                        }
                    }
                } else {
                    var city_zone = geolib.isPointInside(
                        {
                            latitude: Number(req.body.pickup_latitude),
                            longitude: Number(req.body.pickup_longitude)
                        },
                        city_detail.city_locations);
                    if (city_zone) {
                        if (distanceFromSubAdminCity < finalDistance) {
                            finalDistance = distanceFromSubAdminCity;
                            finalCityDetail = city_detail;

                            finalCityId = city_detail._id;
                            city_unit = city_detail.unit;
                        }
                    }
                }
                if (count == size) {
                    if (finalCityId != null) {
                        var city_id = finalCityId;
                        var time = req.body.time;
                        var timeMinutes;
                        timeMinutes = time * 0.0166667;

                        var distance = req.body.distance;
                        var distanceKmMile = distance;
                        if (city_unit == 1) {
                            distanceKmMile = distance * 0.001;
                        } else {
                            distanceKmMile = distance * 0.000621371;
                        }

                        var lookup = {
                            $lookup:
                                {
                                    from: "types",
                                    localField: "typeid",
                                    foreignField: "_id",
                                    as: "type_detail"
                                }
                        };
                        var unwind = {$unwind: "$type_detail"};
                        // var condition = {$match: {'cityid': {$eq: Schema(city_id)}}};
                        var condition = { $match: {
                            $and : [
                                {'cityid': {$eq: Schema(city_id)}},
                                {$or : [  { 'is_ride_share': { $eq: null }} , { 'is_ride_share': { $ne: 1 }}]}
                            ]}
                          }
						CityType.aggregate([condition, lookup, unwind]).then((city_type_list) => { 
							if(city_type_list.length == 0){
                                res.json({success: false, error_code: error_message.ERROR_CODE_OUR_BUSINESS_NOT_IN_YOUR_CITY});
							} else {
								var array = [];
                                var count = 0;
                                city_type_list.forEach(function (citytype) {
                                    count++;
                                    var base_distance = citytype.base_price_distance;
                                    var base_price = citytype.base_price;
                                    var price_per_unit_distance1 = citytype.price_per_unit_distance;
                                    var price_for_total_time1 = citytype.price_for_total_time;
                                    var tax = citytype.tax;
                                    var min_fare = citytype.min_fare;
                                    var surge_multiplier = citytype.surge_multiplier;
                                    var user_tax = citytype.user_tax;
                                    var user_miscellaneous_fee = citytype.user_miscellaneous_fee;

                                    if (distanceKmMile <= base_distance) {
                                        price_per_unit_distance = 0;
                                    } else {
                                        price_per_unit_distance = (price_per_unit_distance1 * (distanceKmMile - base_distance)).toFixed(2);
                                    }

                                    price_for_total_time = Number(price_for_total_time1 * timeMinutes);
                                    var total = 0;
                                    total = +base_price + +price_per_unit_distance + +price_for_total_time;
                                    // tax cal
                                    total = total + total * 0.01 * tax;
                                    try {
                                        if (Number(body.is_surge_hours) == 1) {
                                            total = total * surge_multiplier;
                                        }
                                    } catch (error) {

                                    }
                                    var is_min_fare_used = 0;
                                    var user_tax_fee = Number((user_tax * 0.01 * total).toFixed(2));

                                    total =total + user_tax_fee + user_miscellaneous_fee;
                                    if (total < min_fare) {
                                        total = min_fare;
                                        is_min_fare_used = 1;
                                    }
                                    var estimated_fare = Math.ceil(total);
                                    array.push({ _id: citytype._id, cancellation_fee: citytype.cancellation_fee, min_fare: min_fare, name: citytype.type_detail.typename, user_tax_fee: user_tax_fee, user_miscellaneous_fee: user_miscellaneous_fee, message: success_messages.MESSAGE_CODE_YOU_GET_FARE_ESTIMATE, time: timeMinutes, distance: (distanceKmMile).toFixed(2), is_min_fare_used: is_min_fare_used, base_price: base_price, price_per_unit_distance: price_per_unit_distance, price_per_unit_time: price_for_total_time, estimated_fare: estimated_fare });

                                	if(count == city_type_list.length){
                                        Country.findOne({countryname: city_type_list[0].countryname}, function(error, country){
                                            res.json({
                                                success: true,
                                                type_list: array,
                                                currencysign: country.currencysign,
                                                city_detail: finalCityDetail,
                                                country_detail: country,
                                                server_time: server_time
                                            });
                                        })
									}
                                })
							}
                        }, (err) => {
                                    utils.error_response(err, req, res)
                        })
                    } else {
                        res.json({success: false, error_code: error_message.ERROR_CODE_OUR_BUSINESS_NOT_IN_YOUR_CITY});
					}
                }
            });
        }
	});

}

exports.get_nearby_provider = async function (req, res) {
    const setting_detail = await Settings.findOne({});


    var default_Search_radious = setting_detail.default_Search_radious;
    var distance = default_Search_radious / constant_json.DEGREE_TO_KM;
    var accessibility = req.body.accessibility;
    var accessibility_query = {};
    if (accessibility != undefined && accessibility.length > 0) {
        accessibility_query = {
            $and: [{
                "vehicle_detail.accessibility": {
                    $exists: true,
                    $ne: [],
                    $all: accessibility
                }
            }]
        };
    }
    

    const service_type = await Citytype.findById(req.body.service_type_id);
    const type_id = service_type.typeid;
    const country_id = service_type.countryid;



    var query = Provider.find({
        'providerLocation': {
            $near: [
            req.body.latitude,
            req.body.longitude
            ],
            $maxDistance: distance

        }, "is_active": 1, "is_available": 1,
        $or: [
        {
            $and: [
            {"provider_type": Number(constant_json.PROVIDER_TYPE_NORMAL)},
            {"is_approved": 1}]
        },
        {
            $and: [
            {"provider_type": Number(constant_json.PROVIDER_TYPE_PARTNER)},
            {"is_approved": 1},
            {"is_partner_approved_by_admin": 1}
            ]
        }
        ],
        is_trip: [],
        country_id : country_id,
        admintypeid : type_id,

        // service_type: req.body.service_type_id,
        accessibility_query
    }).exec().then((providers) => { 
        if (providers.length == 0) {
            res.json({
                success: false,
                error_code: error_message.ERROR_CODE_NO_PROVIDER_FOUND_SELECTED_SERVICE_TYPE_AROUND_YOU
            });
        } else {
            res.json({
                success: true,
                message: success_messages.MESSAGE_CODE_YOU_GET_NEARBY_DRIVER_LIST, providers: providers
            });
        }
    });
    
}

exports.generate_user_history_export_excel = function (req, res) {
    if(req.body.user_id){
        req.session.user = {_id:req.body.user_id}
    }

    if (typeof req.session.user != 'undefined') {


        if (req.body.search_item == undefined) {
            var request = req.path.split('/')[1];
            search_item = 'unique_id';
            search_value = '';
            sort_order = -1;
            sort_field = 'unique_id';
            filter_start_date = '';
            filter_end_date = '';

        } else {
            var request = req.body.request;
            var value = req.body.search_value;
            value = value.trim();
            value = value.replace(/ +(?= )/g, '');
            value = new RegExp(value, 'i');


            sort_order = req.body.sort_item[1];
            sort_field = req.body.sort_item[0];
            search_item = req.body.search_item
            search_value = req.body.search_value;
            filter_start_date = req.body.start_date;
            filter_end_date = req.body.end_date;

        }

        if (req.body.start_date == '' || req.body.end_date == '') {
            if (req.body.start_date == '' && req.body.end_date == '') {
                var date = new Date(Date.now());
                date = date.setHours(0, 0, 0, 0);
                start_date = new Date(0);
                end_date = new Date(Date.now());
            } else if (req.body.start_date == '') {
                start_date = new Date(0);
                var end_date = req.body.end_date;
                end_date = new Date(end_date);
                end_date = end_date.setHours(23, 59, 59, 999);
                end_date = new Date(end_date);
            } else {
                var start_date = req.body.start_date;
                start_date = new Date(start_date);
                start_date = start_date.setHours(0, 0, 0, 0);
                start_date = new Date(start_date);
                end_date = new Date(Date.now());
            }
        } else if (req.body.start_date == undefined || req.body.end_date == undefined) {
            start_date = new Date(0);
            end_date = new Date(Date.now());
        } else {
            var start_date = req.body.start_date;
            var end_date = req.body.end_date;
            start_date = new Date(start_date);
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
            end_date = new Date(end_date);
            end_date = end_date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);
        }


        var lookup = {
            $lookup:
            {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_detail"
            }
        };
        var unwind = { $unwind: "$user_detail" };

        var lookup1 = {
            $lookup:
            {
                from: "providers",
                localField: "confirmed_provider",
                foreignField: "_id",
                as: "provider_detail"
            }
        };


        value = search_value;
        value = value.trim();
        value = value.replace(/ +(?= )/g, '');

        if (search_item == "unique_id") {

            var query1 = {};
            if (value != "") {
                value = Number(value)
                query1[search_item] = { $eq: value };
                var search = { "$match": query1 };
            } else {
                var search = { $match: {} };
            }

        } else if (search_item == "provider_detail.first_name") {
            var query1 = {};
            var query2 = {};
            var query3 = {};
            var query4 = {};
            var query5 = {};
            var query6 = {};

            var full_name = value.split(' ');
            if (typeof full_name[0] == 'undefined' || typeof full_name[1] == 'undefined') {

                query1[search_item] = { $regex: new RegExp(value, 'i') };
                query2['provider_detail.last_name'] = { $regex: new RegExp(value, 'i') };

                var search = { "$match": { $or: [query1, query2] } };
            } else {

                query1[search_item] = { $regex: new RegExp(value, 'i') };
                query2['provider_detail.last_name'] = { $regex: new RegExp(value, 'i') };
                query3[search_item] = { $regex: new RegExp(full_name[0], 'i') };
                query4['provider_detail.last_name'] = { $regex: new RegExp(full_name[0], 'i') };
                query5[search_item] = { $regex: new RegExp(full_name[1], 'i') };
                query6['provider_detail.last_name'] = { $regex: new RegExp(full_name[1], 'i') };

                var search = { "$match": { $or: [query1, query2, query3, query4, query5, query6] } };
            }
        } else {
            var search = { "$match": { search_item: { $regex: new RegExp(value, 'i') } } };
        }


        query1['created_at'] = { $gte: start_date, $lt: end_date };
        var filter = { "$match": query1 };

        var sort = { "$sort": {} };
        sort["$sort"][sort_field] = parseInt(sort_order);


        var mongoose = require('mongoose');
        var Schema = mongoose.Types.ObjectId;
        var condition = { $match: { 'user_id': { $eq: Schema(req.session.user._id) } } };
        Trip_history.aggregate([condition, lookup, unwind, lookup1, search, filter, sort]).then((array) => {

            var date = new Date()
            var time = date.getTime()
            var wb = new xl.Workbook();
            var ws = wb.addWorksheet('sheet1');
            var col = 1;

            let title
            if(req.body.header){
                title = req.body.header
            }else{
                title = {
                    id : 'Id',
                    user_id : 'UserId',
                    user : 'User',
                    driver_id : 'DriverId',
                    driver : 'Driver',
                    date : 'Date',
                    status : 'Status',
                    amout : 'Amount',
                    payment : 'Payment',
                    payment_status : 'Payment Status',
                    title_status_cancel_by_provider : 'Cancelled By Provider',
                    title_status_cancel_by_user : 'Cancelled By User',
                    title_trip_status_coming : 'Coming',
                    title_trip_status_arrived : 'Arrived',
                    title_trip_status_trip_started : 'Started',
                    title_trip_status_completed : 'Compeleted',
                    title_trip_status_accepted : 'Accepted',
                    title_trip_status_waiting : 'Waiting',
                    title_pay_by_cash : 'Cash',
                    title_pay_by_card : 'Card',
                    title_pending : 'Pending',
                    title_paid : 'Paid',
                    title_not_paid : 'Not Paid'
                }
            }

            ws.cell(1, col++).string(title.id);
            ws.cell(1, col++).string(title.user_id);
            ws.cell(1, col++).string(title.user);
            ws.cell(1, col++).string(title.driver_id);
            ws.cell(1, col++).string(title.driver);
            ws.cell(1, col++).string(title.date);
            ws.cell(1, col++).string(title.status);
            ws.cell(1, col++).string(title.amount);
            ws.cell(1, col++).string(title.payment);
            ws.cell(1, col++).string(title.payment_status);


            array.forEach(function (data, index) {
                col = 1;
                ws.cell(index + 2, col++).number(data.unique_id);
                ws.cell(index + 2, col++).number(data.user_detail.unique_id);
                ws.cell(index + 2, col++).string(data.user_detail.first_name + ' ' + data.user_detail.last_name);

                if (data.provider_detail.length > 0) {
                    ws.cell(index + 2, col++).number(data.provider_detail[0].unique_id);
                    ws.cell(index + 2, col++).string(data.provider_detail[0].first_name + ' ' + data.provider_detail[0].last_name);
                } else {
                    col += 2;
                }
                ws.cell(index + 2, col++).string(moment(data.created_at).format("DD MMM 'YY") + ' ' + moment(data.created_at).format("hh:mm a"));

                if (data.is_trip_cancelled == 1) {
                    if (data.is_trip_cancelled_by_provider == 1) {
                        // ws.cell(index + 2, col++).string(req.__('title_total_cancelled_by_provider'));
                        ws.cell(index + 2, col++).string(title.title_status_cancel_by_provider);
                    } else if (data.is_trip_cancelled_by_user == 1) {
                        // ws.cell(index + 2, col++).string(req.__('title_total_cancelled_by_user'));
                        ws.cell(index + 2, col++).string(title.title_status_cancel_by_user);
                    } else {
                        // ws.cell(index + 2, col++).string(req.__('title_total_cancelled'));
                        ws.cell(index + 2, col++).string(title.title_trip_status_cancelled);
                    }
                } else {
                    if (data.is_provider_status == PROVIDER_STATUS.COMING) {
                        // ws.cell(index + 2, col++).string(req.__('title_trip_status_coming'));
                        ws.cell(index + 2, col++).string(title.title_trip_status_coming );
                    } else if (data.is_provider_status == PROVIDER_STATUS.ARRIVED) {
                        // ws.cell(index + 2, col++).string(req.__('title_trip_status_arrived'));
                        ws.cell(index + 2, col++).string(title.title_trip_status_arrived );
                    } else if (data.is_provider_status == PROVIDER_STATUS.TRIP_STARTED) {
                        // ws.cell(index + 2, col++).string(req.__('title_trip_status_trip_started'));
                        ws.cell(index + 2, col++).string(title.title_trip_status_trip_started);
                    } else if (data.is_provider_status == PROVIDER_STATUS.TRIP_COMPLETED) {
                        // ws.cell(index + 2, col++).string(req.__('title_trip_status_completed'));
                        ws.cell(index + 2, col++).string(title.title_trip_status_completed );
                    } else if (data.is_provider_status == PROVIDER_STATUS.ACCEPTED || data.is_provider_status == PROVIDER_STATUS.WAITING) {
                        if (data.is_provider_accepted == 1) {
                            // ws.cell(index + 2, col++).string(req.__('title_trip_status_accepted'));
                            ws.cell(index + 2, col++).string(title.title_trip_status_accepted );
                        } else {
                            // ws.cell(index + 2, col++).string(req.__('title_trip_status_waiting'));
                            ws.cell(index + 2, col++).string(title.title_trip_status_waiting);
    
                        }
                    }
                }

                ws.cell(index + 2, col++).number(data.total);

                if (data.payment_mode == 1) {
                    ws.cell(index + 2, col++).string(title.title_pay_by_cash);
                } else {
                    ws.cell(index + 2, col++).string(title.title_pay_by_card);
                }
    
                if (data.payment_status == 0) {
                    ws.cell(index + 2, col++).string(title.title_pending);
                } else {
                    if (data.payment_status == 1) {
                        ws.cell(index + 2, col++).string(title.title_paid);
                    } else {
                        // ws.cell(index + 2, col++).string(req.__('title_not_paid'));
                        ws.cell(index + 2, col++).string(title.title_not_paid);
                    }
                }


                if (index == array.length - 1) {
                    wb.write('data/xlsheet/' + time + '_user_history.xlsx', function (err) {
                        if (err) {
                            console.error(err);
                        } else {
                            var url = req.protocol + "://" + req.get('host') + "/xlsheet/" + time + "_user_history.xlsx";
                            res.json(url);
                            setTimeout(function () {
                                fs.unlink('data/xlsheet/' + time + '_user_history.xlsx', function () {
                                });
                            }, 10000)
                        }
                    });
                }
            });
        }, (err) => {
            utils.error_response(err, req, res)
        });

    } else {
        res.redirect('/login');
    }
};

exports.userReviews = async function (req, res) {
    var condition = {$match : { $and : [ {'userReview' : {$ne :''}}, {'userReview' : {$exists: true}}]}}
    var lookup = {
        $lookup:
        {
            from: "users",
            localField: "user_id",
            foreignField: "_id",
            as: "user_detail"
        }
    };
    var unwind = { $unwind: "$user_detail" };
    var limit = {$limit : 5};
    // var project = {  $project: {

    //         // created_at: "$created_at",
    //         // order_status: "$order_status",
    //         // order_status_id: "$order_status_id",
    //         // completed_at: "$completed_at",
    //         // unique_id: "$unique_id",
    //         // request_id: "$request_id",
    //         // total: "$total",
    //         // delivery_type: '$delivery_type',
    //         // image_url: '$image_url',
    //         user_detail: { name: { $ifNull: [{ $arrayElemAt: ["$user_detail.name"] }, { $ifNull: [{ $arrayElemAt: ["$user_detail.name", 0] }, ""] }] }, phone: { $cond: ["$user_detail", "$user_detail.phone", ''] }, image_url: { $cond: ["$user_detail", "$user_detail.image_url", ''] } },

    //     }
    // }
    let userReviews = await Reviews.aggregate([condition,lookup,unwind,limit])
    return res.json({ success: true, userReviews })
}

exports.fetch_mass_notification_for_user = async function (req, res) {
    try {
        console.log('runiii');
        const setting_detail = await Settings.findOne({});
        let response = await utils.check_request_params_async(req.body, [{name: 'user_id', type: 'string'}, {name: 'device_type', type: 'string'}, {name: 'user_type', type: 'number'}])
        if (!response.success) {
            res.json(response)
            return;
        }
        const type = Number(req.body.user_type)
        let Table
        const device_type = req.body.device_type
        switch (type) {
            case TYPE_VALUE.USER:
                Table = User;
                console.log('User');
                break;
            case TYPE_VALUE.PROVIDER:
                Table = Provider;
                break;
            default:
                Table = User;
                break;
        }
        console.log(req.body);
        const user = await Table.findOne({ _id: req.body.user_id})
        console.log(user);
        if(!user) return res.json({ success: false, error_code: error_message.ERROR_CODE_DETAIL_NOT_FOUND})
        const notifications = await MassNotification.aggregate([
            {
                $lookup: {
                    from: 'countries',
                    localField: 'country',
                    foreignField: '_id',
                    as: 'country_detail'
                }
            },
            {
                $unwind: {
                    path: '$country_detail',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: {
                   $and: [ {user_type: type},
                    {created_at: { $gte: user.created_at}},
                    {$or: [{ device_type: device_type}, {device_type: 'both'}, {device_type: 'all'}]},
                    {$or: [{ $and: [{ user_type: 1}, { $or:[{ 'country_detail.countryname': user.country }, { 'country_detail.countryname': null }] }] },{ $and: [{ user_type: 2 }, { $or: [{ country: user.country_id }, { country: null }] }] }]}
                ]
                }
            },
            {
                $project: {
                    country_detail :0
                }
            },
            {
                $sort: { unique_id: -1}
            }
        ])

        var timezone = setting_detail.timezone_for_display_date;
        
        return res.json({ success: true, notifications, timezone})
    } catch (error) {
        utils.error_response(error, req, res)
    }
}