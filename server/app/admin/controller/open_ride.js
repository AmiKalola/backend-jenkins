var OpenRide = require('mongoose').model('Open_Ride');
var TripLocation = require('mongoose').model('trip_location');
let utils = require('../../controllers/utils')
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var User = require('mongoose').model('User');
var Provider = require('mongoose').model('Provider');
require('../../controllers/constant');
var TripLocation = require('mongoose').model('trip_location');
var moment = require('moment');
var Settings = require('mongoose').model('Settings')


exports.openride_get_trip_detail = async function (req, res) {
    try {
        let params_array = [{ name: "trip_id", type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response)
            return;
        }
        let user_lookup = {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                pipeline:[{$project:{first_name:1,last_name:1,rate:1,email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1,phone:!req.headers.is_show_phone ?  HIDE_DETAILS.PHONE : 1, country_phone_code:!req.headers.is_show_phone ?  HIDE_DETAILS.COUNTRY_CODE : 1}}],
                foreignField: '_id',
                as: 'user_details'
            }
        }
        let user_unwind = {
            $unwind: {
                path: "$user_details",
                preserveNullAndEmptyArrays: true
            }
        };

        let provider_lookup = {
            $lookup: {
                from: 'providers',
                localField: 'provider_id',
                pipeline:[{$project:{first_name:1,last_name:1,rate:1,email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1,phone:!req.headers.is_show_phone ?  HIDE_DETAILS.PHONE : 1, country_phone_code:!req.headers.is_show_phone ?  HIDE_DETAILS.COUNTRY_CODE : 1}}],
                foreignField: '_id',
                as: 'provider_details'
            }
        }
        let provider_unwind = {
            $unwind: {
                path: "$provider_details",
                preserveNullAndEmptyArrays: true
            }
        };
        let vehicle_type_lookup = {
            $lookup: {
                from: 'types',
                localField: 'type_id',
                foreignField: '_id',
                as: 'vehicle_type_details'
            }
        }
        let vehicle_unwind = {
            $unwind: {
                path: "$vehicle_type_details",
                preserveNullAndEmptyArrays: true
            }
        };
        let service_lookup = {
            $lookup: {
                from: 'trip_services',
                localField: 'trip_service_city_type_id',
                foreignField: '_id',
                as: 'service'
            }
        }
        let service_unwind = {
            $unwind: {
                path: "$service",
                preserveNullAndEmptyArrays: true
            }
        };
        let promo_lookup = {
            $lookup: {
                from: 'promo_codes',
                localField: 'promo_id',
                foreignField: '_id',
                as: 'promo_detail'
            }
        }
        let promo_unwind = {
            $unwind: {
                path: "$promo_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        // let Table;
        // switch (type) {
        //     case '1':
        //         Table = Trip
        //         break;
        //     case '2':
        //         Table = Trip_history
        //         break;
        // }

        let lookup = {
                // $lookup:
                // {
                //     from: "providers",
                //     localField: "providers_id_that_rejected_trip",
                //     foreignField: "_id",
                //     pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, email: 1, phone: 1, rate: 1, picture: 1 } }],
                //     as: "Rejected_provider_detail"
                // }
                
                    $lookup: {
                      from: "providers",
                      let: {
                        rejectedTripProviderId: "$providers_id_that_rejected_trip",
                        scheduleRejectedTripProviderId: "$providers_id_that_rejected_trip_for_schedule"
                      },
                      pipeline: [
                        {
                            $match: {
                                $expr: {
                                  $or: [
                                    { $in: ["$_id", "$$rejectedTripProviderId"] },
                                    { $in: ["$_id", "$$scheduleRejectedTripProviderId"] }
                                  ]
                                }
                            }
                        },
                        {
                          $project: {
                            _id: 1,
                            first_name: 1,
                            last_name: 1,
                            email: 1,
                            phone: 1,
                            rate: 1,
                            picture: 1
                          }
                        }
                      ],
                      as: "Rejected_provider_detail"
                    }
                  
            };

        var trip_location_data=await TripLocation.findOne({tripID:req.body.trip_id});
        var trip_condition = { "$match": { '_id': { $eq: mongoose.Types.ObjectId(req.body.trip_id) } } };
        // let trip_detail = await OpenRide.aggregate([trip_condition, user_lookup, user_unwind, provider_lookup, provider_unwind, vehicle_type_lookup, vehicle_unwind, service_lookup, service_unwind,lookup, promo_lookup, promo_unwind])
        // if (trip_detail.length == 0) {
            trip_detail = await OpenRide.aggregate([trip_condition,   vehicle_type_lookup, vehicle_unwind, service_lookup, service_unwind ])
            if (trip_detail.length == 0) {
                return res.json({ success: false }, error_message.ERROR_CODE_YOUR_TRIP_DETAIL_NOT_FOUND)
            }
            res.json({ success: true, trip_detail: trip_detail,trip_location_data:trip_location_data })
        // } 
        // else {
        //     res.json({ success: true, trip_detail: trip_detail,trip_location_data:trip_location_data })
        // }
    } catch (error) {
        utils.error_response(error, req, res)
    }
}


exports.scheduled_open_ride_cancel_by_admin = async function (req, res) {
    try {
        let params_array = [{ name: 'trip_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let trip_detail = await OpenRide.findOne({
            _id: req.body.trip_id,
            is_trip_cancelled: 0,
            is_trip_cancelled_by_provider: 0,
        })
        if (!trip_detail) {
            res.json({ success: false });
            return;
        }
       
        let complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip_detail.timezone);
        let complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);

        // Set trip status
        trip_detail.trip_status = await utils.addTripStatusTimeline(trip_detail, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, TYPE_VALUE.ADMIN, req.headers.username, req.headers.admin_id )


        if (req.body.user_id) {
            let userdetails_index = trip_detail.user_details.findIndex(item => item.user_id.toString() == req.body.user_id.toString())

            if (userdetails_index != -1) {
                trip_detail.user_details[userdetails_index].booking_cancelled = 1 
                trip_detail.user_details[userdetails_index].payment_status = 1
                trip_detail.cancel_reason = req.body?.cancel_reason;
            }
            let user = await User.findOne({ _id: req.body.user_id })
            utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);
            if (String(trip_detail._id) == String(user.current_trip_id)) {
                user.current_trip_id = null;
                await User.updateOne({ _id: user._id }, user.getChanges())
            }
            
        } else {
            const updatedUserDetails = trip_detail.user_details.filter(userDetail =>
                userDetail.booking_cancelled !== 1 && userDetail.booking_cancelled_by_user !== 1 && userDetail.booking_cancelled_by_provider !== 1 
            );      
    
            for (let i = 0; i < updatedUserDetails.length; i++) {
                let user = await User.findOne({ _id: updatedUserDetails[i].user_id })
                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);
                if (String(trip_detail._id) == String(user.current_trip_id)) {
                    user.current_trip_id = null;
                    await User.updateOne({ _id: user._id }, user.getChanges())
                }
                let userdetails_index = trip_detail.user_details.findIndex(item => item.user_id.toString() == updatedUserDetails[i].user_id.toString())

                if (userdetails_index != -1) {
                    trip_detail.user_details[userdetails_index].booking_cancelled = 1 
                    trip_detail.user_details[userdetails_index].payment_status = 1
                    trip_detail.cancel_reason = req.body?.cancel_reason;
                }
            }

            let provider = await Provider.findOne({ _id: trip_detail.confirmed_provider })
            if (provider) {
                provider = utils.remove_is_trip_from_provider(provider, trip_detail._id, trip_detail.initialDestinationLocation)
                if (!provider.is_near_trip) { provider.is_near_trip = [] }
    
                if ((String(provider.is_trip[0]) == String(trip_detail._id))) {
                    provider.is_near_available = 1;
                    provider.is_near_trip = [];
                    provider.is_trip = [];
                    provider.is_available = 1;
                }
                await Provider.updateOne({ _id: provider._id }, provider.getChanges())
                utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
            }

            trip_detail.is_trip_end = 1;
            trip_detail.is_trip_cancelled = 1;
            trip_detail.is_trip_cancelled_by_admin = 1
            trip_detail.cancel_reason = '';
            trip_detail.payment_status = 1
            trip_detail.provider_trip_end_time = new Date();
            trip_detail.complete_date_in_city_timezone = complete_date_in_city_timezone;
            trip_detail.complete_date_tag = complete_date_tag;
        }





        await OpenRide.updateOne({ _id: trip_detail._id }, trip_detail.getChanges())

        message = admin_messages.success_message_trip_cancelled;
        utils.update_request_status_socket(trip_detail._id);


        res.json({ success: true,message:message });
        return;
    } catch (err) {
        console.log("exports.scheduled_trip_cancel_by_admin")
        utils.error_response(err, req, res)
    }
};

exports.open_ride_cancel_by_admin = async function (req, res) {
    try {
        let params_array = [{ name: 'trip_id', type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_array)
        if (!response.success) {
            res.json(response);
            return;
        }

        let trip = await OpenRide.findOne({
            _id: req.body.trip_id,
            is_trip_cancelled: 0,
            is_trip_cancelled_by_provider: 0,
        })
        if (!trip) {
            res.json({ success: false });
            return;
        }
        
        let complete_date_in_city_timezone = utils.get_date_now_at_city(new Date(), trip.timezone);
        let complete_date_tag = moment(moment(complete_date_in_city_timezone).startOf('day')).format(constant_json.DATE_FORMAT_MMM_D_YYYY);
        

        // Set trip status
        trip.trip_status = await utils.addTripStatusTimeline(trip, TRIP_STATUS_TIMELIME.TRIP_CANCELLED, TYPE_VALUE.ADMIN, req.headers.username, req.headers.admin_id )

        if (req.body.user_id) {
            let userdetails_index = trip.user_details.findIndex(item => item.user_id.toString() == req.body.user_id.toString())

            if (userdetails_index != -1) {
                trip.user_details[userdetails_index].booking_cancelled = 1 
                trip.user_details[userdetails_index].payment_status = 1
                trip.cancel_reason = req.body?.cancel_reason;
            }
            let user = await User.findOne({ _id: req.body.user_id })
            utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);
            if (String(trip._id) == String(user.current_trip_id)) {
                user.current_trip_id = null;
                await User.updateOne({ _id: user._id }, user.getChanges())
            }
            
        } else {
            const updatedUserDetails = trip.user_details.filter(userDetail =>
                userDetail.booking_cancelled !== 1 && userDetail.booking_cancelled_by_user !== 1 && userDetail.booking_cancelled_by_provider !== 1 
            );      
    
            for (let i = 0; i < updatedUserDetails.length; i++) {
                let user = await User.findOne({ _id: updatedUserDetails[i].user_id })
                utils.sendPushNotification(constant_json.USER_UNIQUE_NUMBER, user.device_type, user.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS,"",user.webpush_config,req.headers.langCode);
                if (String(trip._id) == String(user.current_trip_id)) {
                    user.current_trip_id = null;
                    await User.updateOne({ _id: user._id }, user.getChanges())
                }
                let userdetails_index = trip.user_details.findIndex(item => item.user_id.toString() == updatedUserDetails[i].user_id.toString())

                if (userdetails_index != -1) {
                    trip.user_details[userdetails_index].booking_cancelled = 1 
                    trip.user_details[userdetails_index].payment_status = 1
                    trip.cancel_reason = req.body?.cancel_reason;
                }
            }


            let provider = await Provider.findOne({ _id: trip.confirmed_provider })
            if (provider) {
                provider = utils.remove_is_trip_from_provider(provider, trip._id, trip.initialDestinationLocation)
                if (!provider.is_near_trip) { provider.is_near_trip = [] }
    
                if ((String(provider.is_trip[0]) == String(trip._id))) {
                    provider.is_near_available = 1;
                    provider.is_near_trip = [];
                    provider.is_trip = [];
                    provider.is_available = 1;
                }
                await Provider.updateOne({ _id: provider._id }, provider.getChanges())
                utils.sendPushNotification(constant_json.PROVIDER_UNIQUE_NUMBER, provider.device_type, provider.device_token, push_messages.PUSH_CODE_FOR_TRIP_CANCELLED_BY_ADMIN, constant_json.PUSH_NOTIFICATION_SOUND_FILE_IN_IOS);
            }


            trip.is_trip_end = 1;
            trip.is_trip_cancelled = 1;
            trip.is_trip_cancelled_by_admin = 1
            trip.cancel_reason = '';
            trip.payment_status = 1
            trip.provider_trip_end_time = new Date();
            trip.complete_date_in_city_timezone = complete_date_in_city_timezone;
            trip.complete_date_tag = complete_date_tag;
        }
        
       

        await OpenRide.updateOne({ _id: trip._id }, trip.getChanges())

        message = admin_messages.success_message_trip_cancelled;
        utils.update_request_status_socket(trip._id);

        res.json({ success: true, message: message });
        return;
    } catch (err) {
        console.log("exports.trip_cancel_by_admin")
        utils.error_response(err, req, res)
    }
};

//api for get statement  for trip earning
exports.open_ride_statement_provider_trip_earning = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({})
        let params_trips = [{ name: "trip_id", type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_trips)
        if (!response.success) {
            res.json(response)
            return;
        }
        let timezone_for_display_date = setting_detail.timezone_for_display_date;
        let trips = [];
        let page = req.path.split('/');
        let query = { $match: {} };
        let Schema = mongoose.Types.ObjectId
        query["$match"]['_id'] = { $eq: Schema(req.body.trip_id) }


        let user_lookup = {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1 } }],
                as: 'user_detail'
            }
        }
        let user_unwind = {
            $unwind: {
                path: "$user_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let Trip_service_lookup = {
            $lookup: {
                from: 'trip_services',
                localField: 'trip_service_city_type_id',
                foreignField: '_id',
                as: 'trip_service_detail'
            }
        }
        let Trip_service_unwind = {
            $unwind: {
                path: "$trip_service_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let city_type_lookup = {
            $lookup: {
                from: 'city_types',
                localField: 'service_type_id',
                foreignField: '_id',
                as: 'city_type_detail'
            }
        }
        let city_type_unwind = {
            $unwind: {
                path: "$city_type_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let Type_lookup = {
            $lookup: {
                from: 'types',
                localField: 'typeid',
                foreignField: '_id',
                as: 'type_detail'
            }
        }
        let Type_unwind = {
            $unwind: {
                path: "$type_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let provider_lookup = {
            $lookup: {
                from: 'providers',
                localField: 'current_provider',
                foreignField: '_id',
                pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1 } }],
                as: 'providers_detail'
            }
        }
        let provider_unwind = {
            $unwind: {
                path: "$providers_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let trip = await OpenRide.aggregate([query, user_lookup, user_unwind, Trip_service_lookup, Trip_service_unwind, city_type_lookup, city_type_unwind, Type_lookup, Type_unwind, provider_lookup, provider_unwind]);
        let rental_package;
        if (trip.car_rental_id) {
            rental_package = await City_type.findById(trip.car_rental_id);
        }
        res.json({ success: true, rental_package, detail: trip, type: req.body.type, timezone_for_display_date: timezone_for_display_date, provider_detail: "$providers_detail", user_detail: "$user_detail", type_detail: "$type_detail", service_detail: "$city_type_detail", moment: moment, tripservice: "$trip_service_detail" });
    } catch (err) {
        utils.error_response(err, req, res)
    }
}