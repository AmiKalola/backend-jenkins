const request = require("request");
let Country = require('mongoose').model('Country')
let Trip_history = require('mongoose').model('Trip_history')
let Trip = require('mongoose').model('Trip')
let Transfer_history = require('mongoose').model('transfer_history')
let Provider = require('mongoose').model('Provider')
let Provider_daily_analytic = require('mongoose').model('provider_daily_analytic')
let Wallet_history = require('mongoose').model('Wallet_history')
let City_type = require('mongoose').model('city_type')
let City = require('mongoose').model('City')
let Corporate = require('mongoose').model('Corporate')
let moment = require('moment');
let utils = require('../../controllers/utils')
require('../../controllers/constant')
let xl = require('excel4node');
let fs = require("fs");
let mongoose = require('mongoose');
let Schema = mongoose.Types.ObjectId;
var Settings = require('mongoose').model('Settings')

//api for get trip list for trip earning
exports.trip_earning = async function (req, res) {
    const setting_detail = await Settings.findOne({});
    let page;
    let next;
    let pre;
    let search_item;
    let search_value;
    let sort_field;
    let filter_start_date;
    let filter_end_date;
    let start_date;
    let end_date;
    let selected_country = req.body.selected_country;
    let selected_city = req.body.selected_city;
    if (req.body.page == undefined) {
        page = 0;
        next = 1;
        pre = 0;
    } else {
        page = req.body.page;
        next = parseInt(req.body.page) + 1;
        pre = req.body.page - 1;
    }

    if (req.body.search_item == undefined) {
        search_item = 'provider_detail.first_name';
        search_value = '';
        filter_start_date = '';
        filter_end_date = '';


    } else {
        search_item = req.body.search_item;
        search_value = req.body.search_value;

        filter_start_date = req.body.start_date;
        filter_end_date = req.body.end_date;
    }

    if (req.body.start_date == '' || req.body.start_date == undefined) {
        let date = new Date();
        start_date = date.setHours(0, 0, 0, 0);
        start_date = new Date(null);
        end_date = date.setHours(23, 59, 59, 999);
        end_date = new Date(end_date);
    } else {
        start_date = req.body.start_date;
        start_date = new Date(start_date);
        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);

        end_date = req.body.end_date;
        end_date = new Date(end_date);
        end_date = end_date.setHours(23, 59, 59, 999);
        end_date = new Date(end_date);
    }

    let number_of_rec = req.body.limit;
    let lookup = {
        $lookup:
        {
            from: "providers",
            localField: "current_provider",
            pipeline:[{$project:{unique_id:1,first_name:1,last_name:1,email:!req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1,phone:!req.headers.is_show_phone ?  HIDE_DETAILS.PHONE : 1,country_phone_code:!req.headers.is_show_phone ?  HIDE_DETAILS.COUNTRY_CODE : 1}}],
            foreignField: "_id",
            as: "provider_detail"
        }
    };


    let country_filter = { $match: {} };
    let city_filter = { $match: {} };
    let timezone = "";

    if (selected_country !== 'all') {
        let country = await Country.findOne({ _id: Schema(selected_country) })
        if (country) {
            timezone = country.country_all_timezone[0];
        }
        country_filter['$match']['country_id'] = { $eq: Schema(selected_country) };
        if (selected_city !== 'all') {
            let city = await City.findOne({ _id: selected_city })
            if (city) {
                timezone = city.timezone;
            }
            city_filter['$match']['city_id'] = { $eq: Schema(selected_city) };
        }
    }

    let search = { "$match": {} };
    let value = search_value;
    value = value.trim();
    value = value.replace(/ +(?= )/g, '');
    if (search_item == "provider_detail.first_name") {
        let query1 = {};
        let query2 = {};
        let query3 = {};
        let query4 = {};
        let query5 = {};
        let query6 = {};

        let full_name = value.split(' ');
        if (typeof full_name[0] == 'undefined' || typeof full_name[1] == 'undefined') {

            query1[search_item] = { $regex: new RegExp(value, 'i') };
            query2['provider_detail.last_name'] = { $regex: new RegExp(value, 'i') };

            search = { "$match": { $or: [query1, query2] } };
        } else {

            query4['provider_detail.first_name'] = { $regex: new RegExp(full_name[0], 'i') };
            query5['provider_detail.last_name'] = { $regex: new RegExp(full_name[1], 'i') };
            search = { "$match": { $and: [query4, query5] } };
        }
    } else {

        search["$match"][search_item] = { $regex: value };

        if(search_item == "unique_id"){
         search["$match"][search_item] =  Number(value)
        }
    }
    let trip_filter = { "$match": {} };
    ///// For Count number of result /////
    let count = { $group: { _id: null, total: { $sum: 1 }, data: { $push: '$data' } } };

    //// For skip number of result /////
    let skip = {};
    skip["$skip"] = page * number_of_rec;

    ///// For limitation on result /////
    let limit = {};
    limit["$limit"] = number_of_rec;

    let sort = {}
    let sort_item = req.body.sort_item 
    let sort_order = Number(req.body.sort_order)
    if(sort_item && sort_order){
        sort = {$sort:{
            [sort_item] : sort_order
        }}
    } else {
        sort = { $sort: { provider_trip_end_time: -1 } }
    }

    let country_list = await Country.find({})
    let timezone_for_display_date = setting_detail.timezone_for_display_date;

    if (selected_city == 'all') {
        selected_city = null
    }

    if (timezone != "") {
        let today_start_date_time = utils.get_date_in_city_timezone(start_date, timezone);
        let today_end_date_time = utils.get_date_in_city_timezone(end_date, timezone);
        trip_filter["$match"]['complete_date_in_city_timezone'] = { $gte: today_start_date_time, $lt: today_end_date_time };
    } else {
        trip_filter["$match"]['complete_date_in_city_timezone'] = { $gte: start_date, $lt: end_date };
    }

    let trip_condition = { 'is_trip_completed': 1 };
    let trip_condition_new = { $and: [{ 'is_trip_cancelled_by_user': 1 }, { 'pay_to_provider': { $gt: 0 } }] };
    trip_condition = { $match: { $or: [trip_condition, trip_condition_new] } };

    if (req.body.is_export) {
        let trips = await Trip_history.aggregate([trip_condition,trip_filter, country_filter, city_filter, lookup, search ,sort])
        generate_trip_earning_excel(req, res, trips , req.body.header)
        return
    }

    let trips = await Trip_history.aggregate([trip_condition,trip_filter, country_filter, city_filter, lookup, search, sort,count])
    if (trips.length == 0) {
        trips = [];
        res.json({ success: true, detail: trips, 'current_page': 1, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

    } else {
        let pages = Math.ceil(trips[0].total / number_of_rec);
        trips = await Trip_history.aggregate([trip_condition,trip_filter, country_filter, city_filter, lookup, search, sort, skip, limit])

        if (trips.length == 0) {
            trips = [];
            res.json({ success: true, detail: trips, 'current_page': 1, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

        } else {

            let trip_group_condition_total = {
                $group: {
                    _id: null,
                    total_trip: { $sum: 1 },
                    completed_trip: { $sum: { $cond: [{ $eq: ["$is_trip_completed", 1] }, 1, 0] } },
                    total: { $sum: '$total' },
                    promo_payment: { $sum: '$promo_payment' },
                    card_payment: { $sum: '$card_payment' },
                    cash_payment: { $sum: '$cash_payment' },
                    wallet_payment: { $sum: '$wallet_payment' },
                    admin_earning: { $sum: { $subtract: ['$total', '$provider_service_fees'] } },
                    admin_earning_in_currency: { $sum: { $subtract: ['$total_in_admin_currency', '$provider_service_fees_in_admin_currency'] } },
                    provider_earning: { $sum: '$provider_service_fees_in_admin_currency' },
                    provider_have_cash: { $sum: '$provider_have_cash' },
                    pay_to_provider: { $sum: '$pay_to_provider' }
                }
            }

            let trip_total = await Trip_history.aggregate([trip_condition,trip_filter, country_filter, city_filter,lookup, search,trip_group_condition_total])

            if (trip_total.length == 0) {
                trips = [];
                res.json({ success: true, detail: trips, 'current_page': 1, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });
            } else {
                res.json({ success: true, detail: trips, timezone_for_display_date: timezone_for_display_date, 'current_page': page, trip_total: trip_total, type: req.body.type, 'pages': pages, 'next': next, 'pre': pre, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date,is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

            }
        }
    }

}

//api for get statement  for trip earning
exports.statement_provider_trip_earning = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({})
        let params_trips = [{ name: "trip_id", type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_trips)
        if (!response.success) {
            res.json(response)
            return;
        }
        let timezone_for_display_date = setting_detail.timezone_for_display_date;
        let trips = [];
        let page = req.path.split('/');
        let query = { $match: {} };
        query["$match"]['_id'] = { $eq: Schema(req.body.trip_id) }


        let user_lookup = {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1 } }],
                as: 'user_detail'
            }
        }
        let user_unwind = {
            $unwind: {
                path: "$user_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let Trip_service_lookup = {
            $lookup: {
                from: 'trip_services',
                localField: 'trip_service_city_type_id',
                foreignField: '_id',
                as: 'trip_service_detail'
            }
        }
        let Trip_service_unwind = {
            $unwind: {
                path: "$trip_service_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let city_type_lookup = {
            $lookup: {
                from: 'city_types',
                localField: 'service_type_id',
                foreignField: '_id',
                as: 'city_type_detail'
            }
        }
        let city_type_unwind = {
            $unwind: {
                path: "$city_type_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let Type_lookup = {
            $lookup: {
                from: 'types',
                localField: 'typeid',
                foreignField: '_id',
                as: 'type_detail'
            }
        }
        let Type_unwind = {
            $unwind: {
                path: "$type_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let provider_lookup = {
            $lookup: {
                from: 'providers',
                localField: 'current_provider',
                foreignField: '_id',
                pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1 } }],
                as: 'providers_detail'
            }
        }
        let provider_unwind = {
            $unwind: {
                path: "$providers_detail",
                preserveNullAndEmptyArrays: true
            }
        };
        let trip = await Trip_history.aggregate([query, user_lookup, user_unwind, Trip_service_lookup, Trip_service_unwind, city_type_lookup, city_type_unwind, Type_lookup, Type_unwind, provider_lookup, provider_unwind]);
        if (!trip) {
            trip = await Trip.aggregate([query, user_lookup, user_unwind, Trip_service_lookup, Trip_service_unwind, city_type_lookup, city_type_unwind, Type_lookup, Type_unwind, provider_lookup, provider_unwind]);
        }
        let rental_package;
        if (trip.car_rental_id) {
            rental_package = await City_type.findById(trip.car_rental_id);
        }
        res.json({ success: true, rental_package, detail: trip, type: req.body.type, timezone_for_display_date: timezone_for_display_date, provider_detail: "$providers_detail", user_detail: "$user_detail", type_detail: "$type_detail", service_detail: "$city_type_detail", moment: moment, tripservice: "$trip_service_detail" });
    } catch (err) {
        utils.error_response(err, req, res)
    }
}

//api for get trip list for daily and weekly earning
exports.weekly_and_daily_earning = async function (req, res) {
    try {
        let params_trips = [{ name: "earning_type", type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_trips)
        if (!response.success) {
            res.json(response)
            return;
        }
        let page;
        let next;
        let pre;
        let search_item;
        let search_value;
        let filter_start_date;
        let filter_end_date;
        let week_start_date_view = "";
        let week_end_date_view = "";
        let selected_country = req.body.selected_country;
        let selected_city = req.body.selected_city;
        if (req.body.page == undefined) {
            page = 0;
            next = 1;
            pre = 0;
        } else {
            page = req.body.page;
            next = parseInt(req.body.page) + 1;
            pre = req.body.page - 1;
        }

        if (req.body.search_item == undefined) {
            search_item = 'provider_detail.first_name';
            search_value = '';
            filter_start_date = '';
            filter_end_date = '';
        } else {
            search_item = req.body.search_item;
            search_value = req.body.search_value;
            filter_start_date = req.body.start_date;
            filter_end_date = req.body.end_date;
        }

        let date = new Date();
        let start_date = req.body.start_date
        let end_date = req.body.end_date
        let earning_type = req.body.earning_type


        if (start_date == '' || start_date == undefined) {
            start_date = date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
        } else {
            start_date = new Date(start_date);
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
        }

        if (end_date == '' || end_date == undefined) {
            end_date = date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);
        } else {
            end_date = new Date(end_date);
            end_date = end_date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);
        }


        let number_of_rec = req.body.limit;

        let lookup = {
            $lookup:
            {
                from: "providers",
                localField: "_id",
                foreignField: "_id",
                pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1, phone: !req.headers.is_show_phone ?  HIDE_DETAILS.PHONE : 1, wallet_currency_code: 1, country_phone_code: !req.headers.is_show_phone ?  HIDE_DETAILS.COUNTRY_CODE : 1 } }],
                as: "provider_detail"
            }
        };

        let country_filter = { "$match": {} };
        let city_filter = { "$match": {} };
        if (selected_country != 'all') {
            country_filter["$match"]['country_id'] = { $eq: Schema(selected_country) };

            if (selected_city != 'all') {
                city_filter["$match"]['city_id'] = { $eq: Schema(selected_city) };
            }
        }
        let search = { "$match": {} };
        let value = search_value;
        value = value.trim();
        value = value.replace(/ +(?= )/g, '');
        if (search_item == "provider_detail.first_name") {
            let query1 = {};
            let query2 = {};
            let query3 = {};
            let query4 = {};
            let query5 = {};
            let query6 = {};

            let full_name = value.split(' ');
            if (typeof full_name[0] == 'undefined' || typeof full_name[1] == 'undefined') {

                query1[search_item] = { $regex: new RegExp(value, 'i') };
                query2['provider_detail.last_name'] = { $regex: new RegExp(value, 'i') };

                search = { "$match": { $or: [query1, query2] } };
            } else {

               query4['provider_detail.first_name'] = { $regex: new RegExp(full_name[0], 'i') };
                query5['provider_detail.last_name'] = { $regex: new RegExp(full_name[1], 'i') };
                search = { "$match": { $and: [query4, query5] } };
            }
        } else {
            search["$match"][search_item] = { $regex: value };
        }


        let trip_filter = { "$match": {} };
        trip_filter["$match"]['complete_date_in_city_timezone'] = { $gte: start_date, $lt: end_date };

        ///// For Count number of result /////
        let count = { $group: { _id: null, total: { $sum: 1 }, data: { $push: '$data' } } };

        //// For skip number of result /////
        let skip = {};
        skip["$skip"] = page * number_of_rec;


        let sort = {}
        let sort_item = req.body.sort_item 
        let sort_order = Number(req.body.sort_order)
        if(sort_item && sort_order){
            sort = {$sort:{
                [sort_item] : sort_order
            }}
        } else {
            sort = { $sort: { provider_trip_end_time: -1 } }
        }

        ///// For limitation on result /////
        let limit = {};
        limit["$limit"] = number_of_rec;

        let trip_condition = { 'is_trip_completed': 1 };
        let trip_condition_new = { $and: [{ 'is_trip_cancelled_by_user': 1 }, { 'pay_to_provider': { $gt: 0 } }] };
        trip_condition = { $match: { $or: [trip_condition, trip_condition_new] } };


        let country_list = await Country.find({})

        if (selected_country == null) {
            if (country_list.length > 0) {
                selected_country = country_list[0]._id;
            }
        }
        let trip_group_condition = {
            $group: {
                _id: '$provider_id',
                total_trip: { $sum: 1 },
                completed_trip: { $sum: { $cond: [{ $eq: ["$is_trip_completed", 1] }, 1, 0] } },
                total: { $sum: '$total' },
                provider_have_cash: { $sum: '$provider_have_cash' },
                provider_service_fees: { $sum: '$provider_service_fees' },
                card_payment: { $sum: '$card_payment' },
                cash_payment: { $sum: '$cash_payment' },
                wallet_payment: { $sum: '$wallet_payment' },
                // pay_to_provider: { $sum: '$pay_to_provider' },
                unique_id: { $first: '$unique_id' },
                pay_to_provider: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, { '$eq': ['$is_transfered', false] }] }, '$pay_to_provider', 0] } },
            }
        }
        if (req.body.is_export) {
            let trips = await Trip_history.aggregate([trip_condition, trip_filter, country_filter, city_filter, trip_group_condition, lookup, search,sort])
            generate_trip_earning_excel(req, res, trips , req.body.header)
            return
        }

        let trips = await Trip_history.aggregate([trip_condition, trip_filter, country_filter, city_filter, trip_group_condition, lookup, search,sort, count])

        if (trips.length == 0) {
            trips = [];
            res.json({ success: true, detail: trips, 'current_page': 1, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, start_date: start_date, end_date: end_date, search_item, search_value, filter_start_date, filter_end_date, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

        } else {
            let pages = Math.ceil(trips[0].total / number_of_rec);

            trips = await Trip_history.aggregate([trip_condition, trip_filter, country_filter, city_filter, trip_group_condition, lookup, search, sort, skip, limit])

            if (trips.length == 0) {
                trips = [];
                res.json({ success: true, detail: trips, 'current_page': 1, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, start_date: start_date, end_date: end_date, filter_start_date, filter_end_date, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, search_item, search_value, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

            } else {
                let trip_group_condition_total = {
                    $group: {
                        _id: null,
                        total_trip: { $sum: 1 },
                        completed_trip: { $sum: { $cond: [{ $eq: ["$is_trip_completed", 1] }, 1, 0] } },
                        total: { $sum: '$total' },
                        promo_payment: { $sum: '$promo_payment' },
                        card_payment: { $sum: '$card_payment' },
                        cash_payment: { $sum: '$cash_payment' },
                        wallet_payment: { $sum: '$wallet_payment' },
                        admin_earning: { $sum: { $subtract: ['$total', '$provider_service_fees'] } },
                        admin_earning_in_currency: { $sum: { $subtract: ['$total_in_admin_currency', '$provider_service_fees_in_admin_currency'] } },
                        provider_earning: { $sum: '$provider_service_fees' },
                        provider_have_cash: { $sum: '$provider_have_cash' },
                        pay_to_provider: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, { '$eq': ['$is_transfered', false] }] }, '$pay_to_provider', 0] } },

                    }
                }
                if (earning_type == 'weekly') {
                    trip_group_condition_total = {
                        $group: {
                            _id: null,
                            total_trip: { $sum: 1 },
                            completed_trip: { $sum: { $cond: [{ $eq: ["$is_trip_completed", 1] }, 1, 0] } },
                            total: { $sum: '$total' },
                            promo_payment: { $sum: '$promo_payment' },
                            card_payment: { $sum: '$card_payment' },
                            cash_payment: { $sum: '$cash_payment' },
                            wallet_payment: { $sum: '$wallet_payment' },
                            admin_earning: { $sum: { $subtract: ['$total', '$provider_service_fees'] } },
                            admin_earning_in_currency: { $sum: { $subtract: ['$total_in_admin_currency', '$provider_service_fees_in_admin_currency'] } },
                            provider_earning: { $sum: '$provider_service_fees' },
                            provider_have_cash: { $sum: '$provider_have_cash' },
                            pay_to_provider: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, { '$eq': ['$is_transfered', false] }] }, '$pay_to_provider', 0] } },

                        }
                    }
                }

                let lookup1 = {
                    $lookup:
                    {
                        from: "providers",
                        localField: "provider_id",
                        foreignField: "_id",
                        pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1, phone: 1, wallet_currency_code: 1, country_phone_code: 1 } }],
                        as: "provider_detail"
                    }
                };

                let trip_total = await Trip_history.aggregate([trip_condition, trip_filter, country_filter, city_filter,lookup1,search,trip_group_condition_total])

                if (trip_total.length == 0) {
                    trips = [];
                    res.json({ success: true, detail: trips, 'current_page': 1, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, start_date: start_date, end_date: end_date, week_start_date_view, week_end_date_view: week_end_date_view, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });
                } else {

                    res.json({ success: true, detail: trips, 'current_page': page, trip_total: trip_total, type: req.body.type, 'pages': pages, 'next': next, 'pre': pre, moment: moment, start_date: start_date, end_date: end_date, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

                }
            }
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }
}

//api for get statement of provider weekly and daily earning
exports.statement_provider_daily_and_weekly_earning = async function (req, res) {
    try {
        let params_trips = [{ name: "provider_id", type: 'string' }]
        let response = await utils.check_request_params_async(req.body, params_trips)
        if (!response.success) {
            res.json(response)
            return;
        }

        let provider_id = req.body.provider_id
        let start_date = new Date(req.body.start_date);
        let end_date = new Date(req.body.end_date);

        // let provider = Provider.findOne({ _id: req.body.provider_id })
        let provider_match_condition = { $match: { provider_id: { $eq: Schema(provider_id) } } };
        let provider_daily_analytic_data = [];
        let timeDiff = end_date.getTime() - start_date.getTime();
        let dayDiff = timeDiff / (1000 * 3600 * 24);
        dayDiff = Math.ceil(dayDiff);
        let date_for_tag = new Date(start_date);
        let date_string = '';
        for (let i = 0; i <= dayDiff; i++) {
            if (i == 0) {
                date_string = date_string + moment(date_for_tag).format(constant_json.DATE_FORMAT_MMM_D_YYYY)
            }
            if (i == dayDiff - 1) {
                date_string = date_string + ' - ' + moment(date_for_tag).format(constant_json.DATE_FORMAT_MMM_D_YYYY)
            }
            provider_daily_analytic_data.push(moment(date_for_tag).format(constant_json.DATE_FORMAT_MMM_D_YYYY));
            date_for_tag = moment(date_for_tag).add(1, 'days');
        }

        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);
        end_date = end_date.setHours(23, 59, 59, 999);
        end_date = new Date(end_date);

        let provider_daily_analytic_query = { $match: { date_tag: { $in: provider_daily_analytic_data } } }
        let group_analytic_data_condition = {
            $group: {
                _id: null,
                received: { $sum: '$received' },
                accepted: { $sum: '$accepted' },
                rejected: { $sum: '$rejected' },
                not_answered: { $sum: '$not_answered' },
                cancelled: { $sum: '$cancelled' },
                completed: { $sum: '$completed' },
                acception_ratio: { $sum: '$acception_ratio' },
                rejection_ratio: { $sum: '$rejection_ratio' },
                cancellation_ratio: { $sum: '$cancellation_ratio' },
                completed_ratio: { $sum: '$completed_ratio' },
                total_online_time: { $sum: '$total_online_time' }
            }
        }
        let provider_daily_analytic = await Provider_daily_analytic.aggregate([provider_match_condition, provider_daily_analytic_query, group_analytic_data_condition])


        if (provider_daily_analytic.length > 0) {
            provider_daily_analytic_data = provider_daily_analytic[0];
            if ((Number(provider_daily_analytic_data.received)) > 0) {
                received = provider_daily_analytic_data.received;
                provider_daily_analytic_data.acception_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.accepted * 100) / received));
                provider_daily_analytic_data.cancellation_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.cancelled * 100) / received));
                provider_daily_analytic_data.completed_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.completed * 100) / received));
                provider_daily_analytic_data.rejection_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.rejected * 100) / received));
            }
        }

        let provider_condition = { $match: { provider_id: { $eq: Schema(provider_id) } } };

        let filter = { "$match": {} };
        filter["$match"]['complete_date_in_city_timezone'] = { $gte: start_date, $lt: end_date };
        let trip_condition1 = { 'is_trip_completed': 1 };
        let trip_condition2 = { $and: [{ 'is_trip_cancelled_by_user': 1 }, { 'pay_to_provider': { $gt: 0 } }] };

        let trip_condition = { $match: { $or: [trip_condition1, trip_condition2] } };

        let trip_group_condition = {
            $group: {
                _id: '$provider_id',
                total_distance: { $sum: '$total_distance' },
                total_time: { $sum: '$total_time' },
                total_waiting_time: { $sum: '$total_waiting_time' },
                total_service_surge_fees: { $sum: '$surge_fee' },
                service_total: { $sum: '$total_after_surge_fees' },

                total_provider_tax_fees: { $sum: '$provider_tax_fee' },
                total_provider_miscellaneous_fees: { $sum: '$provider_miscellaneous_fee' },
                total_toll_amount: { $sum: '$toll_amount' },
                total_tip_amount: { $sum: '$tip_amount' },
                total_provider_service_fees: { $sum: '$provider_service_fees' },

                total_provider_have_cash: { $sum: { '$cond': [{ '$eq': ['$payment_mode', 1] }, '$cash_payment', 0] } },
                total_deduct_wallet_amount: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, { '$ne': ['$is_provider_earning_added_in_wallet', true] }, { '$eq': ['$payment_mode', 1] }] }, '$provider_income_set_in_wallet', 0] } },           
                                total_added_wallet_amount: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, {$or: [{ '$eq': ['$is_provider_earning_added_in_wallet', true] }, { '$eq': ['$payment_mode', 0] } ]} ] }, '$provider_income_set_in_wallet', 0] } },
                                total_paid_in_wallet_payment: { $sum: { '$cond': [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, '$provider_income_set_in_wallet', 0] } },

                total_transferred_amount: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', false] }, { '$eq': ['$is_transfered', true] }] }, '$pay_to_provider', 0] } },
                total_pay_to_provider: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', false] }, { '$eq': ['$is_transfered', false] }] }, '$pay_to_provider', 0] } },

                currency: { $first: '$currency' },
                date: { $first: '$provider_trip_end_time' },
                unit: { $first: '$unit' },
                statement_number: { $first: '$invoice_number' },
            }
        }

        let trips = await Trip_history.aggregate([provider_condition, trip_condition, filter, trip_group_condition,])
        if (trips.length == 0) {
            trips = {};
            trips = [];
            res.json({ success: true, detail: trips, trips: trips, date_string: date_string, provider_daily_analytic_data: provider_daily_analytic_data, moment: moment });

        } else {
            let trip = await Trip_history.aggregate([provider_condition, trip_condition, filter])
            res.json({ success: true, detail: trips[0], date_string: date_string, trips: trip, provider_daily_analytic_data: provider_daily_analytic_data, moment: moment });

        }
    } catch (err) {
        utils.error_response(err, req, res)
    }
};

//api for get partner weekly earning
exports.partner_weekly_earning = async function (req, res) {
    try {

        let page;
        let next;
        let pre;
        let search_item;
        let search_value;
        let sort_field;
        let filter_start_date;
        let filter_end_date;
        let selected_country = req.body.selected_country;
        let selected_city = req.body.selected_city;
        if (req.body.page == undefined) {
            page = 0;
            next = 1;
            pre = 0;
        } else {
            page = req.body.page;
            next = parseInt(req.body.page) + 1;
            pre = req.body.page - 1;
        }

        if (req.body.search_item == undefined) {
            search_item = 'provider_detail.first_name';
            search_value = '';
        } else {
            search_item = req.body.search_item;
            search_value = req.body.search_value;
        }

        let week_start_date_view = "";
        let week_end_date_view = "";

        if (req.body.date != undefined) {

            let weekDuration = req.body.date;
            weekDuration = weekDuration.split('-');

            week_start_date_view = weekDuration[0];
            week_end_date_view = weekDuration[1];

            start_date = new Date(week_start_date_view);
            end_date = new Date(week_end_date_view);

            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
            end_date = end_date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);

        } else {

            let today = new Date();
            end_date = new Date(today.setDate(today.getDate() + 6 - today.getDay()));
            today = new Date(end_date);
            start_date = new Date(today.setDate(today.getDate() - 6));

            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
            end_date = end_date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);

            let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            week_start_date_view = start_date.getDate() + ' ' + monthNames[start_date.getMonth()] + ' ' + start_date.getFullYear();
            week_end_date_view = end_date.getDate() + ' ' + monthNames[end_date.getMonth()] + ' ' + end_date.getFullYear();
        }

        let number_of_rec = Number(req.body.limit);

        let lookup = {
            $lookup:
            {
                from: "partners",
                localField: "_id",
                pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1, phone: !req.headers.is_show_phone ?  HIDE_DETAILS.PHONE : 1, 
                            wallet_currency_code: 1, country_phone_code: !req.headers.is_show_phone ?  HIDE_DETAILS.COUNTRY_CODE : 1 } }],
                foreignField: "_id",
                as: "provider_detail"
            }
        };

        let country_filter = { "$match": {} };
        let city_filter = { "$match": {} };

        if (selected_country != 'all') {
            country_filter["$match"]['country_id'] = { $eq: Schema(selected_country) };

            if (selected_city != 'all') {
                city_filter["$match"]['city_id'] = { $eq: Schema(selected_city) };
            }
        }

        let value = search_value;
        let search = { "$match": {} };
        value = value.trim();
        value = value.replace(/ +(?= )/g, '');
        if (search_item == "provider_detail.first_name") {
            let query1 = {};
            let query2 = {};
            let query3 = {};
            let query4 = {};
            let query5 = {};
            let query6 = {};

            let full_name = value.split(' ');
            if (typeof full_name[0] == 'undefined' || typeof full_name[1] == 'undefined') {

                query1[search_item] = { $regex: new RegExp(value, 'i') };
                query2['provider_detail.last_name'] = { $regex: new RegExp(value, 'i') };

                search = { "$match": { $or: [query1, query2] } };
            } else {

                query4['provider_detail.first_name'] = { $regex: new RegExp(full_name[0], 'i') };
                query5['provider_detail.last_name'] = { $regex: new RegExp(full_name[1], 'i') };
                search = { "$match": { $and: [query4, query5] } };
            }
        } else {
            search["$match"][search_item] = { $regex: value };
        }

        let trip_filter = { "$match": {} };
        trip_filter["$match"]['complete_date_in_city_timezone'] = { $gte: start_date, $lt: end_date };

        let sort = {}
        let sort_item = req.body.sort_item 
        let sort_order = Number(req.body.sort_order)
        if(sort_item && sort_order){
            sort = {$sort:{
                [sort_item] : sort_order
            }}
        } else {
            sort = { $sort: { provider_trip_end_time: -1 } }
        }

        ///// For Count number of result /////
        let count = { $group: { _id: null, total: { $sum: 1 }, data: { $push: '$data' } } };

        //// For skip number of result /////
        let skip = {};
        skip["$skip"] = page * number_of_rec;

        ///// For limitation on result /////
        let limit = {};
        limit["$limit"] = number_of_rec;

        let trip_condition = { 'is_trip_completed': 1 };
        let trip_condition_new = { $and: [{ 'is_trip_cancelled_by_user': 1 }, { 'pay_to_provider': { $gt: 0 } }] };
        trip_condition = { $match: { $or: [trip_condition, trip_condition_new] } };

        let provider_type_condition = { $match: { 'provider_type': Number(constant_json.PROVIDER_TYPE_PARTNER) } };

        if (req.body.partner_id) {
            provider_type_condition = { $match: { $and: [{ 'provider_type': Number(constant_json.PROVIDER_TYPE_PARTNER) }, { provider_type_id: Schema(req.body.partner_id) }] } }
        }
        let provider_weekly_analytic_data = {};

        let trip_group_condition = {
            $group: {
                _id: '$provider_type_id',
                total_trip: { $sum: 1 },
                completed_trip: { $sum: { $cond: [{ $eq: ["$is_trip_completed", 1] }, 1, 0] } },
                card_payment: { $sum: '$card_payment' },
                cash_payment: { $sum: '$cash_payment' },
                wallet_payment: { $sum: '$wallet_payment' },
                total: { $sum: '$total' },
                provider_have_cash: { $sum: '$cash_payment' },
                provider_service_fees: { $sum: '$provider_service_fees' },
                pay_to_provider: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', false] }, { '$eq': ['$is_transfered', false] }] }, '$pay_to_provider', 0] } },
                remaining_payment:{$first:'$remaining_payment'}

            }

        }

        if (req.body.is_export) {
            let trips = await Trip_history.aggregate([trip_condition, trip_filter, provider_type_condition, country_filter, city_filter, trip_group_condition, lookup, search,sort])
            generate_partner_earning(req, res, trips, req.body.header)
            return
        }

        let trips = await Trip_history.aggregate([trip_condition, trip_filter, provider_type_condition, country_filter, city_filter, trip_group_condition, lookup, search,sort, count])

        if (trips.length == 0) {
            trips = [];
            res.json({ success: true, detail: trips, 'current_page': 1, provider_weekly_analytic: provider_weekly_analytic_data, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });

        } else {
            let pages = Math.ceil(trips[0].total / number_of_rec);

            trips = await Trip_history.aggregate([trip_condition, trip_filter, provider_type_condition, country_filter, city_filter, trip_group_condition, lookup, search, sort, skip, limit])

            if (trips.length == 0) {
                trips = [];
                res.json({ success: true, detail: trips, 'current_page': 1, provider_weekly_analytic: provider_weekly_analytic_data, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });
            } else {

                let trip_group_condition_total = {
                    $group: {
                        _id: null,
                        total_trip: { $sum: 1 },
                        completed_trip: { $sum: { $cond: [{ $eq: ["$is_trip_completed", 1] }, 1, 0] } },
                        total: { $sum: '$total' },
                        promo_payment: { $sum: '$promo_payment' },
                        card_payment: { $sum: '$card_payment' },
                        cash_payment: { $sum: '$cash_payment' },
                        wallet_payment: { $sum: '$wallet_payment' },
                        admin_earning: { $sum: { $subtract: ['$total', '$provider_service_fees'] } },
                        admin_earning_in_currency: { $sum: { $subtract: ['$total_in_admin_currency', '$provider_service_fees_in_admin_currency'] } },
                        provider_earning: { $sum: '$provider_service_fees_in_admin_currency' },
                        provider_have_cash: { $sum: '$provider_have_cash' },
                        pay_to_provider: { $sum: '$pay_to_provider' },
                        remaining_payment:{$first:'$remaining_payment'}

                    }
                }

                let lookup1 = {
                    $lookup:
                    {
                        from: "partners",
                        localField: "provider_type_id",
                        foreignField: "_id",
                        pipeline: [{ $project: { _id: 1, first_name: 1, last_name: 1, unique_id: 1, phone: 1, wallet_currency_code: 1, country_phone_code: 1 } }],
                        as: "provider_detail"
                    }
                };

                let trip_total = await Trip_history.aggregate([lookup1, search,trip_condition, trip_filter, provider_type_condition, country_filter, city_filter, trip_group_condition_total])
                if (trip_total.length == 0) {
                    trips = [];
                    res.json({ success: true, detail: trips, 'current_page': 1, provider_weekly_analytic: provider_weekly_analytic_data, type: req.body.type, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date , is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone});
                } else {
                    res.json({ success: true, detail: trips, 'current_page': page, provider_weekly_analytic: provider_weekly_analytic_data, trip_total: trip_total, type: req.body.type, 'pages': pages, 'next': next, 'pre': pre, moment: moment, week_start_date_view: week_start_date_view, week_end_date_view: week_end_date_view, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });
                }

            }

        }
    } catch (err) {
        utils.error_response(err, req, res)
    }

};

//api for get statement of partner weekly earning
exports.partner_weekly_earning_statement = async function (req, res) {
    try {
        partner_id = req.body.partner_id;
        start_date = new Date(req.body.week_start_date);
        end_date = new Date(req.body.week_end_date);

        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);
        end_date = end_date.setHours(23, 59, 59, 999);
        end_date = new Date(end_date);


        let provider_match_condition = { $match: { partner_id: { $eq: Schema(partner_id) } } };
        let provider_daily_analytic_data = [];

        let date_for_tag = new Date(start_date);
        for (let i = 0; i < 7; i++) {
            provider_daily_analytic_data.push(moment(date_for_tag).format(constant_json.DATE_FORMAT_MMM_D_YYYY));
            date_for_tag = moment(date_for_tag).add(1, 'days');
        }

        let provider_daily_analytic_query = { $match: { date_tag: { $in: provider_daily_analytic_data } } }
        let group_analytic_data_condition = {
            $group: {
                _id: null,
                received: { $sum: '$received' },
                accepted: { $sum: '$accepted' },
                rejected: { $sum: '$rejected' },
                not_answered: { $sum: '$not_answered' },
                cancelled: { $sum: '$cancelled' },
                completed: { $sum: '$completed' },
                acception_ratio: { $sum: '$acception_ratio' },
                rejection_ratio: { $sum: '$rejection_ratio' },
                cancellation_ratio: { $sum: '$cancellation_ratio' },
                completed_ratio: { $sum: '$completed_ratio' },
                total_online_time: { $sum: '$total_online_time' }
            }
        }

        let cond1 = { $match: { provider_type_id: { $eq: Schema(partner_id) } } }
        let proj1 = {
            $project: {
                _id: 1
            }
        }
        let partner_provider_list = await Provider.aggregate([cond1, proj1])
        let provider_ids = []
        partner_provider_list.forEach(provider => {
            provider_ids.push(provider._id)
        })
        provider_match_condition = { $match: { provider_id: { $in: provider_ids } } };

        let provider_daily_analytic = await Provider_daily_analytic.aggregate([provider_match_condition, provider_daily_analytic_query, group_analytic_data_condition])

        if (provider_daily_analytic.length > 0) {
            provider_daily_analytic_data = provider_daily_analytic[0];
            if ((Number(provider_daily_analytic_data.received)) > 0) {
                received = provider_daily_analytic_data.received;
                provider_daily_analytic_data.acception_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.accepted * 100) / received));
                provider_daily_analytic_data.cancellation_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.cancelled * 100) / received));
                provider_daily_analytic_data.completed_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.completed * 100) / received));
                provider_daily_analytic_data.rejection_ratio = utils.precisionRoundTwo(Number((provider_daily_analytic_data.rejected * 100) / received));
            }
        }

        let provider_condition = { $match: { provider_type_id: { $eq: Schema(partner_id) } } };

        let filter = { "$match": {} };
        filter["$match"]['complete_date_in_city_timezone'] = { $gte: start_date, $lt: end_date };

        let trip_condition = { 'is_trip_completed': 1 };
        let trip_condition_new = { $and: [{ 'is_trip_cancelled_by_user': 1 }, { 'pay_to_provider': { $gt: 0 } }] };
        trip_condition = { $match: { $or: [trip_condition, trip_condition_new] } };

        let trip_group_condition = {
            $group: {
                _id: '$provider._id',
                total_distance: { $sum: '$total_distance' },
                total_time: { $sum: '$total_time' },
                total_waiting_time: { $sum: '$total_waiting_time' },
                total_service_surge_fees: { $sum: '$surge_fee' },
                service_total: { $sum: '$total_after_surge_fees' },
                total_cancellation_fees: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_trip_cancelled_by_user', 1] }, { '$gt': ['$pay_to_provider', 0] }] }, '$total_service_fees', 0] } },

                total_provider_tax_fees: { $sum: '$provider_tax_fee' },
                total_provider_miscellaneous_fees: { $sum: '$provider_miscellaneous_fee' },
                total_toll_amount: { $sum: '$toll_amount' },
                total_tip_amount: { $sum: '$tip_amount' },
                total_provider_service_fees: { $sum: '$provider_service_fees' },

                total_provider_have_cash: { $sum: { '$cond': [{ '$eq': ['$payment_mode', 1] }, '$cash_payment', 0] } },
                total_deduct_wallet_amount: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, { '$ne': ['$is_provider_earning_added_in_wallet', true] }, { '$eq': ['$payment_mode', 1] }] }, '$provider_income_set_in_wallet', 0] } },
                                total_added_wallet_amount: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, {$or: [{ '$eq': ['$is_provider_earning_added_in_wallet', true] }, { '$eq': ['$payment_mode', 0] } ]} ] }, '$provider_income_set_in_wallet', 0] } },
                                total_paid_in_wallet_payment: { $sum: { '$cond': [{ '$eq': ['$is_provider_earning_set_in_wallet', true] }, '$provider_income_set_in_wallet', 0] } },
                total_transferred_amount: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', false] }, { '$eq': ['$is_transfered', true] }] }, '$pay_to_provider', 0] } },
                total_pay_to_provider: { $sum: { '$cond': [{ $and: [{ '$eq': ['$is_provider_earning_set_in_wallet', false] }, { '$eq': ['$is_transfered', false] }] }, '$pay_to_provider', 0] } },

                currency: { $first: '$currency' },
                unit: { $first: '$unit' },
                created_at:{ $first: '$created_at'},
                statement_number: { $first: '$invoice_number' },
            }
        }
        let trips = await Trip_history.aggregate([provider_condition, trip_condition, filter, trip_group_condition])

        if (trips.length == 0) {
            trips = {};
            res.json({ sucess: true, detail: trips, type: req.body.type, provider_daily_analytic_data: provider_daily_analytic_data, moment: moment });
        } else {
            res.json({ sucess: true, detail: trips[0], type: req.body.type, provider_daily_analytic_data: provider_daily_analytic_data, moment: moment });
        }



    } catch (err) {
        utils.error_response(err, req, res)
    }
};

//admin wakket history
exports.wallet_history = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({})
        let page;
        let next;
        let pre;
        let search_item;
        let search_value;
        let sort_order = -1;
        let sort_field = 'unique_id';
        let filter_start_date = '';
        let filter_end_date = '';

        type = 0;
        if (req.body.page == undefined) {
            page = 0;
            next = 1;
            pre = 0;
        } else {
            page = req.body.page;
            next = parseInt(req.body.page) + 1;
            pre = req.body.page - 1;
        }

        if (req.body.search_item == undefined) {
            search_item = 'wallet_description';
            search_value = '';

        } else {
            let value = req.body.search_value;
            value = value.trim();
            value = value.replace(/ +(?= )/g, '');
            value = new RegExp(value, 'i');

            search_item = req.body.search_item
            search_value = req.body.search_value;
        }
        if (req.body.sort_order && req.body.sort_order != undefined) {
            sort_order = req.body.sort_order;
        }
        if (req.body.sort_item || req.body.sort_item == undefined) {
            sort_field = req.body.sort_item;
        }
        if (req.body.start_date && req.body.start_date != undefined) {
            filter_start_date = req.body.start_date;
        }
        if (req.body.end_date && req.body.end_date != undefined) {
            filter_end_date = req.body.end_date;
        }
        if (req.body.type && req.body.type != undefined) {
            type = Number(req.body.type);
        }

        let end_date = req.body.end_date;
        let start_date = req.body.start_date;
        if (end_date == '' || end_date == undefined) {
            end_date = new Date();
        } else {
            end_date = new Date(end_date);
            end_date = end_date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);
        }

        if (start_date == '' || start_date == undefined) {
            start_date = new Date(end_date.getTime() - (6 * 24 * 60 * 60 * 1000));
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
        } else {
            start_date = new Date(start_date);
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
        }

        let number_of_rec = req.body.limit;

        let lookup = {
            $lookup:
            {
                from: "users",
                localField: "user_id",
                pipeline: [{ $project: { 
                    _id: 1, 
                    email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1,
                    unique_id: 1, 
                    wallet_currency_code: 1 } }],
                foreignField: "_id",
                as: "user_detail"
            }
        };

        let lookup1 = {
            $lookup:
            {
                from: "providers",
                localField: "user_id",
                pipeline: [{ $project: { _id: 1, email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1, unique_id: 1, wallet_currency_code: 1 } }],
                foreignField: "_id",
                as: "provider_detail"
            }
        };

        let lookup2 = {
            $lookup:
            {
                from: "partners",
                localField: "user_id",
                pipeline: [{ $project: { _id: 1, email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1, unique_id: 1, wallet_currency_code: 1 } }],
                foreignField: "_id",
                as: "partner_detail"
            }
        };

        let lookup3 = {
            $lookup:
            {
                from: "corporates",
                localField: "user_id",
                pipeline: [{ $project: { _id: 1, email: !req.headers.is_show_email ?  HIDE_DETAILS.EMAIL : 1, unique_id: 1, wallet_currency_code: 1 } }],
                foreignField: "_id",
                as: "corporate_detail"
            }
        };
        value = search_value;
        value = value.trim();
        value = value.replace(/ +(?= )/g, '');

        let type_condition = {$match:{}}
        let type_value = req.body.type

        if(type_value != 0){
            type_condition['$match']['user_type'] = Number(type_value)
        }

        let search = { "$match": {} };
        if(search_value){
            search["$match"][search_item] = { $regex: new RegExp(value, 'i') }
            if(search_item == 'email'){
                search = {
                    $match:{
                        $or:[
                            {
                                'user_detail.email': { $regex: new RegExp(value, 'i') }
                            },
                            {
                                'provider_detail.email': { $regex: new RegExp(value, 'i') }
                            },
                            {
                                'partner_detail.email': { $regex: new RegExp(value, 'i') }
                            },
                            {
                                'corporate_detail.email': { $regex: new RegExp(value, 'i') }
                            }
                        ]
                    }
                }
            }
        }
      
        let filter = { "$match": {} };
        filter["$match"]['created_at'] = { $gte: start_date, $lt: end_date };

        let sort = { "$sort": {unique_id:-1} };
        if(sort_field && sort_order){
            sort["$sort"][sort_field] = parseInt(sort_order);
        }

        let count = { $group: { _id: null, total: { $sum: 1 }, data: { $push: '$data' } } };

        let skip = {};
        skip["$skip"] = page * number_of_rec;

        let limit = {};
        limit["$limit"] = number_of_rec;

        if (req.body.is_export) {
            let trips = await Wallet_history.aggregate([type_condition, lookup, lookup1, lookup2, lookup3, search, filter])
            wallet_excel(req, res, trips)
            return
        }

        let wallet_history = await Wallet_history.aggregate([type_condition, lookup, lookup1, lookup2, lookup3, search, filter, count])
        if (wallet_history.length == 0) {
            array = [];
            res.json({ success: true, detail: wallet_history, 'current_page': 1, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });
        } else {
            let is_public_demo = setting_detail.is_public_demo;

            let pages = Math.ceil(wallet_history[0].total / number_of_rec);
            wallet_history = await Wallet_history.aggregate([type_condition, lookup, lookup1, lookup2, lookup3, search, filter, sort, skip, limit])

            res.json({ success: true, is_public_demo: is_public_demo, timezone_for_display_date: setting_detail.timezone_for_display_date, detail: wallet_history, 'current_page': page, 'pages': pages, 'next': next, 'pre': pre, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date, is_show_email: req.headers.is_show_email, is_show_phone : req.headers.is_show_phone });
            delete message;
        }

    } catch (err) {
        utils.error_response(err, req, res)
    }
};

//admin transaction history
exports.transaction_history = async function (req, res) {
    try {
        const setting_detail = await Settings.findOne({})
        let page;
        let next;
        let pre;
        let search_item;
        let search_value;
        let sort_order = -1;
        let sort_field = 'unique_id';
        let filter_start_date = '';
        let filter_end_date = '';
        if (req.body.page == undefined) {
            page = 0;
            next = 1;
            pre = 0;
        } else {
            page = Number(req.body.page);
            next = parseInt(req.body.page) + 1;
            pre = req.body.page - 1;
        }

        if (req.body.search_item == undefined) {
            search_item = '';
            search_value = '';
            sort_order = -1;
            sort_field = 'unique_id';
            filter_start_date = '';
            filter_end_date = '';

        } else {
            let value = req.body.search_value;
            value = value.trim();
            value = value.replace(/ +(?= )/g, '');
            value = new RegExp(value, 'i');
            search_item = req.body.search_item
            search_value = req.body.search_value;
        }
        if (req.body.sort_order && req.body.sort_order != undefined) {
            sort_order = req.body.sort_order;
        }
        if (req.body.sort_item || req.body.sort_item == undefined) {
            sort_field = req.body.sort_item;
        }
        if (req.body.start_date && req.body.start_date != undefined) {
            filter_start_date = req.body.start_date;
        }
        if (req.body.end_date && req.body.end_date != undefined) {
            filter_end_date = req.body.end_date;
        }
        if (req.body.type && req.body.type != undefined) {
            type = Number(req.body.type);
        }

        let end_date = req.body.end_date;
        let start_date = req.body.start_date;
        if (end_date == '' || end_date == undefined) {
            end_date = new Date();
        } else {
            end_date = new Date(end_date);
            end_date = end_date.setHours(23, 59, 59, 999);
            end_date = new Date(end_date);
        }

        if (start_date == '' || start_date == undefined) {
            start_date = new Date(end_date.getTime() - (6 * 24 * 60 * 60 * 1000));
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
        } else {
            start_date = new Date(start_date);
            start_date = start_date.setHours(0, 0, 0, 0);
            start_date = new Date(start_date);
        }

        let number_of_rec = Number(req.body.limit);
        let lookup1 = {
            $lookup:
            {
                from: "providers",
                localField: "user_id",
                foreignField: "_id",
                as: "provider_detail"
            }
        };

        let lookup2 = {
            $lookup:
            {
                from: "partners",
                localField: "user_id",
                foreignField: "_id",
                as: "partner_detail"
            }
        };

        value = search_value;
        value = value.trim();
        value = value.replace(/ +(?= )/g, '');


        let search = { "$match": {} };
        search["$match"][search_item] = { $regex: new RegExp(value, 'i') }


        let filter = { "$match": {} };
        filter["$match"]['created_at'] = { $gte: start_date, $lt: end_date };

        let sort = { "$sort": {} };
        sort["$sort"][sort_field] = parseInt(sort_order);


        let skip = {};
        skip["$skip"] = page * number_of_rec;

        let limit = {};
        limit["$limit"] = number_of_rec;


        let transfer_history = await Transfer_history.aggregate([lookup1, lookup2])

        if(req.body.is_export){
            let transfer_history = await Transfer_history.aggregate([lookup1, lookup2])
            transfer_history(req,res,transfer_history)
            return
        }

        if (transfer_history.length == 0) {
            res.json({ sucess: true, detail: transfer_history, 'current_page': 1, 'pages': 0, 'next': 1, 'pre': 0, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date });
        }
        else {
            let is_public_demo = setting_detail.is_public_demo;
            let pages = Math.ceil(transfer_history[0].total / number_of_rec);
            transfer_history = await Transfer_history.aggregate([lookup1, lookup2, search, filter, sort, skip, limit])
            res.json({ success: true, is_public_demo: is_public_demo, timezone_for_display_date: setting_detail.timezone_for_display_date, detail: transfer_history, 'current_page': page, 'pages': pages, 'next': next, 'pre': pre, moment: moment, sort_field, sort_order, search_item, search_value, filter_start_date, filter_end_date });
            delete message;
        }
    } catch (err) {
        utils.error_response(err, req, res)
    }
};


function generate_trip_earning_excel(req, res, array, header) {
    var date = new Date()
    var time = date.getTime()
    var wb = new xl.Workbook();
    var ws = wb.addWorksheet('sheet1');
    var col = 1;
    let title = JSON.parse(header);
    ws.cell(1, col++).string(title.trip_id);
    ws.cell(1, col++).string(title.trip_end);
    ws.cell(1, col++).string(title.driver_id);
    ws.cell(1, col++).string(title.name);
    ws.cell(1, col++).string(title.phone);
    ws.cell(1, col++).string(title.total);
    ws.cell(1, col++).string(title.cash);
    ws.cell(1, col++).string(title.driver_profit);
    ws.cell(1, col++).string(title.pay_to_driver);

    array.forEach(function (data, index) {
        col = 1;
        ws.cell(index + 2, col++).number(data.unique_id || data.provider_detail.unique_id);
        ws.cell(index + 2, col++).string(moment(data.provider_trip_end_time).format("DD MMM 'YY") + ' ' + moment(data.created_at).format("hh:mm a"));

        if (data.provider_detail.length > 0) {
            ws.cell(index + 2, col++).number(data.provider_detail[0].unique_id);
            ws.cell(index + 2, col++).string(data.provider_detail[0].first_name + ' ' + data.provider_detail[0].last_name);
            ws.cell(index + 2, col++).string(data.provider_detail[0].country_phone_code + data.provider_detail[0].phone);
        } else {
            col += 3;
        }

        ws.cell(index + 2, col++).number(data.total);
        ws.cell(index + 2, col++).number(data.provider_have_cash);
        ws.cell(index + 2, col++).number(data.provider_service_fees);
        ws.cell(index + 2, col++).number(data.pay_to_provider);

        if (index == array.length - 1) {
            wb.write('data/xlsheet/' + time + '_trip_earning.xlsx', function (err) {
                if (err) {
                    console.error(err);
                } else {
                    var url = req.protocol + "://" + req.get('host') + "/xlsheet/" + time + "_trip_earning.xlsx";
                    res.json(url);
                    setTimeout(function () {
                        fs.unlink('data/xlsheet/' + time + '_trip_earning.xlsx', function () {
                        });
                    }, 10000)
                }
            });
        }
    });
}

function generate_partner_earning(req, res, array, header) {

    var date = new Date()
    var time = date.getTime()
    var wb = new xl.Workbook();
    var ws = wb.addWorksheet('sheet1');
    var col = 1;
    let title = JSON.parse(header)
    ws.cell(1, col++).string(title.partner_id);
    ws.cell(1, col++).string(title.name);
    ws.cell(1, col++).string(title.phone);
    ws.cell(1, col++).string(title.total);
    ws.cell(1, col++).string(title.cash);
    ws.cell(1, col++).string(title.driver_profit);
    ws.cell(1, col++).string(title.pay_to_partner);

    array.forEach(function (data, index) {
        col = 1;
        if (data.provider_detail.length > 0) {
            ws.cell(index + 2, col++).number(data.provider_detail[0].unique_id);
            ws.cell(index + 2, col++).string(data.provider_detail[0].first_name + ' ' + data.provider_detail[0].last_name);
            ws.cell(index + 2, col++).string(data.provider_detail[0].country_phone_code + data.provider_detail[0].phone);
        } else {
            col += 3;
        }

        ws.cell(index + 2, col++).number(data.total);
        ws.cell(index + 2, col++).number(data.provider_have_cash);
        ws.cell(index + 2, col++).number(data.provider_service_fees);
        ws.cell(index + 2, col++).number(data.pay_to_provider);

        if (index == array.length - 1) {
            wb.write('data/xlsheet/' + time + '_partner_weekly_earning.xlsx', function (err) {
                if (err) {
                    console.error(err);
                } else {
                    var url = req.protocol + "://" + req.get('host') + "/xlsheet/" + time + "_partner_weekly_earning.xlsx";
                    res.json(url);
                    setTimeout(function () {
                        fs.unlink('data/xlsheet/' + time + '_partner_weekly_earning.xlsx', function () {
                        });
                    }, 10000)
                }
            });
        }
    })
}
function wallet_excel(req, res, array) {
    var date = new Date()
    var time = date.getTime()
    var wb = new xl.Workbook();
    var ws = wb.addWorksheet('sheet1');
    var col = 1;
    ws.cell(1, col++).string(req.__('title_id'));
    ws.cell(1, col++).string(req.__('title_type'));
    ws.cell(1, col++).string(req.__('title_date'));
    ws.cell(1, col++).string(req.__('title_email'));
    ws.cell(1, col++).string(req.__('title_currency'));
    ws.cell(1, col++).string(req.__('title_wallet_amount'));
    ws.cell(1, col++).string(req.__('title_add_cut'));
    ws.cell(1, col++).string(req.__('title_wallet'));
    ws.cell(1, col++).string(req.__('title_wallet_description'));

    array.forEach(function (data, index) {
        col = 1;
        ws.cell(index + 2, col++).number(data.unique_id);
        if (data.user_type == constant_json.USER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(req.__('title_user'));
        } else if (data.user_type == constant_json.PROVIDER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(req.__('title_provider'));
        } else if (data.user_type == constant_json.PARTNER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(req.__('title_partner'));
        }
        ws.cell(index + 2, col++).string(moment(data.created_at).format("DD MMM 'YY") + ' ' + moment(data.created_at).format("hh:mm a"));

        if (data.user_type == constant_json.USER_UNIQUE_NUMBER) {
            if (data.user_detail.length > 0) {
                ws.cell(index + 2, col++).string(data.user_detail[0].email);
            } else {
                col++;
            }
        } else if (data.user_type == constant_json.PROVIDER_UNIQUE_NUMBER) {
            if (data.provider_detail.length > 0) {
                ws.cell(index + 2, col++).string(data.provider_detail[0].email);
            } else {
                col++;
            }
        } else if (data.user_type == constant_json.PARTNER_UNIQUE_NUMBER) {
            if (data.partner_detail.length > 0) {
                ws.cell(index + 2, col++).string(data.partner_detail[0].email);
            } else {
                col++;
            }
        }

        if (data.user_type == constant_json.USER_UNIQUE_NUMBER) {
            if (data.user_detail.length > 0) {
                ws.cell(index + 2, col++).string(data.user_detail[0].wallet_currency_code);
            } else {
                col++;
            }
        } else if (data.user_type == constant_json.PROVIDER_UNIQUE_NUMBER) {
            if (data.provider_detail.length > 0) {
                ws.cell(index + 2, col++).string(data.provider_detail[0].wallet_currency_code);
            } else {
                col++;
            }
        } else if (data.user_type == constant_json.PARTNER_UNIQUE_NUMBER) {
            if (data.partner_detail.length > 0) {
                ws.cell(index + 2, col++).string(data.partner_detail[0].wallet_currency_code);
            } else {
                col++;
            }
        }
        ws.cell(index + 2, col++).number(data.wallet_amount);
        ws.cell(index + 2, col++).number(data.added_wallet);
        ws.cell(index + 2, col++).number(data.total_wallet_amount);
        ws.cell(index + 2, col++).string(data.wallet_description);

        if (index == array.length - 1) {
            wb.write('data/xlsheet/' + time + '_wallet_history.xlsx', function (err) {
                if (err) {
                    console.error(err);
                } else {
                    var url = req.protocol + "://" + req.get('host') + "/xlsheet/" + time + "_wallet_history.xlsx";
                    res.json(url);
                    setTimeout(function () {
                        fs.unlink('data/xlsheet/' + time + '_wallet_history.xlsx', function () {
                        });
                    }, 10000)
                }
            });
        }
    });
}

function transaction_excel(req, res, array) {
    var date = new Date()
    var time = date.getTime()
    var wb = new xl.Workbook();
    var ws = wb.addWorksheet('sheet1');
    var col = 1;

    ws.cell(1, col++).string(req.__('title_id'));
    ws.cell(1, col++).string(req.__('title_promo_type'));
    ws.cell(1, col++).string(req.__('title_date'));
    ws.cell(1, col++).string(req.__('title_email'));
    ws.cell(1, col++).string(req.__('title_currency'));
    ws.cell(1, col++).string(req.__('title_amount'));
    ws.cell(1, col++).string(req.__('title_status'));
    ws.cell(1, col++).string(req.__('title_transfer_by'));

    array.forEach(function (data, index) {
        col = 1;
        ws.cell(index + 2, col++).number(data.unique_id);
        if (data.user_type == constant_json.USER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(req.__('title_user'));
        } else if (data.user_type == constant_json.PROVIDER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(req.__('title_provider'));
        }
        ws.cell(index + 2, col++).string(moment(data.created_at).format("DD MMM 'YY") + ' ' + moment(data.created_at).format("hh:mm a"));

        if (data.user_type == constant_json.USER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(data.user_detail[0].email);
        } else if (data.user_type == constant_json.PROVIDER_UNIQUE_NUMBER) {
            ws.cell(index + 2, col++).string(data.provider_detail[0].email);
        }

        ws.cell(index + 2, col++).string(data.currency_code);
        ws.cell(index + 2, col++).number(data.amount);
        ws.cell(index + 2, col++).string(data.transfer_status);
        ws.cell(index + 2, col++).string("Admin");

        if (index == array.length - 1) {
            wb.write('data/xlsheet/' + time + '_transfer_history.xlsx', function (err) {
                if (err) {
                    console.error(err);
                } else {
                    var url = req.protocol + "://" + req.get('host') + "/xlsheet/" + time + "_transfer_history.xlsx";
                    res.json(url);
                    setTimeout(function () {
                        fs.unlink('data/xlsheet/' + time + '_transfer_history.xlsx', function () {
                        });
                    }, 10000)
                }
            });
        }
    })
}