let earning = require('../controller/earning')
let router = require('express').Router()

router.route('/trip_earning').post(earning.trip_earning)
router.route('/statement_provider_trip_earning').post(earning.statement_provider_trip_earning)
router.route('/weekly_and_daily_earning').post(earning.weekly_and_daily_earning)
router.route('/statement_provider_daily_and_weekly_earning').post(earning.statement_provider_daily_and_weekly_earning)
router.route('/partner_weekly_earning').post(earning.partner_weekly_earning)
router.route('/partner_weekly_earning_statement').post(earning.partner_weekly_earning_statement)
router.route('/wallet_history').post(earning.wallet_history)
router.route('/transaction_history').post(earning.transaction_history)

module.exports = router