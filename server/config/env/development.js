module.exports = {
    db: 'mongodb://localhost:27017/EBER'
    // db: 'mongodb://localhost:27017/EBER_LOCAL' //dev
    // db: 'mongodb://localhost:27017/EBER_LOCAL_NEW' //old db
    // db: 'mongodb://localhost:27017/NEW_EBER_LOCAL' //new db dev
    // db: 'mongodb://localhost:27017/NEW_EBER_LOCAL_CLEAN_DB' //eber dev CLEAN DB OLD
    // db: 'mongodb://localhost:27017/NEW_EBER_LOCAL_CLEAN_DB_NEW', //eber dev CLEAN DB NEW
};

