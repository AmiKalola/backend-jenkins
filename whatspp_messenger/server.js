var async = require("async");

setup = require('./config/setup');
setting_detail = {};
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
init();

var multer = require('multer');
const path = require('path');

function parallel(middlewares) {
  return function (req, res, next) {
      async.each(middlewares, function (mw, cb) {
          mw(req, res, cb);
      }, next);
  };
}

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/data/');
  },
  filename: function (req, file, cb) {
    const originalExt = path.extname(file.originalname); // Get the original file extension
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + originalExt);
  },
});



async function init() {
    var port = setup.port;

    const express = require('express');
    const bodyParser = require('body-parser');
    const cors = require('cors')
    const http = require('http');

    mongoose = await require('./config/mongoose');

    db = await mongoose(),
    app = await express();
    app.use(cors())

    const upload = multer({ storage: storage, limits: {
      fileSize: 30 * 1024 * 1024, // Limiting file size to 30MB (adjust this value as needed)
    }});
    app.use(parallel([
      express.static('./data', {maxAge: '1d'}),
      bodyParser.json({ limit: '50mb' }),
      bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }),
      upload.any(),
    ]));
    
    require('./app/controllers/constant');
    require('./app/controllers/whatsapp');
    
    const whatsappRoute = require('./app/routes/whatsapp.js');
    app.use('/whatsapp',whatsappRoute);
    
    app.get('/check_status', function (req, res) {
      res.sendStatus(200);        
    });

    const server = http.Server(app);
    server.listen(port + (process.env.NODE_APP_INSTANCE ? parseInt(process.env.NODE_APP_INSTANCE) : 0), async () => {
     
    });

    var Settings = require('mongoose').model('Settings');
    Settings.findOne({}, function (error, setting) {
      setting_detail = setting
      console.log('Magic happens on port' + port);
    });
    module.exports = app;
}
