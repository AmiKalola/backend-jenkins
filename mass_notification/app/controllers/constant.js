exports = TYPE_VALUE = {
    USER: 1,
    PROVIDER: 2,
    PARTNER: 3,
    CORPORATE: 4,
    HOTEL: 5,
    DISPATCHER: 6,
    VEHICLE:7,
}

exports = COLLECTION = {
    MASS_NOTIFICATION: 14
}